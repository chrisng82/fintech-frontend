/**
 * The initial values for the redux state.
 */
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: {
    data: [],
    total: 0,
    page_size: 20,
    last_page: 1,
    current_page: 1,
    is_loading: false,
    filters: {
      code: "",
      debtorName: "",
      debtorCode: ""
    },
    sorts: [
      {
        field: "docCode",
        order: ""
      },
      {
        field: "statement_date",
        order: ""
      },
      {
        field: "debtor_code",
        order: ""
      },
      {
        field: "debtor_name",
        order: ""
      }
    ]
  }
}
