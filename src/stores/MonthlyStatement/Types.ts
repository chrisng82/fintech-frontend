import { BaseDocumentModel, BaseModel } from "@/Types/GeneralTypes"
import { IFilterType } from "@/components/Datatable/Filter"
import { Action } from "@reduxjs/toolkit"

export interface MonthlyStatement extends BaseModel {
  debtor_id: number
  debtor_code: string
  debtor_name: string
  company_id: number
  doc_code: string
  balance: number
  statement_date: string
  url: string
}

export type MonthlyStatementsFilters = {
  [key in IFilterType]: string | boolean | number | string[]
}
export interface MonthlyStatementsSorts {
  field: string
  order: string
}

export interface MonthlyStatementState {
  list: {
    data: MonthlyStatement[]
    total: number
    page_size: number
    last_page: number
    current_page: number
    is_loading: boolean
    filters: {
      code?: string
      debtorName: string
      debtorCode?: string
      debtorId?: number
    }
    sorts: MonthlyStatementsSorts[]
  }
}

export enum ETypesName {
  FETCH_MONTHLY_STATEMENTS = "FETCH_MONTHLY_STATEMENTS",
  SET_MONTHLY_STATEMENTS = "SET_MONTHLY_STATEMENTS",
  SET_MONTHLY_STATEMENTS_PAGINATION = "SET_MONTHLY_STATEMENTS_PAGINATION",
  SET_MONTHLY_STATEMENTS_FILTERS = "SET_MONTHLY_STATEMENTS_FILTERS",
  SET_MONTHLY_STATEMENTS_SORTS = "SET_MONTHLY_STATEMENTS_SORTS",

  SET_MONTHLY_STATEMENTS_LOADING = "SET_MONTHLY_STATEMENTS_LOADING"
}

export type FetchMonthlyStatements = Action<ETypesName.FETCH_MONTHLY_STATEMENTS> & {
  companyCode: string
  page?: number
  page_size?: number
  filters?: MonthlyStatementsFilters
}

export type SetMonthlyStatements = Action<ETypesName.SET_MONTHLY_STATEMENTS> & {
  data: MonthlyStatement[]
}

export type SetMonthlyStatementsPagination =
  Action<ETypesName.SET_MONTHLY_STATEMENTS_PAGINATION> & {
    page_size: number
    current_page: number
    last_page: number
    total: number
  }

export type SetMonthlyStatementsFilters = Action<ETypesName.SET_MONTHLY_STATEMENTS_FILTERS> & {
  filters: {
    code: string
    debtorName: string
    debtorCode: string
    pdfOnly: boolean
  }
}
export type SetMonthlyStatementsSorts = Action<ETypesName.SET_MONTHLY_STATEMENTS_SORTS> & {
  sorts: MonthlyStatementsSorts[]
}

export type SetMonthlyStatementsLoading = Action<ETypesName.SET_MONTHLY_STATEMENTS_LOADING> & {
  is_loading: boolean
}
