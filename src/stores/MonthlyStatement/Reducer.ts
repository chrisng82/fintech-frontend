import { createReducer } from "reduxsauce"
import { MonthlyStatementTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  MonthlyStatementState,
  SetMonthlyStatements,
  SetMonthlyStatementsPagination,
  SetMonthlyStatementsLoading,
  SetMonthlyStatementsFilters,
  SetMonthlyStatementsSorts
} from "./Types"

export const setMonthlyStatements = (
  state: MonthlyStatementState,
  { data }: SetMonthlyStatements
) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setMonthlyStatementsPagination = (
  state: MonthlyStatementState,
  { page_size, current_page, last_page, total }: SetMonthlyStatementsPagination
) => ({
  ...state,
  list: {
    ...state.list,
    page_size,
    current_page,
    last_page,
    total
  }
})

export const setMonthlyStatementsFilters = (
  state: MonthlyStatementState,
  { filters }: SetMonthlyStatementsFilters
) => ({
  ...state,
  list: {
    ...state.list,
    filters
  }
})

export const setMonthlyStatementsSorts = (
  state: MonthlyStatementState,
  { sorts }: SetMonthlyStatementsSorts
) => ({
  ...state,
  list: {
    ...state.list,
    sorts
  }
})

export const setMonthlyStatementsLoading = (
  state: MonthlyStatementState,
  { is_loading }: SetMonthlyStatementsLoading
) => ({
  ...state,
  list: {
    ...state.list,
    is_loading
  }
})

type Actions =
  | SetMonthlyStatements
  | SetMonthlyStatementsPagination
  | SetMonthlyStatementsFilters
  | SetMonthlyStatementsLoading

export const MonthlyStatementReducer = createReducer<MonthlyStatementState, Actions>(
  INITIAL_STATE,
  {
    [MonthlyStatementTypes.SET_MONTHLY_STATEMENTS]: setMonthlyStatements,
    [MonthlyStatementTypes.SET_MONTHLY_STATEMENTS_PAGINATION]: setMonthlyStatementsPagination,
    [MonthlyStatementTypes.SET_MONTHLY_STATEMENTS_FILTERS]: setMonthlyStatementsFilters,
    [MonthlyStatementTypes.SET_MONTHLY_STATEMENTS_SORTS]: setMonthlyStatementsSorts,

    [MonthlyStatementTypes.SET_MONTHLY_STATEMENTS_LOADING]: setMonthlyStatementsLoading
  }
)
