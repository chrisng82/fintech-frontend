import { createActions } from "reduxsauce"
import {
  MonthlyStatement,
  MonthlyStatementsFilters,
  ETypesName,
  FetchMonthlyStatements,
  SetMonthlyStatements,
  SetMonthlyStatementsFilters,
  SetMonthlyStatementsSorts,
  MonthlyStatementsSorts,
  SetMonthlyStatementsPagination,
  SetMonthlyStatementsLoading
} from "./Types"

const { Types, Creators: MonthlyStatementAction } = createActions<
  {
    [ETypesName.FETCH_MONTHLY_STATEMENTS]: string
    [ETypesName.SET_MONTHLY_STATEMENTS]: string
    [ETypesName.SET_MONTHLY_STATEMENTS_PAGINATION]: string
    [ETypesName.SET_MONTHLY_STATEMENTS_FILTERS]: string
    [ETypesName.SET_MONTHLY_STATEMENTS_SORTS]: string

    [ETypesName.SET_MONTHLY_STATEMENTS_LOADING]: string
  },
  {
    fetchMonthlyStatements: (
      page?: number,
      page_size?: number,
      filters?: MonthlyStatementsFilters
    ) => FetchMonthlyStatements
    setMonthlyStatements: (data: MonthlyStatement[]) => SetMonthlyStatements
    setMonthlyStatementsPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetMonthlyStatementsPagination
    setMonthlyStatementsFilters: (filters: MonthlyStatementsFilters) => SetMonthlyStatementsFilters
    setMonthlyStatementsSorts: (sorts: MonthlyStatementsSorts[]) => SetMonthlyStatementsSorts

    setMonthlyStatementsLoading: (is_loading: boolean) => SetMonthlyStatementsLoading
  }
>({
  //saga
  fetchMonthlyStatements: ["page", "page_size", "filters"],

  //reducer
  setMonthlyStatements: ["data"],
  setMonthlyStatementsPagination: ["page_size", "current_page", "last_page", "total"],
  setMonthlyStatementsFilters: ["filters"],
  setMonthlyStatementsSorts: ["sorts"],

  setMonthlyStatementsLoading: ["is_loading"]
})

export const MonthlyStatementTypes = Types
export default MonthlyStatementAction
