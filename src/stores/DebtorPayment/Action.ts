import { createActions } from "reduxsauce"
import {
  DebtorPayment,
  DebtorPaymentsFilters,
  ETypesName,
  FetchDebtorPayment,
  FetchDebtorPayments,
  SetDebtorPayment,
  SetDebtorPaymentLoading,
  SetDebtorPayments,
  SetDebtorPaymentsFilters,
  SetDebtorPaymentsSorts,
  DebtorPaymentSorts,
  SetDebtorPaymentsPagination,
  SetDebtorPaymentsLoading,
  FetchDebtorPaymentDetails,
  SetDebtorPaymentDetails,
  DebtorPaymentDetail,
  SetDebtorPaymentDetailsPagination
} from "./Types"

const { Types, Creators: DebtorPaymentAction } = createActions<
  {
    [ETypesName.FETCH_DEBTOR_PAYMENTS]: string
    [ETypesName.SET_DEBTOR_PAYMENTS]: string
    [ETypesName.SET_DEBTOR_PAYMENTS_PAGINATION]: string
    [ETypesName.SET_DEBTOR_PAYMENTS_FILTERS]: string
    [ETypesName.SET_DEBTOR_PAYMENTS_SORTS]: string

    [ETypesName.FETCH_DEBTOR_PAYMENT]: string
    [ETypesName.SET_DEBTOR_PAYMENT]: string
    [ETypesName.FETCH_DEBTOR_PAYMENT_DETAILS]: string
    [ETypesName.SET_DEBTOR_PAYMENT_DETAILS]: string
    [ETypesName.SET_DEBTOR_PAYMENT_DETAILS_PAGINATION]: string

    [ETypesName.SET_DEBTOR_PAYMENT_LOADING]: string
    [ETypesName.SET_DEBTOR_PAYMENTS_LOADING]: string
    [ETypesName.SET_DEBTOR_PAYMENT_DETAILS_LOADING]: string
  },
  {
    fetchDebtorPayments: (
      page?: number,
      page_size?: number,
      filters?: DebtorPaymentsFilters
    ) => FetchDebtorPayments
    setDebtorPayments: (data: DebtorPayment[]) => SetDebtorPayments
    setDebtorPaymentsPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetDebtorPaymentsPagination
    setDebtorPaymentsFilters: (filters: DebtorPaymentsFilters) => SetDebtorPaymentsFilters
    setDebtorPaymentsSorts: (filters: DebtorPaymentSorts[]) => SetDebtorPaymentsSorts

    fetchDebtorPayment: (id: number) => FetchDebtorPayment
    setDebtorPayment: (data: DebtorPayment) => SetDebtorPayment

    fetchDebtorPaymentDetails: (hdrId: number) => FetchDebtorPaymentDetails
    setDebtorPaymentDetails: (data: DebtorPaymentDetail[]) => SetDebtorPaymentDetails
    setDebtorPaymentDetailsPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetDebtorPaymentDetailsPagination

    setDebtorPaymentLoading: (is_loading: boolean) => SetDebtorPaymentLoading
    setDebtorPaymentsLoading: (is_loading: boolean) => SetDebtorPaymentsLoading
    setDebtorPaymentDetailsLoading: (is_loading: boolean) => SetDebtorPaymentLoading
  }
>({
  //saga
  fetchDebtorPayments: ["page", "page_size", "filters"],
  fetchDebtorPayment: ["id"],
  fetchDebtorPaymentDetails: ["hdrId"],

  //reducer
  setDebtorPayments: ["data"],
  setDebtorPaymentsPagination: ["page_size", "current_page", "last_page", "total"],
  setDebtorPaymentsFilters: ["filters"],
  setDebtorPaymentsSorts: ["sorts"],

  setDebtorPayment: ["data"],
  setDebtorPaymentDetails: ["data"],
  setDebtorPaymentDetailsPagination: ["page_size", "current_page", "last_page", "total"],

  setDebtorPaymentLoading: ["is_loading"],
  setDebtorPaymentsLoading: ["is_loading"],
  setDebtorPaymentDetailsLoading: ["is_loading"]
})

export const debtorPaymentTypes = Types
export default DebtorPaymentAction
