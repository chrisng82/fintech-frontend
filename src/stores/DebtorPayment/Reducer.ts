import { createReducer } from "reduxsauce"
import { debtorPaymentTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  DebtorPaymentState,
  SetDebtorPaymentLoading,
  SetDebtorPayments,
  SetDebtorPaymentsPagination,
  SetDebtorPaymentsLoading,
  SetDebtorPaymentsFilters,
  SetDebtorPaymentsSorts,
  SetDebtorPayment,
  SetDebtorPaymentDetails,
  SetDebtorPaymentDetailsPagination,
  SetDebtorPaymentDetailsLoading
} from "./Types"

export const setDebtorPayments = (state: DebtorPaymentState, { data }: SetDebtorPayments) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setDebtorPaymentsPagination = (
  state: DebtorPaymentState,
  { page_size, current_page, last_page, total }: SetDebtorPaymentsPagination
) => ({
  ...state,
  list: {
    ...state.list,
    page_size,
    current_page,
    last_page,
    total
  }
})

export const setDebtorPaymentsFilters = (
  state: DebtorPaymentState,
  { filters }: SetDebtorPaymentsFilters
) => ({
  ...state,
  list: {
    ...state.list,
    filters
  }
})

export const setDebtorPaymentsSorts = (
  state: DebtorPaymentState,
  { sorts }: SetDebtorPaymentsSorts
) => ({
  ...state,
  list: {
    ...state.list,
    sorts
  }
})

export const setDebtorPayment = (state: DebtorPaymentState, { data }: SetDebtorPayment) => ({
  ...state,
  item: {
    ...state.item,
    data
  }
})

export const setDebtorPaymentDetails = (
  state: DebtorPaymentState,
  { data }: SetDebtorPaymentDetails
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      data
    }
  }
})

export const setDebtorPaymentDetailsPagination = (
  state: DebtorPaymentState,
  { page_size, current_page, last_page, total }: SetDebtorPaymentDetailsPagination
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      page_size,
      current_page,
      last_page,
      total
    }
  }
})

export const setDebtorPaymentsLoading = (
  state: DebtorPaymentState,
  { is_loading }: SetDebtorPaymentsLoading
) => ({
  ...state,
  list: {
    ...state.list,
    is_loading
  }
})

export const setDebtorPaymentLoading = (
  state: DebtorPaymentState,
  { is_loading }: SetDebtorPaymentLoading
) => ({
  ...state,
  item: {
    ...state.item,
    is_loading
  }
})

export const setDebtorPaymentDetailsLoading = (
  state: DebtorPaymentState,
  { is_loading }: SetDebtorPaymentDetailsLoading
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      is_loading
    }
  }
})

type Actions =
  | SetDebtorPayments
  | SetDebtorPaymentsPagination
  | SetDebtorPaymentsFilters
  | SetDebtorPaymentsSorts
  | SetDebtorPayment
  | SetDebtorPaymentDetails
  | SetDebtorPaymentDetailsPagination
  | SetDebtorPaymentsLoading
  | SetDebtorPaymentLoading
  | SetDebtorPaymentDetailsLoading

export const DebtorPaymentReducer = createReducer<DebtorPaymentState, Actions>(INITIAL_STATE, {
  [debtorPaymentTypes.SET_DEBTOR_PAYMENTS]: setDebtorPayments,
  [debtorPaymentTypes.SET_DEBTOR_PAYMENTS_PAGINATION]: setDebtorPaymentsPagination,
  [debtorPaymentTypes.SET_DEBTOR_PAYMENTS_FILTERS]: setDebtorPaymentsFilters,
  [debtorPaymentTypes.SET_DEBTOR_PAYMENTS_SORTS]: setDebtorPaymentsSorts,

  [debtorPaymentTypes.SET_DEBTOR_PAYMENT]: setDebtorPayment,
  [debtorPaymentTypes.SET_DEBTOR_PAYMENT_DETAILS]: setDebtorPaymentDetails,
  [debtorPaymentTypes.SET_DEBTOR_PAYMENT_DETAILS_PAGINATION]: setDebtorPaymentDetailsPagination,

  [debtorPaymentTypes.SET_DEBTOR_PAYMENTS_LOADING]: setDebtorPaymentsLoading,
  [debtorPaymentTypes.SET_DEBTOR_PAYMENT_LOADING]: setDebtorPaymentLoading,
  [debtorPaymentTypes.SET_DEBTOR_PAYMENT_DETAILS_LOADING]: setDebtorPaymentDetailsLoading
})
