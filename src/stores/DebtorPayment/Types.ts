import { BaseDocumentModel, BaseModel } from "@/Types/GeneralTypes"
import { IFilterType } from "@/components/Datatable/Filter"
import { Action } from "@reduxjs/toolkit"

export interface DebtorPayment extends BaseModel, BaseDocumentModel {
  hdrId: number
  companyCode: string
  debtorCode: string
  orderCreatedAt: string
  orderUpdatedAt: string
  creditTerms: string
}

export interface DebtorPaymentDetail extends BaseModel {
  [key: string]: any
  line_no: number
  code: string
  desc_01: string
  desc_02: string
  location_code: string
  uom_code: string
  uom_rate: number
  sale_price: number
  price_disc: number
  qty: number
  gross_amt: number
  gross_local_amt: number
  dtl_disc_amt: number
  dtl_disc_local_amt: number
  hdr_disc_amt: number
  hdr_disc_local_amt: number
  tax_code: string
  net_amt: number
  net_local_amt: number
}

export type DebtorPaymentsFilters = {
  [key in IFilterType]: string | boolean | number | string[]
}
export interface DebtorPaymentSorts {
  field: string
  order: string
}

export interface DebtorPaymentState {
  list: {
    data: DebtorPayment[]
    total: number
    page_size: number
    last_page: number
    current_page: number
    is_loading: boolean
    filters: {
      code?: string
      debtorName: string
      debtorCode?: string
      paymentStatus?: string
      pdfOnly?: boolean
      docDate?: string[]
    }
    sorts: DebtorPaymentSorts[]
  }
  item: {
    is_loading: boolean
    data: DebtorPayment | null
    details: {
      data: DebtorPaymentDetail[]
      total: number
      page_size: number
      last_page: number
      current_page: number
      is_loading: boolean
    }
  }
}

export enum ETypesName {
  FETCH_DEBTOR_PAYMENTS = "FETCH_DEBTOR_PAYMENTS",
  SET_DEBTOR_PAYMENTS = "SET_DEBTOR_PAYMENTS",
  SET_DEBTOR_PAYMENTS_PAGINATION = "SET_DEBTOR_PAYMENTS_PAGINATION",
  SET_DEBTOR_PAYMENTS_FILTERS = "SET_DEBTOR_PAYMENTS_FILTERS",
  SET_DEBTOR_PAYMENTS_SORTS = "SET_DEBTOR_PAYMENTS_SORTS",

  FETCH_DEBTOR_PAYMENT = "FETCH_DEBTOR_PAYMENT",
  SET_DEBTOR_PAYMENT = "SET_DEBTOR_PAYMENT",
  FETCH_DEBTOR_PAYMENT_DETAILS = "FETCH_DEBTOR_PAYMENT_DETAILS",
  SET_DEBTOR_PAYMENT_DETAILS = "SET_DEBTOR_PAYMENT_DETAILS",
  SET_DEBTOR_PAYMENT_DETAILS_PAGINATION = "SET_DEBTOR_PAYMENT_DETAILS_PAGINATION",

  SET_DEBTOR_PAYMENTS_LOADING = "SET_DEBTOR_PAYMENTS_LOADING",
  SET_DEBTOR_PAYMENT_LOADING = "SET_DEBTOR_PAYMENT_LOADING",
  SET_DEBTOR_PAYMENT_DETAILS_LOADING = "SET_DEBTOR_PAYMENT_DETAILS_LOADING"
}

export type FetchDebtorPayments = Action<ETypesName.FETCH_DEBTOR_PAYMENTS> & {
  companyCode: string
  page?: number
  page_size?: number
  filters?: DebtorPaymentsFilters
}

export type SetDebtorPayments = Action<ETypesName.SET_DEBTOR_PAYMENTS> & {
  data: DebtorPayment[]
}

export type SetDebtorPaymentsPagination = Action<ETypesName.SET_DEBTOR_PAYMENTS_PAGINATION> & {
  page_size: number
  current_page: number
  last_page: number
  total: number
}

export type SetDebtorPaymentsFilters = Action<ETypesName.SET_DEBTOR_PAYMENTS_FILTERS> & {
  filters: {
    code: string
    debtorName: string
    debtorCode: string
  }
}
export type SetDebtorPaymentsSorts = Action<ETypesName.SET_DEBTOR_PAYMENTS_SORTS> & {
  sorts: DebtorPaymentSorts[]
}

export type FetchDebtorPayment = Action<ETypesName.FETCH_DEBTOR_PAYMENT> & {
  id: number
}

export type FetchDebtorPaymentDetails = Action<ETypesName.FETCH_DEBTOR_PAYMENT_DETAILS> & {
  hdrId: number
  page?: number
  page_size?: number
}

export type SetDebtorPayment = Action<ETypesName.SET_DEBTOR_PAYMENT> & {
  data: DebtorPayment
}

export type SetDebtorPaymentDetails = Action<ETypesName.SET_DEBTOR_PAYMENT_DETAILS> & {
  data: DebtorPaymentDetail[]
}

export type SetDebtorPaymentDetailsPagination =
  Action<ETypesName.SET_DEBTOR_PAYMENT_DETAILS_PAGINATION> & {
    page_size: number
    current_page: number
    last_page: number
    total: number
  }

export type SetDebtorPaymentLoading = Action<ETypesName.SET_DEBTOR_PAYMENT_LOADING> & {
  is_loading: boolean
}

export type SetDebtorPaymentsLoading = Action<ETypesName.SET_DEBTOR_PAYMENTS_LOADING> & {
  is_loading: boolean
}

export type SetDebtorPaymentDetailsLoading =
  Action<ETypesName.SET_DEBTOR_PAYMENT_DETAILS_LOADING> & {
    is_loading: boolean
  }
