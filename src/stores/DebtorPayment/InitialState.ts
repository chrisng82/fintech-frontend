/**
 * The initial values for the redux state.
 */
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: {
    data: [],
    total: 0,
    page_size: 20,
    last_page: 1,
    current_page: 1,
    is_loading: false,
    filters: {
      code: "",
      debtorName: "",
      debtorCode: ""
    },
    sorts: [
      {
        field: "docCode",
        order: ""
      },
      {
        field: "docDate",
        order: ""
      },
      {
        field: "debtorCode",
        order: ""
      },
      {
        field: "debtorName",
        order: ""
      },
      {
        field: "paymentMethod",
        order: ""
      },
      {
        field: "payment",
        order: ""
      },
      {
        field: "balance",
        order: ""
      }
    ]
  },
  item: {
    is_loading: false,
    data: null,
    details: {
      data: [],
      total: 0,
      page_size: 20,
      last_page: 1,
      current_page: 1,
      is_loading: false
    }
  }
}
