import { BaseModel } from "@/Types/GeneralTypes"
import { FtpSyncDocument } from "../FtpSync/Types"
import { Action } from "@reduxjs/toolkit"
import { CompanyCommunicationSetting } from "../CompanyCommunicationSetting/Types"

export interface DocAttachment extends BaseModel {
  doc_id: number
  ar_table: string
  url: string
  pivot?: FtpSyncDocument
  type?: string | null
}

export interface DocAttachmentState {
  download: {
    isLoading: boolean
  }
  downloadData: DocAttachment[]
}

export enum ETypesName {
  SET_DOWNLOAD_IS_LOADING = "SET_DOWNLOAD_IS_LOADING",

  DOWNLOAD_BY_S3_URL = "DOWNLOAD_BY_S3_URL",
  DOC_ATTACHMENT_GET_DOWNLOAD_DATA = "DOC_ATTACHMENT_GET_DOWNLOAD_DATA",
  DOC_ATTACHMENT_SET_DOWNLOAD_DATA = "DOC_ATTACHMENT_SET_DOWNLOAD_DATA",

  DOC_ATTACHMENT_DL_MIDDLEWARE = "DOC_ATTACHMENT_DL_MIDDLEWARE"
}

export type SetDownloadIsLoading = Action<ETypesName.SET_DOWNLOAD_IS_LOADING> & {
  isLoading: boolean
}

export type DownloadByS3Url = Action<ETypesName.DOWNLOAD_BY_S3_URL> & {
  url: string | string[]
}

export type DocAttachmentGetDownloadData = Action<ETypesName.DOC_ATTACHMENT_GET_DOWNLOAD_DATA> & {
  docAttachment: Partial<DocAttachment>
}

export type DocAttachmentSetDownloadData = Action<ETypesName.DOC_ATTACHMENT_SET_DOWNLOAD_DATA> & {
  downloadData: DocAttachment[]
}

export type DocAttachmentDlMiddleware = Action<ETypesName.DOC_ATTACHMENT_DL_MIDDLEWARE> & {
  docCode: string
  companyCode: string
}
