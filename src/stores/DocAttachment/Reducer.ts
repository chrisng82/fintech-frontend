import { createReducer } from "reduxsauce"
import INITIAL_STATE from "./InitialState"
import { DocAttachmentState, SetDownloadIsLoading, DocAttachmentSetDownloadData } from "./Types"
import { DocAttachmentTypes } from "./Action"

export const setDownloadIsLoading = (
  state: DocAttachmentState,
  { isLoading }: SetDownloadIsLoading
) => ({
  ...state,
  download: {
    ...state.download,
    isLoading
  }
})

export const docAttachmentSetDownloadData = (
  state: DocAttachmentState,
  { downloadData }: DocAttachmentSetDownloadData
) => ({
  ...state,
  downloadData
})

type Actions = SetDownloadIsLoading | DocAttachmentSetDownloadData

export const DocAttachmentReducer = createReducer<DocAttachmentState, Actions>(INITIAL_STATE, {
  [DocAttachmentTypes.SET_DOWNLOAD_IS_LOADING]: setDownloadIsLoading,
  [DocAttachmentTypes.DOC_ATTACHMENT_SET_DOWNLOAD_DATA]: docAttachmentSetDownloadData
})
