import { createActions } from "reduxsauce"
import {
  ETypesName,
  DownloadByS3Url,
  SetDownloadIsLoading,
  DocAttachmentGetDownloadData,
  DocAttachmentSetDownloadData,
  DocAttachment,
  DocAttachmentDlMiddleware
} from "./Types"
import { CompanyCommunicationSetting } from "../CompanyCommunicationSetting/Types"

const { Types, Creators: DocAttachmentAction } = createActions<
  {
    [ETypesName.SET_DOWNLOAD_IS_LOADING]: string

    [ETypesName.DOWNLOAD_BY_S3_URL]: string
    [ETypesName.DOC_ATTACHMENT_GET_DOWNLOAD_DATA]: string
    [ETypesName.DOC_ATTACHMENT_SET_DOWNLOAD_DATA]: string
    [ETypesName.DOC_ATTACHMENT_DL_MIDDLEWARE]: string
  },
  {
    setDownloadIsLoading: (isLoading: boolean) => SetDownloadIsLoading

    downloadByS3Url: (url: string | string[]) => DownloadByS3Url
    docAttachmentGetDownloadData: (
      docAttachment: Partial<DocAttachment>
    ) => DocAttachmentGetDownloadData
    docAttachmentSetDownloadData: (downloadData: DocAttachment[]) => DocAttachmentSetDownloadData
    docAttachmentDlMiddleware: (docCode: string, companyCode: string) => DocAttachmentDlMiddleware
  }
>({
  // Reducer
  setDownloadIsLoading: ["isLoading"],
  docAttachmentSetDownloadData: ["downloadData"],

  // Saga
  downloadByS3Url: ["url"],
  docAttachmentGetDownloadData: ["docAttachment"],
  docAttachmentDlMiddleware: ["docCode", "companyCode"]
})

export const DocAttachmentTypes = Types
export default DocAttachmentAction
