// eslint-disable-next-line import/no-anonymous-default-export
import { AlertColor } from "@mui/material"

export default {
  message: "",
  snackbarType: "info" as AlertColor,
  isOpen: false
}
