import { createReducer } from "reduxsauce"
import { AppTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import { CloseSnackbar, OpenSnackbar, SnackbarState } from "./Types"
import { AlertColor } from "@mui/material"

export const openSnackbar = (state: SnackbarState, { message, snackbarType }: OpenSnackbar) => ({
  ...state,
  isOpen: true,
  message,
  snackbarType
})

export const closeSnackbar = (state: SnackbarState) => ({
  ...state,
  isOpen: false,
  snackbarType: "info" as AlertColor
})

type Actions = OpenSnackbar | CloseSnackbar

export const SnackbarReducer = createReducer<SnackbarState, Actions>(INITIAL_STATE, {
  [AppTypes.OPEN_SNACKBAR]: openSnackbar,
  [AppTypes.CLOSE_SNACKBAR]: closeSnackbar
})
