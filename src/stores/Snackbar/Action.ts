import { createActions } from "reduxsauce"
import { CloseSnackbar, ETypesName, OpenSnackbar } from "./Types"
import { AlertColor } from "@mui/material"

const { Types, Creators: SnackbarAction } = createActions<
  {
    [ETypesName.OPEN_SNACKBAR]: string
    [ETypesName.CLOSE_SNACKBAR]: string
  },
  {
    openSnackbar: (message: string, snackbarType: AlertColor) => OpenSnackbar
    closeSnackbar: () => CloseSnackbar
  }
>({
  // Reducers
  openSnackbar: ["message", "snackbarType"],
  closeSnackbar: []
})

export const AppTypes = Types
export default SnackbarAction
