import { AlertColor } from "@mui/material"
import { Action } from "@reduxjs/toolkit"

export interface SnackbarState {
  message: string
  snackbarType: AlertColor
  isOpen: boolean
}

export enum ETypesName {
  OPEN_SNACKBAR = "OPEN_SNACKBAR",
  CLOSE_SNACKBAR = "CLOSE_SNACKBAR"
}

export type OpenSnackbar = Action<ETypesName.OPEN_SNACKBAR> & {
  message: string
  snackbarType: AlertColor
}

export type CloseSnackbar = Action<ETypesName.CLOSE_SNACKBAR>
