import { createActions } from "reduxsauce"
import {
  ETypesName,
  FetchEficoreSyncs,
  SetEficoreSyncs,
  SetEficoreSyncsLoading,
  CreateEficoreSyncJob,
  SetEficoreSyncsPagination,
  EficoreSyncStats
} from "./Types"

const { Types, Creators: EficoreSyncAction } = createActions<
  {
    [ETypesName.SET_EFICORE_SYNCS]: string
    [ETypesName.SET_EFICORE_SYNCS_PAGINATION]: string
    [ETypesName.SET_EFICORE_SYNCS_LOADING]: string

    [ETypesName.FETCH_EFICORE_SYNCS]: string
    [ETypesName.CREATE_EFICORE_SYNC_JOB]: string
  },
  {
    setEficoreSyncs: (data: any[], stats: EficoreSyncStats) => SetEficoreSyncs
    setEficoreSyncsPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetEficoreSyncsPagination
    setEficoreSyncsLoading: (isLoading: boolean) => SetEficoreSyncsLoading

    fetchEficoreSyncs: (syncType: string, page?: number, page_size?: number) => FetchEficoreSyncs
    createEficoreSyncJob: (syncType: string) => CreateEficoreSyncJob
  }
>({
  // Reducers
  setEficoreSyncs: ["data", "stats"],
  setEficoreSyncsPagination: ["page_size", "current_page", "last_page", "total"],
  setEficoreSyncsLoading: ["isLoading"],

  // Saga
  fetchEficoreSyncs: ["syncType", "page", "page_size"],
  createEficoreSyncJob: ["syncType"]
})

export const EficoreSyncTypes = Types
export default EficoreSyncAction
