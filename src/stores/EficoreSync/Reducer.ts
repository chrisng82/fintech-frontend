import { createReducer } from "reduxsauce"
import INITIAL_STATE from "./InitialState"
import {
  EficoreSyncState,
  SetEficoreSyncs,
  SetEficoreSyncsLoading,
  SetEficoreSyncsPagination
} from "./Types"
import { EficoreSyncTypes } from "./Action"

export const setEficoreSyncs = (state: EficoreSyncState, { data, stats }: SetEficoreSyncs) => ({
  ...state,
  list: {
    ...state.list,
    data,
    stats
  }
})

export const setEficoreSyncsPagination = (
  state: EficoreSyncState,
  { page_size, current_page, last_page, total }: SetEficoreSyncsPagination
) => ({
  ...state,
  list: {
    ...state.list,
    page_size,
    current_page,
    last_page,
    total
  }
})

export const setEficoreSyncsLoading = (
  state: EficoreSyncState,
  { isLoading }: SetEficoreSyncsLoading
) => ({
  ...state,
  list: {
    ...state.list,
    isLoading
  }
})

type Actions = SetEficoreSyncs | SetEficoreSyncsPagination | SetEficoreSyncsLoading

export const EficoreSyncReducer = createReducer<EficoreSyncState, Actions>(INITIAL_STATE, {
  [EficoreSyncTypes.SET_EFICORE_SYNCS]: setEficoreSyncs,
  [EficoreSyncTypes.SET_EFICORE_SYNCS_PAGINATION]: setEficoreSyncsPagination,
  [EficoreSyncTypes.SET_EFICORE_SYNCS_LOADING]: setEficoreSyncsLoading
})
