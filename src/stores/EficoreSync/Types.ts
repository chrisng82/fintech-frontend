import { BaseModel } from "@/Types/GeneralTypes"
import { Action } from "@reduxjs/toolkit"
import { type } from "os"

export interface EficoreSync extends BaseModel {
  sync_type: string
  doc_type: string
  company_code: string
  date_from: string | Date
  date_to: string | Date
  status: number
  page: number
  total_page: number
  page_size: number
  has_next_page: boolean
  offset: number
  total_record: number
  total_synced_record: number
  is_syncing: boolean
}

export interface EficoreSyncStats {
  completed: number
  pending: number
  in_progress: number
  failed: number
}

export interface EficoreSyncState {
  list: {
    data: EficoreSync[]
    page_size: number
    current_page: number
    last_page: number
    total: number
    stats: EficoreSyncStats
    isLoading: boolean
  }
  item: {
    data: EficoreSync | null
    isLoading: boolean
  }
}

export enum ETypesName {
  SET_EFICORE_SYNCS = "SET_EFICORE_SYNCS",
  SET_EFICORE_SYNCS_PAGINATION = "SET_EFICORE_SYNCS_PAGINATION",
  SET_EFICORE_SYNCS_LOADING = "SET_EFICORE_SYNCS_LOADING",

  FETCH_EFICORE_SYNCS = "FETCH_EFICORE_SYNCS",
  CREATE_EFICORE_SYNC_JOB = "CREATE_EFICORE_SYNC_JOB"
}

export type SetEficoreSyncs = Action<ETypesName.SET_EFICORE_SYNCS> & {
  data: any[]
  stats: EficoreSyncStats
}

export type SetEficoreSyncsPagination = Action<ETypesName.SET_EFICORE_SYNCS_PAGINATION> & {
  page_size: number
  current_page: number
  last_page: number
  total: number
}

export type SetEficoreSyncsLoading = Action<ETypesName.SET_EFICORE_SYNCS_LOADING> & {
  isLoading: boolean
}

export type FetchEficoreSyncs = Action<ETypesName.FETCH_EFICORE_SYNCS> & {
  syncType: string
  page?: number
  page_size?: number
}

export type CreateEficoreSyncJob = Action<ETypesName.CREATE_EFICORE_SYNC_JOB> & {
  syncType: string
}
