// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: {
    data: [],
    page_size: 10,
    current_page: 1,
    last_page: 1,
    total: 0,
    stats: {
      canceled: 0,
      completed: 0,
      pending: 0,
      in_progress: 0,
      failed: 0
    },
    isLoading: false
  },
  item: {
    data: null,
    isLoading: false
  }
}
