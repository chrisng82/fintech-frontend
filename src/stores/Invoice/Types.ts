import { BaseDocumentModel, BaseModel } from "@/Types/GeneralTypes"
import { IFilterType } from "@/components/Datatable/Filter"
import { Action } from "@reduxjs/toolkit"

export interface Invoice extends BaseModel, BaseDocumentModel {
  hdrId: number
  companyCode: string
  debtorCode: string
  orderCreatedAt: string
  orderUpdatedAt: string
  creditTerms: string
}

export interface InvoiceDetail extends BaseModel {
  [key: string]: any
  line_no: number
  code: string
  desc_01: string
  desc_02: string
  location_code: string
  uom_code: string
  uom_rate: number
  sale_price: number
  price_disc: number
  qty: number
  gross_amt: number
  gross_local_amt: number
  dtl_disc_amt: number
  dtl_disc_local_amt: number
  hdr_disc_amt: number
  hdr_disc_local_amt: number
  tax_code: string
  net_amt: number
  net_local_amt: number
}

export type InvoicesFilters = { [key in IFilterType]: string | boolean | number | string[] }
export interface InvoiceSorts {
  field: string
  order: string
}

export interface InvoiceState {
  list: {
    data: Invoice[]
    total: number
    page_size: number
    last_page: number
    current_page: number
    is_loading: boolean
    filters: {
      code?: string
      debtorName: string
      debtorCode?: string
      pdfOnly: boolean
    }
    sorts: InvoiceSorts[]
  }
  item: {
    is_loading: boolean
    data: Invoice | null
    details: {
      data: InvoiceDetail[]
      total: number
      page_size: number
      last_page: number
      current_page: number
      is_loading: boolean
    }
  }
}

export enum ETypesName {
  FETCH_INVOICES = "FETCH_INVOICES",
  SET_INVOICES = "SET_INVOICES",
  SET_INVOICES_PAGINATION = "SET_INVOICES_PAGINATION",
  SET_INVOICES_FILTERS = "SET_INVOICES_FILTERS",
  SET_INVOICES_SORTS = "SET_INVOICES_SORTS",

  FETCH_INVOICE = "FETCH_INVOICE",
  SET_INVOICE = "SET_INVOICE",
  FETCH_INVOICE_DETAILS = "FETCH_INVOICE_DETAILS",
  SET_INVOICE_DETAILS = "SET_INVOICE_DETAILS",
  SET_INVOICE_DETAILS_PAGINATION = "SET_INVOICE_DETAILS_PAGINATION",

  SET_INVOICES_LOADING = "SET_INVOICES_LOADING",
  SET_INVOICE_LOADING = "SET_INVOICE_LOADING",
  SET_INVOICE_DETAILS_LOADING = "SET_INVOICE_DETAILS_LOADING"
}

export type FetchInvoices = Action<ETypesName.FETCH_INVOICES> & {
  companyCode: string
  page?: number
  page_size?: number
  filters?: InvoicesFilters
}

export type SetInvoices = Action<ETypesName.SET_INVOICES> & {
  data: Invoice[]
}

export type SetInvoicesPagination = Action<ETypesName.SET_INVOICES_PAGINATION> & {
  page_size: number
  current_page: number
  last_page: number
  total: number
}

export type SetInvoicesFilters = Action<ETypesName.SET_INVOICES_FILTERS> & {
  filters: {
    code: string
    debtorName: string
    debtorCode: string
    pdfOnly: boolean
  }
}

export type SetInvoicesSorts = Action<ETypesName.SET_INVOICES_SORTS> & {
  sorts: InvoiceSorts[]
}

export type FetchInvoice = Action<ETypesName.FETCH_INVOICE> & {
  id: number
}

export type FetchInvoiceDetails = Action<ETypesName.FETCH_INVOICE_DETAILS> & {
  hdrId: number
  page?: number
  page_size?: number
}

export type SetInvoice = Action<ETypesName.SET_INVOICE> & {
  data: Invoice
}

export type SetInvoiceDetails = Action<ETypesName.SET_INVOICE_DETAILS> & {
  data: InvoiceDetail[]
}

export type SetInvoiceDetailsPagination = Action<ETypesName.SET_INVOICE_DETAILS_PAGINATION> & {
  page_size: number
  current_page: number
  last_page: number
  total: number
}

export type SetInvoiceLoading = Action<ETypesName.SET_INVOICE_LOADING> & {
  is_loading: boolean
}

export type SetInvoicesLoading = Action<ETypesName.SET_INVOICES_LOADING> & {
  is_loading: boolean
}

export type SetInvoiceDetailsLoading = Action<ETypesName.SET_INVOICE_DETAILS_LOADING> & {
  is_loading: boolean
}
