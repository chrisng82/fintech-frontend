import { createActions } from "reduxsauce"
import {
  Invoice,
  InvoicesFilters,
  ETypesName,
  FetchInvoice,
  FetchInvoices,
  SetInvoice,
  SetInvoiceLoading,
  SetInvoices,
  SetInvoicesFilters,
  SetInvoicesSorts,
  InvoiceSorts,
  SetInvoicesPagination,
  SetInvoicesLoading,
  FetchInvoiceDetails,
  SetInvoiceDetails,
  InvoiceDetail,
  SetInvoiceDetailsPagination
} from "./Types"

const { Types, Creators: InvoiceAction } = createActions<
  {
    [ETypesName.FETCH_INVOICES]: string
    [ETypesName.SET_INVOICES]: string
    [ETypesName.SET_INVOICES_PAGINATION]: string
    [ETypesName.SET_INVOICES_FILTERS]: string
    [ETypesName.SET_INVOICES_SORTS]: string

    [ETypesName.FETCH_INVOICE]: string
    [ETypesName.SET_INVOICE]: string
    [ETypesName.FETCH_INVOICE_DETAILS]: string
    [ETypesName.SET_INVOICE_DETAILS]: string
    [ETypesName.SET_INVOICE_DETAILS_PAGINATION]: string

    [ETypesName.SET_INVOICE_LOADING]: string
    [ETypesName.SET_INVOICES_LOADING]: string
    [ETypesName.SET_INVOICE_DETAILS_LOADING]: string
  },
  {
    fetchInvoices: (page?: number, page_size?: number, filters?: InvoicesFilters) => FetchInvoices
    setInvoices: (data: Invoice[]) => SetInvoices
    setInvoicesPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetInvoicesPagination
    setInvoicesFilters: (filters: InvoicesFilters) => SetInvoicesFilters
    setInvoicesSorts: (sorts: InvoiceSorts[]) => SetInvoicesSorts

    fetchInvoice: (id: number) => FetchInvoice
    setInvoice: (data: Invoice) => SetInvoice

    fetchInvoiceDetails: (hdrId: number) => FetchInvoiceDetails
    setInvoiceDetails: (data: InvoiceDetail[]) => SetInvoiceDetails
    setInvoiceDetailsPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetInvoiceDetailsPagination

    setInvoiceLoading: (is_loading: boolean) => SetInvoiceLoading
    setInvoicesLoading: (is_loading: boolean) => SetInvoicesLoading
    setInvoiceDetailsLoading: (is_loading: boolean) => SetInvoiceLoading
  }
>({
  //saga
  fetchInvoices: ["page", "page_size", "filters"],
  fetchInvoice: ["id"],
  fetchInvoiceDetails: ["hdrId"],

  //reducer
  setInvoices: ["data"],
  setInvoicesPagination: ["page_size", "current_page", "last_page", "total"],
  setInvoicesFilters: ["filters"],
  setInvoicesSorts: ["sorts"],

  setInvoice: ["data"],
  setInvoiceDetails: ["data"],
  setInvoiceDetailsPagination: ["page_size", "current_page", "last_page", "total"],

  setInvoiceLoading: ["is_loading"],
  setInvoicesLoading: ["is_loading"],
  setInvoiceDetailsLoading: ["is_loading"]
})

export const invoiceTypes = Types
export default InvoiceAction
