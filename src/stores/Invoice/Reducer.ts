import { createReducer } from "reduxsauce"
import { invoiceTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  InvoiceState,
  SetInvoiceLoading,
  SetInvoices,
  SetInvoicesPagination,
  SetInvoicesLoading,
  SetInvoicesFilters,
  SetInvoicesSorts,
  SetInvoice,
  SetInvoiceDetails,
  SetInvoiceDetailsPagination,
  SetInvoiceDetailsLoading
} from "./Types"

export const setInvoices = (state: InvoiceState, { data }: SetInvoices) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setInvoicesPagination = (
  state: InvoiceState,
  { page_size, current_page, last_page, total }: SetInvoicesPagination
) => ({
  ...state,
  list: {
    ...state.list,
    page_size,
    current_page,
    last_page,
    total
  }
})

export const setInvoicesFilters = (state: InvoiceState, { filters }: SetInvoicesFilters) => ({
  ...state,
  list: {
    ...state.list,
    filters
  }
})

export const setInvoicesSorts = (state: InvoiceState, { sorts }: SetInvoicesSorts) => ({
  ...state,
  list: {
    ...state.list,
    sorts
  }
})

export const setInvoice = (state: InvoiceState, { data }: SetInvoice) => ({
  ...state,
  item: {
    ...state.item,
    data
  }
})

export const setInvoiceDetails = (state: InvoiceState, { data }: SetInvoiceDetails) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      data
    }
  }
})

export const setInvoiceDetailsPagination = (
  state: InvoiceState,
  { page_size, current_page, last_page, total }: SetInvoiceDetailsPagination
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      page_size,
      current_page,
      last_page,
      total
    }
  }
})

export const setInvoicesLoading = (state: InvoiceState, { is_loading }: SetInvoicesLoading) => ({
  ...state,
  list: {
    ...state.list,
    is_loading
  }
})

export const setInvoiceLoading = (state: InvoiceState, { is_loading }: SetInvoiceLoading) => ({
  ...state,
  item: {
    ...state.item,
    is_loading
  }
})

export const setInvoiceDetailsLoading = (
  state: InvoiceState,
  { is_loading }: SetInvoiceDetailsLoading
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      is_loading
    }
  }
})

type Actions =
  | SetInvoices
  | SetInvoicesPagination
  | SetInvoicesFilters
  | SetInvoice
  | SetInvoiceDetails
  | SetInvoiceDetailsPagination
  | SetInvoicesLoading
  | SetInvoiceLoading
  | SetInvoiceDetailsLoading

export const InvoiceReducer = createReducer<InvoiceState, Actions>(INITIAL_STATE, {
  [invoiceTypes.SET_INVOICES]: setInvoices,
  [invoiceTypes.SET_INVOICES_PAGINATION]: setInvoicesPagination,
  [invoiceTypes.SET_INVOICES_FILTERS]: setInvoicesFilters,
  [invoiceTypes.SET_INVOICES_SORTS]: setInvoicesSorts,

  [invoiceTypes.SET_INVOICE]: setInvoice,
  [invoiceTypes.SET_INVOICE_DETAILS]: setInvoiceDetails,
  [invoiceTypes.SET_INVOICE_DETAILS_PAGINATION]: setInvoiceDetailsPagination,

  [invoiceTypes.SET_INVOICES_LOADING]: setInvoicesLoading,
  [invoiceTypes.SET_INVOICE_LOADING]: setInvoiceLoading,
  [invoiceTypes.SET_INVOICE_DETAILS_LOADING]: setInvoiceDetailsLoading
})
