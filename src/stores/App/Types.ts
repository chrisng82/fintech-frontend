import { Action } from "@reduxjs/toolkit"
import { User, UserResetPassword } from "../User/Types"
import { ForgotPasswordFormProps } from "@/components/page/Form/ForgotPasswordForm"
import { ResetPasswordFormProps } from "@/components/page/Form/ResetPasswordForm"
import { TermCondition } from "../Company/Types"

export interface AppState {
  apiUrl: string
  appPath: string
  token: string | null
  isLoading: boolean
  user: User | null
  envSettings: EnvSettings
  resetPassword: {
    isValidating: boolean
    user: UserResetPassword | null
  }
  companyTnc?: CompanyTnc
  openTnc?: boolean
}

export interface CompanyTnc {
  tnc: TermCondition | null
  user_tnc: UserTnc | null
}

export interface UserTnc {
  user_id: number | null
  tnc_id: number | null
  company_id: number | null
  status: number | null
  acknowledgement_date: string | null
}

export interface EnvSettings {
  HIDE_ROUTES: string | null
  DISABLED_FEATURES: string | null
  DEBTOR_FORCED_CONTACTS: boolean
  DEBTOR_HIDE_SSM_REGISTRATION: boolean
  IS_DISABLED_SELF_SIGNUP: string | null
}

export enum ETypesName {
  UPDATE_API_URL = "UPDATE_API_URL",
  UPDATE_APP_PATH = "UPDATE_APP_PATH",
  SET_APP_LOADING = "SET_APP_LOADING",
  SET_TOKEN = "SET_TOKEN",
  AUTHENTICATE_SUCCESS = "AUTHENTICATE_SUCCESS",
  LOGOUT_SUCCESS = "LOGOUT_SUCCESS",
  CHANGE_COMPANY_SELECT_SUCCESS = "CHANGE_COMPANY_SELECT_SUCCESS",
  SET_ENV_SETTINGS = "SET_ENV_SETTINGS",
  SET_SELECTED_DEBTOR_ID = "SET_SELECTED_DEBTOR_ID",
  SET_IS_VALIDATING_RESET_TOKEN = "SET_IS_VALIDATING_RESET_TOKEN",
  SET_RESET_PASSWORD_USER = "SET_RESET_PASSWORD_USER",

  STARTUP = "STARTUP",
  AUTHENTICATE = "AUTHENTICATE",
  LOGOUT = "LOGOUT",
  TOKEN_EXPIRED = "TOKEN_EXPIRED",
  CHANGE_COMPANY_SELECT = "CHANGE_COMPANY_SELECT",
  SEND_RESET_PASSWORD_LINK = "SEND_RESET_PASSWORD_LINK",
  VALIDATE_RESET_PASSWORD_TOKEN = "VALIDATE_RESET_PASSWORD_TOKEN",
  RESET_PASSWORD = "RESET_PASSWORD",

  APP_GET_COMPANY_TNC = "APP_GET_COMPANY_TNC",
  APP_SET_COMPANY_TNC = "APP_SET_COMPANY_TNC",
  SET_OPEN_TNC = "SET_OPEN_TNC",
  SET_USER_TNC_STATUS = "SET_USER_TNC_STATUS"
}

export type UpdateApiUrl = Action<ETypesName.UPDATE_API_URL> & {
  apiUrl: string
}

export type UpdateAppPath = Action<ETypesName.UPDATE_APP_PATH> & {
  appPath: string
}

export type SetAppLoading = Action<ETypesName.SET_APP_LOADING> & {
  isLoading: boolean
}

export type SetToken = Action<ETypesName.SET_TOKEN> & {
  token: string | null
}

export type AuthenticateSuccess = Action<ETypesName.AUTHENTICATE_SUCCESS> & {
  token: string
  user: User
}

export type LogoutSuccess = Action<ETypesName.AUTHENTICATE_SUCCESS>

export type SetEnvSettings = Action<ETypesName.SET_ENV_SETTINGS> & {
  envSettings: Partial<EnvSettings>
}

export type SetSelectedDebtorId = Action<ETypesName.SET_SELECTED_DEBTOR_ID> & {
  selectedDebtorId: number
}

export type SetIsValidatingResetToken = Action<ETypesName.SET_IS_VALIDATING_RESET_TOKEN> & {
  isValidating: boolean
}

export type SetResetPasswordUser = Action<ETypesName.SET_RESET_PASSWORD_USER> & {
  userResetPassword: UserResetPassword
}

export type Startup = Action<ETypesName.STARTUP>

export type Authenticate = Action<ETypesName.AUTHENTICATE> & {
  user: any
  setSubmitting: Function
  onSuccess: Function
}

export type Logout = Action<ETypesName.LOGOUT> & {
  onSuccess: Function
}

export type TokenExpired = Action<ETypesName.TOKEN_EXPIRED>

export type ChangeCompanySelect = Action<ETypesName.CHANGE_COMPANY_SELECT> & {
  companyCode: string
  user: User
}

export type ChangeCompanySelectSuccess = Action<ETypesName.CHANGE_COMPANY_SELECT_SUCCESS> & {
  user: User
}

export type SendResetPasswordLink = Action<ETypesName.SEND_RESET_PASSWORD_LINK> & {
  values: ForgotPasswordFormProps
  setSubmitting: Function
  onSuccess: Function
}

export type ValidateResetPasswordToken = Action<ETypesName.VALIDATE_RESET_PASSWORD_TOKEN> & {
  token: string
  onSuccess: Function
  onFail: Function
}

export type ResetPassword = Action<ETypesName.RESET_PASSWORD> & {
  values: ResetPasswordFormProps
  setSubmitting: Function
  onSuccess: Function
}

export type AppGetCompanyTnc = Action<ETypesName.APP_GET_COMPANY_TNC> & {
  user: User
}
export type AppSetCompanyTnc = Action<ETypesName.APP_SET_COMPANY_TNC> & {
  companyTnc: CompanyTnc
}
export type SetOpenTnc = Action<ETypesName.SET_OPEN_TNC> & {
  openTnc: boolean
}
export type SetUserTncStatus = Action<ETypesName.SET_USER_TNC_STATUS> & {
  companyTnc: CompanyTnc
}
