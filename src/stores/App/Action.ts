import { createActions } from "reduxsauce"
import {
  Authenticate,
  AuthenticateSuccess,
  ChangeCompanySelect,
  ChangeCompanySelectSuccess,
  ETypesName,
  EnvSettings,
  Logout,
  LogoutSuccess,
  ResetPassword,
  SendResetPasswordLink,
  SetAppLoading,
  SetEnvSettings,
  SetIsValidatingResetToken,
  SetResetPasswordUser,
  SetSelectedDebtorId,
  SetToken,
  Startup,
  TokenExpired,
  UpdateApiUrl,
  UpdateAppPath,
  ValidateResetPasswordToken,
  CompanyTnc,
  AppGetCompanyTnc,
  AppSetCompanyTnc,
  SetOpenTnc,
  SetUserTncStatus
} from "./Types"
import { User } from "../User/Types"
import { ForgotPasswordFormProps } from "@/components/page/Form/ForgotPasswordForm"
import { ResetPasswordFormProps } from "@/components/page/Form/ResetPasswordForm"

const { Types, Creators: AppAction } = createActions<
  {
    [ETypesName.UPDATE_API_URL]: string
    [ETypesName.UPDATE_APP_PATH]: string
    [ETypesName.SET_APP_LOADING]: string
    [ETypesName.SET_TOKEN]: string
    [ETypesName.AUTHENTICATE_SUCCESS]: string
    [ETypesName.LOGOUT_SUCCESS]: string
    [ETypesName.CHANGE_COMPANY_SELECT_SUCCESS]: string
    [ETypesName.SET_ENV_SETTINGS]: string
    [ETypesName.SET_SELECTED_DEBTOR_ID]: string
    [ETypesName.SET_IS_VALIDATING_RESET_TOKEN]: string
    [ETypesName.SET_RESET_PASSWORD_USER]: string

    [ETypesName.STARTUP]: string
    [ETypesName.AUTHENTICATE]: string
    [ETypesName.LOGOUT]: string
    [ETypesName.TOKEN_EXPIRED]: string
    [ETypesName.CHANGE_COMPANY_SELECT]: string
    [ETypesName.SEND_RESET_PASSWORD_LINK]: string
    [ETypesName.VALIDATE_RESET_PASSWORD_TOKEN]: string
    [ETypesName.RESET_PASSWORD]: string

    [ETypesName.APP_GET_COMPANY_TNC]: string
    [ETypesName.APP_SET_COMPANY_TNC]: string
    [ETypesName.SET_OPEN_TNC]: string
    [ETypesName.SET_USER_TNC_STATUS]: string
  },
  {
    updateApiUrl: (apiUrl: string) => UpdateApiUrl
    updateAppPath: (appPath: string) => UpdateAppPath
    setAppLoading: (isLoading: boolean) => SetAppLoading
    setToken: (token: string | null) => SetToken
    authenticateSuccess: (token: string, user: User) => AuthenticateSuccess
    logoutSuccess: () => LogoutSuccess
    changeCompanySelectSuccess: (user: User) => ChangeCompanySelectSuccess
    setEnvSettings: (envSettings: Partial<EnvSettings>) => SetEnvSettings
    setSelectedDebtorId: (selectedDebtorId: number) => SetSelectedDebtorId
    setIsValidatingResetToken: (isValidating: boolean) => SetIsValidatingResetToken
    setResetPasswordUser: (userResetPassword: User | null) => SetResetPasswordUser

    startup: () => Startup
    authenticate: (user: any, setSubmitting: Function, onSuccess: Function) => Authenticate
    logout: (onSuccess: Function) => Logout
    tokenExpired: () => TokenExpired
    changeCompanySelect: (companyCode: string, user: User) => ChangeCompanySelect
    sendResetPasswordLink: (
      values: ForgotPasswordFormProps,
      setSubmitting: Function,
      onSuccess: Function
    ) => SendResetPasswordLink
    validateResetPasswordToken: (
      token: string,
      onSuccess: Function,
      onFail: Function
    ) => ValidateResetPasswordToken
    resetPassword: (
      values: ResetPasswordFormProps,
      setSubmitting: Function,
      onSuccess: Function
    ) => ResetPassword
    appGetCompanyTnc: (user: User) => AppGetCompanyTnc
    appSetCompanyTnc: (companyTnc: CompanyTnc) => AppSetCompanyTnc
    setOpenTnc: (openTnc: boolean) => SetOpenTnc
    setUserTncStatus: (companyTnc: CompanyTnc) => SetUserTncStatus
  }
>({
  // Reducers
  updateApiUrl: ["apiUrl"],
  updateAppPath: ["appPath"],
  setAppLoading: ["isLoading"],
  setToken: ["token"],
  authenticateSuccess: ["token", "user"],
  logoutSuccess: [],
  changeCompanySelectSuccess: ["user"],
  setEnvSettings: ["envSettings"],
  setSelectedDebtorId: ["selectedDebtorId"],
  setIsValidatingResetToken: ["isValidating"],
  setResetPasswordUser: ["userResetPassword"],
  appSetCompanyTnc: ["companyTnc"],
  setOpenTnc: ["openTnc"],

  // Sagas
  startup: [],
  authenticate: ["user", "setSubmitting", "onSuccess"],
  logout: ["onSuccess"],
  tokenExpired: [],
  changeCompanySelect: ["companyCode", "user"],
  sendResetPasswordLink: ["values", "setSubmitting", "onSuccess"],
  validateResetPasswordToken: ["token", "onSuccess", "onFail"],
  resetPassword: ["values", "setSubmitting", "onSuccess"],
  appGetCompanyTnc: ["user"],
  setUserTncStatus: ["companyTnc"]
})

export const AppTypes = Types
export default AppAction
