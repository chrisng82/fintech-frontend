import { createReducer } from "reduxsauce"
import { AppTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  AppState,
  AuthenticateSuccess,
  ChangeCompanySelectSuccess,
  LogoutSuccess,
  SetAppLoading,
  SetEnvSettings,
  SetIsValidatingResetToken,
  SetResetPasswordUser,
  SetSelectedDebtorId,
  SetToken,
  UpdateApiUrl,
  UpdateAppPath,
  AppSetCompanyTnc,
  SetOpenTnc
} from "./Types"

export const updateApiUrl = (state: AppState, { apiUrl }: UpdateApiUrl) => ({
  ...state,
  apiUrl
})

export const updateAppPath = (state: AppState, { appPath }: UpdateAppPath) => ({
  ...state,
  appPath
})

export const setAppLoading = (state: AppState, { isLoading }: SetAppLoading) => ({
  ...state,
  isLoading
})

export const setToken = (state: AppState, { token }: SetToken) => ({
  ...state,
  token
})

export const authenticateSuccess = (state: AppState, { token, user }: AuthenticateSuccess) => ({
  ...state,
  token,
  user
})

export const logoutSuccess = (state: AppState) => ({
  ...state,
  token: null,
  user: null
})

export const changeCompanySelectSuccess = (
  state: AppState,
  { user }: ChangeCompanySelectSuccess
) => ({
  ...state,
  user
})

export const setEnvSettings = (state: AppState, { envSettings }: SetEnvSettings) => ({
  ...state,
  envSettings: {
    ...state.envSettings,
    ...envSettings
  }
})

export const setSelectedDebtorId = (
  state: AppState,
  { selectedDebtorId }: SetSelectedDebtorId
) => ({
  ...state,
  user: {
    ...state.user!,
    selectedDebtorId
  }
})

export const setIsValidatingResetToken = (
  state: AppState,
  { isValidating }: SetIsValidatingResetToken
) => ({
  ...state,
  resetPassword: {
    ...state.resetPassword,
    isValidating
  }
})

export const setResetPasswordUser = (
  state: AppState,
  { userResetPassword }: SetResetPasswordUser
) => ({
  ...state,
  resetPassword: {
    ...state.resetPassword,
    user: userResetPassword
  }
})

export const appSetCompanyTnc = (state: AppState, { companyTnc }: AppSetCompanyTnc) => ({
  ...state,
  companyTnc
})

export const setOpenTnc = (state: AppState, { openTnc }: SetOpenTnc) => ({
  ...state,
  openTnc
})

type Actions =
  | UpdateAppPath
  | UpdateApiUrl
  | SetAppLoading
  | SetToken
  | AuthenticateSuccess
  | LogoutSuccess
  | ChangeCompanySelectSuccess
  | SetEnvSettings
  | SetSelectedDebtorId
  | SetIsValidatingResetToken
  | SetResetPasswordUser
  | AppSetCompanyTnc
  | SetOpenTnc

export const AppReducer = createReducer<AppState, Actions>(INITIAL_STATE, {
  [AppTypes.UPDATE_API_URL]: updateApiUrl,
  [AppTypes.UPDATE_APP_PATH]: updateAppPath,
  [AppTypes.SET_APP_LOADING]: setAppLoading,
  [AppTypes.SET_TOKEN]: setToken,
  [AppTypes.AUTHENTICATE_SUCCESS]: authenticateSuccess,
  [AppTypes.LOGOUT_SUCCESS]: logoutSuccess,
  [AppTypes.CHANGE_COMPANY_SELECT_SUCCESS]: changeCompanySelectSuccess,
  [AppTypes.SET_ENV_SETTINGS]: setEnvSettings,
  [AppTypes.SET_SELECTED_DEBTOR_ID]: setSelectedDebtorId,
  [AppTypes.SET_IS_VALIDATING_RESET_TOKEN]: setIsValidatingResetToken,
  [AppTypes.SET_RESET_PASSWORD_USER]: setResetPasswordUser,
  [AppTypes.APP_SET_COMPANY_TNC]: appSetCompanyTnc,
  [AppTypes.SET_OPEN_TNC]: setOpenTnc
})
