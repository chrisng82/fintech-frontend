// eslint-disable-next-line import/no-anonymous-default-export
export default {
  apiUrl: "",
  appPath: "",
  locale: "en-US",
  token: null,
  isLoading: false,
  user: null,
  envSettings: {
    HIDE_ROUTES: null,
    DISABLED_FEATURES: null,
    DEBTOR_FORCED_CONTACTS: false,
    DEBTOR_HIDE_SSM_REGISTRATION: false,
    IS_DISABLED_SELF_SIGNUP: null
  },
  resetPassword: {
    isValidating: false,
    user: null
  },
  companyTnc: {
    tnc: null,
    user_tnc: null
  },
  openTnc: false
}
