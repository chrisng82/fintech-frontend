import { createActions } from "reduxsauce"
import {
  ETypesName,
  FetchFtpSyncLogs,
  FetchFtpSyncPendingFiles,
  SetFtpSyncLogs,
  SetFtpSyncLogsLoading,
  SetFtpSyncPendingFiles,
  SetFtpSyncPendingFilesLoading,
  SyncFtpPendingFiles
} from "./Types"

const { Types, Creators: FtpSyncAction } = createActions<
  {
    [ETypesName.SET_FTP_SYNC_PENDING_FILES]: string
    [ETypesName.SET_FTP_SYNC_PENDING_FILES_LOADING]: string
    [ETypesName.SET_FTP_SYNC_LOGS]: string
    [ETypesName.SET_FTP_SYNC_LOGS_LOADING]: string

    [ETypesName.FETCH_FTP_SYNC_PENDING_FILES]: string
    [ETypesName.FETCH_FTP_SYNC_LOGS]: string
    [ETypesName.SYNC_FTP_PENDING_FILES]: string
  },
  {
    setFtpSyncPendingFiles: (data: any[]) => SetFtpSyncPendingFiles
    setFtpSyncPendingFilesLoading: (isLoading: boolean) => SetFtpSyncPendingFilesLoading
    setFtpSyncLogs: (data: any[]) => SetFtpSyncLogs
    setFtpSyncLogsLoading: (isLoading: boolean) => SetFtpSyncLogsLoading

    fetchFtpSyncPendingFiles: () => FetchFtpSyncPendingFiles
    fetchFtpSyncLogs: () => FetchFtpSyncLogs
    syncFtpPendingFiles: () => SyncFtpPendingFiles
  }
>({
  // Reducers
  setFtpSyncPendingFiles: ["data"],
  setFtpSyncPendingFilesLoading: ["isLoading"],
  setFtpSyncLogs: ["data"],
  setFtpSyncLogsLoading: ["isLoading"],

  // Saga
  fetchFtpSyncPendingFiles: [],
  fetchFtpSyncLogs: [],
  syncFtpPendingFiles: []
})

export const FtpSyncTypes = Types
export default FtpSyncAction
