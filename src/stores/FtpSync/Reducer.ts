import { createReducer } from "reduxsauce"
import INITIAL_STATE from "./InitialState"
import {
  FtpSyncState,
  SetFtpSyncLogs,
  SetFtpSyncLogsLoading,
  SetFtpSyncPendingFiles,
  SetFtpSyncPendingFilesLoading
} from "./Types"
import { FtpSyncTypes } from "./Action"

export const setFtpSyncPendingFiles = (state: FtpSyncState, { data }: SetFtpSyncPendingFiles) => ({
  ...state,
  pendingFiles: {
    ...state.pendingFiles,
    data
  }
})

export const setFtpSyncPendingFilesLoading = (
  state: FtpSyncState,
  { isLoading }: SetFtpSyncPendingFilesLoading
) => ({
  ...state,
  pendingFiles: {
    ...state.pendingFiles,
    isLoading
  }
})

export const setFtpSyncLogs = (state: FtpSyncState, { data }: SetFtpSyncLogs) => ({
  ...state,
  syncLogs: {
    ...state.syncLogs,
    data
  }
})

export const setFtpSyncLogsLoading = (
  state: FtpSyncState,
  { isLoading }: SetFtpSyncLogsLoading
) => ({
  ...state,
  syncLogs: {
    ...state.syncLogs,
    isLoading
  }
})

type Actions =
  | SetFtpSyncPendingFiles
  | SetFtpSyncPendingFilesLoading
  | SetFtpSyncLogs
  | SetFtpSyncLogsLoading

export const FtpSyncReducer = createReducer<FtpSyncState, Actions>(INITIAL_STATE, {
  [FtpSyncTypes.SET_FTP_SYNC_PENDING_FILES]: setFtpSyncPendingFiles,
  [FtpSyncTypes.SET_FTP_SYNC_PENDING_FILES_LOADING]: setFtpSyncPendingFilesLoading,
  [FtpSyncTypes.SET_FTP_SYNC_LOGS]: setFtpSyncLogs,
  [FtpSyncTypes.SET_FTP_SYNC_LOGS_LOADING]: setFtpSyncLogsLoading
})
