import { BaseModel } from "@/Types/GeneralTypes"
import { Action } from "@reduxjs/toolkit"
import { DocAttachment } from "../DocAttachment/Types"

export interface FtpSyncLog extends BaseModel {
  pending_files: number
  synced_files: number
  status: number
  doc_attachments?: DocAttachment[]
}

export interface FtpSyncDocument extends BaseModel {
  ftp_sync_log_id: number
  doc_attachment_id: number
  file_name: string
}

export interface FtpSyncState {
  pendingFiles: {
    data: any[]
    isLoading: boolean
  }
  syncLogs: {
    data: any[]
    isLoading: boolean
  }
}

export enum ETypesName {
  SET_FTP_SYNC_PENDING_FILES = "SET_FTP_SYNC_PENDING_FILES",
  SET_FTP_SYNC_PENDING_FILES_LOADING = "SET_FTP_SYNC_PENDING_FILES_LOADING",
  SET_FTP_SYNC_LOGS = "SET_FTP_SYNC_LOGS",
  SET_FTP_SYNC_LOGS_LOADING = "SET_FTP_SYNC_LOGS_LOADING",

  FETCH_FTP_SYNC_PENDING_FILES = "FETCH_FTP_SYNC_PENDING_FILES",
  FETCH_FTP_SYNC_LOGS = "FETCH_FTP_SYNC_LOGS",
  SYNC_FTP_PENDING_FILES = "SYNC_FTP_PENDING_FILES"
}

export type SetFtpSyncPendingFiles = Action<ETypesName.SET_FTP_SYNC_PENDING_FILES> & {
  data: any[]
}

export type SetFtpSyncPendingFilesLoading =
  Action<ETypesName.SET_FTP_SYNC_PENDING_FILES_LOADING> & {
    isLoading: boolean
  }

export type SetFtpSyncLogs = Action<ETypesName.SET_FTP_SYNC_LOGS> & {
  data: any[]
}

export type SetFtpSyncLogsLoading = Action<ETypesName.SET_FTP_SYNC_LOGS_LOADING> & {
  isLoading: boolean
}

export type FetchFtpSyncPendingFiles = Action<ETypesName.FETCH_FTP_SYNC_PENDING_FILES>

export type FetchFtpSyncLogs = Action<ETypesName.FETCH_FTP_SYNC_LOGS>

export type SyncFtpPendingFiles = Action<ETypesName.SYNC_FTP_PENDING_FILES>
