import { BaseDocumentModel, BaseModel } from "@/Types/GeneralTypes"
import { IFilterType } from "@/components/Datatable/Filter"
import { Action } from "@reduxjs/toolkit"

export interface DebitNote extends BaseModel, BaseDocumentModel {
  hdrId: number
  deliveryDate: string
  deliveryStatus: number
}

export interface DebitNoteDetail extends BaseModel {
  [key: string]: any
  line_no: number
  code: string
  desc_01: string
  desc_02: string
  location_code: string
  uom_code: string
  uom_rate: number
  sale_price: number
  price_disc: number
  qty: number
  gross_amt: number
  gross_local_amt: number
  dtl_disc_amt: number
  dtl_disc_local_amt: number
  hdr_disc_amt: number
  hdr_disc_local_amt: number
  tax_code: string
  net_amt: number
  net_local_amt: number
  debtorName: string
  companyName: string
  desc: string
  currencyRate: number
  email: string
  unitNo: string
  buildingName: string
  district01: string
  district02: string
  streetName: string
  postcode: number
  stateName: string
  countryName: string
}

export type DebitNotesFilters = {
  [key in IFilterType]: string | boolean | number | string[]
}
export interface DebitNotesSorts {
  field: string
  order: string
}

export interface DebitNoteState {
  list: {
    data: DebitNote[]
    total: number
    page_size: number
    last_page: number
    current_page: number
    is_loading: boolean
    filters: {
      code?: string
      debtorName: string
      debtorCode?: string
      paymentStatus?: string
      pdfOnly?: boolean
      docDate?: string[]
    }
    sorts: DebitNotesSorts[]
  }
  item: {
    is_loading: boolean
    data: DebitNote | null
    details: {
      data: DebitNoteDetail[]
      total: number
      page_size: number
      last_page: number
      current_page: number
      is_loading: boolean
    }
  }
}

export enum ETypesName {
  FETCH_DEBIT_NOTES = "FETCH_DEBIT_NOTES",
  SET_DEBIT_NOTES = "SET_DEBIT_NOTES",
  SET_DEBIT_NOTES_PAGINATION = "SET_DEBIT_NOTES_PAGINATION",
  SET_DEBIT_NOTES_FILTERS = "SET_DEBIT_NOTES_FILTERS",
  SET_DEBIT_NOTES_SORTS = "SET_DEBIT_NOTES_SORTS",

  FETCH_DEBIT_NOTE = "FETCH_DEBIT_NOTE",
  SET_DEBIT_NOTE = "SET_DEBIT_NOTE",
  FETCH_DEBIT_NOTE_DETAILS = "FETCH_DEBIT_NOTE_DETAILS",
  SET_DEBIT_NOTE_DETAILS = "SET_DEBIT_NOTE_DETAILS",
  SET_DEBIT_NOTE_DETAILS_PAGINATION = "SET_DEBIT_NOTE_DETAILS_PAGINATION",

  SET_DEBIT_NOTES_LOADING = "SET_DEBIT_NOTES_LOADING",
  SET_DEBIT_NOTE_LOADING = "SET_DEBIT_NOTE_LOADING",
  SET_DEBIT_NOTE_DETAILS_LOADING = "SET_DEBIT_NOTE_DETAILS_LOADING"
}

export type FetchDebitNotes = Action<ETypesName.FETCH_DEBIT_NOTES> & {
  companyCode: string
  page?: number
  page_size?: number
  filters?: DebitNotesFilters
}

export type SetDebitNotes = Action<ETypesName.SET_DEBIT_NOTES> & {
  data: DebitNote[]
}

export type SetDebitNotesPagination = Action<ETypesName.SET_DEBIT_NOTES_PAGINATION> & {
  page_size: number
  current_page: number
  last_page: number
  total: number
}

export type SetDebitNotesFilters = Action<ETypesName.SET_DEBIT_NOTES_FILTERS> & {
  filters: {
    code: string
    debtorName: string
    debtorCode: string
  }
}
export type SetDebitNotesSorts = Action<ETypesName.SET_DEBIT_NOTES_SORTS> & {
  sorts: DebitNotesSorts[]
}

export type FetchDebitNote = Action<ETypesName.FETCH_DEBIT_NOTE> & {
  id: number
}

export type FetchDebitNoteDetails = Action<ETypesName.FETCH_DEBIT_NOTE_DETAILS> & {
  hdrId: number
  page?: number
  page_size?: number
}

export type SetDebitNote = Action<ETypesName.SET_DEBIT_NOTE> & {
  data: DebitNote
}

export type SetDebitNoteDetails = Action<ETypesName.SET_DEBIT_NOTE_DETAILS> & {
  data: DebitNoteDetail[]
}

export type SetDebitNoteDetailsPagination = Action<ETypesName.SET_DEBIT_NOTE_DETAILS_PAGINATION> & {
  page_size: number
  current_page: number
  last_page: number
  total: number
}

export type SetDebitNoteLoading = Action<ETypesName.SET_DEBIT_NOTE_LOADING> & {
  is_loading: boolean
}

export type SetDebitNotesLoading = Action<ETypesName.SET_DEBIT_NOTES_LOADING> & {
  is_loading: boolean
}

export type SetDebitNoteDetailsLoading = Action<ETypesName.SET_DEBIT_NOTE_DETAILS_LOADING> & {
  is_loading: boolean
}
