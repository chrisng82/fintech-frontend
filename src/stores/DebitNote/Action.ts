import { createActions } from "reduxsauce"
import {
  DebitNote,
  DebitNotesFilters,
  ETypesName,
  FetchDebitNote,
  FetchDebitNotes,
  SetDebitNote,
  SetDebitNoteLoading,
  SetDebitNotes,
  SetDebitNotesFilters,
  SetDebitNotesSorts,
  DebitNotesSorts,
  SetDebitNotesPagination,
  SetDebitNotesLoading,
  FetchDebitNoteDetails,
  SetDebitNoteDetails,
  DebitNoteDetail,
  SetDebitNoteDetailsPagination
} from "./Types"

const { Types, Creators: DebitNoteAction } = createActions<
  {
    [ETypesName.FETCH_DEBIT_NOTES]: string
    [ETypesName.SET_DEBIT_NOTES]: string
    [ETypesName.SET_DEBIT_NOTES_PAGINATION]: string
    [ETypesName.SET_DEBIT_NOTES_FILTERS]: string
    [ETypesName.SET_DEBIT_NOTES_SORTS]: string

    [ETypesName.FETCH_DEBIT_NOTE]: string
    [ETypesName.SET_DEBIT_NOTE]: string
    [ETypesName.FETCH_DEBIT_NOTE_DETAILS]: string
    [ETypesName.SET_DEBIT_NOTE_DETAILS]: string
    [ETypesName.SET_DEBIT_NOTE_DETAILS_PAGINATION]: string

    [ETypesName.SET_DEBIT_NOTE_LOADING]: string
    [ETypesName.SET_DEBIT_NOTES_LOADING]: string
    [ETypesName.SET_DEBIT_NOTE_DETAILS_LOADING]: string
  },
  {
    fetchDebitNotes: (
      page?: number,
      page_size?: number,
      filters?: DebitNotesFilters
    ) => FetchDebitNotes
    setDebitNotes: (data: DebitNote[]) => SetDebitNotes
    setDebitNotesPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetDebitNotesPagination

    setDebitNotesFilters: (filters: DebitNotesFilters) => SetDebitNotesFilters
    setDebitNotesSorts: (sorts: DebitNotesSorts[]) => SetDebitNotesSorts

    fetchDebitNote: (id: number) => FetchDebitNote
    setDebitNote: (data: DebitNote) => SetDebitNote

    fetchDebitNoteDetails: (hdrId: number) => FetchDebitNoteDetails
    setDebitNoteDetails: (data: DebitNoteDetail[]) => SetDebitNoteDetails
    setDebitNoteDetailsPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetDebitNoteDetailsPagination

    setDebitNoteLoading: (is_loading: boolean) => SetDebitNoteLoading
    setDebitNotesLoading: (is_loading: boolean) => SetDebitNotesLoading
    setDebitNoteDetailsLoading: (is_loading: boolean) => SetDebitNoteLoading
  }
>({
  //saga
  fetchDebitNotes: ["page", "page_size", "filters"],
  fetchDebitNote: ["id"],
  fetchDebitNoteDetails: ["hdrId"],

  //reducer
  setDebitNotes: ["data"],
  setDebitNotesPagination: ["page_size", "current_page", "last_page", "total"],
  setDebitNotesFilters: ["filters"],
  setDebitNotesSorts: ["sorts"],

  setDebitNote: ["data"],
  setDebitNoteDetails: ["data"],
  setDebitNoteDetailsPagination: ["page_size", "current_page", "last_page", "total"],

  setDebitNoteLoading: ["is_loading"],
  setDebitNotesLoading: ["is_loading"],
  setDebitNoteDetailsLoading: ["is_loading"]
})

export const debitNoteTypes = Types
export default DebitNoteAction
