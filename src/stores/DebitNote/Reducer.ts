import { createReducer } from "reduxsauce"
import { debitNoteTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  DebitNoteState,
  SetDebitNoteLoading,
  SetDebitNotes,
  SetDebitNotesPagination,
  SetDebitNotesLoading,
  SetDebitNotesFilters,
  SetDebitNotesSorts,
  SetDebitNote,
  SetDebitNoteDetails,
  SetDebitNoteDetailsPagination,
  SetDebitNoteDetailsLoading
} from "./Types"

export const setDebitNotes = (state: DebitNoteState, { data }: SetDebitNotes) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setDebitNotesPagination = (
  state: DebitNoteState,
  { page_size, current_page, last_page, total }: SetDebitNotesPagination
) => ({
  ...state,
  list: {
    ...state.list,
    page_size,
    current_page,
    last_page,
    total
  }
})

export const setDebitNotesFilters = (state: DebitNoteState, { filters }: SetDebitNotesFilters) => ({
  ...state,
  list: {
    ...state.list,
    filters
  }
})
export const setDebitNotesSorts = (state: DebitNoteState, { sorts }: SetDebitNotesSorts) => ({
  ...state,
  list: {
    ...state.list,
    sorts
  }
})

export const setDebitNote = (state: DebitNoteState, { data }: SetDebitNote) => ({
  ...state,
  item: {
    ...state.item,
    data
  }
})

export const setDebitNoteDetails = (state: DebitNoteState, { data }: SetDebitNoteDetails) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      data
    }
  }
})

export const setDebitNoteDetailsPagination = (
  state: DebitNoteState,
  { page_size, current_page, last_page, total }: SetDebitNoteDetailsPagination
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      page_size,
      current_page,
      last_page,
      total
    }
  }
})

export const setDebitNotesLoading = (
  state: DebitNoteState,
  { is_loading }: SetDebitNotesLoading
) => ({
  ...state,
  list: {
    ...state.list,
    is_loading
  }
})

export const setDebitNoteLoading = (
  state: DebitNoteState,
  { is_loading }: SetDebitNoteLoading
) => ({
  ...state,
  item: {
    ...state.item,
    is_loading
  }
})

export const setDebitNoteDetailsLoading = (
  state: DebitNoteState,
  { is_loading }: SetDebitNoteDetailsLoading
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      is_loading
    }
  }
})

type Actions =
  | SetDebitNotes
  | SetDebitNotesPagination
  | SetDebitNotesFilters
  | SetDebitNotesSorts
  | SetDebitNote
  | SetDebitNoteDetails
  | SetDebitNoteDetailsPagination
  | SetDebitNotesLoading
  | SetDebitNoteLoading
  | SetDebitNoteDetailsLoading

export const DebitNoteReducer = createReducer<DebitNoteState, Actions>(INITIAL_STATE, {
  [debitNoteTypes.SET_DEBIT_NOTES]: setDebitNotes,
  [debitNoteTypes.SET_DEBIT_NOTES_PAGINATION]: setDebitNotesPagination,
  [debitNoteTypes.SET_DEBIT_NOTES_FILTERS]: setDebitNotesFilters,
  [debitNoteTypes.SET_DEBIT_NOTES_SORTS]: setDebitNotesSorts,

  [debitNoteTypes.SET_DEBIT_NOTE]: setDebitNote,
  [debitNoteTypes.SET_DEBIT_NOTE_DETAILS]: setDebitNoteDetails,
  [debitNoteTypes.SET_DEBIT_NOTE_DETAILS_PAGINATION]: setDebitNoteDetailsPagination,

  [debitNoteTypes.SET_DEBIT_NOTES_LOADING]: setDebitNotesLoading,
  [debitNoteTypes.SET_DEBIT_NOTE_LOADING]: setDebitNoteLoading,
  [debitNoteTypes.SET_DEBIT_NOTE_DETAILS_LOADING]: setDebitNoteDetailsLoading
})
