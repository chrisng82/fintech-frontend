import { combineReducers } from "redux"

import configureStore from "./CreateStore"

import { AppState } from "./App/Types"
import { CompanyState } from "./Company/Types"

import { AppReducer } from "./App/Reducer"
import { CompanyReducer } from "./Company/Reducer"
import { SnackbarState } from "./Snackbar/Types"
import { SnackbarReducer } from "./Snackbar/Reducer"
import { CompanyEmailTaskState } from "./CompanyEmailTask/Types"
import { CompanyEmailTaskReducer } from "./CompanyEmailTask/Reducer"
import { SignupLinkReducer } from "./SignupLink/Reducer"
import { SignupLinkState } from "./SignupLink/Types"
import { DebtorReducer } from "./Debtor/Reducer"
import { DebtorState } from "./Debtor/Types"
import { CreditNoteState } from "./CreditNote/Types"
import { CreditNoteReducer } from "./CreditNote/Reducer"
import { InvoiceState } from "./Invoice/Types"
import { InvoiceReducer } from "./Invoice/Reducer"
import { DebitNoteState } from "./DebitNote/Types"
import { DebitNoteReducer } from "./DebitNote/Reducer"
import { DebtorPaymentState } from "./DebtorPayment/Types"
import { DebtorPaymentReducer } from "./DebtorPayment/Reducer"
import { FtpSyncState } from "./FtpSync/Types"
import { FtpSyncReducer } from "./FtpSync/Reducer"
import { DocAttachmentReducer } from "./DocAttachment/Reducer"
import { DocAttachmentState } from "./DocAttachment/Types"
import { EficoreSyncState } from "./EficoreSync/Types"
import { EficoreSyncReducer } from "./EficoreSync/Reducer"
import { MonthlyStatementState } from "./MonthlyStatement/Types"
import { MonthlyStatementReducer } from "./MonthlyStatement/Reducer"
import { UserState } from "./User/Types"
import { UserReducer } from "./User/Reducer"

export type RootState = {
  app: AppState
  company: CompanyState
  companyEmailTask: CompanyEmailTaskState
  debtor: DebtorState
  signupLink: SignupLinkState
  snackbar: SnackbarState
  invoice: InvoiceState
  creditNote: CreditNoteState
  debitNote: DebitNoteState
  debtorPayment: DebtorPaymentState
  ftpSync: FtpSyncState
  eficoreSync: EficoreSyncState
  docAttachment: DocAttachmentState
  monthlyStatement: MonthlyStatementState
  user: UserState
}

export const rootReducer: any = combineReducers<any>({
  app: AppReducer,
  company: CompanyReducer,
  companyEmailTask: CompanyEmailTaskReducer,
  debtor: DebtorReducer,
  signupLink: SignupLinkReducer,
  snackbar: SnackbarReducer,
  invoice: InvoiceReducer,
  creditNote: CreditNoteReducer,
  debitNote: DebitNoteReducer,
  debtorPayment: DebtorPaymentReducer,
  ftpSync: FtpSyncReducer,
  eficoreSync: EficoreSyncReducer,
  docAttachment: DocAttachmentReducer,
  monthlyStatement: MonthlyStatementReducer,
  user: UserReducer
})

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
  return configureStore(rootReducer)
}
