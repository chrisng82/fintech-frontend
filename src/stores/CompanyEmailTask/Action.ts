import { createActions } from "reduxsauce"
import {
  BaseCompanyEmailTask,
  CompanyEmailTask,
  SetCompanyEmailTasks,
  ETypesName,
  ToggleCompanyEmailTask,
  CreateCompanyEmailTask,
  UpdateCompanyEmailTask,
  SetCompanyEmailTask,
  SetCompanyEmailTaskLoading,
  SetCompanyEmailTaskListLoading,
  DeleteCompanyEmailTask,
  FetchCompanyEmailTaskByCompanyCodeAndTaskCode
} from "./Types"
import { deleteCompanyEmailTask } from "@/sagas/CompanyEmailTaskSaga"

const { Types, Creators: CompanyEmailTaskAction } = createActions<
  {
    [ETypesName.SET_COMPANY_EMAIL_TASKS]: string
    [ETypesName.SET_COMPANY_EMAIL_TASK]: string
    [ETypesName.SET_COMPANY_EMAIL_TASK_LOADING]: string
    [ETypesName.SET_COMPANY_EMAIL_TASK_LIST_LOADING]: string

    [ETypesName.FETCH_COMPANY_EMAIL_TASK_BY_COMPANY_CODE_AND_TASK_CODE]: string
    [ETypesName.TOGGLE_COMPANY_EMAIL_TASK]: string
    [ETypesName.CREATE_COMPANY_EMAIL_TASK]: string
    [ETypesName.UPDATE_COMPANY_EMAIL_TASK]: string
    [ETypesName.DELETE_COMPANY_EMAIL_TASK]: string
  },
  {
    setCompanyEmailTasks: (data: CompanyEmailTask[]) => SetCompanyEmailTasks
    setCompanyEmailTask: (data: CompanyEmailTask) => SetCompanyEmailTask
    setCompanyEmailTaskLoading: (isLoading: boolean) => SetCompanyEmailTaskLoading
    setCompanyEmailTaskListLoading: (isLoading: boolean) => SetCompanyEmailTaskListLoading

    fetchCompanyEmailTaskByCompanyCodeAndTaskCode: (
      companyCode: string,
      taskCode: string
    ) => FetchCompanyEmailTaskByCompanyCodeAndTaskCode
    toggleCompanyEmailTask: (id: number) => ToggleCompanyEmailTask
    createCompanyEmailTask: (
      setSubmitting: Function,
      data: BaseCompanyEmailTask,
      onSuccess: Function
    ) => CreateCompanyEmailTask
    updateCompanyEmailTask: (
      setSubmitting: Function,
      id: number,
      data: Partial<BaseCompanyEmailTask>,
      onSuccess: Function
    ) => UpdateCompanyEmailTask
    deleteCompanyEmailTask: (id: number, onSuccess: Function) => DeleteCompanyEmailTask
  }
>({
  // Reducers
  setCompanyEmailTasks: ["data"],
  setCompanyEmailTask: ["data"],
  setCompanyEmailTaskLoading: ["isLoading"],
  setCompanyEmailTaskListLoading: ["isLoading"],

  //Sagas
  fetchCompanyEmailTaskByCompanyCodeAndTaskCode: ["companyCode", "taskCode"],
  toggleCompanyEmailTask: ["id"],
  createCompanyEmailTask: ["setSubmitting", "data", "onSuccess"],
  updateCompanyEmailTask: ["setSubmitting", "id", "data", "onSuccess"],
  deleteCompanyEmailTask: ["id", "onSuccess"]
})

export const CompanyEmailTaskTypes = Types
export default CompanyEmailTaskAction
