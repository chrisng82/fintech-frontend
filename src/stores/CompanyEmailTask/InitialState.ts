// eslint-disable-next-line import/no-anonymous-default-export
export default {
  emailTasks: {
    isLoading: false,
    data: [],
    page: 1,
    hasMore: false
  },
  emailTask: {
    isLoading: false,
    data: null
  }
}
