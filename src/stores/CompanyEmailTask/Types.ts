import { Action } from "@reduxjs/toolkit"

export interface CompanyEmailTask extends BaseCompanyEmailTask {
  created_at: string | Date
  id: number
  updated_at: string | Date
}

export interface BaseCompanyEmailTask {
  body: string
  code: string
  company_id: number
  cron_pattern: string
  extra_cc: string
  extra_recipients: string
  is_active: boolean
  subtitle: string
  task_type: number
  title: string
}

export interface CompanyEmailTaskState {
  emailTasks: {
    data: CompanyEmailTask[]
    isLoading: boolean
    page: number
    hasMore: boolean
  }
  emailTask: {
    data: CompanyEmailTask | null
    isLoading: boolean
  }
}

export enum ETypesName {
  SET_COMPANY_EMAIL_TASKS = "SET_COMPANY_EMAIL_TASKS",
  SET_COMPANY_EMAIL_TASK = "SET_COMPANY_EMAIL_TASK",
  SET_COMPANY_EMAIL_TASK_LOADING = "SET_COMPANY_EMAIL_TASK_LOADING",
  SET_COMPANY_EMAIL_TASK_LIST_LOADING = "SET_COMPANY_EMAIL_TASK_LIST_LOADING",

  FETCH_COMPANY_EMAIL_TASK_BY_COMPANY_CODE_AND_TASK_CODE = "FETCH_COMPANY_EMAIL_TASK_BY_COMPANY_CODE_AND_TASK_CODE",
  TOGGLE_COMPANY_EMAIL_TASK = "TOGGLE_COMPANY_EMAIL_TASK",
  CREATE_COMPANY_EMAIL_TASK = "CREATE_COMPANY_EMAIL_TASK",
  UPDATE_COMPANY_EMAIL_TASK = "UPDATE_COMPANY_EMAIL_TASK",
  DELETE_COMPANY_EMAIL_TASK = "DELETE_COMPANY_EMAIL_TASK"
}

export type SetCompanyEmailTasks = Action<ETypesName.SET_COMPANY_EMAIL_TASKS> & {
  data: CompanyEmailTask[]
}

export type SetCompanyEmailTask = Action<ETypesName.SET_COMPANY_EMAIL_TASK> & {
  data: CompanyEmailTask
}

export type SetCompanyEmailTaskLoading = Action<ETypesName.SET_COMPANY_EMAIL_TASK_LOADING> & {
  isLoading: boolean
}

export type SetCompanyEmailTaskListLoading =
  Action<ETypesName.SET_COMPANY_EMAIL_TASK_LIST_LOADING> & {
    isLoading: boolean
  }

export type FetchCompanyEmailTaskByCompanyCodeAndTaskCode =
  Action<ETypesName.FETCH_COMPANY_EMAIL_TASK_BY_COMPANY_CODE_AND_TASK_CODE> & {
    companyCode: string
    taskCode: string
  }

export type ToggleCompanyEmailTask = Action<ETypesName.TOGGLE_COMPANY_EMAIL_TASK> & {
  id: number
}

export type CreateCompanyEmailTask = Action<ETypesName.CREATE_COMPANY_EMAIL_TASK> & {
  setSubmitting: Function
  data: BaseCompanyEmailTask
  onSuccess: Function
}

export type UpdateCompanyEmailTask = Action<ETypesName.UPDATE_COMPANY_EMAIL_TASK> & {
  setSubmitting: Function
  id: number
  data: Partial<BaseCompanyEmailTask>
  onSuccess: Function
}

export type DeleteCompanyEmailTask = Action<ETypesName.DELETE_COMPANY_EMAIL_TASK> & {
  id: number
  onSuccess: Function
}
