import { createReducer } from "reduxsauce"
import { CompanyEmailTaskTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  CompanyEmailTask,
  CompanyEmailTaskState,
  SetCompanyEmailTask,
  SetCompanyEmailTaskListLoading,
  SetCompanyEmailTaskLoading,
  SetCompanyEmailTasks
} from "./Types"

export const setCompanyEmailTasks = (
  state: CompanyEmailTaskState,
  { data }: SetCompanyEmailTasks
) => ({
  ...state,
  emailTasks: {
    ...state.emailTasks,
    data
  }
})

export const setCompanyEmailTask = (
  state: CompanyEmailTaskState,
  { data }: SetCompanyEmailTask
) => ({
  ...state,
  emailTask: {
    ...state.emailTask,
    data
  }
})

export const setCompanyEmailTaskLoading = (
  state: CompanyEmailTaskState,
  { isLoading }: SetCompanyEmailTaskLoading
) => ({
  ...state,
  emailTask: {
    ...state.emailTask,
    isLoading
  }
})

export const setCompanyEmailTaskListLoading = (
  state: CompanyEmailTaskState,
  { isLoading }: SetCompanyEmailTaskListLoading
) => ({
  ...state,
  emailTasks: {
    ...state.emailTasks,
    isLoading
  }
})

type Actions =
  | SetCompanyEmailTasks
  | SetCompanyEmailTask
  | SetCompanyEmailTaskLoading
  | SetCompanyEmailTaskListLoading

export const CompanyEmailTaskReducer = createReducer<CompanyEmailTaskState, Actions>(
  INITIAL_STATE,
  {
    [CompanyEmailTaskTypes.SET_COMPANY_EMAIL_TASKS]: setCompanyEmailTasks,
    [CompanyEmailTaskTypes.SET_COMPANY_EMAIL_TASK]: setCompanyEmailTask,
    [CompanyEmailTaskTypes.SET_COMPANY_EMAIL_TASK_LOADING]: setCompanyEmailTaskLoading,
    [CompanyEmailTaskTypes.SET_COMPANY_EMAIL_TASK_LIST_LOADING]: setCompanyEmailTaskListLoading
  }
)
