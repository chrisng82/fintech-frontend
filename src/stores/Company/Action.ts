import { createActions } from "reduxsauce"
import {
  BaseCompany,
  Company,
  CompanyListFilters,
  ETypesName,
  FetchCompanyList,
  FetchCompanySelect,
  SetCompanyListFilters,
  SetCompanyList,
  SetCompanyListLoading,
  SetCompanySelect,
  SetCompanySelectLoading,
  SetHasMoreCompany,
  SetShowCompany,
  SetShowCompanyLoading,
  ShowCompany,
  UpdateCompany,
  UpdateWhatsappConfig,
  UpdateSMTPEmailConfig,
  UpdateEficoreSyncSetting,
  UpdateMiddlewareSetting,
  FetchAllCompany,
  TermCondition,
  SetTermCondition,
  FetchTermCondition,
  SetTermConditionLoading,
  CreateTermCondition,
  UpdateTermCondition,
  SetTermConditionSaving,
  FetchTermConditions,
  SetTermConditions,
  TermConditions,
  TermConditionSorts,
  SetTermConditionStatus,
  FetchTermConditionPublic
} from "./Types"
import { CompanyCommunicationSetting } from "../CompanyCommunicationSetting/Types"
import { fetchAllCompany } from "../../sagas/CompanySaga"

const { Types, Creators: CompanyAction } = createActions<
  {
    [ETypesName.SET_COMPANY_SELECT_LOADING]: string
    [ETypesName.SET_COMPANY_SELECT]: string
    [ETypesName.SET_COMPANY_LIST_LOADING]: string
    [ETypesName.SET_COMPANY_LIST]: string
    [ETypesName.SET_HAS_MORE_COMPANY]: string
    [ETypesName.SET_SHOW_COMPANY_LOADING]: string
    [ETypesName.SET_SHOW_COMPANY]: string
    [ETypesName.SET_COMPANY_LIST_FILTERS]: string

    [ETypesName.FETCH_COMPANY_SELECT]: string
    [ETypesName.FETCH_COMPANY_LIST]: string
    [ETypesName.CREATE_COMPANY]: string
    [ETypesName.SHOW_COMPANY]: string
    [ETypesName.UPDATE_COMPANY]: string
    [ETypesName.UPDATE_WHATSAPP_CONFIG]: string
    [ETypesName.UPDATE_SMTP_EMAIL_CONFIG]: string
    [ETypesName.UPDATE_EFICORE_SYNC_SETTING]: string
    [ETypesName.UPDATE_MIDDLEWARE_SETTING]: string
    [ETypesName.FETCH_ALL_COMPANY]: string

    [ETypesName.FETCH_TERM_CONDITION]: string
    [ETypesName.SET_TERM_CONDITION]: string
    [ETypesName.SET_TERM_CONDITION_LOADING]: string
    [ETypesName.CREATE_TERM_CONDITION]: string
    [ETypesName.UPDATE_TERM_CONDITION]: string
    [ETypesName.SET_TERM_CONDITION_SAVING]: string

    [ETypesName.FETCH_TERM_CONDITIONS]: string
    [ETypesName.SET_TERM_CONDITIONS]: string
    [ETypesName.SET_TERM_CONDITION_STATUS]: string
    [ETypesName.FETCH_TERM_CONDITION_PUBLIC]: string
  },
  {
    setCompanySelectLoading: (isLoading: boolean) => SetCompanySelectLoading
    setCompanySelect: (data: any[]) => SetCompanySelect
    setCompanyListLoading: (isLoading: boolean) => SetCompanyListLoading
    setCompanyList: (data: any[]) => SetCompanyList
    setHasMoreCompany: (hasMore: boolean, page: number) => SetHasMoreCompany
    setShowCompanyLoading: (isLoading: boolean) => SetShowCompanyLoading
    setShowCompany: (data: Company) => SetShowCompany
    setCompanyListFilters: (filters: CompanyListFilters) => SetCompanyListFilters

    fetchCompanySelect: () => FetchCompanySelect
    fetchCompanyList: (filters?: CompanyListFilters) => FetchCompanyList
    createCompany: (setSubmitting: Function, data: BaseCompany, onSuccess: Function) => any
    showCompany: (code: string) => ShowCompany
    updateCompany: (
      setSubmitting: Function,
      id: number,
      data: Partial<Company>,
      onSuccess: (newData: Company) => void
    ) => UpdateCompany
    updateWhatsappConfig: (
      setSubmitting: (isSubmitting: boolean) => void,
      data: Partial<CompanyCommunicationSetting>,
      onSuccess: () => void
    ) => UpdateWhatsappConfig
    updateSMTPEmailConfig: (
      setSubmitting: (isSubmitting: boolean) => void,
      data: Partial<CompanyCommunicationSetting>,
      onSuccess: () => void
    ) => UpdateSMTPEmailConfig
    updateEficoreSyncSetting: (
      setSubmitting: (isSubmitting: boolean) => void,
      data: Partial<CompanyCommunicationSetting>,
      onSuccess: () => void
    ) => UpdateEficoreSyncSetting
    updateMiddlewareSetting: (
      setSubmitting: (isSubmitting: boolean) => void,
      data: Partial<CompanyCommunicationSetting>,
      onSuccess: () => void
    ) => UpdateMiddlewareSetting

    fetchAllCompany: () => FetchAllCompany

    fetchTermCondition: (company_id: number) => FetchTermCondition
    setTermCondition: (termCondition: TermCondition) => SetTermCondition
    setTermConditionLoading: (termConditionLoading: boolean) => SetTermConditionLoading
    createTermCondition: (company_id: number) => CreateTermCondition
    updateTermCondition: (id: number, contents: string) => UpdateTermCondition
    setTermConditionSaving: (termConditionSaving: boolean) => SetTermConditionSaving

    fetchTermConditions: (company_id: number, page: number) => FetchTermConditions
    setTermConditions: (termConditions: TermConditions) => SetTermConditions
    setTermConditionStatus: (id: number) => SetTermConditionStatus

    fetchTermConditionPublic: (company_id: number) => FetchTermConditionPublic
  }
>({
  //Reducers
  setCompanySelectLoading: ["isLoading"],
  setCompanySelect: ["data"],
  setCompanyListLoading: ["isLoading"],
  setCompanyList: ["data"],
  setHasMoreCompany: ["hasMore", "page"],
  setShowCompanyLoading: ["isLoading"],
  setShowCompany: ["data"],
  setCompanyListFilters: ["filters"],
  setTermCondition: ["termCondition"],
  setTermConditionLoading: ["termConditionLoading"],
  setTermConditionSaving: ["termConditionSaving"],
  setTermConditions: ["termConditions"],

  // Sagas
  fetchCompanySelect: [],
  fetchCompanyList: ["filters"],
  createCompany: ["setSubmitting", "data", "onSuccess"],
  showCompany: ["code"],
  updateCompany: ["setSubmitting", "id", "data", "onSuccess"],
  updateWhatsappConfig: ["setSubmitting", "data", "onSuccess"],
  updateSMTPEmailConfig: ["setSubmitting", "data", "onSuccess"],
  updateEficoreSyncSetting: ["setSubmitting", "data", "onSuccess"],
  updateMiddlewareSetting: ["setSubmitting", "data", "onSuccess"],
  fetchAllCompany: [],
  fetchTermCondition: ["company_id"],
  createTermCondition: ["company_id"],
  updateTermCondition: ["id", "contents"],
  fetchTermConditions: ["company_id", "page"],
  setTermConditionStatus: ["id"],
  fetchTermConditionPublic: ["company_id"]
})

export const CompanyTypes = Types
export default CompanyAction
