// eslint-disable-next-line import/no-anonymous-default-export
export default {
  companySelect: {
    data: [],
    isLoading: false
  },
  companyList: {
    isLoading: false,
    data: [],
    page: 1,
    hasMore: false,
    filters: {
      code: "",
      name: ""
    }
  },
  showCompany: {
    isLoading: false,
    data: null
  },
  termCondition: {
    id: 0,
    code: "",
    revision_code: "",
    company_id: 0,
    contents: "",
    status: ""
  },
  termConditions: {
    total: 0,
    page: 1,
    data: [
      {
        id: 0,
        code: "",
        revision_code: "",
        company_id: 0,
        contents: "",
        status: "",
        actions: ""
      }
    ],
    sorts: [
      {
        field: "revision_code",
        value: "desc"
      }
    ]
  },
  termConditionLoading: false,
  termConditionSaving: false
}
