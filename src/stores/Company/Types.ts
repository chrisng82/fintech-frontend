import { Action } from "@reduxjs/toolkit"
import { CompanyCommunicationSetting } from "../CompanyCommunicationSetting/Types"

export interface Company extends BaseCompany {
  created_at: string | Date
  communication_settings: CompanyCommunicationSetting
  id: number
  updated_at: string | Date
}

export interface BaseCompany {
  bill_attention: string
  bill_building_name: string
  bill_country_name: string
  bill_district_01: string
  bill_district_02: string
  bill_email_01: string
  bill_email_02: string
  bill_fax_01: string
  bill_fax_02: string
  bill_phone_01: string
  bill_phone_02: string
  bill_postcode: string
  bill_state_name: string
  bill_street_name: string
  bill_unit_no: string
  co_reg_no: string
  code: string
  ref_code: string
  name_01: string
  name_02: string
  ship_attention: string
  ship_building_name: string
  ship_country_name: string
  ship_district_01: string
  ship_district_02: string
  ship_email_01: string
  ship_email_02: string
  ship_fax_01: string
  ship_fax_02: string
  ship_phone_01: string
  ship_phone_02: string
  ship_postcode: string
  ship_state_name: string
  ship_street_name: string
  ship_unit_no: string
  status: number
  tax_reg_no: string
}

export interface CompanyOption {
  value: number
  label: string
}

export interface CompanyListFilters {
  code: string
  name: string
}

export interface TermConditionSorts {
  field: string
  value: string | number | null
}

export interface TermConditions {
  total: number
  page: number
  data: TermCondition[]
  sorts: TermConditionSorts[]
}

export interface TermCondition {
  id: number
  code: string
  revision_code: string
  company_id: number
  contents: string
  status: string
}

export interface CompanyState {
  companySelect: {
    data: CompanyOption[]
    isLoading: boolean
  }
  companyList: {
    isLoading: boolean
    data: Company[]
    page: number
    hasMore: boolean
    filters: CompanyListFilters
  }
  showCompany: {
    isLoading: boolean
    data: Company | null
  }
  termCondition?: TermCondition
  termConditions?: TermConditions
  termConditionLoading?: boolean
  termConditionSaving?: boolean
}

export enum ETypesName {
  SET_COMPANY_SELECT_LOADING = "SET_COMPANY_SELECT_LOADING",
  SET_COMPANY_SELECT = "SET_COMPANY_SELECT",
  SET_COMPANY_LIST_LOADING = "SET_COMPANY_LIST_LOADING",
  SET_COMPANY_LIST = "SET_COMPANY_LIST",
  SET_HAS_MORE_COMPANY = "SET_HAS_MORE_COMPANY",
  SET_SHOW_COMPANY_LOADING = "SET_SHOW_COMPANY_LOADING",
  SET_SHOW_COMPANY = "SET_SHOW_COMPANY",
  SET_COMPANY_LIST_FILTERS = "SET_COMPANY_LIST_FILTERS",

  FETCH_COMPANY_SELECT = "FETCH_COMPANY_SELECT",
  FETCH_COMPANY_LIST = "FETCH_COMPANY_LIST",
  CREATE_COMPANY = "CREATE_COMPANY",
  SHOW_COMPANY = "SHOW_COMPANY",
  UPDATE_COMPANY = "UPDATE_COMPANY",
  UPDATE_WHATSAPP_CONFIG = "UPDATE_WHATSAPP_CONFIG",
  UPDATE_SMTP_EMAIL_CONFIG = "UPDATE_SMTP_EMAIL_CONFIG",
  UPDATE_EFICORE_SYNC_SETTING = "UPDATE_EFICORE_SYNC_SETTING",
  UPDATE_MIDDLEWARE_SETTING = "UPDATE_MIDDLEWARE_SETTING",
  FETCH_ALL_COMPANY = "FETCH_ALL_COMPANY",

  FETCH_TERM_CONDITION = "FETCH_TERM_CONDITION",
  SET_TERM_CONDITION = "SET_TERM_CONDITION",
  SET_TERM_CONDITION_LOADING = "SET_TERM_CONDITION_LOADING",
  CREATE_TERM_CONDITION = "CREATE_TERM_CONDITION",
  UPDATE_TERM_CONDITION = "UPDATE_TERM_CONDITION",
  SET_TERM_CONDITION_SAVING = "SET_TERM_CONDITION_SAVING",

  FETCH_TERM_CONDITIONS = "FETCH_TERM_CONDITIONS",
  SET_TERM_CONDITIONS = "SET_TERM_CONDITIONS",
  SET_TERM_CONDITION_STATUS = "SET_TERM_CONDITION_STATUS",

  FETCH_TERM_CONDITION_PUBLIC = "FETCH_TERM_CONDITION_PUBLIC"
}

export type SetCompanySelectLoading = Action<ETypesName.SET_COMPANY_SELECT_LOADING> & {
  isLoading: boolean
}

export type SetCompanySelect = Action<ETypesName.SET_COMPANY_SELECT> & {
  data: CompanyOption[]
}

export type FetchCompanySelect = Action<ETypesName.FETCH_COMPANY_SELECT>

export type FetchCompanyList = Action<ETypesName.FETCH_COMPANY_LIST> & {
  filters?: CompanyListFilters
}

export type SetCompanyListLoading = Action<ETypesName.FETCH_COMPANY_LIST> & {
  isLoading: boolean
}

export type SetCompanyList = Action<ETypesName.FETCH_COMPANY_LIST> & {
  data: Company[]
}

export type SetHasMoreCompany = Action<ETypesName.FETCH_COMPANY_LIST> & {
  hasMore: boolean
  page: number
}

export type CreateCompany = Action<ETypesName.CREATE_COMPANY> & {
  setSubmitting: Function
  data: BaseCompany
  onSuccess: Function
}

export type ShowCompany = Action<ETypesName.SHOW_COMPANY> & {
  code: string
}

export type SetShowCompanyLoading = Action<ETypesName.SET_SHOW_COMPANY_LOADING> & {
  isLoading: boolean
}

export type SetShowCompany = Action<ETypesName.SET_SHOW_COMPANY> & {
  data: Company
}

export type UpdateCompany = Action<ETypesName.UPDATE_COMPANY> & {
  setSubmitting: Function
  id: number
  data: Partial<Company>
  onSuccess: (newData: Company) => void
}

export type SetCompanyListFilters = Action<ETypesName.SET_COMPANY_LIST_FILTERS> & {
  filters: CompanyListFilters
}

export type UpdateWhatsappConfig = Action<ETypesName.UPDATE_WHATSAPP_CONFIG> & {
  setSubmitting: (isSubmitting: boolean) => void
  data: Partial<CompanyCommunicationSetting>
  onSuccess: () => void
}

export type UpdateSMTPEmailConfig = Action<ETypesName.UPDATE_SMTP_EMAIL_CONFIG> & {
  setSubmitting: (isSubmitting: boolean) => void
  data: Partial<CompanyCommunicationSetting>
  onSuccess: () => void
}

export type UpdateEficoreSyncSetting = Action<ETypesName.UPDATE_EFICORE_SYNC_SETTING> & {
  setSubmitting: (isSubmitting: boolean) => void
  data: Partial<CompanyCommunicationSetting>
  onSuccess: () => void
}

export type UpdateMiddlewareSetting = Action<ETypesName.UPDATE_MIDDLEWARE_SETTING> & {
  setSubmitting: (isSubmitting: boolean) => void
  data: Partial<CompanyCommunicationSetting>
  onSuccess: () => void
}

export type FetchTermCondition = Action<ETypesName.FETCH_TERM_CONDITION> & {
  company_id: number
}

export type SetTermCondition = Action<ETypesName.SET_TERM_CONDITION> & {
  termCondition: TermCondition
}

export type SetTermConditionLoading = Action<ETypesName.SET_TERM_CONDITION_LOADING> & {
  termConditionLoading: boolean
}

export type CreateTermCondition = Action<ETypesName.CREATE_TERM_CONDITION> & {
  company_id: number
}

export type UpdateTermCondition = Action<ETypesName.UPDATE_TERM_CONDITION> & {
  id: number
  contents: string
}

export type SetTermConditionSaving = Action<ETypesName.SET_TERM_CONDITION_SAVING> & {
  termConditionSaving: boolean
}

export type FetchTermConditions = Action<ETypesName.FETCH_TERM_CONDITIONS> & {
  company_id: number
  page: number
}

export type SetTermConditions = Action<ETypesName.SET_TERM_CONDITIONS> & {
  termConditions: TermConditions
}

export type SetTermConditionStatus = Action<ETypesName.SET_TERM_CONDITION_STATUS> & {
  id: number
}

export type FetchTermConditionPublic = Action<ETypesName.FETCH_TERM_CONDITION_PUBLIC> & {
  company_id: number
}

export type FetchAllCompany = Action<ETypesName.FETCH_ALL_COMPANY>
