import { createReducer } from "reduxsauce"
import { CompanyTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  CompanyState,
  SetCompanyList,
  SetCompanyListFilters,
  SetCompanyListLoading,
  SetCompanySelect,
  SetCompanySelectLoading,
  SetHasMoreCompany,
  SetShowCompany,
  SetShowCompanyLoading,
  SetTermCondition,
  SetTermConditionLoading,
  SetTermConditionSaving,
  SetTermConditions
} from "./Types"

export const setCompanySelectLoading = (
  state: CompanyState,
  { isLoading }: SetCompanySelectLoading
) => ({
  ...state,
  companySelect: {
    ...state.companySelect,
    isLoading
  }
})

export const setCompanySelect = (state: CompanyState, { data }: SetCompanySelect) => ({
  ...state,
  companySelect: {
    ...state.companySelect,
    data
  }
})

export const setCompanyListLoading = (
  state: CompanyState,
  { isLoading }: SetCompanyListLoading
) => ({
  ...state,
  companyList: {
    ...state.companyList,
    isLoading
  }
})

export const setCompanyList = (state: CompanyState, { data }: SetCompanyList) => ({
  ...state,
  companyList: {
    ...state.companyList,
    data
  }
})

export const setHasMoreCompany = (state: CompanyState, { hasMore, page }: SetHasMoreCompany) => ({
  ...state,
  companyList: {
    ...state.companyList,
    hasMore,
    page
  }
})

export const setShowCompanyLoading = (
  state: CompanyState,
  { isLoading }: SetShowCompanyLoading
) => ({
  ...state,
  showCompany: {
    ...state.showCompany,
    isLoading
  }
})

export const setShowCompany = (state: CompanyState, { data }: SetShowCompany) => ({
  ...state,
  showCompany: {
    ...state.showCompany,
    data
  }
})

export const setCompanyListFilters = (state: CompanyState, { filters }: SetCompanyListFilters) => ({
  ...state,
  companyList: {
    ...state.companyList,
    filters
  }
})

export const setTermCondition = (state: CompanyState, { termCondition }: SetTermCondition) => ({
  ...state,
  termCondition
})

export const setTermConditionLoading = (
  state: CompanyState,
  { termConditionLoading }: SetTermConditionLoading
) => ({
  ...state,
  termConditionLoading
})

export const setTermConditionSaving = (
  state: CompanyState,
  { termConditionSaving }: SetTermConditionSaving
) => ({
  ...state,
  termConditionSaving
})

export const setTermConditions = (state: CompanyState, { termConditions }: SetTermConditions) => ({
  ...state,
  termConditions
})

type Actions =
  | SetCompanySelectLoading
  | SetCompanySelect
  | SetCompanyListLoading
  | SetCompanyList
  | SetHasMoreCompany
  | SetShowCompanyLoading
  | SetShowCompany
  | SetCompanyListFilters
  | SetTermCondition
  | SetTermConditionLoading
  | SetTermConditionSaving
  | SetTermConditions

export const CompanyReducer = createReducer<CompanyState, Actions>(INITIAL_STATE, {
  [CompanyTypes.SET_COMPANY_SELECT_LOADING]: setCompanySelectLoading,
  [CompanyTypes.SET_COMPANY_SELECT]: setCompanySelect,
  [CompanyTypes.SET_COMPANY_LIST_LOADING]: setCompanyListLoading,
  [CompanyTypes.SET_COMPANY_LIST]: setCompanyList,
  [CompanyTypes.SET_HAS_MORE_COMPANY]: setHasMoreCompany,
  [CompanyTypes.SET_SHOW_COMPANY_LOADING]: setShowCompanyLoading,
  [CompanyTypes.SET_SHOW_COMPANY]: setShowCompany,
  [CompanyTypes.SET_COMPANY_LIST_FILTERS]: setCompanyListFilters,
  [CompanyTypes.SET_TERM_CONDITION]: setTermCondition,
  [CompanyTypes.SET_TERM_CONDITION_LOADING]: setTermConditionLoading,
  [CompanyTypes.SET_TERM_CONDITION_SAVING]: setTermConditionSaving,
  [CompanyTypes.SET_TERM_CONDITIONS]: setTermConditions
})
