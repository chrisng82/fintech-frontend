import { configureStore, Reducer } from "@reduxjs/toolkit"
import { persistReducer, persistStore } from "redux-persist"
import createSagaMiddleware from "redux-saga"

import rootSaga from "../sagas"
import { RootState } from "."

import createWebStorage from "redux-persist/lib/storage/createWebStorage"

const createNoopStorage = () => {
  return {
    getItem(_key: string) {
      return Promise.resolve(null)
    },
    setItem(_key: string, value: string) {
      return Promise.resolve(value)
    },
    removeItem(_key: string) {
      return Promise.resolve()
    }
  }
}

const storage = typeof window === "undefined" ? createNoopStorage() : createWebStorage("local")

const persistConfig = {
  key: "ARXcelerate",
  storage: storage,
  whitelist: ["app"]
}

const configureStoreAndPersist = (rootReducer: Reducer<RootState>) => {
  // Connect the sagas to the redux store
  const sagaMiddleware = createSagaMiddleware()

  if (process.env.NODE_ENV === `development`) {
    // middleware.push(logger)
  }

  // Redux persist
  const persistedReducer = persistReducer(persistConfig, rootReducer)

  const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: ["persist/PERSIST"],
          ignoredActionPaths: ["meta.arg", "payload.timestamp"],
          ignoredPaths: ["items.dates"]
        }
      }).concat(sagaMiddleware)
  })
  const persistor = persistStore(store)

  // Kick off the root saga
  sagaMiddleware.run(rootSaga)

  return { store, persistor }
}

export default configureStoreAndPersist
