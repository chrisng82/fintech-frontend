// eslint-disable-next-line import/no-anonymous-default-export
export default {
  selfSignupUser: {
    list: {
      data: [],
      total: 0,
      page_size: 20,
      last_page: 1,
      current_page: 1,
      is_loading: false,
      filters: {
        name: "",
        companyName: "",
        status: "",
        debtor_codes: []
      }
    },
    item: {
      data: null,
      is_loading: false
    }
  },
  kernelArtisanCommand: false
}
