import { DebtorCodeOption } from "@/components/page/Form/SignupForm"
import { Company } from "../Company/Types"
import { Debtor } from "../Debtor/Types"
import { Action } from "@reduxjs/toolkit"
import { CompanyTnc } from "../App/Types"

export interface User extends BaseUser {
  id: number
  debtors: Debtor[] | null
  created_at: string | Date
  updated_at: string | Date
}

export interface UserResetPassword {
  id: number
  user_id: number
  email: string
  token: string
  expired_at: string | Date
  created_at: string | Date
  updated_at: string | Date
}

export interface BaseUser {
  debtor_id: number
  email: string
  phone: string | null
  first_name: string
  last_name: string
  status: number
  user_type: number
  last_login: string | Date
  email_verified_at: string | Date | null
  phone_verified_at: string | Date | null
  password_changed_at: string | Date | null
  companyId: number
  companyCode: string
  type_str: string
  contacts: any[]
  token: string
  selectedDebtorId: number
}

export interface SelfSignupUser {
  id: number
  customer_type: string
  companies: SelfSignupUserCompany[]
  company_name: string
  company_reg_no: string
  company_addr: string
  first_name: string
  last_name: string
  email: string
  phone: string
  password: string
  confirm_password: string
  debtor_code: string
  status: number
  documents: SelfSignupUserDocument[]
  approval_1: User | null
  approval_1_at: string | null | Date
  approval_2: User | null
  approval_2_at: string | null | Date
  rejected_by: User | null
  rejected_at: string | null | Date
  returned_by: User | null
  returned_at: string | null | Date
  returned_remark: string | null
  tnc_confirmation: TncConfirmation | null
  user_id?: number
  user_status?: number
  user_tncs?: CompanyTnc
}

interface ExtendedDebtor extends Debtor {
  user_debtor_code: string
}

export interface SelfSignupUserCompany {
  id: number
  name_01: string
  code: string
  self_singup_id: number
  company_id: number
  user_debtor_code: string[]
  debtor_id: number
  debtor?: Debtor
  company: Company
  debtors: ExtendedDebtor[]
}

export interface SelfSignupUserDocument {
  id: number
  file_name: string
  type: string
  company_id: number | null
}

export interface TncConfirmation {
  company_name: string
  name: string
  ic_number: string
  designation: string
}

export interface SelfSignupUserFilters {
  name: string
  companyName: string
  status: string
  debtor_codes: string[]
  user_email?: string
  user_status?: string | number
}

export interface UserState {
  selfSignupUser: {
    list: {
      data: SelfSignupUser[]
      total: number
      page_size: number
      last_page: number
      current_page: number
      is_loading: boolean
      filters: SelfSignupUserFilters
    }
    item: {
      data: SelfSignupUser | null
      is_loading: boolean
    }
  }
  kernelArtisanCommand: boolean
}

export interface CompanyDebtorCodes {
  companyId: number
  debtorCodes: DebtorCodeOption[]
}

const UserType = {
  NULL: "NULL",
  SUPER_ADMIN: "SUPER_ADMIN",
  EFICHAIN_ADMIN: "EFICHAIN_ADMIN",
  EFICHAIN_PRODUCT: "EFICHAIN_PRODUCT",
  INTEGRATION: "INTEGRATION",
  COMPANY_ADMIN: "COMPANY_ADMIN",
  COMPANY_STAFF: "COMPANY_STAFF",
  DEBTOR: "DEBTOR"
}

export const UserTypeValues = {
  [UserType.NULL]: 0,
  0: UserType.NULL,

  [UserType.SUPER_ADMIN]: 1,
  1: UserType.SUPER_ADMIN,

  [UserType.EFICHAIN_ADMIN]: 2,
  2: UserType.EFICHAIN_ADMIN,

  [UserType.EFICHAIN_PRODUCT]: 3,
  3: UserType.EFICHAIN_PRODUCT,

  [UserType.INTEGRATION]: 4,
  4: UserType.INTEGRATION,

  [UserType.COMPANY_ADMIN]: 5,
  5: UserType.COMPANY_ADMIN,

  [UserType.COMPANY_STAFF]: 6,
  6: UserType.COMPANY_STAFF,

  [UserType.DEBTOR]: 7,
  7: UserType.DEBTOR
}

export enum ETypesName {
  SET_SELF_SIGNUP_USERS = "SET_SELF_SIGNUP_USERS",
  SET_SELF_SIGNUP_USERS_LOADING = "SET_SELF_SIGNUP_USERS_LOADING",
  SET_SELF_SIGNUP_USERS_FILTERS = "SET_SELF_SIGNUP_USERS_FILTERS",
  SET_SELF_SIGNUP_USERS_PAGINATION = "SET_SELF_SIGNUP_USERS_PAGINATION",
  SET_SELF_SIGNUP_USER = "SET_SELF_SIGNUP_USER",
  SET_SELF_SIGNUP_USER_LOADING = "SET_SELF_SIGNUP_USER_LOADING",

  FETCH_SELF_SIGNUP_USERS = "FETCH_SELF_SIGNUP_USERS",
  FETCH_SELF_SIGNUP_USER = "FETCH_SELF_SIGNUP_USER",
  DOWNLOAD_DOCUMENT_BY_ID = "DOWNLOAD_DOCUMENT_BY_ID",
  APPROVE_SELF_SIGNUP_BY_ID = "APPROVE_SELF_SIGNUP_BY_ID",
  ADMIN_APPROVE_SELF_SIGNUP_BY_ID = "ADMIN_APPROVE_SELF_SIGNUP_BY_ID",
  REJECT_SELF_SIGNUP_BY_ID = "REJECT_SELF_SIGNUP_BY_ID",
  RETURN_SELF_SIGNUP_BY_ID = "RETURN_SELF_SIGNUP_BY_ID",
  UPDATE_APPROVED_SELF_SIGNUP_BY_ID = "UPDATE_APPROVED_SELF_SIGNUP_BY_ID",

  SET_USER_STATUS = "SET_USER_STATUS",

  GET_ARTISAN_KERNEL_COMMAND = "GET_ARTISAN_KERNEL_COMMAND",
  SET_ARTISAN_KERNEL_COMMAND = "SET_ARTISAN_KERNEL_COMMAND",
  UPDATE_ARTISAN_KERNEL_COMMAND = "UPDATE_ARTISAN_KERNEL_COMMAND"
}

export type SetSelfSignupUsers = Action<ETypesName.SET_SELF_SIGNUP_USERS> & {
  data: SelfSignupUser[]
}

export type SetSelfSignupUsersFilters = Action<ETypesName.SET_SELF_SIGNUP_USERS_FILTERS> & {
  filters: SelfSignupUserFilters
}

export type SetSelfSignupUsersPagination = Action<ETypesName.SET_SELF_SIGNUP_USERS_PAGINATION> & {
  page_size: number
  last_page: number
  current_page: number
  total: number
}

export type SetSelfSignupUsersLoading = Action<ETypesName.SET_SELF_SIGNUP_USERS_LOADING> & {
  is_loading: boolean
}

export type FetchSelfSignupUsers = Action<ETypesName.FETCH_SELF_SIGNUP_USERS> & {
  fetchType: string
  page?: number
  page_size?: number
  filters?: SelfSignupUserFilters
}

export type SetSelfSignupUserLoading = Action<ETypesName.SET_SELF_SIGNUP_USER_LOADING> & {
  is_loading: boolean
}

export type FetchSelfSignupUser = Action<ETypesName.FETCH_SELF_SIGNUP_USER> & {
  id: number
}

export type SetSelfSignupUser = Action<ETypesName.SET_SELF_SIGNUP_USER> & {
  data: SelfSignupUser
}

export type DownloadDocumentById = Action<ETypesName.DOWNLOAD_DOCUMENT_BY_ID> & {
  id: number
  fileName: string
}

export type ApproveSelfSignupById = Action<ETypesName.APPROVE_SELF_SIGNUP_BY_ID> & {
  id: number
  data: any[]
  onFail: Function
  onSuccess: Function
}

export type AdminApproveSelfSignupById = Action<ETypesName.ADMIN_APPROVE_SELF_SIGNUP_BY_ID> & {
  id: number
}

export type RejectSelfSignupById = Action<ETypesName.REJECT_SELF_SIGNUP_BY_ID> & {
  id: number
}

export type ReturnSelfSignupById = Action<ETypesName.RETURN_SELF_SIGNUP_BY_ID> & {
  id: number
  values: Pick<SelfSignupUser, "returned_remark">
}

export type UpdateApprovedSelfSignupById = Action<ETypesName.UPDATE_APPROVED_SELF_SIGNUP_BY_ID> & {
  id: number
  debtorCodes: CompanyDebtorCodes[]
}

export type SetUserStatus = Action<ETypesName.SET_USER_STATUS> & {
  data: SelfSignupUser
}

export type GetArtisanKernelCommand = Action<ETypesName.GET_ARTISAN_KERNEL_COMMAND> & {}
export type SetArtisanKernelCommand = Action<ETypesName.SET_ARTISAN_KERNEL_COMMAND> & {
  kernelArtisanCommand: boolean
}
export type UpdateArtisanKernelCommand = Action<ETypesName.UPDATE_ARTISAN_KERNEL_COMMAND> & {
  kernelArtisanCommandCheck: boolean
}

export default UserType
