import { createActions } from "reduxsauce"
import {
  AdminApproveSelfSignupById,
  ApproveSelfSignupById,
  CompanyDebtorCodes,
  RejectSelfSignupById,
  ReturnSelfSignupById,
  UpdateApprovedSelfSignupById
} from "./Types"
import {
  ETypesName,
  FetchSelfSignupUsers,
  SetSelfSignupUsers,
  SetSelfSignupUsersLoading,
  SelfSignupUserFilters,
  SetSelfSignupUsersFilters,
  SetSelfSignupUsersPagination,
  SelfSignupUser,
  SetSelfSignupUserLoading,
  SetSelfSignupUser,
  FetchSelfSignupUser,
  DownloadDocumentById,
  SetUserStatus,
  GetArtisanKernelCommand,
  SetArtisanKernelCommand,
  UpdateArtisanKernelCommand
} from "./Types"

const { Types, Creators: UserAction } = createActions<
  {
    [ETypesName.SET_SELF_SIGNUP_USERS]: string
    [ETypesName.SET_SELF_SIGNUP_USERS_LOADING]: string
    [ETypesName.SET_SELF_SIGNUP_USERS_FILTERS]: string
    [ETypesName.SET_SELF_SIGNUP_USERS_PAGINATION]: string
    [ETypesName.SET_SELF_SIGNUP_USER_LOADING]: string
    [ETypesName.SET_SELF_SIGNUP_USER]: string

    [ETypesName.FETCH_SELF_SIGNUP_USERS]: string
    [ETypesName.FETCH_SELF_SIGNUP_USER]: string
    [ETypesName.DOWNLOAD_DOCUMENT_BY_ID]: string
    [ETypesName.APPROVE_SELF_SIGNUP_BY_ID]: string
    [ETypesName.ADMIN_APPROVE_SELF_SIGNUP_BY_ID]: string
    [ETypesName.REJECT_SELF_SIGNUP_BY_ID]: string
    [ETypesName.RETURN_SELF_SIGNUP_BY_ID]: string
    [ETypesName.UPDATE_APPROVED_SELF_SIGNUP_BY_ID]: string
    [ETypesName.SET_USER_STATUS]: string
    [ETypesName.GET_ARTISAN_KERNEL_COMMAND]: string
    [ETypesName.SET_ARTISAN_KERNEL_COMMAND]: string
    [ETypesName.UPDATE_ARTISAN_KERNEL_COMMAND]: string
  },
  {
    setSelfSignupUsers: (data: SelfSignupUser[]) => SetSelfSignupUsers
    setSelfSignupUsersLoading: (is_loading: boolean) => SetSelfSignupUsersLoading
    setSelfSignupUsersFilters: (filters: SelfSignupUserFilters) => SetSelfSignupUsersFilters
    setSelfSignupUsersPagination: (
      page_size: number,
      last_page: number,
      current_page: number,
      total: number
    ) => SetSelfSignupUsersPagination
    setSelfSignupUserLoading: (is_loading: boolean) => SetSelfSignupUserLoading
    setSelfSignupUser: (data: SelfSignupUser) => SetSelfSignupUser

    fetchSelfSignupUsers: (
      fetchType: string,
      page?: number,
      page_size?: number,
      filters?: SelfSignupUserFilters
    ) => FetchSelfSignupUsers
    fetchSelfSignupUser: (id: number) => FetchSelfSignupUser
    downloadDocumentById: (id: number, fileName: string) => DownloadDocumentById
    approveSelfSignupById: (
      id: number,
      data: any,
      onFail: Function,
      onSuccess: Function
    ) => ApproveSelfSignupById
    adminApproveSelfSignupById: (id: number) => AdminApproveSelfSignupById
    rejectSelfSignupById: (id: number) => RejectSelfSignupById
    returnSelfSignupById: (
      id: number,
      values: Pick<SelfSignupUser, "returned_remark">
    ) => ReturnSelfSignupById
    updateApprovedSelfSignupById: (
      id: number,
      debtorCodes: CompanyDebtorCodes[]
    ) => UpdateApprovedSelfSignupById
    setUserStatus: (data: SelfSignupUser) => SetUserStatus
    getArtisanKernelCommand: () => GetArtisanKernelCommand
    setArtisanKernelCommand: (kernelArtisanCommand: boolean) => SetArtisanKernelCommand
    updateArtisanKernelCommand: (kernelArtisanCommandActive: boolean) => UpdateArtisanKernelCommand
  }
>({
  // Reducers
  setSelfSignupUsers: ["data"],
  setSelfSignupUsersLoading: ["is_loading"],
  setSelfSignupUsersFilters: ["filters"],
  setSelfSignupUsersPagination: ["page_size", "last_page", "current_page", "total"],
  setSelfSignupUserLoading: ["is_loading"],
  setSelfSignupUser: ["data"],
  setArtisanKernelCommand: ["kernelArtisanCommand"],

  // Sagas
  fetchSelfSignupUsers: ["fetchType", "page", "page_size", "filters"],
  fetchSelfSignupUser: ["id"],
  downloadDocumentById: ["id", "fileName"],
  approveSelfSignupById: ["id", "data", "onFail", "onSuccess"],
  adminApproveSelfSignupById: ["id"],
  rejectSelfSignupById: ["id"],
  returnSelfSignupById: ["id", "values"],
  updateApprovedSelfSignupById: ["id", "debtorCodes"],
  setUserStatus: ["data"],
  getArtisanKernelCommand: [],
  updateArtisanKernelCommand: ["kernelArtisanCommandActive"]
})

export const UserTypes = Types
export default UserAction
