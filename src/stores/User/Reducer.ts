import { createReducer } from "reduxsauce"
import { UserTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  SetArtisanKernelCommand,
  SetSelfSignupUser,
  SetSelfSignupUserLoading,
  SetSelfSignupUsers,
  SetSelfSignupUsersFilters,
  SetSelfSignupUsersLoading,
  SetSelfSignupUsersPagination,
  UserState
} from "./Types"

const setSelfSignupUsers = (state: UserState, { data }: SetSelfSignupUsers) => ({
  ...state,
  selfSignupUser: {
    ...state.selfSignupUser,
    list: {
      ...state.selfSignupUser.list,
      data
    }
  }
})

const setSelfSignupUsersFilters = (state: UserState, { filters }: SetSelfSignupUsersFilters) => ({
  ...state,
  selfSignupUser: {
    ...state.selfSignupUser,
    list: {
      ...state.selfSignupUser.list,
      filters
    }
  }
})

const setSelfSignupUsersPagination = (
  state: UserState,
  { page_size, last_page, current_page, total }: SetSelfSignupUsersPagination
) => ({
  ...state,
  selfSignupUser: {
    ...state.selfSignupUser,
    list: {
      ...state.selfSignupUser.list,
      page_size,
      last_page,
      current_page,
      total
    }
  }
})

const setSelfSignupUsersLoading = (
  state: UserState,
  { is_loading }: SetSelfSignupUsersLoading
) => ({
  ...state,
  selfSignupUser: {
    ...state.selfSignupUser,
    list: {
      ...state.selfSignupUser.list,
      is_loading
    }
  }
})

const setSelfSignupUserLoading = (state: UserState, { is_loading }: SetSelfSignupUserLoading) => ({
  ...state,
  selfSignupUser: {
    ...state.selfSignupUser,
    item: {
      ...state.selfSignupUser.item,
      is_loading
    }
  }
})

const setSelfSignupUser = (state: UserState, { data }: SetSelfSignupUser) => ({
  ...state,
  selfSignupUser: {
    ...state.selfSignupUser,
    item: {
      ...state.selfSignupUser.item,
      data
    }
  }
})

const setArtisanKernelCommand = (
  state: UserState,
  { kernelArtisanCommand }: SetArtisanKernelCommand
) => ({
  ...state,
  kernelArtisanCommand
})

type Actions =
  | SetSelfSignupUsers
  | SetSelfSignupUsersLoading
  | SetSelfSignupUsersFilters
  | SetSelfSignupUsersPagination
  | SetSelfSignupUserLoading
  | SetSelfSignupUser
  | SetArtisanKernelCommand

export const UserReducer = createReducer<UserState, Actions>(INITIAL_STATE, {
  [UserTypes.SET_SELF_SIGNUP_USERS]: setSelfSignupUsers,
  [UserTypes.SET_SELF_SIGNUP_USERS_FILTERS]: setSelfSignupUsersFilters,
  [UserTypes.SET_SELF_SIGNUP_USERS_PAGINATION]: setSelfSignupUsersPagination,
  [UserTypes.SET_SELF_SIGNUP_USERS_LOADING]: setSelfSignupUsersLoading,
  [UserTypes.SET_SELF_SIGNUP_USER_LOADING]: setSelfSignupUserLoading,
  [UserTypes.SET_SELF_SIGNUP_USER]: setSelfSignupUser,
  [UserTypes.SET_ARTISAN_KERNEL_COMMAND]: setArtisanKernelCommand
})
