import { createActions } from "reduxsauce"
import {
  CreditNote,
  CreditNotesFilters,
  ETypesName,
  FetchCreditNote,
  FetchCreditNotes,
  SetCreditNote,
  SetCreditNoteLoading,
  SetCreditNotes,
  SetCreditNotesFilters,
  SetCreditNotesSorts,
  CreditNotesSorts,
  SetCreditNotesPagination,
  SetCreditNotesLoading,
  FetchCreditNoteDetails,
  SetCreditNoteDetails,
  CreditNoteDetail,
  SetCreditNoteDetailsPagination
} from "./Types"

const { Types, Creators: CreditNoteAction } = createActions<
  {
    [ETypesName.FETCH_CREDIT_NOTES]: string
    [ETypesName.SET_CREDIT_NOTES]: string
    [ETypesName.SET_CREDIT_NOTES_PAGINATION]: string
    [ETypesName.SET_CREDIT_NOTES_FILTERS]: string
    [ETypesName.SET_CREDIT_NOTES_SORTS]: string

    [ETypesName.FETCH_CREDIT_NOTE]: string
    [ETypesName.SET_CREDIT_NOTE]: string
    [ETypesName.FETCH_CREDIT_NOTE_DETAILS]: string
    [ETypesName.SET_CREDIT_NOTE_DETAILS]: string
    [ETypesName.SET_CREDIT_NOTE_DETAILS_PAGINATION]: string

    [ETypesName.SET_CREDIT_NOTE_LOADING]: string
    [ETypesName.SET_CREDIT_NOTES_LOADING]: string
    [ETypesName.SET_CREDIT_NOTE_DETAILS_LOADING]: string
  },
  {
    fetchCreditNotes: (
      page?: number,
      page_size?: number,
      filters?: CreditNotesFilters
    ) => FetchCreditNotes
    setCreditNotes: (data: CreditNote[]) => SetCreditNotes
    setCreditNotesPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetCreditNotesPagination
    setCreditNotesFilters: (filters: CreditNotesFilters) => SetCreditNotesFilters
    setCreditNotesSorts: (sorts: CreditNotesSorts[]) => SetCreditNotesSorts

    fetchCreditNote: (id: number) => FetchCreditNote
    setCreditNote: (data: CreditNote) => SetCreditNote

    fetchCreditNoteDetails: (hdrId: number) => FetchCreditNoteDetails
    setCreditNoteDetails: (data: CreditNoteDetail[]) => SetCreditNoteDetails
    setCreditNoteDetailsPagination: (
      page_size: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetCreditNoteDetailsPagination

    setCreditNoteLoading: (is_loading: boolean) => SetCreditNoteLoading
    setCreditNotesLoading: (is_loading: boolean) => SetCreditNotesLoading
    setCreditNoteDetailsLoading: (is_loading: boolean) => SetCreditNoteLoading
  }
>({
  //saga
  fetchCreditNotes: ["page", "page_size", "filters"],
  fetchCreditNote: ["id"],
  fetchCreditNoteDetails: ["hdrId"],

  //reducer
  setCreditNotes: ["data"],
  setCreditNotesPagination: ["page_size", "current_page", "last_page", "total"],
  setCreditNotesFilters: ["filters"],
  setCreditNotesSorts: ["sorts"],

  setCreditNote: ["data"],
  setCreditNoteDetails: ["data"],
  setCreditNoteDetailsPagination: ["page_size", "current_page", "last_page", "total"],

  setCreditNoteLoading: ["is_loading"],
  setCreditNotesLoading: ["is_loading"],
  setCreditNoteDetailsLoading: ["is_loading"]
})

export const creditNoteTypes = Types
export default CreditNoteAction
