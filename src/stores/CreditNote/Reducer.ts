import { createReducer } from "reduxsauce"
import { creditNoteTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  CreditNoteState,
  SetCreditNoteLoading,
  SetCreditNotes,
  SetCreditNotesPagination,
  SetCreditNotesLoading,
  SetCreditNotesFilters,
  SetCreditNotesSorts,
  SetCreditNote,
  SetCreditNoteDetails,
  SetCreditNoteDetailsPagination,
  SetCreditNoteDetailsLoading
} from "./Types"

export const setCreditNotes = (state: CreditNoteState, { data }: SetCreditNotes) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setCreditNotesPagination = (
  state: CreditNoteState,
  { page_size, current_page, last_page, total }: SetCreditNotesPagination
) => ({
  ...state,
  list: {
    ...state.list,
    page_size,
    current_page,
    last_page,
    total
  }
})

export const setCreditNotesFilters = (
  state: CreditNoteState,
  { filters }: SetCreditNotesFilters
) => ({
  ...state,
  list: {
    ...state.list,
    filters
  }
})

export const setCreditNotesSorts = (state: CreditNoteState, { sorts }: SetCreditNotesSorts) => ({
  ...state,
  list: {
    ...state.list,
    sorts
  }
})

export const setCreditNote = (state: CreditNoteState, { data }: SetCreditNote) => ({
  ...state,
  item: {
    ...state.item,
    data
  }
})

export const setCreditNoteDetails = (state: CreditNoteState, { data }: SetCreditNoteDetails) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      data
    }
  }
})

export const setCreditNoteDetailsPagination = (
  state: CreditNoteState,
  { page_size, current_page, last_page, total }: SetCreditNoteDetailsPagination
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      page_size,
      current_page,
      last_page,
      total
    }
  }
})

export const setCreditNotesLoading = (
  state: CreditNoteState,
  { is_loading }: SetCreditNotesLoading
) => ({
  ...state,
  list: {
    ...state.list,
    is_loading
  }
})

export const setCreditNoteLoading = (
  state: CreditNoteState,
  { is_loading }: SetCreditNoteLoading
) => ({
  ...state,
  item: {
    ...state.item,
    is_loading
  }
})

export const setCreditNoteDetailsLoading = (
  state: CreditNoteState,
  { is_loading }: SetCreditNoteDetailsLoading
) => ({
  ...state,
  item: {
    ...state.item,
    details: {
      ...state.item.details,
      is_loading
    }
  }
})

type Actions =
  | SetCreditNotes
  | SetCreditNotesPagination
  | SetCreditNotesFilters
  | SetCreditNotesSorts
  | SetCreditNote
  | SetCreditNoteDetails
  | SetCreditNoteDetailsPagination
  | SetCreditNotesLoading
  | SetCreditNoteLoading
  | SetCreditNoteDetailsLoading

export const CreditNoteReducer = createReducer<CreditNoteState, Actions>(INITIAL_STATE, {
  [creditNoteTypes.SET_CREDIT_NOTES]: setCreditNotes,
  [creditNoteTypes.SET_CREDIT_NOTES_PAGINATION]: setCreditNotesPagination,
  [creditNoteTypes.SET_CREDIT_NOTES_FILTERS]: setCreditNotesFilters,
  [creditNoteTypes.SET_CREDIT_NOTES_SORTS]: setCreditNotesSorts,

  [creditNoteTypes.SET_CREDIT_NOTE]: setCreditNote,
  [creditNoteTypes.SET_CREDIT_NOTE_DETAILS]: setCreditNoteDetails,
  [creditNoteTypes.SET_CREDIT_NOTE_DETAILS_PAGINATION]: setCreditNoteDetailsPagination,

  [creditNoteTypes.SET_CREDIT_NOTES_LOADING]: setCreditNotesLoading,
  [creditNoteTypes.SET_CREDIT_NOTE_LOADING]: setCreditNoteLoading,
  [creditNoteTypes.SET_CREDIT_NOTE_DETAILS_LOADING]: setCreditNoteDetailsLoading
})
