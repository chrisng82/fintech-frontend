import { BaseDocumentModel, BaseModel } from "@/Types/GeneralTypes"
import { IFilterType } from "@/components/Datatable/Filter"
import { Action } from "@reduxjs/toolkit"

export interface CreditNote extends BaseModel, BaseDocumentModel {
  hdrId: number
  companyCode: string
  debtorCode: string
  orderCreatedAt: string
  orderUpdatedAt: string
  creditTerms: string
}

export interface CreditNoteDetail extends BaseModel {
  [key: string]: any
  line_no: number
  code: string
  desc_01: string
  desc_02: string
  location_code: string
  uom_code: string
  uom_rate: number
  sale_price: number
  price_disc: number
  qty: number
  gross_amt: number
  gross_local_amt: number
  dtl_disc_amt: number
  dtl_disc_local_amt: number
  hdr_disc_amt: number
  hdr_disc_local_amt: number
  tax_code: string
  net_amt: number
  net_local_amt: number
}

export type CreditNotesFilters = {
  [key in IFilterType]: string | boolean | number | string[]
}
export interface CreditNotesSorts {
  field: string
  order: string
}

export interface CreditNoteState {
  list: {
    data: CreditNote[]
    total: number
    page_size: number
    last_page: number
    current_page: number
    is_loading: boolean
    filters: {
      code?: string
      debtorName: string
      debtorCode?: string
      paymentStatus?: string
      pdfOnly?: boolean
      docDate?: string[]
    }
    sorts: CreditNotesSorts[]
  }
  item: {
    is_loading: boolean
    data: CreditNote | null
    details: {
      data: CreditNoteDetail[]
      total: number
      page_size: number
      last_page: number
      current_page: number
      is_loading: boolean
    }
  }
}

export enum ETypesName {
  FETCH_CREDIT_NOTES = "FETCH_CREDIT_NOTES",
  SET_CREDIT_NOTES = "SET_CREDIT_NOTES",
  SET_CREDIT_NOTES_PAGINATION = "SET_CREDIT_NOTES_PAGINATION",
  SET_CREDIT_NOTES_FILTERS = "SET_CREDIT_NOTES_FILTERS",
  SET_CREDIT_NOTES_SORTS = "SET_CREDIT_NOTES_SORTS",

  FETCH_CREDIT_NOTE = "FETCH_CREDIT_NOTE",
  SET_CREDIT_NOTE = "SET_CREDIT_NOTE",
  FETCH_CREDIT_NOTE_DETAILS = "FETCH_CREDIT_NOTE_DETAILS",
  SET_CREDIT_NOTE_DETAILS = "SET_CREDIT_NOTE_DETAILS",
  SET_CREDIT_NOTE_DETAILS_PAGINATION = "SET_CREDIT_NOTE_DETAILS_PAGINATION",

  SET_CREDIT_NOTES_LOADING = "SET_CREDIT_NOTES_LOADING",
  SET_CREDIT_NOTE_LOADING = "SET_CREDIT_NOTE_LOADING",
  SET_CREDIT_NOTE_DETAILS_LOADING = "SET_CREDIT_NOTE_DETAILS_LOADING"
}

export type FetchCreditNotes = Action<ETypesName.FETCH_CREDIT_NOTES> & {
  companyCode: string
  page?: number
  page_size?: number
  filters?: CreditNotesFilters
}

export type SetCreditNotes = Action<ETypesName.SET_CREDIT_NOTES> & {
  data: CreditNote[]
}

export type SetCreditNotesPagination = Action<ETypesName.SET_CREDIT_NOTES_PAGINATION> & {
  page_size: number
  current_page: number
  last_page: number
  total: number
}

export type SetCreditNotesFilters = Action<ETypesName.SET_CREDIT_NOTES_FILTERS> & {
  filters: {
    code: string
    debtorName: string
    debtorCode: string
  }
}

export type SetCreditNotesSorts = Action<ETypesName.SET_CREDIT_NOTES_SORTS> & {
  sorts: CreditNotesSorts[]
}

export type FetchCreditNote = Action<ETypesName.FETCH_CREDIT_NOTE> & {
  id: number
}

export type FetchCreditNoteDetails = Action<ETypesName.FETCH_CREDIT_NOTE_DETAILS> & {
  hdrId: number
  page?: number
  page_size?: number
}

export type SetCreditNote = Action<ETypesName.SET_CREDIT_NOTE> & {
  data: CreditNote
}

export type SetCreditNoteDetails = Action<ETypesName.SET_CREDIT_NOTE_DETAILS> & {
  data: CreditNoteDetail[]
}

export type SetCreditNoteDetailsPagination =
  Action<ETypesName.SET_CREDIT_NOTE_DETAILS_PAGINATION> & {
    page_size: number
    current_page: number
    last_page: number
    total: number
  }

export type SetCreditNoteLoading = Action<ETypesName.SET_CREDIT_NOTE_LOADING> & {
  is_loading: boolean
}

export type SetCreditNotesLoading = Action<ETypesName.SET_CREDIT_NOTES_LOADING> & {
  is_loading: boolean
}

export type SetCreditNoteDetailsLoading = Action<ETypesName.SET_CREDIT_NOTE_DETAILS_LOADING> & {
  is_loading: boolean
}
