export interface CompanyCommunicationSetting {
  company_id: number
  created_at: string
  id: number
  smtp_environment: number
  smtp_from_email: string
  smtp_from_name: string
  smtp_host: string
  smtp_password: string | null
  smtp_port: string
  smtp_secure: string
  smtp_to_email: string
  smtp_username: string | null
  updated_at: string
  whatsapp_environment: number
  whatsapp_phone_number: string
  whatsapp_token: string
  whatsapp_url: string
  eficore_url: string
  eficore_username: string
  eficore_password: string
  middleware_url?: string
}
