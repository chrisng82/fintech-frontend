import { createReducer } from "reduxsauce"
import INITIAL_STATE from "./InitialState"
import {
  DebtorState,
  SetDebtor,
  SetDebtorContacts,
  SetDebtorCodes,
  SetDebtorLoading,
  SetDebtors,
  SetDebtorsFilters,
  SetDebtorsLoading,
  SetDebtorsPagination,
  SetGenerateDocumentLoading,
  SetSuggestedDebtorsLoading,
  SetSuggestedDebtors,
  SetSelectedFilterDebtorId
} from "./Types"
import { DebtorTypes } from "./Action"

export const setDebtorsLoading = (state: DebtorState, { isLoading }: SetDebtorsLoading) => ({
  ...state,
  list: {
    ...state.list,
    isLoading
  }
})

export const setDebtors = (state: DebtorState, { data }: SetDebtors) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setDebtorsPagination = (
  state: DebtorState,
  { per_page, current_page, last_page, total }: SetDebtorsPagination
) => ({
  ...state,
  list: {
    ...state.list,
    per_page,
    current_page,
    last_page,
    total
  }
})

export const setDebtorLoading = (state: DebtorState, { isLoading }: SetDebtorLoading) => ({
  ...state,
  item: {
    ...state.item,
    isLoading
  }
})

export const setDebtor = (state: DebtorState, { data }: SetDebtor) => ({
  ...state,
  item: {
    ...state.item,
    data
  }
})

export const setDebtorsFilters = (state: DebtorState, { filters }: SetDebtorsFilters) => ({
  ...state,
  list: {
    ...state.list,
    filters
  }
})

export const setDebtorContacts = (state: DebtorState, { data }: SetDebtorContacts) => ({
  ...state,
  item: {
    ...state.item,
    data: {
      ...state.item.data!,
      contacts: data
    }
  }
})

export const setDebtorCodes = (state: DebtorState, { data }: SetDebtorCodes) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setGenerateDocumentLoading = (
  state: DebtorState,
  { generateDocumentLoading }: SetGenerateDocumentLoading
) => ({
  ...state,
  generateDocumentLoading
})

export const setSuggestedDebtorsLoading = (
  state: DebtorState,
  { isLoading }: SetSuggestedDebtorsLoading
) => ({
  ...state,
  suggestedDebtors: {
    ...state.suggestedDebtors,
    isLoading
  }
})

export const setSuggestedDebtors = (state: DebtorState, { data }: SetSuggestedDebtors) => ({
  ...state,
  suggestedDebtors: {
    ...state.suggestedDebtors,
    data
  }
})

export const setSelectedFilterDebtorId = (state: DebtorState, { selectedFilterDebtorId }: SetSelectedFilterDebtorId) => ({
    ...state,
    selectedFilterDebtorId
})

type Actions =
  | SetDebtorsLoading
  | SetDebtors
  | SetDebtorsPagination
  | SetDebtorLoading
  | SetDebtor
  | SetDebtorsFilters
  | SetDebtorContacts
  | SetDebtorCodes
  | SetGenerateDocumentLoading
  | SetSuggestedDebtorsLoading
  | SetSuggestedDebtors
  | SetSelectedFilterDebtorId

export const DebtorReducer = createReducer<DebtorState, Actions>(INITIAL_STATE, {
  [DebtorTypes.SET_DEBTORS_LOADING]: setDebtorsLoading,
  [DebtorTypes.SET_DEBTORS]: setDebtors,
  [DebtorTypes.SET_DEBTORS_PAGINATION]: setDebtorsPagination,
  [DebtorTypes.SET_DEBTOR_LOADING]: setDebtorLoading,
  [DebtorTypes.SET_DEBTOR]: setDebtor,
  [DebtorTypes.SET_DEBTORS_FILTERS]: setDebtorsFilters,
  [DebtorTypes.SET_DEBTOR_CONTACTS]: setDebtorContacts,
  [DebtorTypes.SET_DEBTOR_CODES]: setDebtorCodes,
  [DebtorTypes.SET_GENERATE_DOCUMENT_LOADING]: setGenerateDocumentLoading,
  [DebtorTypes.SET_SUGGESTED_DEBTORS_LOADING]: setSuggestedDebtorsLoading,
  [DebtorTypes.SET_SUGGESTED_DEBTORS]: setSuggestedDebtors,
  [DebtorTypes.SET_SELECTED_FILTER_DEBTOR_ID]: setSelectedFilterDebtorId
})
