import { createActions } from "reduxsauce"
import {
  BaseDebtor,
  CreateDebtorContact,
  Debtor,
  DebtorContact,
  DebtorsFilters,
  DeleteDebtorContact,
  ETypesName,
  FetchDebtor,
  FetchDebtorById,
  FetchDebtors,
  FetchDebtorCodes,
  GenerateOverdue,
  GenerateStatement,
  SetDebtor,
  SetDebtorContacts,
  SetDebtorLoading,
  SetDebtors,
  SetDebtorCodes,
  SetDebtorsFilters,
  SetDebtorsLoading,
  SetDebtorsPagination,
  SetGenerateDocumentLoading,
  UpdateDebtor,
  UpdateDebtorContact,
  FetchDebtorByCompanyCode,
  FetchSuggestedDebtorsByCompanyCode,
  SetSuggestedDebtorsLoading,
  SetSelectedFilterDebtorId
} from "./Types"

const { Types, Creators: DebtorAction } = createActions<
  {
    [ETypesName.SET_DEBTORS_LOADING]: string
    [ETypesName.SET_DEBTORS]: string
    [ETypesName.SET_DEBTORS_PAGINATION]: string
    [ETypesName.SET_DEBTOR_LOADING]: string
    [ETypesName.SET_DEBTOR]: string
    [ETypesName.SET_DEBTORS_FILTERS]: string
    [ETypesName.SET_DEBTOR_CONTACTS]: string
    [ETypesName.SET_DEBTOR_CODES]: string
    [ETypesName.SET_GENERATE_DOCUMENT_LOADING]: string
    [ETypesName.SET_SUGGESTED_DEBTORS_LOADING]: string
    [ETypesName.SET_SUGGESTED_DEBTORS]: string
    [ETypesName.SET_SELECTED_FILTER_DEBTOR_ID]: string

    [ETypesName.FETCH_DEBTORS]: string
    [ETypesName.CREATE_DEBTOR]: string
    [ETypesName.FETCH_DEBTOR]: string
    [ETypesName.FETCH_DEBTOR_BY_ID]: string
    [ETypesName.UPDATE_DEBTOR]: string
    [ETypesName.UPDATE_DEBTOR_CONTACT]: string
    [ETypesName.CREATE_DEBTOR_CONTACT]: string
    [ETypesName.DELETE_DEBTOR_CONTACT]: string
    [ETypesName.FETCH_DEBTOR_CODES]: string
    [ETypesName.GENERATE_STATEMENT]: string
    [ETypesName.GENERATE_OVERDUE]: string
    [ETypesName.FETCH_DEBTOR_BY_COMPANY_CODE]: string
    [ETypesName.FETCH_SUGGESTED_DEBTORS_BY_COMPANY_CODE]: string
  },
  {
    setDebtorsLoading: (isLoading: boolean) => SetDebtorsLoading
    setDebtors: (data: any[]) => SetDebtors
    setDebtorsPagination: (
      per_page: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetDebtorsPagination
    setDebtorLoading: (isLoading: boolean) => SetDebtorLoading
    setDebtor: (data: Debtor | null) => SetDebtor
    setDebtorsFilters: (filters: DebtorsFilters) => SetDebtorsFilters
    setDebtorContacts: (data: DebtorContact[]) => SetDebtorContacts
    setDebtorCodes: (data: any[], currentCompanyCode?: string) => SetDebtorCodes
    setGenerateDocumentLoading: (generateDocumentLoading: boolean) => SetGenerateDocumentLoading
    setSuggestedDebtorsLoading: (isLoading: boolean) => SetSuggestedDebtorsLoading
    setSuggestedDebtors: (data: Debtor[]) => SetSuggestedDebtorsLoading
    setSelectedFilterDebtorId: (selectedDebtorId: number |null) => SetSelectedFilterDebtorId

    fetchDebtors: (
      page?: number,
      perPage?: number,
      filters?: DebtorsFilters,
      onSuccess?: Function
    ) => FetchDebtors
    createDebtor: (setSubmitting: Function, data: BaseDebtor, onSuccess: Function) => any
    fetchDebtor: (code: string) => FetchDebtor
    fetchDebtorById: (id: number) => FetchDebtorById
    fetchDebtorCodes: (search?: string, onSuccess?: Function) => FetchDebtorCodes
    updateDebtor: (
      setSubmitting: Function,
      id: number,
      data: Partial<Debtor>,
      onSuccess: (newData: Debtor) => void
    ) => UpdateDebtor
    updateDebtorContact: (
      setSubmitting: Function,
      id: number,
      data: Partial<DebtorContact>,
      onSuccess: () => void
    ) => UpdateDebtorContact
    createDebtorContact: (
      setSubmitting: Function,
      data: Partial<DebtorContact>,
      onSuccess: () => void
    ) => CreateDebtorContact
    deleteDebtorContact: (id: number | undefined, onSuccess: () => void) => DeleteDebtorContact
    generateStatement: (
      debtorId: number,
      companyCode: string,
      isOpenBill: boolean
    ) => GenerateStatement
    generateOverdue: (debtorId: number, companyCode: string) => GenerateOverdue
    fetchDebtorByCompanyCode: (companyCode: string, filters?: any) => FetchDebtorByCompanyCode
    fetchSuggestedDebtorsByCompanyCode: (
      companyCode: string,
      filters: string[]
    ) => FetchSuggestedDebtorsByCompanyCode
  }
>({
  //Reducers
  setDebtorsLoading: ["isLoading"],
  setDebtors: ["data"],
  setDebtorsPagination: ["per_page", "current_page", "last_page", "total"],
  setDebtorLoading: ["isLoading"],
  setDebtor: ["data"],
  setDebtorsFilters: ["filters"],
  setDebtorContacts: ["data"],
  setDebtorCodes: ["data", "currentCompanyCode"],
  setGenerateDocumentLoading: ["generateDocumentLoading"],
  setSuggestedDebtorsLoading: ["isLoading"],
  setSuggestedDebtors: ["data"],
  setSelectedFilterDebtorId: ["selectedFilterDebtorId"],

  // Sagas
  fetchDebtors: ["page", "perPage", "filters", "onSuccess"],
  createDebtor: ["setSubmitting", "data", "onSuccess"],
  fetchDebtor: ["code"],
  fetchDebtorById: ["id"],
  updateDebtor: ["setSubmitting", "id", "data", "onSuccess"],
  updateDebtorContact: ["setSubmitting", "id", "data", "onSuccess"],
  createDebtorContact: ["setSubmitting", "data", "onSuccess"],
  deleteDebtorContact: ["id", "onSuccess"],
  fetchDebtorCodes: ["search", "onSuccess"],
  generateStatement: ["debtorId", "companyCode", "isOpenBill"],
  generateOverdue: ["debtorId", "companyCode"],
  fetchDebtorByCompanyCode: ["companyCode", "filters"],
  fetchSuggestedDebtorsByCompanyCode: ["companyCode", "filters"]
})

export const DebtorTypes = Types
export default DebtorAction
