import { BaseModel } from "@/Types/GeneralTypes"
import { Action } from "@reduxjs/toolkit"
import { Invoice } from "../Invoice/Types"
import { CreditNote } from "../CreditNote/Types"
import { DebitNote } from "../DebitNote/Types"
import { DebtorPayment } from "../DebtorPayment/Types"

export interface DebtorContact extends BaseDebtorContact, BaseModel {
  debtor_id: number
}

export interface BaseDebtorContact {
  email: string
  phone: string
  name: string
  department: string
  title: string
  direct_phone: string
  direct_fax: string
  im: string
  remark: string
  is_main: boolean
}

export interface Debtor extends BaseDebtor, BaseModel {
  overdues?: (Invoice | CreditNote | DebitNote)[]
  statements?: (Invoice | CreditNote | DebitNote | DebtorPayment)[]
  total_statements?: number
  total_overdues?: number
  contacts: DebtorContact[]
  suggested?: true
}

export interface BaseDebtor {
  company_id: number
  contacts: DebtorContact[]
  code: string
  ref_code_01: string
  name_01: string
  name_02: string
  co_reg_no: string
  tax_reg_no: string
  term_code: string
  area_code: string
  area_desc_01: string
  bill_unit_no: string
  bill_building_name: string
  bill_street_name: string
  bill_district_01: string
  bill_district_02: string
  bill_postcode: string
  bill_state_name: string
  bill_attention: string
  bill_phone_01: string
  bill_phone_02: string
  bill_fax_01: string
  bill_fax_02: string
  bill_email_01: string
  bill_email_02: string
  bill_country_name: string
  ship_unit_no: string
  ship_building_name: string
  ship_street_name: string
  ship_district_01: string
  ship_district_02: string
  ship_postcode: string
  ship_state_name: string
  ship_attention: string
  ship_phone_01: string
  ship_phone_02: string
  ship_fax_01: string
  ship_fax_02: string
  ship_email_01: string
  ship_email_02: string
  ship_country_name: string
}

export interface DebtorsFilters {
  code: string
  name: string
  has_statements: boolean
  has_overdues: boolean
  end_date: string | null
}

export interface DebtorState {
  list: {
    isLoading: boolean
    data: Debtor[]
    per_page: number
    current_page: number
    last_page: number
    total: number
    filters: DebtorsFilters
    currentCompanyCode: string | null
  }
  item: {
    isLoading: boolean
    data: Debtor | null
  }
  generateDocumentLoading: boolean
  suggestedDebtors: {
    isLoading: boolean
    data: Debtor[]
  }
  selectedFilterDebtorId: number | null
}

export enum ETypesName {
  SET_DEBTORS_LOADING = "SET_DEBTORS_LOADING",
  SET_DEBTORS = "SET_DEBTORS",
  SET_DEBTORS_PAGINATION = "SET_DEBTORS_PAGINATION",
  SET_DEBTOR_LOADING = "SET_DEBTOR_LOADING",
  SET_DEBTOR = "SET_DEBTOR",
  SET_DEBTORS_FILTERS = "SET_DEBTORS_FILTERS",
  SET_DEBTOR_CONTACTS = "SET_DEBTOR_CONTACTS",
  SET_DEBTOR_CODES = "SET_DEBTOR_CODES",
  SET_GENERATE_DOCUMENT_LOADING = "SET_GENERATE_DOCUMENT_LOADING",
  SET_SUGGESTED_DEBTORS_LOADING = "SET_SUGGESTED_DEBTORS_LOADING",
  SET_SUGGESTED_DEBTORS = "SET_SUGGESTED_DEBTORS",
  SET_SELECTED_FILTER_DEBTOR_ID = "SET_SELECTED_FILTER_DEBTOR_ID",

  FETCH_DEBTORS = "FETCH_DEBTORS",
  CREATE_DEBTOR = "CREATE_DEBTOR",
  FETCH_DEBTOR = "FETCH_DEBTOR",
  FETCH_DEBTOR_BY_ID = "FETCH_DEBTOR_BY_ID",
  UPDATE_DEBTOR = "UPDATE_DEBTOR",
  UPDATE_DEBTOR_CONTACT = "UPDATE_DEBTOR_CONTACT",
  CREATE_DEBTOR_CONTACT = "CREATE_DEBTOR_CONTACT",
  DELETE_DEBTOR_CONTACT = "DELETE_DEBTOR_CONTACT",
  FETCH_DEBTOR_CODES = "FETCH_DEBTOR_CODES",
  GENERATE_STATEMENT = "GENERATE_STATEMENT",
  GENERATE_OVERDUE = "GENERATE_OVERDUE",
  FETCH_DEBTOR_BY_COMPANY_CODE = "FETCH_DEBTOR_BY_COMPANY_CODE",
  FETCH_SUGGESTED_DEBTORS_BY_COMPANY_CODE = "FETCH_SUGGESTED_DEBTORS_BY_COMPANY_CODE"
}

export type FetchDebtors = Action<ETypesName.FETCH_DEBTORS> & {
  page?: number
  perPage?: number
  filters?: DebtorsFilters
  onSuccess?: Function
}

export type FetchDebtorCodes = Action<ETypesName.FETCH_DEBTOR_CODES> & {
  search?: string
  onSuccess?: Function
}

export type SetDebtorCodes = Action<ETypesName.SET_DEBTOR_CODES> & {
  data: Debtor[]
  currentCompanyCode: String | undefined
}

export type SetDebtorsLoading = Action<ETypesName.SET_DEBTORS_LOADING> & {
  isLoading: boolean
}

export type SetDebtors = Action<ETypesName.SET_DEBTORS> & {
  data: Debtor[]
}

export type SetDebtorsPagination = Action<ETypesName.SET_DEBTORS_PAGINATION> & {
  per_page: number
  current_page: number
  last_page: number
  total: number
}

export type SetDebtorContacts = Action<ETypesName.SET_DEBTOR_CONTACTS> & {
  data: DebtorContact[]
}

export type SetDebtorLoading = Action<ETypesName.SET_DEBTOR_LOADING> & {
  isLoading: boolean
}

export type SetDebtor = Action<ETypesName.SET_DEBTOR> & {
  data: Debtor | null
}

export type SetGenerateDocumentLoading = Action<ETypesName.SET_GENERATE_DOCUMENT_LOADING> & {
  generateDocumentLoading: boolean
}

export type SetSuggestedDebtorsLoading = Action<ETypesName.SET_SUGGESTED_DEBTORS_LOADING> & {
  isLoading: boolean
}

export type SetSuggestedDebtors = Action<ETypesName.SET_SUGGESTED_DEBTORS> & {
  data: Debtor[]
}

export type SetSelectedFilterDebtorId = Action<ETypesName.SET_SELECTED_FILTER_DEBTOR_ID> & {
    selectedFilterDebtorId: number | null
}

export type CreateDebtor = Action<ETypesName.CREATE_DEBTOR> & {
  setSubmitting: Function
  data: BaseDebtor
  onSuccess: Function
}

export type FetchDebtor = Action<ETypesName.FETCH_DEBTOR> & {
  code: string
}

export type FetchDebtorById = Action<ETypesName.FETCH_DEBTOR_BY_ID> & {
  id: number
}

export type UpdateDebtor = Action<ETypesName.UPDATE_DEBTOR> & {
  setSubmitting: Function
  id: number
  data: Partial<Debtor>
  onSuccess: (newData: Debtor) => void
}

export type SetDebtorsFilters = Action<ETypesName.SET_DEBTORS_FILTERS> & {
  filters: DebtorsFilters
}

export type UpdateDebtorContact = Action<ETypesName.UPDATE_DEBTOR_CONTACT> & {
  setSubmitting: Function
  id: number
  data: Partial<DebtorContact>
  onSuccess: () => void
}

export type CreateDebtorContact = Action<ETypesName.CREATE_DEBTOR_CONTACT> & {
  setSubmitting: Function
  data: Partial<DebtorContact>
  onSuccess: () => void
}

export type DeleteDebtorContact = Action<ETypesName.DELETE_DEBTOR_CONTACT> & {
  id: number | undefined
  onSuccess: () => void
}

export type GenerateStatement = Action<ETypesName.GENERATE_STATEMENT> & {
  debtorId: number
  companyCode: string
  isOpenBill: boolean
}

export type GenerateOverdue = Action<ETypesName.GENERATE_OVERDUE> & {
  debtorId: number
  companyCode: string
}

export type FetchDebtorByCompanyCode = Action<ETypesName.FETCH_DEBTOR_BY_COMPANY_CODE> & {
  companyCode: string
  filters?: any
}

export type FetchSuggestedDebtorsByCompanyCode =
  Action<ETypesName.FETCH_SUGGESTED_DEBTORS_BY_COMPANY_CODE> & {
    companyCode: string
    filters?: string[]
  }
