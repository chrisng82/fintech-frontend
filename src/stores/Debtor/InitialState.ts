// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: {
    isLoading: false,
    data: [],
    per_page: 12,
    current_page: 1,
    last_page: 1,
    total: 0,
    filters: {
      code: "",
      name: "",
      has_statements: false,
      has_overdues: false,
      end_date: null
    },
    currentCompanyCode: null
  },
  item: {
    isLoading: false,
    data: null
  },
  generateDocumentLoading: false,
  suggestedDebtors: {
    isLoading: false,
    data: [],
    filters: {
      suggestedName: ""
    }
  },
  selectedFilterDebtorId: null
}
