// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: {
    isLoading: false,
    data: [],
    per_page: 10,
    current_page: 1,
    last_page: 1,
    total: 0
  },
  item: {
    isLoading: false,
    data: null
  },
  isValidating: false,
  isSending: false
}
