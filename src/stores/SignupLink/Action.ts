import { createActions } from "reduxsauce"
import {
  BaseSignupLink,
  SignupLink,
  SetSignupLinks,
  ETypesName,
  GenerateSignupLink,
  InvalidateSignupLink,
  ValidateSignupLinkToken,
  SetSignupLink,
  SetSignupLinkLoading,
  SetSignupLinksLoading,
  FetchSignupLinks,
  SetIsValidating,
  SignupBySignupLink,
  SetSignupLinksPagination,
  SetSignupLinkSending,
  SendSignupLinkTo,
  SelfSignup,
  SelfSignUpData,
  SendBatchSignupLinksTo
} from "./Types"
import { BaseUser } from "../User/Types"

const { Types, Creators: SignupLinkAction } = createActions<
  {
    [ETypesName.SET_SIGNUP_LINKS]: string
    [ETypesName.SET_SIGNUP_LINKS_PAGINATION]: string
    [ETypesName.SET_SIGNUP_LINK]: string
    [ETypesName.SET_SIGNUP_LINK_LOADING]: string
    [ETypesName.SET_SIGNUP_LINKS_LOADING]: string
    [ETypesName.SET_IS_VALIDATING]: string
    [ETypesName.SET_SIGNUP_LINK_SENDING]: string

    [ETypesName.FETCH_SIGNUP_LINKS]: string
    [ETypesName.GENERATE_SIGNUP_LINK]: string
    [ETypesName.INVALIDATE_SIGNUP_LINK]: string
    [ETypesName.VALIDATE_SIGNUP_LINK_TOKEN]: string
    [ETypesName.SIGNUP_BY_SIGNUP_LINK]: string
    [ETypesName.SEND_SIGNUP_LINK_TO]: string
    [ETypesName.SEND_BATCH_SIGNUP_LINKS_TO]: string
    [ETypesName.SELF_SIGNUP]: string
  },
  {
    setSignupLinks: (data: SignupLink[]) => SetSignupLinks
    setSignupLinksPagination: (
      per_page: number,
      current_page: number,
      last_page: number,
      total: number
    ) => SetSignupLinksPagination
    setSignupLink: (data: SignupLink) => SetSignupLink
    setSignupLinkLoading: (isLoading: boolean) => SetSignupLinkLoading
    setSignupLinksLoading: (isLoading: boolean) => SetSignupLinksLoading
    setIsValidating: (isValidating: boolean) => SetIsValidating
    setSignupLinkSending: (isSending: boolean) => SetSignupLinkSending

    fetchSignupLinks: (page?: number, perPage?: number) => FetchSignupLinks
    generateSignupLink: (
      setSubmitting: Function,
      data: BaseSignupLink,
      onSuccess: Function
    ) => GenerateSignupLink
    invalidateSignupLink: (id: number) => InvalidateSignupLink
    validateSignupLinkToken: (token: string, onFail: Function) => ValidateSignupLinkToken
    signupBySignupLink: (
      setSubmitting: Function,
      data: Partial<BaseUser> & { user_type: number },
      onSuccess: Function
    ) => SignupBySignupLink
    sendSignupLinkTo: (id: number, actionType: string) => SendSignupLinkTo
    sendBatchSignupLinksTo: (
      ids: number[],
      isAll: boolean,
      actionType: string
    ) => SendBatchSignupLinksTo
    selfSignup: (
      setSubmitting: Function,
      data: SelfSignUpData,
      onFail: Function,
      onSuccess: Function
    ) => SelfSignup
  }
>({
  // Reducers
  setSignupLinks: ["data"],
  setSignupLinksPagination: ["per_page", "current_page", "last_page", "total"],
  setSignupLink: ["data"],
  setSignupLinkLoading: ["isLoading"],
  setSignupLinksLoading: ["isLoading"],
  setIsValidating: ["isValidating"],
  setSignupLinkSending: ["isSending"],

  //Sagas
  fetchSignupLinks: ["page", "perPage"],
  generateSignupLink: ["setSubmitting", "data", "onSuccess"],
  invalidateSignupLink: ["id"],
  validateSignupLinkToken: ["token", "onFail"],
  signupBySignupLink: ["setSubmitting", "data", "onSuccess"],
  sendSignupLinkTo: ["id", "actionType"],
  sendBatchSignupLinksTo: ["ids", "isAll", "actionType"],
  selfSignup: ["setSubmitting", "data", "onFail", "onSuccess"]
})

export const SignupLinkTypes = Types
export default SignupLinkAction
