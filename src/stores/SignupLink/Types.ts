import { Action } from "@reduxjs/toolkit"
import { BaseUser, User } from "../User/Types"
import { Debtor } from "../Debtor/Types"

export interface SignupLink extends BaseSignupLink {
  created_at: string | Date
  id: number
  updated_at: string | Date
}

export interface BaseSignupLink {
  email: string
  phone: string
  company_id: number
  debtor_code?: string[]
  token?: string
  link_type: number
  token_expiry?: string | Date
  created_by?: User
  is_valid?: boolean
  debtor?: Debtor
}

export interface SignupLinkState {
  list: {
    data: SignupLink[]
    isLoading: boolean
    per_page: number
    current_page: number
    last_page: number
    total: number
  }
  item: {
    data: SignupLink | null
    isLoading: boolean
  }
  isValidating: boolean
}

export interface SelfSignUpData {
  [key: string]: any
  customer_type: string
  companies: number[]
  company_name: string
  company_reg_no: string
  company_addr: string
  first_name: string
  last_name: string
  email: string
  phone: string
  password: string
  confirm_password: string
  ssm_form: File | null
  form_49: File | null
  bank_statement: File | null
  tnc_confirmation: {
    company_name: string
    name: string
    ic_number: string | number
    designation: string
  }
}

export enum ETypesName {
  SET_SIGNUP_LINKS = "SET_SIGNUP_LINKS",
  SET_SIGNUP_LINKS_PAGINATION = "SET_SIGNUP_LINKS_PAGINATION",
  SET_SIGNUP_LINK = "SET_SIGNUP_LINK",
  SET_SIGNUP_LINK_LOADING = "SET_SIGNUP_LINK_LOADING",
  SET_SIGNUP_LINKS_LOADING = "SET_SIGNUP_LINKS_LOADING",
  SET_IS_VALIDATING = "SET_IS_VALIDATING",
  SET_SIGNUP_LINK_SENDING = "SET_SIGNUP_LINK_SENDING",

  FETCH_SIGNUP_LINKS = "FETCH_SIGNUP_LINKS",
  GENERATE_SIGNUP_LINK = "GENERATE_SIGNUP_LINK",
  INVALIDATE_SIGNUP_LINK = "INVALIDATE_SIGNUP_LINK",
  VALIDATE_SIGNUP_LINK_TOKEN = "VALIDATE_SIGNUP_LINK_TOKEN",
  SIGNUP_BY_SIGNUP_LINK = "SIGNUP_BY_SIGNUP_LINK",
  SEND_SIGNUP_LINK_TO = "SEND_SIGNUP_LINK_TO",
  SEND_BATCH_SIGNUP_LINKS_TO = "SEND_BATCH_SIGNUP_LINKS_TO",
  SELF_SIGNUP = "SELF_SIGNUP"
}

export type SetSignupLinks = Action<ETypesName.SET_SIGNUP_LINKS> & {
  data: SignupLink[]
}

export type SetSignupLinksPagination = Action<ETypesName.SET_SIGNUP_LINKS_PAGINATION> & {
  per_page: number
  current_page: number
  last_page: number
  total: number
}

export type SetSignupLink = Action<ETypesName.SET_SIGNUP_LINK> & {
  data: SignupLink
}

export type SetSignupLinkLoading = Action<ETypesName.SET_SIGNUP_LINK_LOADING> & {
  isLoading: boolean
}

export type SetSignupLinksLoading = Action<ETypesName.SET_SIGNUP_LINKS_LOADING> & {
  isLoading: boolean
}

export type SetIsValidating = Action<ETypesName.SET_IS_VALIDATING> & {
  isValidating: boolean
}

export type SetSignupLinkSending = Action<ETypesName.SET_SIGNUP_LINK_SENDING> & {
  isSending: boolean
}

export type FetchSignupLinks = Action<ETypesName.FETCH_SIGNUP_LINKS> & {
  page?: number
  perPage?: number
}

export type GenerateSignupLink = Action<ETypesName.GENERATE_SIGNUP_LINK> & {
  setSubmitting: Function
  data: BaseSignupLink
  onSuccess: Function
}

export type InvalidateSignupLink = Action<ETypesName.INVALIDATE_SIGNUP_LINK> & {
  id: number
}

export type ValidateSignupLinkToken = Action<ETypesName.VALIDATE_SIGNUP_LINK_TOKEN> & {
  token: string
  onFail: Function
}

export type SignupBySignupLink = Action<ETypesName.SIGNUP_BY_SIGNUP_LINK> & {
  setSubmitting: Function
  data: BaseUser
  onSuccess: Function
}

export type SendSignupLinkTo = Action<ETypesName.SEND_SIGNUP_LINK_TO> & {
  id: number[]
  actionType: string
}

export type SendBatchSignupLinksTo = Action<ETypesName.SEND_BATCH_SIGNUP_LINKS_TO> & {
  ids: number[]
  isAll: boolean
  actionType: string
}

export type SelfSignup = Action<ETypesName.SELF_SIGNUP> & {
  setSubmitting: Function
  data: SelfSignUpData
  onFail: Function
  onSuccess: Function
}
