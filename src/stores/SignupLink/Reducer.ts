import { createReducer } from "reduxsauce"
import { SignupLinkTypes } from "./Action"
import INITIAL_STATE from "./InitialState"
import {
  SignupLinkState,
  SetSignupLink,
  SetSignupLinksLoading,
  SetSignupLinkLoading,
  SetSignupLinks,
  SetIsValidating,
  SetSignupLinksPagination,
  SetSignupLinkSending
} from "./Types"

export const setSignupLinks = (state: SignupLinkState, { data }: SetSignupLinks) => ({
  ...state,
  list: {
    ...state.list,
    data
  }
})

export const setSignupLinksPagination = (
  state: SignupLinkState,
  { per_page, current_page, last_page, total }: SetSignupLinksPagination
) => ({
  ...state,
  list: {
    ...state.list,
    per_page,
    current_page,
    last_page,
    total
  }
})

export const setSignupLink = (state: SignupLinkState, { data }: SetSignupLink) => ({
  ...state,
  item: {
    ...state.item,
    data
  }
})

export const setSignupLinkLoading = (
  state: SignupLinkState,
  { isLoading }: SetSignupLinkLoading
) => ({
  ...state,
  item: {
    ...state.item,
    isLoading
  }
})

export const setSignupLinksLoading = (
  state: SignupLinkState,
  { isLoading }: SetSignupLinksLoading
) => ({
  ...state,
  list: {
    ...state.list,
    isLoading
  }
})

export const setIsValidating = (state: SignupLinkState, { isValidating }: SetIsValidating) => ({
  ...state,
  isValidating
})

export const setSignupLinkSending = (
  state: SignupLinkState,
  { isSending }: SetSignupLinkSending
) => ({
  ...state,
  isSending
})

type Actions =
  | SetSignupLinks
  | SetSignupLink
  | SetSignupLinkLoading
  | SetSignupLinksLoading
  | SetIsValidating
  | SetSignupLinkSending

export const SignupLinkReducer = createReducer<SignupLinkState, Actions>(INITIAL_STATE, {
  [SignupLinkTypes.SET_SIGNUP_LINKS]: setSignupLinks,
  [SignupLinkTypes.SET_SIGNUP_LINKS_PAGINATION]: setSignupLinksPagination,
  [SignupLinkTypes.SET_SIGNUP_LINK]: setSignupLink,
  [SignupLinkTypes.SET_SIGNUP_LINK_LOADING]: setSignupLinkLoading,
  [SignupLinkTypes.SET_SIGNUP_LINKS_LOADING]: setSignupLinksLoading,
  [SignupLinkTypes.SET_IS_VALIDATING]: setIsValidating,
  [SignupLinkTypes.SET_SIGNUP_LINK_SENDING]: setSignupLinkSending
})
