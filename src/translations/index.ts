import enUS from "./enUS.json"

const languageObject = {
  "en-US": enUS
}

export default languageObject
