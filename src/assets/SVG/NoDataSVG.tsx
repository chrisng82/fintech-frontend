import React, { ReactElement } from "react"
import NoData1 from "../../../public/no_data_1.svg"
import NoData2 from "../../../public/no_data_2.svg"
import { Box, Typography } from "@mui/material"
import { useIntl } from "react-intl"

const NoDataSVG: React.FC = (): ReactElement => {
  const intl = useIntl()

  const svgs = [
    {
      svg: <NoData1 />,
      link: "https://www.freepik.com/free-vector/no-data-concept-illustration_5928292.htm#query=no%20data&position=0&from_view=search&track=ais"
    },
    {
      svg: <NoData2 />,
      link: "https://www.freepik.com/free-vector/no-data-concept-illustration_8961448.htm#query=no%20data&position=3&from_view=search&track=ais"
    }
  ]

  const random = (Math.random() * svgs.length) | 0

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        gap: "1rem",
        opacity: 0.5
      }}>
      <Typography
        sx={{
          color: "#9e9e9e"
        }}>
        {intl.formatMessage({ id: "no_data_found" })}
      </Typography>
      {svgs[random].svg}
      <Typography
        sx={{
          fontSize: "8px",
          color: "#9e9e9e",
          fontWeight: 300
        }}>
        <a href={svgs[random].link} target="_blank">
          Image by storyset
        </a>{" "}
        on Freepik
      </Typography>
    </Box>
  )
}

export default NoDataSVG
