import { Box, Button, Menu, MenuItem, Paper, Popover, TextField, Typography } from "@mui/material"
import GreyButton from "../Base/GreyButton"
import AddIcon from "@mui/icons-material/Add"
import React, { useEffect } from "react"
import { palette } from "../utils/theme"
import { useIntl } from "react-intl"
import { set } from "lodash"
import useAuth from "../hooks/useAuth"
import { SelectedTabEnum } from "../page/Documents/DocumentsContainer"
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers"
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import dayjs from "dayjs"

interface IProp {
  selectedTab: string
  filters: IFilter[]
  setFilters: React.Dispatch<React.SetStateAction<IFilter[]>>
}

export type IFilterType =
  | "code"
  | "debtorCode"
  | "debtorName"
  | "pdfOnly"
  | "debtorId"
  | "paymentStatus"
  | "docDate"

export interface IFilter {
  type: IFilterType
  value: string | boolean | number | string[]
}

const Filter: React.FC<IProp> = ({ selectedTab, filters, setFilters }) => {
  const intl = useIntl()

  const { isDebtor } = useAuth()

  const [filterTypeAnchorEl, setfilterTypeAnchorEl] = React.useState<null | HTMLElement>(null)
  const [filterValueAnchorEl, setfilterValueAnchorEl] = React.useState<null | HTMLElement>(null)
  const [filterOptionAnchorEl, setfilterOptionAnchorEl] = React.useState<null | HTMLElement>(null)
  const [filterDateAnchorEl, setfilterDateAnchorEl] = React.useState<null | HTMLElement>(null)
  const [filter, setFilter] = React.useState<IFilter>()
  const [filterOptions, setFilterOptions] = React.useState<{ value: string; label: string }[]>([])

  const filterTypeIsOpen = Boolean(filterTypeAnchorEl)
  const filterValueIsOpen = Boolean(filterValueAnchorEl)
  const filterOptionIsOpen = Boolean(filterOptionAnchorEl)
  const filterDateIsOpen = Boolean(filterDateAnchorEl)

  const paymentStatusOptions = [
    { value: "paid", label: intl.formatMessage({ id: "paid" }) },
    { value: "not_paid", label: intl.formatMessage({ id: "not_paid" }) }
  ]

  const handleAddFilter = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setfilterTypeAnchorEl(event.currentTarget)
    setFilter(undefined)
  }

  const handleSelectFilter = (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>,
    filterType: IFilterType
  ) => {
    setfilterValueAnchorEl(event.currentTarget)
    event.currentTarget.style.backgroundColor = palette.purple.main
    event.currentTarget.style.color = "#fff"
    setFilter({ type: filterType, value: "" })
  }

  const handleSelectDateFilter = (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>,
    filterType: IFilterType
  ) => {
    setfilterDateAnchorEl(event.currentTarget)
    event.currentTarget.style.backgroundColor = palette.purple.main
    event.currentTarget.style.color = "#fff"
    setFilter({ type: filterType, value: ["", ""] })
  }

  const handleSetFilter = () => {
    if (filter) {
      // check if filter.type already exists in filters
      const filterTypeExists = filters.find((f) => f.type === filter.type)
      if (filterTypeExists) {
        // if exists, replace the value
        setFilters((prev) => {
          return prev.map((f) => {
            if (f.type === filter.type) {
              return filter
            }
            return f
          })
        })
      } else {
        // if not exists, add to filters
        setFilters((prev) => [...prev, filter])
      }
    }
    handleClose()
  }

  const handleRemoveFilter = (filterType: string) => {
    setFilters((prev) => prev.filter((f) => f.type !== filterType))
  }

  const handleClose = () => {
    setfilterTypeAnchorEl(null)
    setfilterValueAnchorEl(null)
    setfilterOptionAnchorEl(null)
    setfilterDateAnchorEl(null)
    setFilter(undefined)
  }

  const handleSetIsPdfFilter = () => {
    setFilters((prev) => {
      const isPdfFilter = prev.find((f) => f.type === "pdfOnly")
      if (isPdfFilter) {
        return prev
      }
      return [...prev, { type: "pdfOnly", value: true }]
    })

    handleClose()
  }

  const handleSelectFilterWithOption = (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>,
    filterType: IFilterType,
    options: { value: string; label: string }[]
  ) => {
    setfilterOptionAnchorEl(event.currentTarget)
    event.currentTarget.style.backgroundColor = palette.purple.main
    event.currentTarget.style.color = "#fff"
    setFilter({ type: filterType, value: "" })
    setFilterOptions(options)
  }

  const getFormatedDocDate = (docDate: string[]) => {
    docDate = docDate.map((d) => d && dayjs(d).format("DD-MM-YYYY"))

    if (docDate[0] && docDate[1]) {
      return docDate.join(" - ")
    } else if (!docDate[1]) {
      return `${docDate[0]} ${intl.formatMessage({ id: "onwards" })}`
    } else if (!docDate[0]) {
      return `${intl.formatMessage({ id: "before" })} ${docDate[1]}`
    }
    return ""
  }

  return (
    <Box
      sx={{
        display: "flex",
        gap: "1rem"
      }}>
      <Typography variant="h6" sx={{ fontWeight: 400 }}>
        {intl.formatMessage({ id: "filters" })}:
      </Typography>
      <Box sx={{ display: "flex", gap: "1rem", flexWrap: "wrap" }}>
        {filters.map((f) => (
          <Paper
            key={f.type}
            sx={{
              display: "flex",
              color: "#fff",
              backgroundColor: palette.purple.main,
              gap: "0.5rem",
              padding: "0.5rem 1rem"
            }}>
            {f.type === "pdfOnly" ? (
              <Typography
                sx={{
                  color: "#ffff",
                  fontWeight: 700,
                  display: "inline",
                  whiteSpace: "nowrap"
                }}>
                {intl.formatMessage({ id: f.type })}
              </Typography>
            ) : (
              <React.Fragment>
                <Typography
                  sx={{
                    color: "#ffff",
                    fontWeight: 700,
                    display: "inline",
                    whiteSpace: "nowrap"
                  }}>
                  {f.type == "code"
                    ? `${intl.formatMessage({ id: selectedTab })} No.`
                    : intl.formatMessage({ id: f.type })}
                  :
                </Typography>{" "}
                <Typography
                  sx={{
                    color: "#ffff",
                    fontWeight: 400,
                    display: "inline",
                    whiteSpace: "break-spaces"
                  }}>
                  {f.type === "paymentStatus"
                    ? paymentStatusOptions.find((o) => o.value === f.value)?.label
                    : f.type === "docDate"
                      ? getFormatedDocDate(f.value as string[])
                      : f.value}
                </Typography>
              </React.Fragment>
            )}
            <Typography
              onClick={() => handleRemoveFilter(f.type)}
              sx={{ color: "#ffff", marginLeft: "10px", cursor: "pointer" }}>
              X
            </Typography>
          </Paper>
        ))}
        <Box>
          <GreyButton variant="contained" sx={{ padding: "0.5rem 1rem" }} onClick={handleAddFilter}>
            <AddIcon />
          </GreyButton>
          <Menu
            anchorEl={filterTypeAnchorEl}
            open={filterTypeIsOpen}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "left"
            }}>
            <MenuItem
              onClick={(e) => {
                handleSelectFilter(e, "code")
              }}>
              {intl.formatMessage({ id: selectedTab })} No.
            </MenuItem>
            {!isDebtor && (
              <div>
                <MenuItem
                  onClick={(e) => {
                    handleSelectFilter(e, "debtorCode")
                  }}>
                  {intl.formatMessage({ id: "debtor_code" })}
                </MenuItem>
                <MenuItem
                  onClick={(e) => {
                    handleSelectFilter(e, "debtorName")
                  }}>
                  {intl.formatMessage({ id: "debtor_name" })}
                </MenuItem>
              </div>
            )}
            {selectedTab !== SelectedTabEnum.MONTHLY_STATEMENT && (
              <React.Fragment>
                <MenuItem
                  onClick={(e) => {
                    handleSelectDateFilter(e, "docDate")
                  }}>
                  {intl.formatMessage({ id: "doc_date" })}
                </MenuItem>
                <MenuItem
                  onClick={(e) => {
                    handleSelectFilterWithOption(e, "paymentStatus", paymentStatusOptions)
                  }}>
                  {intl.formatMessage({ id: "payment_status" })}
                </MenuItem>
                <MenuItem onClick={handleSetIsPdfFilter}>
                  {intl.formatMessage({ id: "pdfOnly" })}
                </MenuItem>
              </React.Fragment>
            )}
          </Menu>

          {/* Input value */}
          <Popover
            open={filterValueIsOpen}
            anchorEl={filterValueAnchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "left"
            }}>
            <Box sx={{ p: 2 }}>
              <TextField
                variant="outlined"
                fullWidth
                autoFocus
                onChange={(e) => {
                  setFilter((prev) => {
                    if (!prev) {
                      return prev
                    }
                    return set(prev, "value", e.target.value)
                  })
                }}
              />
              <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
                <GreyButton variant="contained" onClick={handleClose}>
                  {intl.formatMessage({ id: "cancel" })}
                </GreyButton>
                <Button variant="contained" onClick={handleSetFilter}>
                  {intl.formatMessage({ id: "apply" })}
                </Button>
              </Box>
            </Box>
          </Popover>

          {/* Select option */}
          <Menu
            anchorEl={filterOptionAnchorEl}
            open={filterOptionIsOpen}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "left"
            }}>
            {filterOptions.map((option) => (
              <MenuItem
                key={option.value}
                onClick={(e) => {
                  setFilter((prev) => {
                    if (!prev) {
                      return prev
                    }
                    return set(prev, "value", option.value)
                  })
                  handleSetFilter()
                }}>
                {option.label}
              </MenuItem>
            ))}
          </Menu>

          {/* Date picker */}
          <Popover
            open={filterDateIsOpen}
            anchorEl={filterDateAnchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "left"
            }}>
            <Box sx={{ p: 2 }}>
              <Box sx={{ display: "flex", alignItems: "center", gap: "1rem" }}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    label="Start Date"
                    format="DD/MM/YYYY"
                    value={dayjs(filter?.value && Array.isArray(filter.value) ? filter.value[0] : "")}
                    disableHighlightToday
                    shouldDisableDate={(date) => {
                      const endDate = dayjs(filter?.value && Array.isArray(filter.value) ? filter.value[1] : null)
                      return date.isAfter(endDate)
                    }}
                    onChange={(value) => {
                      setFilter((prev) => {
                        if (!prev) {
                          return prev
                        }

                        const endDate = Array.isArray(prev.value) ? prev.value[1] : ""
                        const newValue = [dayjs(value).utc(true).format("YYYY-MM-DD"), endDate]

                        return set(prev, "value", newValue)
                      })
                    }}
                  />
                  {" - "}
                  <DatePicker
                    label="End Date"
                    format="DD/MM/YYYY"
                    value={dayjs(filter?.value && Array.isArray(filter.value) ? filter.value[1] : "")}
                    disableHighlightToday
                    shouldDisableDate={(date) => {
                      const startDate = dayjs(filter?.value && Array.isArray(filter.value) ? filter.value[0] : null)
                      return date.isBefore(startDate)
                    }}
                    onChange={(value: any) => {
                      setFilter((prev) => {
                        if (!prev) {
                          return prev
                        }

                        const startDate = Array.isArray(prev.value) ? prev.value[0] : ""
                        const newValue = [startDate, dayjs(value).utc(true).format("YYYY-MM-DD")]

                        return set(prev, "value", newValue)
                      })
                    }}
                  />
                </LocalizationProvider>
              </Box>
              <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
                <GreyButton variant="contained" onClick={handleClose}>
                  {intl.formatMessage({ id: "cancel" })}
                </GreyButton>
                <Button variant="contained" onClick={handleSetFilter}>
                  {intl.formatMessage({ id: "apply" })}
                </Button>
              </Box>
            </Box>
          </Popover>
        </Box>
      </Box>
    </Box>
  )
}

export default Filter
