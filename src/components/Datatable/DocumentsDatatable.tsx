import React, { useState, useMemo, ReactElement } from "react"
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogContent,
  DialogTitle,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  TextField,
  Typography
} from "@mui/material"
import { useRouter } from "next/navigation"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"
import ArticleIcon from "@mui/icons-material/Article"
import DownloadIcon from "@mui/icons-material/Download"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import DocAttachmentAction from "@/stores/DocAttachment/Action"
import { SelectedTabEnum } from "../page/Documents/DocumentsContainer"
import NoDataSVG from "@/assets/SVG/NoDataSVG"
import { DocAttachment } from "@/stores/DocAttachment/Types"
import CircularProgress from "@mui/material/CircularProgress"

interface IProps {
  docType: string
  data: any[]
  columns: any[]
  totalRows: number
  rowsPerPage: number
  page: number
  isLoading: boolean
  idKey: string
  rowStyle?: (row: any) => any
  handleChangePage: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null,
    newPage: number
  ) => void
  handleChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => void
  onRowClick?: (row: any) => void
}

export interface IColumn {
  id: string
  label: string
  minWidth?: number
  align?: "right" | "left" | "center"
  format?: (value: number | string) => string | number
  sortable?: boolean
  order?: string
  handleSort?: () => void
  filterable?: boolean
  type?: "string" | "number" | "date"
  render?: (row: any, column: IColumn) => ReactElement | string | number
}

const Datatable: React.FC<IProps> = (props): ReactElement => {
  const intl = useIntl()
  const {
    docType,
    data,
    columns,
    totalRows,
    rowsPerPage,
    page,
    isLoading,
    idKey,
    rowStyle,
    handleChangePage,
    handleChangeRowsPerPage,
    onRowClick
  } = props

  const router = useRouter()
  const dispatch = useDispatch()

  const { isLoading: isDownloadLoading } = useSelector(
    (state: RootState) => state.docAttachment.download
  )

  const { downloadData } = useSelector((state: RootState) => state.docAttachment)

  const [sortConfig, setSortConfig] = useState<any>(null)
  const [filters, setFilters] = useState<any>({})
  const [selectedDocuments, setSelectedDocuments] = React.useState<string[]>([])
  const [openDialog, setOpenDialog] = React.useState(false)

  const sortedData = useMemo(() => {
    let sortableData = data

    // Filter the data
    Object.keys(filters).forEach((key: string) => {
      sortableData = sortableData.filter((row) =>
        row[key].toLowerCase().includes(filters[key].toLowerCase())
      )
    })

    if (sortConfig !== null) {
      sortableData.sort((a, b) => {
        let aValue = ""
        let bValue = ""
        if (sortConfig.render) {
          aValue = sortConfig.render(a, sortConfig)
          bValue = sortConfig.render(b, sortConfig)
        } else {
          aValue = a[sortConfig.key]
          bValue = b[sortConfig.key]
        }
        let comparison = 0

        if (sortConfig.type === "date") {
          // Date comparison
          const dateA = new Date(aValue)
          const dateB = new Date(bValue)
          comparison = dateA.getTime() - dateB.getTime()
        } else if (sortConfig.type === "number") {
          // Numeric comparison
          comparison = parseFloat(aValue) - parseFloat(bValue)
        } else if (sortConfig.type === "string") {
          const aString = aValue ?? ""
          const bString = bValue ?? ""
          comparison = aString.localeCompare(bString)
        }

        return sortConfig.direction === "asc" ? comparison : -comparison
      })
    }
    return sortableData
  }, [data, sortConfig, filters])

  const handleFilterChange = (key: string, value: string) => {
    setFilters((prevFilters: any) => ({ ...prevFilters, [key]: value }))
  }

  const requestSort = (key: string, type: string, render: boolean) => {
    let direction = "asc"
    if (sortConfig && sortConfig.key === key && sortConfig.direction === "asc") {
      direction = "desc"
    }
    setSortConfig({ key, direction, type, render })
  }

  const handleClickOpenDialog = (row: any) => {
    const docAttachmentData: Partial<DocAttachment> = {
      doc_id: Number(row.id),
      ar_table: String(docType),
      url: ""
    }
    dispatch(DocAttachmentAction.docAttachmentGetDownloadData(docAttachmentData))
    setTimeout(() => {
      setOpenDialog(true)
    }, 1000)
  }

  const handleCloseDialog = () => {
    setOpenDialog(false)
    dispatch(DocAttachmentAction.docAttachmentSetDownloadData([]))
  }

  return (
    <>
      <TableContainer>
        {selectedDocuments.length > 0 ? (
          <LoadingButton
            variant="contained"
            size="small"
            startIcon={<DownloadIcon />}
            loading={isDownloadLoading}
            onClick={(e) => {
              e.preventDefault()
              dispatch(DocAttachmentAction.downloadByS3Url(selectedDocuments))
            }}
            sx={{
              color: "white"
            }}>
            {intl.formatMessage({ id: "Download batch documents" })}
          </LoadingButton>
        ) : (
          ""
        )}
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns?.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth, verticalAlign: "top" }}>
                  {column.sortable ? (
                    <TableSortLabel
                      active={column.order ?? false}
                      direction={column.order ?? "desc"}
                      onClick={() => {
                        if (column.handleSort) {
                          column.handleSort()
                        }
                      }}>
                      {column.label}
                    </TableSortLabel>
                  ) : (
                    column.label
                  )}
                  {column.filterable ? (
                    <TextField
                      size="small"
                      placeholder="filter..."
                      onChange={(e) => handleFilterChange(column.id, e.target.value)}
                      style={{ display: "block" }}
                    />
                  ) : null}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  Loading...
                </TableCell>
              </TableRow>
            ) : sortedData.length > 0 ? (
              sortedData.map((row, index) => {
                return (
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.doc_code}
                    onClick={() => {
                      onRowClick && onRowClick(row)
                    }}
                    sx={{
                      ...(rowStyle && rowStyle(row)),
                      cursor: onRowClick ? "pointer" : "default"
                    }}>
                    {columns?.map((column) => {
                      const value = row[column.id]

                      if (column.id === "checkbox" && row.url) {
                        let isChecked = false
                        selectedDocuments.map((document) => {
                          if (document == row.url) {
                            isChecked = true
                          }
                        })

                        return (
                          <TableCell key={`head-${column.id}`} align="center">
                            <Checkbox
                              checked={isChecked}
                              onChange={function (event) {
                                if (event.target.checked) {
                                  setSelectedDocuments((prevDocuments) => [
                                    ...prevDocuments,
                                    row.url
                                  ])
                                } else {
                                  setSelectedDocuments((prevDocuments) =>
                                    prevDocuments.filter((document) => document !== row.url)
                                  )
                                }
                              }}
                            />
                          </TableCell>
                        )
                      }

                      if (column.id === "action") {
                        return (
                          <TableCell key={`head-${column.id}`} align="center">
                            {(docType === SelectedTabEnum.DEBTOR_PAYMENT && row.hdrId !== 0) ||
                            (row.hdrId === 0 && !row.url) ? null : (
                              <LoadingButton
                                variant="contained"
                                size="small"
                                startIcon={
                                  row.hdrId === 0 ||
                                  docType === SelectedTabEnum.MONTHLY_STATEMENT ||
                                  row.url ? (
                                    <DownloadIcon />
                                  ) : (
                                    <ArticleIcon />
                                  )
                                }
                                loading={isDownloadLoading}
                                onClick={(e) => {
                                  e.preventDefault()
                                  if (
                                    row.hdrId === 0 ||
                                    docType === SelectedTabEnum.MONTHLY_STATEMENT ||
                                    row.url
                                  ) {
                                    switch (docType) {
                                      case SelectedTabEnum.INVOICE:
                                      case SelectedTabEnum.CREDIT_NOTE:
                                      case SelectedTabEnum.DEBIT_NOTE:
                                        handleClickOpenDialog(row)
                                        break
                                      default:
                                        dispatch(DocAttachmentAction.downloadByS3Url(row.url))
                                    }
                                  } else {
                                    router.push(`/documents/${docType}/${row[idKey]}`)
                                  }
                                }}
                                sx={{
                                  color: "white"
                                }}>
                                {intl.formatMessage({
                                  id:
                                    row.hdrId === 0 ||
                                    docType === SelectedTabEnum.MONTHLY_STATEMENT ||
                                    row.url
                                      ? "pdf"
                                      : "view"
                                })}
                              </LoadingButton>
                            )}
                          </TableCell>
                        )
                      }

                      if (column.id === "no") {
                        return (
                          <TableCell key={`head-${column.id}`} align="center">
                            {(page - 1) * rowsPerPage + index + 1}
                          </TableCell>
                        )
                      }

                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.render
                            ? column.render(row, column)
                            : column.format
                              ? column.format(value)
                              : value}
                        </TableCell>
                      )
                    })}
                  </TableRow>
                )
              })
            ) : (
              <TableRow>
                <TableCell colSpan={8}>
                  <Box
                    sx={{
                      padding: "5rem"
                    }}>
                    <NoDataSVG />
                  </Box>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[20, 50, 100]}
        component="div"
        count={totalRows}
        rowsPerPage={rowsPerPage}
        page={page - 1}
        showFirstButton
        showLastButton
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <Dialog open={openDialog} onClose={handleCloseDialog}>
        <DialogTitle>
          {intl.formatMessage({
            id: "select_attachment"
          })}
        </DialogTitle>
        <DialogContent>
          <Stack gap={2} alignItems="center">
            {downloadData.length === 0 ? (
              <CircularProgress />
            ) : (
              downloadData.map((attachment) => (
                <LoadingButton
                  key={attachment.id}
                  variant="contained"
                  size="small"
                  startIcon={<DownloadIcon />}
                  loading={isDownloadLoading}
                  onClick={(e) => {
                    e.preventDefault()
                    dispatch(DocAttachmentAction.downloadByS3Url(attachment.url))
                  }}
                  sx={{
                    color: "white",
                    width: "100%"
                  }}>
                  {intl.formatMessage({
                    id: attachment.type === null ? "invoice" : "e_invoice"
                  })}
                </LoadingButton>
              ))
            )}
          </Stack>
        </DialogContent>
      </Dialog>
    </>
  )
}

export default Datatable
