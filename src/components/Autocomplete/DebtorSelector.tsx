import { RootState } from '@/stores'
import { Autocomplete, Box, CircularProgress, FormLabel, Stack, TextField, Typography } from '@mui/material'
import React, { ReactElement, useState } from 'react'
import { useIntl } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import { DebtorCodeOption } from '../page/Form/SignupForm'
import DebtorAction from '@/stores/Debtor/Action'
import { debounce } from 'lodash'
import { Debtor } from '@/stores/Debtor/Types'

interface Props {
    data: DebtorCodeOption[]
    labelId?: string
    required?: boolean
    suggestedName?: string
    onChange: (value: DebtorCodeOption[]) => void
}

const DebtorSelector: React.FC<Props> = ({
    data,
    labelId = "debtor_codes",
    required = false,
    suggestedName,
    onChange
}): ReactElement => {
    const intl = useIntl()
    const dispatch = useDispatch()

    const user = useSelector((state: RootState) => state.app.user)
    const { data: debtors, isLoading: isLoading } = useSelector((state: RootState) => state.debtor.list)
    const { data: suggestedDebtors, isLoading: isSuggestedLoading } = useSelector((state: RootState) => state.debtor.suggestedDebtors)

    const [open, setOpen] = useState<boolean>(false);
    const [error, setError] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string>('');

    const handleOpenSelect = () => {
        if (user) {
            dispatch(DebtorAction.fetchDebtorByCompanyCode(user.companyCode))

            if (suggestedName) {
                dispatch(DebtorAction.fetchSuggestedDebtorsByCompanyCode(user.companyCode, [`suggestedName:${encodeURIComponent(suggestedName)}`, `excludeDebtors:${data.map((dC) => dC.value).join(",")}`]))
            }
        }
    }

    const handleOnDebtorCodeChange = (value: string) => {
        if (user) {
            dispatch(DebtorAction.fetchDebtorByCompanyCode(user.companyCode, [`selfSignup:${value}`]))
        }
    }

    const debounceOnChange = debounce(handleOnDebtorCodeChange, 1000)

    const mappedData = data.map((dC) => {
        const splitLabel = dC.label.split(":Group:")

        if (splitLabel.length > 1) {
            return {
                value: dC.value,
                label: splitLabel[1]
            }
        }
        return {
            value: dC.value,
            label: dC.label
        }
    })

    return (
        <Stack
            spacing={1}
            sx={{
                width: "100%"
            }}>
            <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{intl.formatMessage({ id: labelId })}</FormLabel>
            <Autocomplete
                multiple
                disablePortal
                groupBy={(option) => option.label.split(":Group:")[0]}
                options={isLoading
                    ? []
                    : suggestedDebtors.map((debtor: Debtor) => ({
                        value: debtor.code,
                        label: `${intl.formatMessage({ id: "suggested_debtors" })}:Group:${debtor.name_01} (${debtor.code})`,
                    })).concat(debtors.map((debtor: Debtor) => ({
                        value: debtor.code,
                        label: `${intl.formatMessage({ id: "select_debtors" })}:Group:${debtor.name_01} (${debtor.code})`,
                    })))
                }
                getOptionDisabled={(option) => {
                    return !!mappedData.find((dC) => dC.value === option.value)
                }}
                loading={isLoading}
                open={open}
                onOpen={handleOpenSelect}
                onChange={(e, value) => {
                    onChange(value)

                    if (required && value.length === 0) {
                        setError(true)
                        setErrorMessage(intl.formatMessage({ id: "error_debtor_code_required" }))
                    } else {
                        setError(false)
                        setErrorMessage('')
                    }
                }}
                onFocus={() => setOpen(true)}
                onBlur={() => setOpen(false)}
                value={mappedData}
                onInputChange={(e, value) => {
                    if (value !== "") {
                        debounceOnChange(value)
                    }
                }}
                renderInput={(params) => {
                    return <TextField
                        {...params}
                        error={error}
                        helperText={<Typography color="error">{error ? errorMessage : ''}</Typography>}
                    />
                }}
                renderOption={(props, option) => {
                    return (
                        <li {...props}>
                            <Typography>
                                {
                                    option.label.split(":Group:")[1]
                                }
                            </Typography>
                        </li>
                    )
                }}
                renderGroup={(params) => (
                    <li key={params.key}>
                        <Box sx={(theme) => ({
                            position: 'sticky',
                            top: '-8px',
                            padding: '4px 10px',
                            color: "#000",
                            backgroundColor:
                                theme.palette.mode === 'light'
                                    ? theme.palette.primary.light
                                    : theme.palette.primary.main,
                        })}>{params.group}</Box>
                        {params.group === intl.formatMessage({ id: "suggested_debtors" }) && isSuggestedLoading ? (
                            <CircularProgress sx={{
                                p: 1
                            }} />
                        ) : (
                            <ul style={{ padding: 0 }}>{params.children}</ul>
                        )}
                    </li>
                )}
            />
        </Stack>
    )
}


export default DebtorSelector