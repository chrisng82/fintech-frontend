"use client"
import React, { PropsWithChildren, useEffect } from "react"
import { Alert, Snackbar, ThemeProvider } from "@mui/material"
import { IntlProvider } from "react-intl"
import languageObject from "@/translations"
import { Provider, useDispatch, useSelector } from "react-redux"
import { PersistGate } from "redux-persist/integration/react"
import { usePathname, useRouter, useSearchParams } from "next/navigation"
import dayjs from "dayjs"
import utc from "dayjs/plugin/utc"

import createStore, { RootState } from "../../stores"
import theme from "../utils/theme"

import AppAction from "../../stores/App/Action"
import Navbar from "../page/Navbar"
import SnackbarAction from "@/stores/Snackbar/Action"
import useAuth from "../hooks/useAuth"
import { useIntl } from "react-intl"
import DebtorAction from "@/stores/Debtor/Action"
import { useIsDebtorContactAccessible } from "../utils/DebtorContactHelper"
import { useUpdateEffect } from "usehooks-ts"
import { isEnvValueTrue } from "@/helpers"
import AcknowledgementDialog from "../page/Users/Tnc/AcknowledgementDialog"

const { store, persistor } = createStore()

dayjs.extend(utc)

const publicRoutes = [
  "/auth/login",
  "/auth/signup",
  "/not-found",
  "/auth/forgot-password",
  "/auth/reset-password"
]

export function Providers({ children }: PropsWithChildren) {
  const pathname = usePathname()
  const showNavbar = !publicRoutes.includes(pathname)

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={theme}>
          <IntlProvider locale={"en-US"} messages={languageObject["en-US"]}>
            <StartupWrapper>
              {showNavbar && <Navbar />}
              {children}
            </StartupWrapper>
          </IntlProvider>
        </ThemeProvider>
      </PersistGate>
    </Provider>
  )
}

const StartupWrapper: React.FC<PropsWithChildren> = ({ children }) => {
  const dispatch = useDispatch()
  const router = useRouter()
  const pathname = usePathname()
  const searchParams = useSearchParams()
  const { isDebtor } = useAuth()
  const intl = useIntl()
  const { user, token, isLoading, envSettings, openTnc, companyTnc } = useSelector(
    (state: RootState) => state.app
  )
  const { item } = useSelector((state: RootState) => state.debtor)
  const {
    isOpen: isSnackbarOpen,
    message: snackbarMessage,
    snackbarType
  } = useSelector((state: RootState) => state.snackbar)

  useEffect(() => {
    dispatch(AppAction.startup())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  //   useEffect(() => {
  //     if ((apiUrl == null || apiUrl === "") && process.env.NEXT_PUBLIC_BASE_URL) {
  //       window.location.href = `${process.env.NEXT_PUBLIC_BASE_URL}/${pathname}`
  //     }
  //   }, [apiUrl, pathname])

  useEffect(() => {
    if (!publicRoutes.includes(pathname)) {
      if (token === null || token === "" || !user) {
        router.replace("/auth/login")
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, token])

  const onClose = () => dispatch(SnackbarAction.closeSnackbar())

  useUpdateEffect(() => {
    if (!isLoading) {
      const isDebtorContactEnabled = envSettings.DEBTOR_FORCED_CONTACTS
        ? envSettings.DEBTOR_FORCED_CONTACTS
        : false

      if (isDebtorContactEnabled && isDebtor && !publicRoutes.includes(pathname)) {
        if (item.data && user && item.data.id !== user.selectedDebtorId) {
          dispatch(DebtorAction.fetchDebtorById(user.selectedDebtorId))
        }

        if (user && item.data == null) {
          dispatch(DebtorAction.fetchDebtorById(user.selectedDebtorId))
        }

        if (item.data && item.data.contacts.length === 0) {
          router.push("/debtor/profile/contacts")

          dispatch(
            SnackbarAction.openSnackbar(intl.formatMessage({ id: "contacts_is_empty" }), "error")
          )
        }
      }

      if (
        isEnvValueTrue(envSettings.IS_DISABLED_SELF_SIGNUP) &&
        pathname == "/auth/signup" &&
        searchParams.get("ref") == null
      ) {
        router.push("/auth/login")

        dispatch(
          SnackbarAction.openSnackbar(intl.formatMessage({ id: "sign_up_is_disabled" }), "error")
        )
      }
    }
  }, [user?.selectedDebtorId, item.data, pathname])

  return (
    <React.Fragment>
      <Snackbar
        open={isSnackbarOpen}
        onClose={onClose}
        autoHideDuration={3000}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}>
        <Alert severity={snackbarType} onClose={onClose} variant="filled">
          {snackbarMessage}
        </Alert>
      </Snackbar>
      {/* <AcknowledgementDialog
        contents={companyTnc?.tnc?.contents ?? ""}
        open={openTnc ?? false}
        handleOnRefuse={() => {
          dispatch(
            AppAction.logout(() => {
              router.push("/auth/login")
            })
          )
          dispatch(AppAction.setOpenTnc(false))
        }}
        handleOnConfirm={() => {
          if (companyTnc) {
            dispatch(AppAction.setUserTncStatus(companyTnc))
          }
        }}
      /> */}
      {children}
    </React.Fragment>
  )
}
