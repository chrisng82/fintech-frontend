import { createTheme } from "@mui/material"
import { grey, blueGrey, blue } from "@mui/material/colors"

export const palette = {
  primary: {
    light: grey[300],
    main: "#0a150f"
  },
  grey: {
    light: grey[300],
    main: grey[500],
    dark: grey[700]
  },
  secondary: {
    main: "#fff"
  },
  purple: {
    main: "#9155fd",
    light: "#ba94fe",
    lighter: "#b2a5ff",
    lightest: "#ece6f4",
    dark: "#804bdf"
  },
  danger: {
    main: "#f43b1c",
    dark: "#e6381a"
  },
  background: "#f4f5fa"
}

const theme = createTheme({
  palette,
  typography: {
    fontFamily: "REM, sans-serif",
    h5: {
      color: "black",
      fontWeight: 900
    },
    body1: {
      color: "black",
      fontSize: 16,
      fontWeight: 900
    },
    body2: {
      color: blue[700],
      fontSize: 16,
      fontWeight: 900,
      lineHeight: 1.2
    },
    caption: {
      color: grey[500],
      lineHeight: 0
    },
    subtitle1: {
      fontSize: 18,
      color: blue[700],
      lineHeight: 1,
      fontWeight: 900
    },
    subtitle2: {
      fontSize: 14,
      color: blueGrey[500],
      lineHeight: 1,
      fontWeight: 500
    }
  },
  components: {
    MuiButton: {
      // Override text color if the color property is purple
      styleOverrides: {
        root: {
          textTransform: "none",
          borderRadius: 10,
          fontWeight: 900,
          fontSize: 16,
          padding: "10px 20px"
        },
        containedPrimary: {
          color: "#fff",
          backgroundColor: "#9155fd",
          "&:hover": {
            backgroundColor: "#804bdf"
          }
        }
      }
    },

    MuiPaper: {
      styleOverrides: {
        root: {
          backgroundColor: "#fff",
          borderRadius: 10
        }
      }
    },

    MuiList: {
      styleOverrides: {
        root: {
          backgroundColor: "#fff",
          borderRadius: 10,
          boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)"
        }
      }
    },

    // Update MuiButtonBase-root-MuiMenuItem-root
    MuiMenuItem: {
      styleOverrides: {
        root: {
          backgroundColor: "#fff",

          "&:hover": {
            backgroundColor: "#ba94fe",
            color: "#fff"
          }
        }
      }
    },

    MuiDataGrid: {
      styleOverrides: {
        root: {
          border: 1,
          borderColor: palette.grey.light,
          borderStyle: "solid",
          borderRadius: 10,
          boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.25)",
          backgroundColor: palette.grey.dark,
          color: "#C1C2C5",
          padding: 10
        }
      }
    }
  }
})

export default theme
