export const convertNullToUndefined = (obj: Record<string, any>) => {
  const newObj: Record<string, any> = {}
  for (const [key, value] of Object.entries(obj)) {
    newObj[key] = value === null ? undefined : value
  }
  return newObj
}

export const createFilters = <T extends Record<string, any>>(filters: T) => {
  return Object.keys(filters).map((key) => `${key}:${filters[key as keyof T]}`)
}

export const getURLParams = (getData: Record<string, any>) => {
  const urlParams = new URLSearchParams()
  for (const [key, value] of Object.entries(getData)) {
    if (Array.isArray(value)) {
      value.forEach((v) => {
        urlParams.append(`${key}[]`, v)
      })
    } else {
      urlParams.append(key, value)
    }
  }
  return urlParams
}
