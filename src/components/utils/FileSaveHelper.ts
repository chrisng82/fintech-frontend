import FileSaver from "file-saver"

const saveFile = (data: BlobPart, fileName: string): void => {
  const blob = new Blob([data], { type: "application/octet-stream" })

  FileSaver.saveAs(blob, fileName)
}

export default saveFile
