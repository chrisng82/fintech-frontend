import { RootState } from "@/stores"
import { useSelector } from "react-redux"

export const useIsDebtorContactAccessible = () => {
  const { envSettings } = useSelector((state: RootState) => state.app)
  const debtorForcedActions = envSettings.DEBTOR_FORCED_CONTACTS
    ? envSettings.DEBTOR_FORCED_CONTACTS
    : false

  return debtorForcedActions;
}
