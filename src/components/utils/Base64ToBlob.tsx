// Base64 to Blob conversion function
const base64ToBlob = (base64Data: string, mimeType: string): Blob => {
  const byteCharacters = atob(base64Data) // Decoding base64 to binary string
  const byteArrays = []

  // Convert the binary string into an array of bytes
  for (let offset = 0; offset < byteCharacters.length; offset += 1024) {
    const slice = byteCharacters.slice(offset, offset + 1024)
    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }
    byteArrays.push(new Uint8Array(byteNumbers))
  }

  // Create a Blob from the byte arrays with the correct MIME type
  return new Blob(byteArrays, { type: mimeType })
}

export default base64ToBlob
