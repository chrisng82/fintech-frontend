import { RootState } from "@/stores"
import { useSelector } from "react-redux"

export const useIsFeatureAccessible = (feature: string) => {
  const { envSettings } = useSelector((state: RootState) => state.app)
  const disabedFeatures = envSettings.DISABLED_FEATURES
    ? envSettings.DISABLED_FEATURES.split(",")
    : []

  return !disabedFeatures.includes(feature)
}
