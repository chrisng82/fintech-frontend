// eslint-disable-next-line import/no-anonymous-default-export
import { useRouter } from "next/navigation"
import React, { useEffect } from "react"

interface Props {
  navigateTo: string
}

const AutoNavigate: React.FC<Props> = ({ navigateTo }): null => {
  const router = useRouter()
  useEffect(() => {
    router.push(navigateTo)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return null
}

export default AutoNavigate
