import { RootState } from "@/stores"
import { useSelector } from "react-redux"

export const useIsRouteAccessible = (pathname: string) => {
  const { envSettings } = useSelector((state: RootState) => state.app)

  const hiddenRoutes = envSettings.HIDE_ROUTES ? envSettings.HIDE_ROUTES.split(",") : []
  const pathSegments = pathname.split("/")

  const filteredPathSegments = pathSegments.filter(Boolean)

  const rootSegment = filteredPathSegments[0]
  const terminalSegment = filteredPathSegments.slice(1, filteredPathSegments.length).join("/")

  for (const hiddenRoute of hiddenRoutes) {
    const [hiddenRoot, hiddenTerminal] = hiddenRoute.split(":")

    if (hiddenRoot && hiddenTerminal) {
      // Check both the root and terminal segments
      if (rootSegment === hiddenRoot && terminalSegment.includes(hiddenTerminal)) {
        return false
      }
    } else {
      // Check only the specified segment (this behaves like your initial version)
      if (pathname.includes(hiddenRoute)) {
        return false
      }
    }
  }

  return true
}
