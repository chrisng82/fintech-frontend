import NumberToWords from "number-to-words"

export const toProperCase = (text: string, ignoreSymbol: boolean = false) => {
  if (!text) text = ""
  if (!ignoreSymbol) {
    text = text.replace("-", " ")
    text = text.replace("_", " ")
  }
  return text.replace(/\w\S*/g, (txt: string) => {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  })
}

export const numberToRinggitMalaysiaWords = (value: number) => {
  // Split the number into integer and decimal parts
  const [integerPart, decimalPart] = String(value).split(".")

  // Convert the integer part to words
  const integerWords = NumberToWords.toWords(Number(integerPart))
    .replace("minus", "negative")
    .replace(",", "")
    .toUpperCase()

  // If there's no decimal part, just return the integer words
  if (!decimalPart) return integerWords

  // Convert the decimal part to words
  const decimalWords = NumberToWords.toWords(Number(decimalPart)).replace(",", "").toUpperCase()

  // Combine both parts
  return `RINGGIT MALAYSIA ${integerWords} AND ${decimalWords} CENTS`
}
