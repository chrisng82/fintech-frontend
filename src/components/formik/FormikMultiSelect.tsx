import { Box, Chip, FormLabel, MenuItem, Select, Stack, TextField, Typography } from "@mui/material"
import { ErrorMessage, FormikProps, FormikValues } from "formik"
import React, { ReactElement } from "react"

interface Props {
  form: FormikProps<FormikValues>
  field: any
  label: string
  type: string
  options: { label: string, value: string | number }[]
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


const FormikMultiSelect: React.FC<Props> = ({ form, field, label, options, ...rest }): ReactElement => {
  let isError = false
  if (form.errors[field.name] && form.touched[field.name]) {
    isError = true
  }
  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>
      <Select
        {...field}
        {...rest}
        multiple
        error={isError}
        sx={{
          "& .MuiSelect-multiple": {
            backgroundColor: "#fff"
          }
        }}
        renderValue={(selected: any[]) => (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
            {options.filter((option) => selected.includes(option.value)).map((option) => (
              <Chip key={option.value} label={option.label} />
            ))}
          </Box>
        )}
        MenuProps={MenuProps}
      >
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            <Typography>{option.label}</Typography>
          </MenuItem>
        ))}
      </Select>
      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
    </Stack>
  )
}

export default FormikMultiSelect
