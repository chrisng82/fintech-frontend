import { Autocomplete, FormLabel, Stack, TextField, Typography } from "@mui/material"
import { ErrorMessage } from "formik"
import React, { ReactElement } from "react"

interface Props {
  form: any
  field: any
  label: string
  type: string
  options: any[]
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
  maxValues?: number
  getOptionLabel?: (option: any) => string
  optionDisabled?: (option: any) => boolean
  onOpen?: (event: any) => void
  onClose?: (event: any) => void
  debounceOnChange?: (value: any) => void
}

const FormikMultiSearchSelectChipped: React.FC<Props> = ({
  form,
  label,
  options,
  field,
  maxValues,
  getOptionLabel,
  optionDisabled,
  debounceOnChange
}): ReactElement => {
  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>

      <Autocomplete
        multiple
        disablePortal
        options={options}
        onChange={(e, value) => {
          let newValues = [...value]
          if (maxValues) {
            newValues = newValues.slice(-maxValues)
          }

          if (getOptionLabel) {
            form.setFieldValue(field.name, newValues)
          } else {
            const fieldValue = newValues.map((item) =>
              item.hasOwnProperty("value") ? item.value : item
            )
            form.setFieldValue(field.name, fieldValue)
          }
        }}
        onInputChange={(e, value) => {
          if (debounceOnChange) {
            debounceOnChange(value)
          }
        }}
        getOptionLabel={getOptionLabel ? getOptionLabel : (option) => typeof option == 'object' ? option.label : option}
        value={form.values[field.name]}
        renderInput={(params) => <TextField {...params} />}
        getOptionDisabled={optionDisabled}
      />

      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
    </Stack>
  )
}

export default FormikMultiSearchSelectChipped
