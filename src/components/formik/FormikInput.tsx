import { FormLabel, Stack, TextField, Typography } from "@mui/material"
import { ErrorMessage } from "formik"
import React, { ReactElement } from "react"

interface Props {
  form: any
  field: any
  label: string
  type: string
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
}

const FormikInput: React.FC<Props> = ({ form, field, label, ...rest }): ReactElement => {
  let isError = false
  if (form.errors[field.name] && form.touched[field.name]) {
    isError = true
  }
  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>
      <TextField
        {...field}
        {...rest}
        error={isError}
        sx={{
          "& .MuiInputBase-root": {
            backgroundColor: "#fff"
          }
        }}
      />
      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
    </Stack>
  )
}

export default FormikInput
