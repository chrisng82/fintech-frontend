import { ErrorMessage } from "formik"
import { FormLabel, Stack, TextField, Typography } from "@mui/material"
import { DatePicker } from "@mui/x-date-pickers/DatePicker"
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider"

interface Props {
  form: any
  field: any
  label: string
  placeholder?: string
}

const FormikDatePicker: React.FC<Props> = ({ form, field, label, placeholder }) => {
  let isError = false
  if (form.errors[field.name] && form.touched[field.name]) {
    isError = true
  }

  const handleDateChange = (value: any) => {
    form.setFieldValue(field.name, value)
  }

  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DatePicker
          value={field.value}
          onChange={handleDateChange}
          disablePast
          format="DD/MM/YYYY"
        />
      </LocalizationProvider>
      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
      {placeholder && (
        <Typography
          variant="caption"
          color="textSecondary"
          sx={{
            display: "block",
            mt: 2
          }}>
          {placeholder}
        </Typography>
      )}
    </Stack>
  )
}

export default FormikDatePicker
