import React, { ReactElement } from "react"
import { ErrorMessage } from "formik"
import { FormLabel, Input, Stack, Typography } from "@mui/material"
import { initialValues } from "../page/Form/AddCompanyForm"

interface Props {
  form: any
  field: any
  label: string
  type: string
  name: string
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
}

const FormikFile: React.FC<Props> = ({ form, field, label, name, ...rest }): ReactElement => {
  let isError = false
  if (form.errors[field.name] && form.touched[field.name]) {
    isError = true
  }
  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>
      <Input
        {...field}
        {...rest}
        error={isError}
        initialValues={null}
        style={{
          height: 59,
          backgroundColor: "#fff",
          paddingLeft: 14,
          paddingRight: 14,
          borderRadius: "inherit",
          borderStyle: "solid",
          borderWidth: 1,
          overflow: "hidden",
          borderColor: "rgba(0, 0, 0, 0.23)"
        }}
        value={null}
        type="file"
        onChange={(event) => {
          const target = event.target as HTMLInputElement

          if (target.files) {
            form.setFieldValue(field.name, target.files[0])
          }
        }}
      />
      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
    </Stack>
  )
}

export default FormikFile
