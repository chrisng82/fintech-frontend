import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormLabel,
  Stack,
  TextField,
  Typography
} from "@mui/material"
import { ErrorMessage } from "formik"
import React, { ReactElement } from "react"

interface Props {
  form: any
  field: any
  label: string
  type: string
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
}

const FormikCheckbox: React.FC<Props> = ({ form, field, label, ...rest }): ReactElement => {
  let isError = false
  if (form.errors[field.name] && form.touched[field.name]) {
    isError = true
  }
  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormControlLabel label={label} control={<Checkbox />} />
      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
    </Stack>
  )
}

export default FormikCheckbox
