import { Autocomplete, FormLabel, Stack, TextField, Typography } from "@mui/material"
import { ErrorMessage } from "formik"
import React, { ReactElement } from "react"

interface Props {
  form: any
  field: any
  label: string
  type: string
  options: any[]
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
  onOpen?: (event: any) => void
  onClose?: (event: any) => void
  debounceOnChange?: (value: any) => void
}

const FormikSearchSelect: React.FC<Props> = ({
  form,
  label,
  options,
  field,
  onOpen,
  onClose,
  debounceOnChange
}): ReactElement => {
  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>

      <Autocomplete
        disablePortal
        options={options}
        onChange={(e, value) => {
          form.setFieldValue(field.name, value?.value)
        }}
        onInputChange={(e, value) => {
          if (debounceOnChange) {
            debounceOnChange(value)
          }
        }}
        value={form.values[field.name]}
        renderInput={(params) => <TextField {...params} />}
      />

      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
    </Stack>
  )
}

export default FormikSearchSelect
