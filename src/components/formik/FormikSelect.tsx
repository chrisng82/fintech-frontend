import { FormLabel, MenuItem, Select, Stack, TextField, Typography } from "@mui/material"
import { ErrorMessage } from "formik"
import React, { ReactElement } from "react"

interface Props {
  form: any
  field: any
  label: string
  type: string
  options: any[]
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
}

const FormikSelect: React.FC<Props> = ({ form, field, label, options, ...rest }): ReactElement => {
  let isError = false
  if (form.errors[field.name] && form.touched[field.name]) {
    isError = true
  }
  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>
      <Select
        {...field}
        {...rest}
        error={isError}
        sx={{
          "& .MuiSelect-select": {
            backgroundColor: "#fff"
          }
        }}>
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            <Typography>{option.label}</Typography>
          </MenuItem>
        ))}
      </Select>
      <ErrorMessage
        name={field.name}
        render={(msg) => <Typography color="error">{msg}</Typography>}
      />
    </Stack>
  )
}

export default FormikSelect
