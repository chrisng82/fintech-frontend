import { Box, Chip, FormLabel, MenuItem, Select, Stack, TextField, Typography } from "@mui/material"
import { ErrorMessage, FormikProps, FormikValues } from "formik"
import React, { ChangeEvent, KeyboardEvent, ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"

interface Props {
  form: FormikProps<FormikValues>
  field: any
  label: string
  type: string
  options: any[]
  fullWidth?: boolean
  disabled?: boolean
  endAdornment?: ReactElement
}

const FormikMultiSelectChipped: React.FC<Props> = ({
  form,
  field,
  label,
  options,
  ...rest
}): ReactElement => {
  const intl = useIntl()

  const [inputValue, setInputValue] = useState("")
  const [isFocused, setIsFocused] = useState(false)

  let isError = false
  if (
    Object.entries(form.errors).some((error) => {
      return error[0].startsWith(field.name)
    })
  ) {
    isError = true
  }

  useEffect(() => {
    if (!form.values[field.name] || form.values[field.name] === "") {
      setInputValue("")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.values[field.name]])

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value)

    if (e.target.value.endsWith(" ")) {
      const trimmedValue = e.target.value.trim()
      if (trimmedValue !== "") {
        if (form.values[field.name] === "") {
          form.setFieldValue(field.name, trimmedValue)
        } else {
          form.setFieldValue(field.name, `${form.values[field.name]};${trimmedValue}`)
        }
        setInputValue("")
      }
    }
  }

  const handleDeleteEmail = (index: number) => {
    const splitValues = form.values[field.name]
      .split(";")
      .filter((_: string, i: number) => i !== index)
    form.setFieldValue(field.name, splitValues.join(";"))
  }

  const handleKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Backspace" && inputValue === "") {
      const splitValues = form.values[field.name].split(";")

      form.setFieldValue(field.name, splitValues.slice(0, splitValues.length - 1).join(";"))
    }
  }

  return (
    <Stack
      spacing={1}
      sx={{
        width: "100%"
      }}>
      <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>{label}</FormLabel>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          alignItems: "center",
          padding: "16.5px 14px",
          borderStyle: "solid",
          borderWidth: isFocused ? "2px" : "1px",
          borderRadius: "4px",
          borderColor: isError ? "error.main" : isFocused ? "#0a150f" : "rgba(0, 0, 0, 0.23)",
          backgroundColor: "white"
        }}>
        {form.values[field.name] &&
          form.values[field.name].split(";").map((value: string, index: number) => (
            <Chip
              key={`${field.name}_${index}`}
              label={
                <Typography
                  sx={{
                    color: form.errors[`${field.name}_${index}`] ? "white" : "default"
                  }}>
                  {value}
                </Typography>
              }
              onDelete={() => handleDeleteEmail(index)}
              color={form.errors[`${field.name}_${index}`] ? "error" : "default"}
              sx={{
                mr: 1,
                mb: 1
              }}
            />
          ))}
        <TextField
          value={inputValue}
          onChange={handleInputChange}
          onKeyDown={handleKeyDown}
          onFocus={() => setIsFocused(true)}
          onBlur={() => {
            if (inputValue !== "") {
              const trimmedValue = inputValue.trim()

              form.setFieldValue(field.name, `${form.values[field.name]};${trimmedValue}`)
              setInputValue("")
            }

            setIsFocused(false)
          }}
          sx={{
            flex: 1,
            mb: 1,
            "& .MuiInputBase-input": {
              padding: 0
            },
            "& .MuiOutlinedInput-notchedOutline": {
              borderStyle: "none",
              padding: 0
            }
          }}
        />
      </Box>
      <Typography color="error">{form.errors[field.name] as string}</Typography>
      <Typography variant="caption" color="textSecondary">
        {intl.formatMessage({ id: "formik_chipped_input_helper" })}
      </Typography>
    </Stack>
  )
}

export default FormikMultiSelectChipped
