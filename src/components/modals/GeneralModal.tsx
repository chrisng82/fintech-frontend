"use client"

import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"

interface Props {
  open: boolean
  title: string
  content?: string
  children?: ReactElement
  isLoading: boolean
  confirmColor?: 'inherit' | 'primary' | 'secondary' | 'success' | 'error' | 'info' | 'warning'

  onClose?: () => void
  onConfirm?: () => void
}

const GeneralModal: React.FC<Props> = ({
  open,
  title,
  content = "",
  confirmColor = "primary",
  children,
  onClose,
  onConfirm,
  isLoading
}): ReactElement => {
  const intl = useIntl()

  return (
    <Dialog open={open} onClose={onClose} fullWidth>
      <Box
        sx={{
          backgroundColor: "white",
          padding: 2
        }}>
        <DialogTitle
          id="responsive-dialog-title"
          sx={{
            backgroundColor: "white"
          }}>
          {title}
        </DialogTitle>


        {children
          ? children
          : (
            <DialogContent
              sx={{
                backgroundColor: "white"
              }}>
              <DialogContentText>{content}</DialogContentText>
            </DialogContent>
          )
        }

        {(onClose || onConfirm) && (
          <DialogActions
            sx={{
              backgroundColor: "white"
            }}>
            {onClose && (
              <LoadingButton variant="outlined" onClick={onClose} loading={isLoading}>
                {intl.formatMessage({ id: "cancel" })}
              </LoadingButton>
            )}
            {onConfirm && (
              <LoadingButton color={confirmColor} variant="contained" loading={isLoading} onClick={onConfirm}>
                {intl.formatMessage({ id: "confirm" })}
              </LoadingButton>
            )}
          </DialogActions>
        )}
      </Box>
    </Dialog>
  )
}

export default GeneralModal
