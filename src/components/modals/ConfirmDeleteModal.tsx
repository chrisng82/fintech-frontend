"use client"

import { RootState } from "@/stores"
import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import AppAction from "@/stores/App/Action"
import { LoadingButton } from "@mui/lab"
import { useRouter } from "next/navigation"

interface Props {
  open: boolean
  title?: string
  content?: string

  onClose: () => void
  onConfirm: () => void
}

const ConfirmDeleteModal: React.FC<Props> = ({
  open,
  title,
  content,
  onClose,
  onConfirm
}): ReactElement => {
  const intl = useIntl()
  const { isLoading } = useSelector((state: RootState) => state.app)

  return (
    <Dialog open={open} onClose={onClose}>
      <Box
        sx={{
          backgroundColor: "white",
          padding: 2
        }}>
        <DialogTitle
          id="responsive-dialog-title"
          sx={{
            backgroundColor: "white"
          }}>
          {title ?? intl.formatMessage({ id: "confirm_delete" })}
        </DialogTitle>
        <DialogContent
          sx={{
            backgroundColor: "white"
          }}>
          <DialogContentText>
            {content ?? intl.formatMessage({ id: "are_you_sure_delete" })}
          </DialogContentText>
        </DialogContent>
        <DialogActions
          sx={{
            backgroundColor: "white"
          }}>
          <LoadingButton onClick={onClose} loading={isLoading}>
            {intl.formatMessage({ id: "cancel" })}
          </LoadingButton>
          <LoadingButton variant="contained" color="error" loading={isLoading} onClick={onConfirm}>
            {intl.formatMessage({ id: "confirm" })}
          </LoadingButton>
        </DialogActions>
      </Box>
    </Dialog>
  )
}

export default ConfirmDeleteModal
