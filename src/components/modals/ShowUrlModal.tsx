import React from "react"
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Stack,
  TextField,
  Typography
} from "@mui/material"
import { useIntl } from "react-intl"

interface Props {
  open: boolean
  url?: string
  onClose: () => void
}

const ShowUrlModal: React.FC<Props> = ({ open, url, onClose }) => {
  const intl = useIntl()

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      fullWidth>
      <Box
        sx={{
          backgroundColor: "white"
        }}>
        <DialogTitle id="alert-dialog-title">
          <Typography>{intl.formatMessage({ id: "copy_signup_link" })}</Typography>
        </DialogTitle>
        <DialogContent>
          <Stack
            spacing={2}
            sx={{
              justifyContent: "center",
              alignItems: "center"
            }}>
            <DialogContentText id="alert-dialog-description">
              <Typography>{intl.formatMessage({ id: "highlight_copy_signup_link" })}</Typography>
            </DialogContentText>
            <TextField
              inputProps={{ readOnly: true }}
              value={url}
              variant="outlined"
              fullWidth
              multiline
            />
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={onClose}>
            {intl.formatMessage({ id: "close" })}
          </Button>
        </DialogActions>
      </Box>
    </Dialog>
  )
}

export default ShowUrlModal
