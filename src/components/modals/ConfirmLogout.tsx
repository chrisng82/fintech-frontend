"use client"

import { RootState } from "@/stores"
import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import AppAction from "@/stores/App/Action"
import { LoadingButton } from "@mui/lab"
import { useRouter } from "next/navigation"

interface Props {
  open: boolean
  onClose: () => void
}

const ConfirmLogout: React.FC<Props> = ({ open, onClose }): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()
  const { isLoading } = useSelector((state: RootState) => state.app)

  const handleClose = (
    event: React.MouseEvent<HTMLButtonElement>,
    reason: "backdropClick" | "escapeKeyDown"
  ) => {
    if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
      onClose()
    }
  }

  return (
    <Dialog open={open} onClose={handleClose}>
      <Box
        sx={{
          backgroundColor: "white",
          padding: 2
        }}>
        <DialogTitle
          id="responsive-dialog-title"
          sx={{
            backgroundColor: "white"
          }}>
          {intl.formatMessage({ id: "logout" })}
        </DialogTitle>
        <DialogContent
          sx={{
            backgroundColor: "white"
          }}>
          <DialogContentText>{intl.formatMessage({ id: "logout_confirm" })}</DialogContentText>
        </DialogContent>
        <DialogActions
          sx={{
            backgroundColor: "white"
          }}>
          <LoadingButton onClick={onClose} loading={isLoading}>
            {intl.formatMessage({ id: "cancel" })}
          </LoadingButton>
          <LoadingButton
            variant="contained"
            color="error"
            loading={isLoading}
            onClick={() =>
              dispatch(
                AppAction.logout(() => {
                  onClose()
                  router.push("/auth/login")
                })
              )
            }>
            {intl.formatMessage({ id: "confirm" })}
          </LoadingButton>
        </DialogActions>
      </Box>
    </Dialog>
  )
}

export default ConfirmLogout
