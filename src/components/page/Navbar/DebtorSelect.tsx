import { RootState } from "@/stores"
import AppAction from "@/stores/App/Action"
import DebtorAction from "@/stores/Debtor/Action"
import { Debtor } from "@/stores/Debtor/Types"
import {
  Box,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent
} from "@mui/material"
import { useRouter } from "next/navigation"
import React, { ReactElement, useEffect } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

const DebtorSelect: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { user } = useSelector((state: RootState) => state.app)
  const { item } = useSelector((state: RootState) => state.debtor)

  const handleChange = (event: SelectChangeEvent<string>) => {
    if (user && user.debtors !== null) {
        user.debtors.map((debtor: Debtor) => {
            if (debtor.code == event.target.value) {
                dispatch(AppAction.setSelectedDebtorId(debtor.id))
            }
        });
    }
  }

  return (
    <FormControl sx={{ m: 1, minWidth: 120, position: "relative" }} fullWidth>
      <InputLabel id="debtor-select-label">{intl.formatMessage({ id: "debtor" })}</InputLabel>
      <Select
        labelId="company-select-label"
        id="company-select"
        value={item?.data ? item.data.code : ''}
        label={intl.formatMessage({ id: "company" })}
        onChange={handleChange}
        size="small"
        sx={{
          background: "white"
        }}>
        {user?.debtors && user?.debtors?.length == 0 ? (
          <MenuItem
            value=""
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}>
            <CircularProgress size={20} />
          </MenuItem>
        ) : (
          user?.debtors && user.debtors.map((debtor: Debtor) => (
            <MenuItem key={`${debtor.name_01}-${debtor.code}`} value={debtor.code}>  
              {debtor.code}
            </MenuItem>
          ))
        )}
      </Select>
      {user?.debtors && user?.debtors?.length == 0 && (
        <Box
          sx={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(255, 255, 255, 0.7)"
          }}>
          <CircularProgress size={20} />
        </Box>
      )}
    </FormControl>
  )
}

export default DebtorSelect
