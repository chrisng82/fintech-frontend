import { palette } from "@/components/utils/theme"
import { Menu } from "@/constants/SidebarMenu"
import { ListItem, ListItemButton, ListItemIcon, ListItemText, Typography } from "@mui/material"
import { usePathname, useRouter } from "next/navigation"
import React, { ReactElement } from "react"

interface Props {
  menu: Menu
  handleDrawerClose: () => void
}

const MenuItem: React.FC<Props> = ({ menu, handleDrawerClose }): ReactElement => {
  const router = useRouter()
  const pathname = usePathname()

  return (
    <ListItem
      disablePadding
      onClick={() => {
        handleDrawerClose()
      }}
      sx={{
        background: pathname.startsWith(menu.link!) ? palette.purple.main : "transparent",
        borderRadius: "0 2rem 2rem 0",
        width: "224px",
        padding: "8px 16px",
        margin: "4px 0",
        transition: "all 0.3s cubic-bezier(.25,.8,.25,1)",
        cursor: "pointer",

        "& p": {
          color: pathname.startsWith(menu.link!) ? "#fff" : "inherit"
        },

        "&:hover": {
          background: palette.purple.main,

          "& p": {
            color: "#fff"
          }
        }
      }}>
      {menu.icon && <ListItemIcon>{menu.icon}</ListItemIcon>}
      <ListItemText
        primary={
          <Typography
            sx={{
              fontWeight: 500
            }}>
            {menu.label}
          </Typography>
        }
      />
    </ListItem>
  )
}

export default MenuItem
