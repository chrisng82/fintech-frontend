"use client"
import {
  Box,
  Divider,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Stack,
  Typography
} from "@mui/material"
import React, { ReactElement, useState } from "react"
import { useSelector } from "react-redux"
import { useRouter } from "next/navigation"
import { useIntl } from "react-intl"
import UserAvatar from "../Avatar/UserAvatar"
import { RootState } from "@/stores"
import LogoutIcon from "@mui/icons-material/Logout"
import ConfirmLogout from "@/components/modals/ConfirmLogout"
import SettingsIcon from "@mui/icons-material/Settings"
import PersonIcon from '@mui/icons-material/Person';
import Link from "next/link"
import useAuth from "@/components/hooks/useAuth"

const UserMenu: React.FC = (): ReactElement | null => {
  const router = useRouter()
  const intl = useIntl()
  const { user } = useSelector((state: RootState) => state.app)

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null)
  const [openLogoutModal, setOpenLogoutModal] = useState<boolean>(false)

  const handleCloseUserDropdown = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)

  const handleOpenUserDropdown = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const { isDebtor } = useAuth()

  if (!user) return null

  return (
    <Box
      sx={{
        position: "absolute",
        right: 0,
        mr: 2
      }}>
      <IconButton onClick={handleOpenUserDropdown}>
        <UserAvatar name={`${user.first_name} ${user.last_login}`} />
      </IconButton>
      <Menu
        id="user-dropdown"
        open={open}
        anchorEl={anchorEl}
        onClose={handleCloseUserDropdown}
        onClick={handleCloseUserDropdown}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center"
        }}>
        <MenuItem
          onClick={(event) => {
            event.stopPropagation()
          }}
          sx={{
            cursor: "default",
            "&:hover": {
              backgroundColor: "transparent"
            },

            "& .MuiTouchRipple-root": {
              display: "none"
            }
          }}>
          <ListItemIcon>
            <UserAvatar name={`${user.first_name} ${user.last_login}`} />
          </ListItemIcon>
          <Stack ml={2}>
            <Typography>{user.first_name}</Typography>
            <Typography variant="subtitle2">{user.type_str}</Typography>
          </Stack>
        </MenuItem>
        <Divider />
        <Link href="#">
          <MenuItem disabled>
            <ListItemIcon>
              <SettingsIcon fontSize="small" />
            </ListItemIcon>
            <Typography>
              {intl.formatMessage({
                id: "settings"
              })}
              &nbsp;({intl.formatMessage({ id: "coming_soon" })})
            </Typography>
          </MenuItem>
        </Link>
        {isDebtor && (
          <Link href="/debtor/profile/summary">
            <MenuItem>
              <ListItemIcon>
                <PersonIcon fontSize="small" />
              </ListItemIcon>
              <Typography>
                {intl.formatMessage({
                  id: "profile"
                })}
              </Typography>
            </MenuItem>
          </Link>
        )}
        <MenuItem onClick={() => setOpenLogoutModal(true)}>
          <ListItemIcon>
            <LogoutIcon fontSize="small" />
          </ListItemIcon>
          <Typography>
            {intl.formatMessage({
              id: "logout"
            })}
          </Typography>
        </MenuItem>
      </Menu>
      <ConfirmLogout open={openLogoutModal} onClose={() => setOpenLogoutModal(false)} />
    </Box>
  )
}

export default UserMenu
