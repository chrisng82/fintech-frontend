"use client"
import { AppBar, Box, Divider, Drawer, IconButton, List, Toolbar, Typography } from "@mui/material"
import React, { ReactElement, useState } from "react"
import UserMenu from "./UserMenu"
import MenuIcon from "@mui/icons-material/Menu"
import Logo from "../Logo"
import { palette } from "@/components/utils/theme"
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft"
import { Menu, sidebarMenuGroups } from "@/constants/SidebarMenu"
import { useIntl } from "react-intl"
import MenuItem from "./MenuItem"
import CompanySelect from "./CompanySelect"
import Link from "next/link"
import { useSelector } from "react-redux"
import useAuth from "@/components/hooks/useAuth"
import DebtorSelect from "./DebtorSelect"

const drawerWidth = 240

const Navbar: React.FC = (): ReactElement | null => {
  const intl = useIntl()

  const { isDebtor } = useAuth()

  const { user } = useSelector((state: any) => state.app)

  const [open, setOpen] = useState<boolean>(false)

  const handleDrawerOpen = () => {
    setOpen(true)
  }

  const handleDrawerClose = () => {
    setOpen(false)
  }

  return (
    <React.Fragment>
      <AppBar
        position="fixed"
        sx={{
          background: "#ece6f4",
          margin: "5px",
          width: open ? `calc(100% - ${drawerWidth}px - 24px)` : `calc(100% - 16px)`,
          transition: "all 0.3s cubic-bezier(.25,.8,.25,1)"
        }}>
        <Toolbar
          sx={{
            position: "relative"
          }}>
          <IconButton
            id="open-drawer-button"
            onClick={handleDrawerOpen}
            sx={{
              display: open ? "none" : "flex",
              mr: 2,
              alignItems: "center",
              color: "white"
            }}>
            <MenuIcon />
          </IconButton>

          <Link href="/">
            <Box
              sx={{
                cursor: "pointer"
              }}>
              <Logo />
            </Box>
          </Link>

          <UserMenu />
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            margin: "8px",
            width: drawerWidth,
            boxSizing: "border-box",
            border: "1px solid lightgrey",
            height: "calc(100% - 16px)",
            background: palette.purple.lightest
          },

          "& .MuiBackdrop-root": {
            backgroundColor: "transparent"
          }
        }}
        PaperProps={{
          elevation: 3
        }}
        onClose={handleDrawerClose}
        anchor="left"
        open={open}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            padding: "8px",
            justifyContent: "flex-end"
          }}>
          <Box
            sx={{
              display: "inline",
              width: "100%",
              padding: "8px"
            }}>
            <CompanySelect />
            {isDebtor && <DebtorSelect />}
          </Box>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </Box>
        <List
          sx={{
            boxShadow: "none",
            background: "transparent"
          }}>
          {sidebarMenuGroups(intl).map((menu: Menu) => {
            return menu.user_type?.includes(user?.type_str) ? (
              <React.Fragment key={`group-${menu.key}`}>
                <Divider
                  textAlign="center"
                  sx={{
                    m: 1
                  }}>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      fontSize: "0.8rem"
                    }}>
                    {menu.label}
                  </Typography>
                </Divider>
                {menu.children!.map((child: Menu) => {
                  return child.user_type?.includes(user?.type_str) ? (
                    <Link href={child.link ?? "#"} key={child.key}>
                      <MenuItem menu={child} handleDrawerClose={handleDrawerClose} />
                    </Link>
                  ) : null
                })}
              </React.Fragment>
            ) : null
          })}
        </List>
      </Drawer>
    </React.Fragment>
  )
}

export default Navbar
