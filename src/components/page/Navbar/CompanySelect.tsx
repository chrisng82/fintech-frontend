import { RootState } from "@/stores"
import AppAction from "@/stores/App/Action"
import CompanyAction from "@/stores/Company/Action"
import { CompanyOption } from "@/stores/Company/Types"
import {
  Box,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent
} from "@mui/material"
import { useRouter } from "next/navigation"
import React, { ReactElement, useEffect } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

const CompanySelect: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { user } = useSelector((state: RootState) => state.app)
  const { data, isLoading } = useSelector((state: RootState) => state.company.companySelect)

  useEffect(() => {
    dispatch(CompanyAction.fetchCompanySelect())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleChange = (event: SelectChangeEvent<string>) => {
    if (user) {
      dispatch(AppAction.changeCompanySelect(event.target.value, user))
    }
  }

  return (
    <FormControl sx={{ m: 1, minWidth: 120, position: "relative" }} fullWidth>
      <InputLabel id="company-select-label">{intl.formatMessage({ id: "company" })}</InputLabel>
      <Select
        labelId="company-select-label"
        id="company-select"
        value={user?.companyCode}
        label={intl.formatMessage({ id: "company" })}
        onChange={handleChange}
        size="small"
        sx={{
          background: "white"
        }}>
        {isLoading ? (
          <MenuItem
            value=""
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}>
            <CircularProgress size={20} />
          </MenuItem>
        ) : (
          data.map((company: CompanyOption) => (
            <MenuItem key={`${company.label}-${company.value}`} value={company.label}>
              {company.label}
            </MenuItem>
          ))
        )}
      </Select>
      {isLoading && (
        <Box
          sx={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(255, 255, 255, 0.7)"
          }}>
          <CircularProgress size={20} />
        </Box>
      )}
    </FormControl>
  )
}

export default CompanySelect
