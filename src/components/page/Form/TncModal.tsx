"use client"

import {
  Box,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  FormControlLabel,
  Stack,
  Typography
} from "@mui/material"
import React, { ReactElement, useEffect, useRef, useState } from "react"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"
import Link from "next/link"
import { SelfSignupFormValues } from "./SelfSignupForm"
import { Field, Form, useFormikContext } from "formik"
import FormikInput from "@/components/formik/FormikInput"
import { isEmpty } from "lodash"

interface Props {
  open: boolean
  isAcknowledge: boolean
  isLoading?: boolean
  confirmColor?: "inherit" | "primary" | "secondary" | "success" | "error" | "info" | "warning"

  onAcknowledgeChange: () => void
  onClose: () => void
  onConfirm: () => void
}

const normalText = {
  fontSize: "1rem",
  lineHeight: "1.5rem",
  fontWeight: "normal"
}

const underlineTitle = {
  textDecoration: "underline"
}

const privacyNoticeList = [
  "Providing our services;",
  "Processing transactions and fulfilling orders;",
  "Sending newsletters and promotional materials;",
  "Analysing and improving our products and services;",
  "Responding to your inquiries and providing customer support;",
  "Sharing Company's products information; and",
  "Promoting Company's products and services."
]

const privacyNoticeBMList = [
  "Menyediakan perkhidmatan kami;",
  "Memproses transaksi dan melaksanakan pesanan;",
  "Menghantar buletin dan bahan promosi;",
  "Menganalisis dan meningkatkan produk dan perkhidmatan kami;",
  "Menjawab pertanyaan anda dan menyediakan sokongan pelanggan;",
  "Menghantar maklumat produk kami; dan",
  "Mempromosikan produk dan perkhidmatan kami."
]

const TncModal: React.FC<Props> = ({
  open,
  onClose,
  onConfirm,
  isLoading,
  isAcknowledge,
  onAcknowledgeChange
}): ReactElement => {
  const intl = useIntl()
  const dialogRef = useRef<HTMLDivElement | null>(null)
  const form = useFormikContext<SelfSignupFormValues>()

  useEffect(() => {
    if (open && dialogRef.current) {
      dialogRef.current.scrollTop = 0
    }
  }, [open])

  useEffect(() => {
    if (isAcknowledge) {
      form.setFieldValue("tnc_confirmation.company_name", "your company name")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAcknowledge])

  const appTitle = process.env.NEXT_PUBLIC_APP_TITLE

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth="lg">
      <Box
        ref={dialogRef}
        sx={{
          backgroundColor: "white",
          padding: 2,
          maxHeight: "80vh"
        }}>
        <DialogContent
          sx={{
            display: "flex",
            flexDirection: "column",
            gap: "1rem"
          }}>
          <Typography
            sx={{
              textAlign: "center"
            }}>
            Terms of Use
          </Typography>
          <Typography sx={normalText}>
            Welcome to {appTitle}! Throughout the site, the terms &quot;we&quot;, &quot;us&quot; and
            &quot;our&quot; refer to {appTitle}. {appTitle} (&quot;the Company&quot;) offers this
            website, including all information, tools and services available from this site to you,
            the user, conditioned upon your acceptance of all terms, conditions, policies and
            notices stated here. By accessing or using our website, you agree to comply with and be
            bound by these terms.
          </Typography>
          <Typography>Section 1: Acceptance of Terms</Typography>
          <Typography sx={normalText}>
            By accessing or using the Company&apos;s website (the &quot;Site&quot;), you agree to be
            bound by these Terms of Use, our Privacy Policy, and all applicable laws and
            regulations. If you do not agree with any of the Terms of Use outlined herein, kindly
            refrain from proceeding with the use of this Site.
          </Typography>
          <Typography>Section 2: Use of the Site</Typography>
          <Typography sx={normalText}>You must be 18 years or older to use the Site.</Typography>
          <Typography sx={normalText}>
            You bear full responsibility for safeguarding the confidentiality of the information
            associated with your account, encompassing your password, and for all activities arising
            from any lapses in maintaining the security and confidentiality of this information. It
            is imperative that you promptly inform the Company of any unauthorized access to your
            account, password, or any security breach. Failure to uphold the security and
            confidentiality of your account information may render you liable for losses suffered by
            the Company or any other user or visitor to the Site, stemming from the misuse of your
            user ID, password, or account due to your negligence in maintaining the required
            security measures.
          </Typography>
          <Typography sx={normalText}>
            You agree not to engage in any activity that interferes with or disrupts the Site, or
            the servers and networks connected to the Site.
          </Typography>
          <Typography sx={normalText}>
            Any action, representation, or transaction undertaken by your representatives on this
            Site (including but not limited to directors, officers, employees, agents, attorneys,
            accountant, consultants), whether expressly authorised or not, shall be considered an
            act on your behalf and shall be binding upon you. By acknowledging and committing to
            ratify all such actions, you accept responsibility for any consequences arising from
            these activities. It is recognised that the authority granted to your representatives on
            this Site is affirmed, and any actions conducted within the scope of their employment
            are assumed to be carried out with your implied or express authority. All commitments,
            obligations, and agreements resulting from the actions of your representatives on this
            Site shall be deemed to have the same legal effect as if directly performed by the you
            and your authorised director.
          </Typography>
          <Typography>Section 3: Customer Account</Typography>
          <Typography sx={normalText}>
            As the account applicant, you are responsible for and agree to indemnify the Company for
            any losses incurred due to orders placed by you, your employees, agents, or
            representatives, whether in writing or otherwise.
          </Typography>
          <Typography sx={normalText}>
            Adherence to the credit terms granted by the Company is crucial. Failure to comply may
            result in the imposition of a 1.5% per month interest charge on outstanding debts. The
            Company reserves the right to determine this rate at its discretion. Statements provided
            by the Company serve as final evidence, and you unconditionally agree to pay any
            interest charges.
          </Typography>
          <Typography sx={normalText}>
            The Company may, at its absolute discretion and without notice, vary the credit terms
            granted to you.
          </Typography>
          <Typography sx={normalText}>
            The Company reserves the right to vary or terminate the credit facility for reasons
            including non-compliance with payment terms or for any other reasons whatsoever. The
            Company reserves the right not to disclose any reason for such variation and/or
            termination.
          </Typography>
          <Typography sx={normalText}>
            In case of termination, you agree to settle all outstanding payments promptly and return
            all advertising materials within one month from the termination date.
          </Typography>
          <Typography sx={normalText}>
            Statements issued by the Company, whether in softcopy through registered email, this
            Site, or hardcopy via normal or registered post, are considered final and conclusive
            evidence of any outstanding debts. Failure to address discrepancies within two weeks of
            receipt validates the statement.
          </Typography>
          <Typography sx={normalText}>
            The Company, at its absolute discretion, may discontinue the supply of goods or decline
            orders due to circumstances beyond the Company&apos;s control, including but not limited
            to raw material shortages, supply chain disruption, machinery breakdown, electricity
            disruption, fire, flood or any reasons thereto. No legal proceedings or recourse shall
            be sought against the Company for such actions.
          </Typography>
          <Typography sx={normalText}>
            The Company reserves the right to revise pricing and discount structures at any time and
            in any manner without prior notice.
          </Typography>
          <Typography sx={normalText}>
            The Company reserves the right to reject Customer Account applications at its discretion
            without providing any reasons.
          </Typography>
          <Typography>Section 4: Electronic Commerce Act 2006</Typography>
          <Typography sx={normalText}>
            Electronic communication and transactions are parts of the Company’s service delivery.
            To safeguard your personal data, all electronic storage and transmission of personal
            data are secured and stored with appropriate security technologies.
          </Typography>
          <Typography sx={normalText}>
            Given the possibility of e-signatures in electronic transactions during the access to
            the Site, it is essential to note that the Electronic Commerce Act 2006 in Malaysia
            recognizes and governs the use of electronic signatures. Under this act, electronic
            signatures are considered legally binding, providing a framework for the legality and
            enforceability of electronic contracts. Therefore, any electronic signatures associated
            with the Customer Account application process on the Company&apos;s website are subject
            to the provisions of the Electronic Commerce Act 2006.
          </Typography>
          <Typography>Section 5: Personal Information</Typography>
          <Typography sx={normalText}>
            The submission of personal information through the Site is subject to the regulations
            outlined in our Privacy Policy, accessible by clicking here. In addition, your use of
            the Site constitutes acknowledgment and agreement that internet transmissions inherently
            lack complete privacy or security. It is essential to recognize that any message or
            information transmitted to the Site may be susceptible to reading or interception by
            third parties, notwithstanding the presence of a specific notice indicating encryption
            for a particular transmission. This understanding underscores the dynamic nature of
            online communication and emphasizes the need for cautious discretion when sharing
            sensitive information through the Site.
          </Typography>
          <Typography>Section 6: Intellectual Property</Typography>
          <Typography sx={normalText}>
            The entirety of the content hosted on the Site, encompassing but not limited to text,
            graphics, logos, images, and software, is unequivocally recognized as the exclusive
            property of the Company and is safeguarded by the prevailing intellectual property laws
            in Malaysia.
          </Typography>
          <Typography sx={normalText}>
            You are strictly prohibited from engaging in the reproduction, distribution, display, or
            transmission of any content found on the Site. This prohibition applies to any medium,
            whether electronic, mechanical, photocopying, recording, or otherwise, and extends to
            both commercial and non-commercial use.
          </Typography>
          <Typography sx={normalText}>
            Furthermore, any unauthorized use encompasses any attempts to modify, adapt, reverse
            engineer, or create derivative works based on the content available on the Site shall be
            deemed as an infringement upon the intellectual property rights of the Company and are
            subject to legal recourse as stipulated by the intellectual property laws of Malaysia.
          </Typography>
          <Typography>Section 7: Disclaimer</Typography>
          <Typography sx={normalText}>
            We do not guarantee, represent, or warrant that your utilization of our service will be
            continuous, punctual, impervious to security breaches, or devoid of errors.
            Additionally, we do not assert that the outcomes derived from the use of the Site will
            be unfailingly accurate or reliable.
          </Typography>
          <Typography sx={normalText}>
            Acknowledging the inherent uncertainties of technological services, you expressly
            consent to the occasional removal of the Site for indefinite durations or the
            termination of the Site at any time, without prior notice to you. It is explicitly
            agreed that your engagement with the Site, or any difficulties you encounter in its use,
            are undertaken entirely at your sole risk. By utilizing the service, you explicitly
            acknowledge and accept the aforementioned disclaimers, absolving us from any liability
            stemming from the utilization or unavailability of the Site, and recognizing that such
            utilization is at your own risk.
          </Typography>
          <Typography>Section 8: Limitation of Liability</Typography>
          <Typography sx={normalText}>
            In no case shall the Company, its directors, officers, employees, affiliates, agents,
            contractors, suppliers, service providers and other representatives be liable for any
            injury, loss, claim, or any direct, indirect, incidental, punitive, special, or
            consequential damages of any kind, including, without limitation lost profits, lost
            revenue, lost savings, loss of data, replacement costs, or any similar damages, whether
            based in contract, tort (including negligence), strict liability or otherwise, arising
            from your use of any of the service or any products procured using the Site and/or
            Customer Account, or for any other claim related in any way to your use of the Site
            and/or Customer Account, including, but not limited to, any errors or omissions in any
            content, or any loss or damage of any kind incurred as a result of the use of the Site
            and/or Customer Account, transmitted, or otherwise made available via the Site and/or
            Customer Account, even if advised of their possibility.
          </Typography>
          <Typography>Section 9: Indemnification</Typography>
          <Typography sx={normalText}>
            You agree to indemnify and hold the Company, its officers, directors, shareholders,
            predecessors, successors and assigns, employees, agents, subsidiaries and affiliates,
            harmless from any demands, loss, liability, claims or expenses (including attorneys’
            fees), made against the Company by any third party due to or arising out of or in
            connection with your use of the Site.
          </Typography>
          <Typography>Section 10: Termination</Typography>
          <Typography sx={normalText}>
            We reserve the right to terminate or suspend your account and access to the Site at our
            sole discretion, without prior notice, for any reason.
          </Typography>
          <Typography>Section 11: Severability</Typography>
          <Typography sx={normalText}>
            If any provision hereof shall be held, for any reason, to be illegal, invalid or non-
            enforceable, the remaining provisions shall nonetheless be legal, valid and enforceable
            provisions.
          </Typography>
          <Typography>Section 12: Governing Law</Typography>
          <Typography sx={normalText}>
            These Terms of Use are governed by and construed in accordance with the laws of
            Malaysia.
          </Typography>
          <Typography>Section 13: Modifications</Typography>
          <Typography sx={normalText}>
            The Company reserves the right, at its sole discretion, to change, modify, add or remove
            portions of these Terms of Use, at any time. It is your responsibility to check these
            Terms of Use periodically for changes. Your continued use of the Site following the
            posting of changes will mean that you accept and agree to the changes. As long as you
            comply with these Terms of Use, the Company grants you a personal, non- exclusive,
            non-transferable, limited privilege to enter and use the Site.
          </Typography>
          <Typography>Section 14: Contact Information</Typography>
          <Typography sx={normalText}>
            If you have any questions or concerns about these Terms of Use, please contact us
            at&nbsp;
            <span style={{ color: "blue" }}>
              <Link href="mailto:mc.koh@teoseng.com.my">ccd@teoseng.com.my</Link>
            </span>
            .
          </Typography>

          <Typography
            mt={8}
            sx={{
              ...underlineTitle,
              textAlign: "center"
            }}>
            Privacy Notice
            <br />
            Pursuant to the Personal Data Protection Act 2010
          </Typography>
          <Typography sx={normalText}>
            Considering the Personal Data Protection Act 2010 (&quot;PDPA&quot;), you possess the
            right to be informed about the reasons for the collection of your personal data and the
            methods by which we may disclose such information. Additionally, you hold the right to
            access the personal data retained by us, allowing you to rectify or update this
            information or control the processing of your personal data by us. Through this notice,
            we aim to communicate the procedures involved in the processing of your personal data by
            our company.
          </Typography>

          <Typography sx={underlineTitle}>
            Purposes of Collecting and Processing of Personal Data
          </Typography>
          <Typography sx={normalText}>
            We may collect and process your personal data for the business purposes relating to Site
            and/or Customer Account and for other purposes which include but is not limited to the
            following:
          </Typography>

          <ol
            style={{
              counterReset: "item",
              listStyleType: "none"
            }}>
            {privacyNoticeList.map((item, index) => (
              <li
                key={index}
                style={{
                  counterIncrement: "item",
                  marginBottom: "5px"
                }}>
                <Typography
                  sx={{
                    ...normalText,
                    "::before": {
                      content: `"${String.fromCharCode(97 + index)}) "`,
                      paddingRight: "2em"
                    }
                  }}>
                  {item}
                </Typography>
              </li>
            ))}
          </ol>

          <Typography sx={underlineTitle}>Sharing of Personal Data</Typography>
          <Typography sx={normalText}>
            We do not sell, trade, or rent your personal information to third parties. However, we
            may share your information with trusted partners and service providers who assist us in
            delivering our products and services when necessary.
          </Typography>

          <Typography sx={underlineTitle}>Choice to Provide Personal Data</Typography>
          <Typography sx={normalText}>
            The provision of your personal data to us is voluntary. However, if you do not provide
            your personal data to us, you may not be able to enjoy the services that we provided for
            via the Site and/or Customer Account.
          </Typography>

          <Typography sx={underlineTitle}>Your Consent</Typography>
          <Typography sx={normalText}>
            By continuing to use our Site, you are hereby deemed to have consented to us processing
            your personal data in the manner identified in this notice. However, if you wish to
            withdraw or limit your consent, kindly notify us immediately.
          </Typography>

          <Typography sx={underlineTitle}>
            Access, Correction Request, Inquiries or Complaints about Personal Data
          </Typography>
          <Typography sx={normalText}>
            Under the PDPA, you are entitled to request access to, correct your personal data, make
            inquiries or complaints about your personal data and limit the manner in which your
            personal data is processed by contacting us as follows:
          </Typography>

          <ul style={{ listStyle: "none", marginLeft: "3.5rem" }}>
            <li
              style={{
                marginBottom: "5px"
              }}>
              <Typography sx={normalText}>
                Email:
                <br />
                <span style={{ color: "blue" }}>
                  <Link href="mailto:ccd@teoseng.com.my">ccd@teoseng.com.my</Link>.
                </span>
              </Typography>
            </li>
            <li
              style={{
                marginBottom: "5px"
              }}>
              <Typography sx={normalText}>
                Contact Number:
                <br />
                +6013-7703908
              </Typography>
            </li>
            <li
              style={{
                marginBottom: "5px"
              }}>
              <Typography sx={normalText}>
                Address:
                <br />
                Lot PTD 25740, Batu 4, Jalan Air Hitam, 83700 Yong Peng, Johor
              </Typography>
            </li>
          </ul>

          <Typography sx={normalText}>
            Please note that under the PDPA, we have the right to refuse your requests to access or
            correct your personal data for certain reasons permitted under the PDPA. Also, we have
            the right to charge an administrative fee for responding to your requests.
          </Typography>

          <Typography sx={underlineTitle}>Our Commitment</Typography>
          <Typography sx={normalText}>
            We are dedicated to ensuring the security and confidentiality of your personal data at
            all times. Our dedication to safeguarding your information involves implementing
            rigorous physical and electronic measures to prevent unauthorized access. To align with
            our contextual needs, we retain the right to periodically update and modify this notice.
            We assure you that any amendments will be made with the utmost consideration for your
            privacy and in accordance with applicable laws and regulations.
          </Typography>

          <Typography
            mt={8}
            sx={{
              ...underlineTitle,
              textAlign: "center"
            }}>
            Notis Peribadi
            <br />
            Menurut Akta Perlindungan Data Peribadi 2010
          </Typography>
          <Typography sx={normalText}>
            Berdasarkan Akta Perlindungan Data Peribadi 2010 (&quot;PDPA&quot;), anda mempunyai hak
            untuk diberitahu tentang tujuan pengumpulan data peribadi anda dan cara kami mendedahkan
            maklumat tersebut. Selain itu, anda mempunyai hak untuk mengakses data peribadi yang
            kami simpan membolehkan anda membetulkan atau mengemaskini maklumat ini atau mengawal
            pemprosesan data peribadi anda. Melalui notis ini, kami akan menyampaikan prosedur yang
            terlibat dalam pemprosesan data peribadi anda oleh syarikat kami.
          </Typography>

          <Typography sx={underlineTitle}>
            Tujuan Pengumpulan dan Pemprosesan Data Peribadi
          </Typography>
          <Typography sx={normalText}>
            Kami mengumpul dan memproses data peribadi anda untuk tujuan perniagaan yang berkaitan
            dengan Laman Web dan/atau Akaun Pelanggan serta tujuan lain yang termasuk tetapi tidak
            terhad kepada yang berikut:
          </Typography>

          <ol
            style={{
              counterReset: "item",
              listStyleType: "none"
            }}>
            {privacyNoticeBMList.map((item, index) => (
              <li
                key={index}
                style={{
                  counterIncrement: "item",
                  marginBottom: "5px"
                }}>
                <Typography
                  sx={{
                    ...normalText,
                    "::before": {
                      content: `"${String.fromCharCode(97 + index)}) "`,
                      paddingRight: "2em"
                    }
                  }}>
                  {item}
                </Typography>
              </li>
            ))}
          </ol>

          <Typography sx={underlineTitle}>Pendedahan Data Peribadi</Typography>
          <Typography sx={normalText}>
            Kami tidak menjual, berdagang, atau menyewakan maklumat peribadi anda kepada pihak
            ketiga. Walau bagaimanapun, kami boleh berkongsi maklumat anda dengan rakan yang
            dipercayai dan pembekal perkhidmatan yang membantu kami dalam penyampaian produk dan
            perkhidmatan kami jika diperlukan.
          </Typography>

          <Typography sx={underlineTitle}>Pilihan Untuk Membekalkan Data Peribadi</Typography>
          <Typography sx={normalText}>
            Pembekalan data peribadi anda kepada kami adalah secara sukarela. Walau bagaimanapun,
            jika anda tidak membekalkan data peribadi anda kepada kami, anda mungkin tidak dapat
            menikmati perkhidmatan yang disediakan melalui Laman Web dan/atau Akaun Pelanggan.
          </Typography>

          <Typography sx={underlineTitle}>Persetujuan Anda</Typography>
          <Typography sx={normalText}>
            Dengan menggunakan Laman Web kami, anda dianggap telah bersetuju dengan pemprosesan data
            peribadi anda sebagaimana yang dikenalpasti dalam notis peribadi ini. Walau
            bagaimanapun, jika anda ingin menarik balik atau mengehadkan persetujuan anda, sila
            beritahu kami dengan segera.
          </Typography>

          <Typography sx={underlineTitle}>
            Akses, Permintaan Pembetulan, Pertanyaan atau Aduan Mengenai Data Peribadi
          </Typography>
          <Typography sx={normalText}>
            Di bawah PDPA, anda berhak untuk meminta akses, membetulkan data peribadi anda, membuat
            pertanyaan atau aduan mengenai data peribadi anda dan membataskan cara data peribadi
            anda diproses dengan menghubungi kami seperti berikut:
          </Typography>

          <ul style={{ listStyle: "none", marginLeft: "3.5rem" }}>
            <li
              style={{
                marginBottom: "5px"
              }}>
              <Typography sx={normalText}>
                Email:
                <br />
                <span style={{ color: "blue" }}>
                  <Link href="mailto:ccd@teoseng.com.my">ccd@teoseng.com.my</Link>.
                </span>
              </Typography>
            </li>
            <li
              style={{
                marginBottom: "5px"
              }}>
              <Typography sx={normalText}>
                No. Telephone:
                <br />
                +6013-7703908
              </Typography>
            </li>
            <li
              style={{
                marginBottom: "5px"
              }}>
              <Typography sx={normalText}>
                Alamat:
                <br />
                Lot PTD 25740, Batu 4, Jalan Air Hitam, 83700 Yong Peng, Johor
              </Typography>
            </li>
          </ul>

          <Typography sx={normalText}>
            Sila ambil perhatian bahawa di bawah PDPA, kami berhak untuk menolak permintaan anda
            untuk mengakses atau membetulkan data peribadi anda atas alasan tertentu yang dibenarkan
            di bawah PDPA. Selain itu, kami berhak untuk mengenakan bayaran pentadbiran untuk
            memberi jawapan kepada permintaan anda.
          </Typography>

          <Typography sx={underlineTitle}>Komitmen Kami</Typography>
          <Typography sx={normalText}>
            Kami berazam untuk memastikan keselamatan dan kerahsiaan data peribadi anda pada setiap
            masa. Komitmen kami untuk melindungi maklumat anda melibatkan pelaksanaan
            langkah-langkah fizikal dan elektronik yang ketat untuk mengelakkan akses yang tidak
            dibenarkan. Untuk menyesuaikan dengan keperluan kontekstual kami, kami menyimpan hak
            untuk mengemaskini dan mengubahsuai notis ini dari semasa ke semasa. Kami memberi
            jaminan bahawa sebarang pindaan akan dibuat dengan pertimbangan yang sangat tinggi
            terhadap peribadi anda dan selaras dengan undang-undang dan peraturan yang berkenaan.
          </Typography>
        </DialogContent>

        <DialogActions
          sx={{
            display: "flex",
            justifyContent: "space-between",
            gap: "1rem",
            padding: "1rem"
          }}>
          <Form>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "baseline",
                gap: "1rem"
              }}>
              <FormControlLabel
                control={<Checkbox checked={isAcknowledge} onChange={onAcknowledgeChange} />}
                label={intl.formatMessage({ id: "i_acknowledge_and_accept" })}
              />
              <Field
                name="tnc_confirmation.company_name"
                label={intl.formatMessage({ id: "company_name" })}
                fullWidth
                component={FormikInput}
              />
              <Field
                name="tnc_confirmation.name"
                label={intl.formatMessage({ id: "name" })}
                fullWidth
                component={FormikInput}
              />
              <Field
                name="tnc_confirmation.ic_number"
                label={intl.formatMessage({ id: "ic_number" })}
                fullWidth
                component={FormikInput}
              />
              <Field
                name="tnc_confirmation.designation"
                label={intl.formatMessage({ id: "designation" })}
                fullWidth
                component={FormikInput}
              />
            </Box>
          </Form>
          <Stack
            direction="row"
            gap={2}
            sx={{
              alignSelf: "flex-end"
            }}>
            <LoadingButton
              color="secondary"
              variant="contained"
              onClick={onClose}
              loading={isLoading}>
              {intl.formatMessage({ id: "cancel" })}
            </LoadingButton>
            <LoadingButton
              color="primary"
              variant="contained"
              loading={isLoading}
              disabled={!isAcknowledge || form.errors.tnc_confirmation !== undefined}
              onClick={onConfirm}>
              {intl.formatMessage({ id: "confirm" })}
            </LoadingButton>
          </Stack>
        </DialogActions>
      </Box>
    </Dialog>
  )
}

export default TncModal
