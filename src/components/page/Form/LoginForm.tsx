import React, { ReactElement, useState } from "react"
import { Field, Form, Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { Box, IconButton, InputAdornment, Typography } from "@mui/material"
import { useIntl } from "react-intl"
import { object, string } from "yup"
import { LoadingButton } from "@mui/lab"
import VisibilityIcon from "@mui/icons-material/Visibility"
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff"
import { useRouter, useSearchParams } from "next/navigation"
import Link from "next/link"

import Logo from "../Logo"
import FormikInput from "@/components/formik/FormikInput"
import { RootState } from "@/stores"
import AppAction from "@/stores/App/Action"
import FormikCheckbox from "@/components/formik/FormikCheckbox"
import { isEnvValueTrue } from "@/helpers"

const LoginForm: React.FC = (): ReactElement => {
  const dispatch = useDispatch()
  const intl = useIntl()
  const router = useRouter()
  const query = useSearchParams()

  const { isLoading, envSettings } = useSelector((state: RootState) => state.app)

  const [showPassword, setShowPassword] = useState<boolean>(false)

  const initialValues = {
    email: "",
    password: ""
  }

  const validationSchema = object({
    email: string()
      .required(intl.formatMessage({ id: "error_email_required" }))
      .email(intl.formatMessage({ id: "error_email_invalid" })),
    password: string().required(intl.formatMessage({ id: "error_password_required" }))
  })

  return (
    <Box
      sx={{
        width: "75%"
      }}>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, actions) => {
          dispatch(
            AppAction.authenticate(values, actions.setSubmitting, () => {
              router.replace(query.get("next") ?? "/")
            })
          )
        }}>
        {({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "baseline",
                gap: "1rem"
              }}>
              <Box mb="2rem">
                <Logo button />
              </Box>
              <Typography>{intl.formatMessage({ id: "sign_in_desc" })}</Typography>
              <Field
                name="email"
                label={intl.formatMessage({ id: "email" })}
                fullWidth
                component={FormikInput}
              />
              <Field
                name="password"
                type={showPassword ? "text" : "password"}
                label={intl.formatMessage({ id: "password" })}
                fullWidth
                component={FormikInput}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        onClick={() => setShowPassword((prev) => !showPassword)}
                        edge="end">
                        {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
              />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "100%"
                }}>
                <Field
                  name="rememberMe"
                  type="checkbox"
                  label={intl.formatMessage({ id: "remember_me" })}
                  component={FormikCheckbox}
                />

                <Link href="/auth/forgot-password">
                  <Typography
                    sx={{
                      whiteSpace: "nowrap"
                    }}>
                    {intl.formatMessage({ id: "forgot_password" })}
                  </Typography>
                </Link>
              </Box>
              <LoadingButton
                loading={isSubmitting || isLoading}
                type="submit"
                variant="contained"
                fullWidth>
                {intl.formatMessage({ id: "login" })}
              </LoadingButton>

              {!isEnvValueTrue(envSettings.IS_DISABLED_SELF_SIGNUP) && (
                <LoadingButton
                  type="button"
                  variant="outlined"
                  fullWidth
                  onClick={() => router.push("/auth/signup")}>
                  {intl.formatMessage({ id: "no_account_signup" })}
                </LoadingButton>
              )}
            </Box>
          </Form>
        )}
      </Formik>
    </Box>
  )
}

export default LoginForm
