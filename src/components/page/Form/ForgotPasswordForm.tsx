import React, { ReactElement, useState } from "react"
import { Field, Form, Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { Box, Button, Typography } from "@mui/material"
import { useIntl } from "react-intl"
import { object, string } from "yup"
import { LoadingButton } from "@mui/lab"

import Logo from "../Logo"
import FormikInput from "@/components/formik/FormikInput"
import { RootState } from "@/stores"
import { useRouter } from "next/navigation"
import AppAction from "@/stores/App/Action"

export interface ForgotPasswordFormProps {
  email: string
}

const ForgotPasswordForm: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const [sendLinkSuccess, setSendLinkSuccess] = useState<boolean>(false)

  const { isLoading } = useSelector((state: RootState) => state.app)

  const initialValues: ForgotPasswordFormProps = {
    email: ""
  }

  const validationSchema = object({
    email: string()
      .required(intl.formatMessage({ id: "error_email_required" }))
      .email(intl.formatMessage({ id: "error_email_invalid" }))
  })

  return (
    <Box
      sx={{
        width: "75%"
      }}>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, actions) => {
          dispatch(
            AppAction.sendResetPasswordLink(values, actions.setSubmitting, () => {
              setSendLinkSuccess(true)
            })
          )
        }}>
        {({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "baseline",
                gap: "1rem"
              }}>
              <Box mb="2rem">
                <Logo button />
              </Box>
              {sendLinkSuccess ? (
                <Typography>{intl.formatMessage({ id: "forgot_password_success" })}</Typography>
              ) : (
                <React.Fragment>
                  <Typography>{intl.formatMessage({ id: "forgot_password_desc" })}</Typography>
                  <Field
                    name="email"
                    label={intl.formatMessage({ id: "email" })}
                    fullWidth
                    component={FormikInput}
                  />
                  <LoadingButton
                    loading={isSubmitting || isLoading}
                    type="submit"
                    variant="contained"
                    fullWidth>
                    {intl.formatMessage({ id: "send_reset_password_link" })}
                  </LoadingButton>
                </React.Fragment>
              )}
              <Button
                type="button"
                variant="contained"
                color="secondary"
                fullWidth
                onClick={() => {
                  router.replace("/auth/login")
                }}>
                {intl.formatMessage({ id: "back_to_login" })}
              </Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Box>
  )
}

export default ForgotPasswordForm
