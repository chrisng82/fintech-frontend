import React, { ReactElement, useState } from "react"
import { Field, Form, Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { Box, Button, IconButton, InputAdornment, Typography } from "@mui/material"
import { useIntl } from "react-intl"
import { object, ref, string } from "yup"
import { LoadingButton } from "@mui/lab"
import Logo from "../Logo"
import FormikInput from "@/components/formik/FormikInput"
import { RootState } from "@/stores"
import { useRouter } from "next/navigation"
import AppAction from "@/stores/App/Action"
import VisibilityIcon from "@mui/icons-material/Visibility"
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff"

export interface ResetPasswordFormProps {
  newPassword: string
  confirmNewPassword: string
}

const ResetPasswordForm: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const [resetPasswordSuccess, setResetPasswordSuccess] = useState<boolean>(false)
  const [showNewPassword, setShowNewPassword] = useState<boolean>(false)
  const [showConfirmNewPassword, setShowConfirmNewPassword] = useState<boolean>(false)

  const { isLoading } = useSelector((state: RootState) => state.app)

  const initialValues: ResetPasswordFormProps = {
    newPassword: "",
    confirmNewPassword: ""
  }

  const validationSchema = object({
    newPassword: string().required(intl.formatMessage({ id: "error_password_required" })),
    confirmNewPassword: string()
      .required(intl.formatMessage({ id: "error_confirm_password_required" }))
      .oneOf([ref("newPassword")], "Passwords must match")
  })

  return (
    <Box
      sx={{
        width: "75%"
      }}>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, actions) => {
          dispatch(
            AppAction.resetPassword(values, actions.setSubmitting, () => {
              setResetPasswordSuccess(true)
            })
          )
        }}>
        {({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "baseline",
                gap: "1rem"
              }}>
              <Box mb="2rem">
                <Logo button />
              </Box>
              {resetPasswordSuccess ? (
                <Typography>{intl.formatMessage({ id: "reset_password_success" })}</Typography>
              ) : (
                <React.Fragment>
                  <Typography>{intl.formatMessage({ id: "reset_password_desc" })}</Typography>
                  <Field
                    name="newPassword"
                    type={showNewPassword ? "text" : "password"}
                    label={intl.formatMessage({ id: "new_password" })}
                    fullWidth
                    component={FormikInput}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            onClick={() => setShowNewPassword((prev) => !prev)}
                            edge="end">
                            {showNewPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                  />
                  <Field
                    name="confirmNewPassword"
                    type={showConfirmNewPassword ? "text" : "password"}
                    label={intl.formatMessage({ id: "confirm_new_password" })}
                    fullWidth
                    component={FormikInput}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            onClick={() => setShowConfirmNewPassword((prev) => !prev)}
                            edge="end">
                            {showConfirmNewPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                  />
                  <LoadingButton
                    loading={isSubmitting || isLoading}
                    type="submit"
                    variant="contained"
                    fullWidth>
                    {intl.formatMessage({ id: "reset_password" })}
                  </LoadingButton>
                </React.Fragment>
              )}
              <Button
                type="button"
                variant="contained"
                color="secondary"
                fullWidth
                onClick={() => {
                  router.replace("/auth/login")
                }}>
                {intl.formatMessage({ id: "back_to_login" })}
              </Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Box>
  )
}

export default ResetPasswordForm
