"use client"

import {
  Box,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  FormControlLabel,
  Stack,
  Typography
} from "@mui/material"
import React, { ReactElement, useEffect, useRef, useState } from "react"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"
import Link from "next/link"
import { SelfSignupFormValues } from "./SelfSignupForm"
import { Field, Form, useFormikContext } from "formik"
import FormikInput from "@/components/formik/FormikInput"
import { isEmpty } from "lodash"
import TncEditor from "../Companies/ViewCompany/TncTab/TncEditor"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import CompanyAction from "@/stores/Company/Action"

interface Props {
  open: boolean
  isAcknowledge: boolean
  isLoading?: boolean
  confirmColor?: "inherit" | "primary" | "secondary" | "success" | "error" | "info" | "warning"

  onAcknowledgeChange: () => void
  onClose: () => void
  onConfirm: () => void
}

const normalText = {
  fontSize: "1rem",
  lineHeight: "1.5rem",
  fontWeight: "normal"
}

const underlineTitle = {
  textDecoration: "underline"
}

const privacyNoticeList = [
  "Providing our services;",
  "Processing transactions and fulfilling orders;",
  "Sending newsletters and promotional materials;",
  "Analysing and improving our products and services;",
  "Responding to your inquiries and providing customer support;",
  "Sharing Company's products information; and",
  "Promoting Company's products and services."
]

const privacyNoticeBMList = [
  "Menyediakan perkhidmatan kami;",
  "Memproses transaksi dan melaksanakan pesanan;",
  "Menghantar buletin dan bahan promosi;",
  "Menganalisis dan meningkatkan produk dan perkhidmatan kami;",
  "Menjawab pertanyaan anda dan menyediakan sokongan pelanggan;",
  "Menghantar maklumat produk kami; dan",
  "Mempromosikan produk dan perkhidmatan kami."
]

const TncModalNew: React.FC<Props> = ({
  open,
  onClose,
  onConfirm,
  isLoading,
  isAcknowledge,
  onAcknowledgeChange
}): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const dialogRef = useRef<HTMLDivElement | null>(null)
  const form = useFormikContext<SelfSignupFormValues>()

  const {
    termCondition,
    showCompany: { data: companyData }
  } = useSelector((state: RootState) => state.company)

  useEffect(() => {
    if (open && dialogRef.current) {
      dialogRef.current.scrollTop = 0
    }
  }, [open])

  useEffect(() => {
    if (isAcknowledge) {
      form.setFieldValue("tnc_confirmation.company_name", "your company name")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAcknowledge])

  useEffect(() => {
    if (termCondition) {
      if (termCondition.contents.length === 0 && companyData) {
        dispatch(CompanyAction.fetchTermConditionPublic(companyData.id))
      }
    }
  }, [termCondition])

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth="lg">
      <Box
        ref={dialogRef}
        sx={{
          backgroundColor: "white",
          padding: 2,
          maxHeight: "80vh"
        }}>
        <DialogContent
          sx={{
            display: "flex",
            flexDirection: "column",
            gap: "1rem"
          }}>
          <TncEditor contents={termCondition?.contents ?? ""} readonly={true} />
        </DialogContent>

        <DialogActions
          sx={{
            display: "flex",
            justifyContent: "space-between",
            gap: "1rem",
            padding: "1rem"
          }}>
          <Form>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "baseline",
                gap: "1rem"
              }}>
              <FormControlLabel
                control={<Checkbox checked={isAcknowledge} onChange={onAcknowledgeChange} />}
                label={intl.formatMessage({ id: "i_acknowledge_and_accept" })}
              />
              <Field
                name="tnc_confirmation.company_name"
                label={intl.formatMessage({ id: "company_name" })}
                fullWidth
                component={FormikInput}
              />
              <Field
                name="tnc_confirmation.name"
                label={intl.formatMessage({ id: "name" })}
                fullWidth
                component={FormikInput}
              />
              <Field
                name="tnc_confirmation.ic_number"
                label={intl.formatMessage({ id: "ic_number" })}
                fullWidth
                component={FormikInput}
              />
              <Field
                name="tnc_confirmation.designation"
                label={intl.formatMessage({ id: "designation" })}
                fullWidth
                component={FormikInput}
              />
            </Box>
          </Form>
          <Stack
            direction="row"
            gap={2}
            sx={{
              alignSelf: "flex-end"
            }}>
            <LoadingButton
              color="secondary"
              variant="contained"
              onClick={onClose}
              loading={isLoading}>
              {intl.formatMessage({ id: "cancel" })}
            </LoadingButton>
            <LoadingButton
              color="primary"
              variant="contained"
              loading={isLoading}
              disabled={!isAcknowledge || form.errors.tnc_confirmation !== undefined}
              onClick={onConfirm}>
              {intl.formatMessage({ id: "confirm" })}
            </LoadingButton>
          </Stack>
        </DialogActions>
      </Box>
    </Dialog>
  )
}

export default TncModalNew
