import IOSSwitch from "@/components/Base/Switch"
import { convertNullToUndefined } from "@/components/utils/MapHelper"
import { DebtorContact } from "@/stores/Debtor/Types"
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  FormControlLabel,
  Grid,
  Typography
} from "@mui/material"
import { Form, Formik } from "formik"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import FieldData from "./FieldData"
import { RootState } from "@/stores"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"
import DebtorAction from "@/stores/Debtor/Action"
import ExpandMoreIcon from "@mui/icons-material/ExpandMore"
import { palette } from "@/components/utils/theme"
import SnackbarAction from "@/stores/Snackbar/Action"
import ConfirmDeleteModal from "@/components/modals/ConfirmDeleteModal"
import * as Yup from "yup"

interface Props {
  index: number
  length: number
  expanded: number
  data?: Partial<DebtorContact>

  setExpended: (panel: number) => void
  handleChange: (panel: number) => void
  removeContact: (index: number) => void
}

const ContactForm: React.FC<Props> = ({
  index,
  length,
  expanded,
  data,
  setExpended,
  handleChange,
  removeContact
}): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isLoading } = useSelector((state: RootState) => state.debtor.item)

  const [editMode, setEditMode] = useState<boolean>(data?.id === 0)
  const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false)

  const DebtorContactSchema = Yup.object().shape({
    phone: Yup.string().required("Required"),
    email: Yup.string().email("Invalid email").required("Required")
  })

  const handleOnCancel = () => {
    if (data?.id && data.id !== 0) {
      setEditMode(false)
    } else {
      removeContact(index)
    }
  }

  const handleDeleteContact = (is_main: boolean) => {
    if (is_main) {
      dispatch(
        SnackbarAction.openSnackbar(
          intl.formatMessage({ id: "main_contact_cannot_delete" }),
          "error"
        )
      )
      return
    } else {
      setOpenDeleteModal(true)
    }
  }

  return (
    <>
      <Formik
        initialValues={
          data
            ? convertNullToUndefined(data)
            : {
                title: "",
                name: "",
                department: "",
                im: "",
                email: "",
                phone: "",
                direct_phone: "",
                direct_fax: "",
                remark: "",
                is_main: false,
                editMode: false
              }
        }
        enableReinitialize
        validationSchema={DebtorContactSchema}
        onSubmit={(values, { setSubmitting }) => {
          if (data?.id && data.id !== 0) {
            dispatch(
              DebtorAction.updateDebtorContact(setSubmitting, data.id, values, () =>
                setEditMode(false)
              )
            )
          } else {
            dispatch(
              DebtorAction.createDebtorContact(setSubmitting, values, () => setEditMode(false))
            )
          }
        }}>
        {({ handleSubmit, setFieldValue, values, isSubmitting }) => (
          <Form onSubmit={handleSubmit}>
            <Card
              sx={{
                padding: expanded !== index ? 0 : "1rem 0.5rem",
                borderRadius:
                  index === 0 ? "10px 10px 0 0" : index + 1 === length ? "0 0 10px 10px" : 0,
                borderBottom: index + 1 === length ? "none" : `1px solid ${palette.grey.main}`,
                marginTop: expanded === index && index !== 0 ? "1rem" : 0,
                marginBottom: expanded === index && index + 1 !== length ? "1rem" : 0
              }}>
              <CardHeader
                title={
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}>
                    <Typography
                      onClick={() => {
                        handleChange(index)
                      }}
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        gap: "1rem",
                        cursor: "pointer"
                      }}>
                      {intl.formatMessage({ id: "contact" })} #{index + 1}
                      <ExpandMoreIcon
                        sx={{
                          transition: "transform 0.3s cubic-bezier(0.4, 0, 0.2, 1)",
                          transform: expanded === index ? "rotate(-180deg)" : "rotate(0deg)"
                        }}
                      />
                    </Typography>
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        gap: "1rem"
                      }}>
                      <FormControlLabel
                        control={
                          <IOSSwitch
                            sx={{ m: 1 }}
                            checked={!!values.is_main}
                            editMode={!editMode}
                            name="is_main"
                            onChange={() => {
                              setFieldValue("is_main", !!!values.is_main)
                            }}
                          />
                        }
                        label={
                          <Typography
                            sx={{
                              fontWeight: 500
                            }}>
                            {intl.formatMessage({ id: "company_contact" })}
                          </Typography>
                        }
                      />
                      <Collapse in={!editMode}>
                        <Button
                          variant="contained"
                          color="warning"
                          onClick={() => {
                            setEditMode(true)
                            setExpended(index)
                          }}>
                          {intl.formatMessage({ id: "edit" })}
                        </Button>
                        <Button
                          variant="contained"
                          color="error"
                          onClick={() => {
                            handleDeleteContact(Boolean(data?.is_main))
                          }}
                          sx={{ ml: 1 }}>
                          {intl.formatMessage({ id: "delete" })}
                        </Button>
                      </Collapse>
                    </Box>
                  </Box>
                }
                sx={{
                  padding: expanded !== index ? "0.5rem 1.5rem" : "0.5rem 1rem"
                }}
              />
              <Collapse in={expanded === index}>
                <CardContent>
                  <Grid container spacing={2} rowSpacing={4}>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="title"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "title" })}
                        value={data?.title}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="name"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "name" }) + "*"}
                        value={data?.name}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="department"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "department" }) + "*"}
                        value={data?.department}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="im"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "im" })}
                        value={data?.im}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="email"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "email" }) + "*"}
                        value={data?.email}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="phone"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "phone" }) + "*"}
                        value={data?.phone}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="direct_phone"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "direct_phone" })}
                        value={data?.direct_phone}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FieldData
                        editMode={editMode}
                        fieldName="direct_fax"
                        isLoading={isLoading}
                        label={intl.formatMessage({ id: "direct_fax" })}
                        value={data?.direct_fax}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <FieldData
                        editMode={editMode}
                        fieldName="remark"
                        isLoading={isLoading}
                        isTextArea
                        label={intl.formatMessage({ id: "remark" })}
                        value={data?.remark}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
                <Collapse in={editMode}>
                  <CardActions
                    sx={{
                      justifyContent: "flex-end"
                    }}>
                    <GreyButton
                      isLoadingButton
                      type="reset"
                      variant="contained"
                      loading={isSubmitting}
                      onClick={handleOnCancel}>
                      {intl.formatMessage({ id: "cancel" })}
                    </GreyButton>
                    <LoadingButton
                      type="reset"
                      variant="contained"
                      color="warning"
                      loading={isSubmitting}>
                      {intl.formatMessage({ id: "reset" })}
                    </LoadingButton>
                    <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                      {intl.formatMessage({ id: "save" })}
                    </LoadingButton>
                  </CardActions>
                </Collapse>
              </Collapse>
            </Card>
          </Form>
        )}
      </Formik>
      <ConfirmDeleteModal
        open={openDeleteModal}
        title={intl.formatMessage({ id: "delete_contact" })}
        content={intl.formatMessage({ id: "are_you_sure_delete" })}
        onClose={() => {
          setOpenDeleteModal(false)
        }}
        onConfirm={() => {
          dispatch(DebtorAction.deleteDebtorContact(data?.id, () => removeContact(index)))
          setOpenDeleteModal(false)
        }}
      />
    </>
  )
}

export default ContactForm
