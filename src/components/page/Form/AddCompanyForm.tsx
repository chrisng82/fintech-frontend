"use client"

import FormikInput from "@/components/formik/FormikInput"
import { LoadingButton } from "@mui/lab"
import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Fab,
  FormControlLabel,
  Grid,
  Typography
} from "@mui/material"
import { Field, Form, Formik } from "formik"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { object, string, number } from "yup"
import "yup-phone-lite"
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft"
import RestartAltIcon from "@mui/icons-material/RestartAlt"
import AddIcon from "@mui/icons-material/Add"
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow"
import { palette } from "@/components/utils/theme"
import CompanyAction from "@/stores/Company/Action"
import { useRouter } from "next/navigation"
import { useDispatch } from "react-redux"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"
import IOSSwitch from "@/components/Base/Switch"
import { motion } from "framer-motion"
import GreyButton from "@/components/Base/GreyButton"
import Link from "next/link"

export const initialValues = {
  code: "",
  ref_code: "",
  co_reg_no: "",
  name_01: "",
  name_02: "",
  tax_reg_no: "",

  bill_email_01: "",
  bill_email_02: "",
  bill_fax_01: "",
  bill_fax_02: "",
  bill_phone_01: "",
  bill_phone_02: "",
  bill_attention: "",
  bill_unit_no: "",
  bill_building_name: "",
  bill_street_name: "",
  bill_district_01: "",
  bill_district_02: "",
  bill_postcode: "",
  bill_state_name: "",
  bill_country_name: "",

  ship_email_01: "",
  ship_email_02: "",
  ship_fax_01: "",
  ship_fax_02: "",
  ship_phone_01: "",
  ship_phone_02: "",
  ship_attention: "",
  ship_unit_no: "",
  ship_building_name: "",
  ship_street_name: "",
  ship_district_01: "",
  ship_district_02: "",
  ship_postcode: "",
  ship_state_name: "",
  ship_country_name: "",
  status: ResStatusValues[ResStatusEnum.ACTIVE] as number
}

const AddCompanyForm: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const validationSchema = object({
    code: string().required(intl.formatMessage({ id: "error_company_code_required" })),
    co_reg_no: string().required(intl.formatMessage({ id: "error_company_reg_no_required" })),
    name_01: string().required(intl.formatMessage({ id: "error_company_name_01_required" })),

    bill_email_01: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .required(intl.formatMessage({ id: "error_company_bill_email_01_required" })),
    bill_email_02: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .optional(),
    bill_fax_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    bill_fax_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    bill_phone_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    bill_phone_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),

    ship_email_01: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .required(intl.formatMessage({ id: "error_company_ship_email_01_required" })),
    ship_email_02: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .optional(),
    ship_fax_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    ship_fax_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    ship_phone_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    ship_phone_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional()
  })

  return (
    <React.Fragment>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, actions) => {
          dispatch(
            CompanyAction.createCompany(actions.setSubmitting, values, () => {
              router.push("/companies")
            })
          )
        }}>
        {({ values, isSubmitting, handleSubmit, setFieldValue }) => (
          <Form onSubmit={handleSubmit}>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.5 }}>
              <Card
                sx={{
                  mb: 2
                }}>
                <CardHeader
                  title={
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center"
                      }}>
                      <Typography>{intl.formatMessage({ id: "company_profile" })}</Typography>
                      <FormControlLabel
                        control={
                          <IOSSwitch
                            sx={{ m: 1 }}
                            checked={values.status === 100}
                            name="status"
                            onChange={() => {
                              setFieldValue("status", values.status === 2 ? 100 : 2)
                            }}
                          />
                        }
                        label={intl.formatMessage({ id: "status" })}
                      />
                    </Box>
                  }
                />
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={4}>
                      <Field
                        name="code"
                        label={intl.formatMessage({ id: "company_code" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="ref_code"
                        label={intl.formatMessage({ id: "ref_code" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="co_reg_no"
                        label={intl.formatMessage({ id: "company_reg_no" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="tax_reg_no"
                        label={intl.formatMessage({ id: "tax_reg_no" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="name_01"
                        label={intl.formatMessage({ id: "company_name_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="name_02"
                        label={intl.formatMessage({ id: "company_name_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </motion.div>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.3, duration: 0.5 }}>
              <Card
                sx={{
                  mb: 2
                }}>
                <CardHeader
                  title={<Typography>{intl.formatMessage({ id: "billing_details" })}</Typography>}
                />
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Field
                        name="bill_email_01"
                        label={intl.formatMessage({ id: "email_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="bill_email_02"
                        label={intl.formatMessage({ id: "email_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="bill_fax_01"
                        label={intl.formatMessage({ id: "fax_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="bill_fax_02"
                        label={intl.formatMessage({ id: "fax_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="bill_phone_01"
                        label={intl.formatMessage({ id: "phone_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="bill_phone_02"
                        label={intl.formatMessage({ id: "phone_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="bill_attention"
                        label={intl.formatMessage({ id: "attention" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="bill_unit_no"
                        label={intl.formatMessage({ id: "unit_no" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="bill_building_name"
                        label={intl.formatMessage({ id: "building_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="bill_street_name"
                        label={intl.formatMessage({ id: "street_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="bill_district_01"
                        label={intl.formatMessage({ id: "district_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="bill_district_02"
                        label={intl.formatMessage({ id: "district_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="bill_postcode"
                        label={intl.formatMessage({ id: "postcode" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="bill_state_name"
                        label={intl.formatMessage({ id: "state_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="bill_country_name"
                        label={intl.formatMessage({ id: "country_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </motion.div>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.6, duration: 0.5 }}>
              <Card
                sx={{
                  mb: 2
                }}>
                <CardHeader
                  title={<Typography>{intl.formatMessage({ id: "shipping_details" })}</Typography>}
                />
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Field
                        name="ship_email_01"
                        label={intl.formatMessage({ id: "email_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="ship_email_02"
                        label={intl.formatMessage({ id: "email_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="ship_tax_01"
                        label={intl.formatMessage({ id: "fax_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="ship_tax_02"
                        label={intl.formatMessage({ id: "fax_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="ship_phone_01"
                        label={intl.formatMessage({ id: "phone_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Field
                        name="ship_phone_02"
                        label={intl.formatMessage({ id: "phone_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="ship_attention"
                        label={intl.formatMessage({ id: "attention" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="ship_unit_no"
                        label={intl.formatMessage({ id: "unit_no" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="ship_building_name"
                        label={intl.formatMessage({ id: "building_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="ship_street_name"
                        label={intl.formatMessage({ id: "street_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="ship_district_01"
                        label={intl.formatMessage({ id: "district_01" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="ship_district_02"
                        label={intl.formatMessage({ id: "district_02" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="ship_postcode"
                        label={intl.formatMessage({ id: "postcode" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="ship_state_name"
                        label={intl.formatMessage({ id: "state_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Field
                        name="ship_country_name"
                        label={intl.formatMessage({ id: "country_name" })}
                        component={FormikInput}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </motion.div>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.9, duration: 0.5 }}>
              <Card>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <Link href="/companies">
                    <GreyButton
                      type="button"
                      variant="contained"
                      isLoadingButton
                      loading={isSubmitting}
                      startIcon={<ChevronLeftIcon />}>
                      {intl.formatMessage({ id: "back" })}
                    </GreyButton>
                  </Link>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}
                    startIcon={<RestartAltIcon />}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton
                    type="submit"
                    variant="contained"
                    loading={isSubmitting}
                    startIcon={<AddIcon />}>
                    {intl.formatMessage({ id: "create" })}
                  </LoadingButton>
                </CardActions>
              </Card>
            </motion.div>
          </Form>
        )}
      </Formik>
      <Fab
        onClick={() => {
          window.scrollTo({
            top: document.body.scrollHeight,
            behavior: "smooth"
          })
        }}
        sx={{
          position: "fixed",
          bottom: "1rem",
          right: "calc(56px + 1.5rem)",
          background: palette.purple.main,
          "&:hover": {
            background: palette.purple.dark
          }
        }}>
        <DoubleArrowIcon
          sx={{
            color: "white",
            transform: "rotate(90deg)"
          }}
        />
      </Fab>
      <Fab
        color="primary"
        onClick={() => {
          window.scrollTo({
            top: 0,
            behavior: "smooth"
          })
        }}
        sx={{
          position: "fixed",
          bottom: "1rem",
          right: "1rem",
          background: palette.purple.main,
          "&:hover": {
            background: palette.purple.dark
          }
        }}>
        <DoubleArrowIcon
          sx={{
            color: "white",
            transform: "rotate(-90deg)"
          }}
        />
      </Fab>
    </React.Fragment>
  )
}

export default AddCompanyForm
