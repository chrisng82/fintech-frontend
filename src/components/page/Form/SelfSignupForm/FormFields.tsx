import FormikInput from "@/components/formik/FormikInput"
import { LoadingButton } from "@mui/lab"
import { Box, Divider, IconButton, InputAdornment, Typography } from "@mui/material"
import { Field, Form, useFormikContext } from "formik"
import React, { FormEvent, ReactElement, useEffect, useState } from "react"
import Logo from "../../Logo"
import { useIntl } from "react-intl"
import { SelfSignupFormValues, customerTypeOptions } from "."
import FormikSelect from "@/components/formik/FormikSelect"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import FormikMultiSelect from "@/components/formik/FormikMultiSelect"
import VisibilityIcon from "@mui/icons-material/Visibility"
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff"
import FormikFile from "@/components/formik/FormikFile"
import FieldData from "../FieldData"
import CompanyAction from "@/stores/Company/Action"

interface Props {
  isSubmitting: boolean
  values: SelfSignupFormValues
  handleSubmit: (event: FormEvent<HTMLFormElement>) => void
}

const FormFields: React.FC<Props> = ({ isSubmitting, values, handleSubmit }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()
  const form = useFormikContext<SelfSignupFormValues>()

  const [showPassword, setShowPassword] = useState<boolean>(false)

  const { isLoading, envSettings } = useSelector((state: RootState) => state.app)
  const { data } = useSelector((state: RootState) => state.signupLink.item)
  const { data: companyData } = useSelector((state: RootState) => state.company.companyList)

  const companyOption =
    companyData.map((company) => ({ value: company.id, label: company.name_01 })) ?? []

  useEffect(() => {
    const entries = Object.entries(form.values)

    entries.forEach(([key, value]) => {
      if (key.includes("debtor_codes")) {
        if (value && value !== "") {
          const splitValues = value.split(";")
          const companyId = key.split("_")[2]
          const uniqueValues = Array.from(new Set(splitValues))

          form.setFieldValue(`debtor_code_${companyId}`, uniqueValues.join(";"))
        }
      }
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.values])

  return (
    <Form onSubmit={handleSubmit}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "baseline",
          gap: "1rem"
        }}>
        <Box mb="2rem">
          <Logo button />
        </Box>

        <Typography>{intl.formatMessage({ id: "sign_up_desc" })}</Typography>

        <Divider
          textAlign="center"
          sx={{
            m: 1,
            width: "100%"
          }}>
          <Typography
            sx={{
              fontWeight: 500
            }}>
            Basic Info
          </Typography>
        </Divider>

        <Field
          name="customer_type"
          fullWidth
          label={intl.formatMessage({ id: "customer_type" })}
          options={customerTypeOptions.map((option) => ({
            value: option.value,
            label: intl.formatMessage({ id: option.label })
          }))}
          component={FormikSelect}
        />
        <Field
          name="companies"
          fullWidth
          label={intl.formatMessage({ id: "companies" })}
          options={companyOption}
          component={FormikMultiSelect}
          readOnly={companyOption.length === 1}
          onChange={(e: any) => {
            form.setFieldValue("companies", e.target.value)

            dispatch(CompanyAction.fetchTermConditionPublic(e.target.value))

            e.target.value.forEach((companyId: number) => {
              if (!form.values[`debtor_code_${companyId}`]) {
                form.setFieldValue(`debtor_code_${companyId}`, "")
              }
            })

            Object.entries(form.values).forEach(([key, value]) => {
              if (key.includes("debtor_codes")) {
                const companyId = key.split("_")[2]

                if (!e.target.value.includes(parseInt(companyId))) {
                  form.setFieldValue(key, undefined)
                }
              }
            })
          }}
        />

        <Field
          name="company_name"
          label={intl.formatMessage({ id: "company_name" })}
          fullWidth
          component={FormikInput}
        />

        <Field
          name="company_reg_no"
          label={intl.formatMessage({ id: "company_reg_no" })}
          fullWidth
          component={FormikInput}
        />

        <Field
          name="company_addr"
          label={intl.formatMessage({ id: "company_addr" })}
          fullWidth
          component={FormikInput}
        />

        <Field
          name="first_name"
          label={intl.formatMessage({ id: "first_name" })}
          fullWidth
          component={FormikInput}
        />
        <Field
          name="last_name"
          label={intl.formatMessage({ id: "last_name" })}
          fullWidth
          component={FormikInput}
        />
        <Field
          name="email"
          label={intl.formatMessage({ id: "email" })}
          fullWidth
          disabled={!!data?.email}
          component={FormikInput}
        />
        <Field
          name="phone"
          label={intl.formatMessage({ id: "phone" })}
          fullWidth
          disabled={!!data?.phone}
          component={FormikInput}
        />
        <Field
          name="password"
          type={showPassword ? "text" : "password"}
          label={intl.formatMessage({ id: "password" })}
          fullWidth
          component={FormikInput}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={() => setShowPassword((prev) => !showPassword)} edge="end">
                  {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                </IconButton>
              </InputAdornment>
            )
          }}
        />
        <Field
          name="confirm_password"
          type={showPassword ? "text" : "password"}
          label={intl.formatMessage({ id: "confirm_password" })}
          fullWidth
          component={FormikInput}
        />

        {values.customer_type === "new_customer" && !envSettings.DEBTOR_HIDE_SSM_REGISTRATION && (
          <>
            <Divider
              textAlign="center"
              sx={{
                m: 1,
                width: "100%"
              }}>
              <Typography
                sx={{
                  fontWeight: 500
                }}>
                Company Document
              </Typography>
            </Divider>

            <Field
              name="ssm_form"
              label={intl.formatMessage({ id: "ssm_form" })}
              fullWidth
              component={FormikFile}
            />
            <Field
              name="form_49"
              label={intl.formatMessage({ id: "form_49" })}
              fullWidth
              component={FormikFile}
            />
            <Field
              name="bank_statement"
              label={intl.formatMessage({ id: "bank_statement" })}
              fullWidth
              component={FormikFile}
            />
          </>
        )}

        <>
          {values.companies.length > 0 && (
            <Divider
              textAlign="center"
              sx={{
                m: 1,
                width: "100%"
              }}>
              <Typography
                sx={{
                  fontWeight: 500
                }}>
                {intl.formatMessage({ id: "selected_company" })}
              </Typography>
            </Divider>
          )}

          {values.companies.map((companyId, index) => (
            <React.Fragment key={`selected-company-${companyId}`}>
              {process.env.NEXT_PUBLIC_ENABLE_COMPANY_AGREEMENT &&
                process.env.NEXT_PUBLIC_ENABLE_COMPANY_AGREEMENT !== "" && (
                  <Field
                    key={companyId}
                    name={`agreement_${companyId}`}
                    label={
                      companyOption.find((option) => option.value === companyId)?.label +
                      " " +
                      intl.formatMessage({ id: "agreement" })
                    }
                    fullWidth
                    component={FormikFile}
                  />
                )}

              <FieldData
                editMode
                fieldName={`debtor_code_${companyId}`}
                isMultiSelectChipped
                isLoading={isLoading}
                label={`${
                  companyOption.find((option) => option.value === companyId)?.label
                } ${intl.formatMessage({ id: "debtor_codes" })} (${intl.formatMessage({
                  id: "optional"
                })})`}
                value={values[`debtor_code_${companyId}`]}
                optionDisabled={() => {
                  return values[`debtor_code_${companyId}`].length > 9
                }}
              />

              {index !== values.companies.length - 1 && <Divider sx={{ width: "100%" }} />}
            </React.Fragment>
          ))}
        </>

        <LoadingButton
          loading={isSubmitting || isLoading}
          type="submit"
          variant="contained"
          fullWidth
          sx={{
            mt: 4
          }}>
          {intl.formatMessage({ id: "signup" })}
        </LoadingButton>
      </Box>
    </Form>
  )
}

export default FormFields
