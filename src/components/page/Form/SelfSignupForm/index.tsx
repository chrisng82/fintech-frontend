import React, { ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

import { palette } from "@/components/utils/theme"

import { Box, Typography } from "@mui/material"

import "yup-phone-lite"
import { array, object, ref, string } from "yup"
import { Formik } from "formik"

import { RootState } from "@/stores"
import CompanyAction from "@/stores/Company/Action"
import SignupLinkAction from "@/stores/SignupLink/Action"
import { SelfSignUpData } from "@/stores/SignupLink/Types"
import FormFields from "./FormFields"
import TncModal from "../TncModal"

export const customerTypeOptions = [
  { value: "existing_customer", label: "existing_customer" },
  { value: "new_customer", label: "new_customer" }
]

export interface SelfSignupFormValues {
  customer_type: string
  companies: number[]
  company_name: string
  company_reg_no: string
  company_addr: string
  first_name: string
  last_name: string
  phone: string
  email: string
  password: string
  confirm_password: string
  ssm_form: File | null
  form_49: File | null
  bank_statement: File | null
  [key: `debtor_code_${number}`]: string
  tnc_confirmation: {
    company_name: string
    name: string
    ic_number: string | number
    designation: string
  }
}

const SelfSignupForm: React.FC = (): ReactElement => {
  const dispatch = useDispatch()
  const intl = useIntl()
  const { data: companyData } = useSelector((state: RootState) => state.company.companyList)

  const [finishSubmit, setFinishSubmit] = useState<boolean>(false)
  const [isTncModalOpen, setIsTncModalOpen] = useState<boolean>(false)
  const [acknowledge, setAcknowledge] = useState<boolean>(false)

  const companyOption =
    companyData.map((company) => ({ value: company.id, label: company.name_01 })) ?? []

  useEffect(() => {
    dispatch(CompanyAction.fetchAllCompany())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const initialValues: SelfSignUpData = {
    customer_type: customerTypeOptions[0].value,
    companies: companyOption.length === 1 ? [companyOption[0].value] : [],
    company_name: "",
    company_reg_no: "",
    company_addr: "",
    first_name: "",
    last_name: "",
    phone: "",
    email: "",
    password: "",
    confirm_password: "",
    ssm_form: null,
    form_49: null,
    bank_statement: null,
    tnc_confirmation: {
      company_name: "",
      name: "",
      ic_number: "",
      designation: ""
    }
  }

  const validationSchema = object({
    companies: array().min(1, intl.formatMessage({ id: "error_companies_required" })),
    company_name: string().required(intl.formatMessage({ id: "error_company_name_required" })),
    company_reg_no: string().required(intl.formatMessage({ id: "error_company_reg_no_required" })),
    company_addr: string().required(intl.formatMessage({ id: "error_company_addr_required" })),
    first_name: string().required(intl.formatMessage({ id: "error_first_name_required" })),
    last_name: string().required(intl.formatMessage({ id: "error_last_name_required" })),
    phone: string()
      .required(intl.formatMessage({ id: "error_phone_required" }))
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" })),
    email: string()
      .required(intl.formatMessage({ id: "error_email_required" }))
      .email(intl.formatMessage({ id: "error_email_invalid" })),
    password: string().required(intl.formatMessage({ id: "error_password_required" })),
    confirm_password: string()
      .required(intl.formatMessage({ id: "error_confirm_password_required" }))
      .oneOf([ref("password")], "Passwords must match"),
    tnc_confirmation: object({
      company_name: acknowledge
        ? string().required(intl.formatMessage({ id: "error_company_name_required" }))
        : string(),
      name: acknowledge
        ? string().required(intl.formatMessage({ id: "error_name_required" }))
        : string(),
      ic_number: acknowledge
        ? string().required(intl.formatMessage({ id: "error_ic_number_required" }))
        : string(),
      designation: acknowledge
        ? string().required(intl.formatMessage({ id: "error_designation_required" }))
        : string()
    })
  })

  return (
    <Box
      sx={{
        width: "75%",
        paddingTop: 10,
        paddingBottom: 10
      }}>
      {!finishSubmit ? (
        <Formik
          enableReinitialize
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values, actions) => {
            setAcknowledge(false)
            setIsTncModalOpen(true)
          }}>
          {({ isSubmitting, handleSubmit, values, setErrors, setSubmitting }) => (
            <React.Fragment>
              <FormFields isSubmitting={isSubmitting} values={values} handleSubmit={handleSubmit} />
              <TncModal
                open={isTncModalOpen}
                isAcknowledge={acknowledge}
                onAcknowledgeChange={() => {
                  setAcknowledge(!acknowledge)
                }}
                onClose={() => {
                  setIsTncModalOpen(false)
                  setSubmitting(false)
                }}
                onConfirm={() => {
                  setIsTncModalOpen(false)
                  dispatch(
                    SignupLinkAction.selfSignup(
                      setSubmitting,
                      values,
                      (errors: any) => {
                        setErrors(errors)
                      },
                      () => {
                        setFinishSubmit(true)
                      }
                    )
                  )
                }}
              />
            </React.Fragment>
          )}
        </Formik>
      ) : (
        <Box
          sx={{
            background: "rgba(255,255,255,0.5)",
            backdropFilter: "blur(10px)",
            height: "75%",
            width: "90%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: "1rem",
            padding: "5rem"
          }}>
          <Typography
            sx={{
              fontSize: "2rem",
              fontWeight: "bold",
              color: palette.purple.dark
            }}>
            {intl.formatMessage({ id: "signup_successfully" })}
          </Typography>
        </Box>
      )}
    </Box>
  )
}

export default SelfSignupForm
