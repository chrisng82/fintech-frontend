import { Box, IconButton, InputAdornment, Typography } from "@mui/material"
import { Field, Form } from "formik"
import React, { FormEvent, ReactElement, useState } from "react"
import Logo from "../../Logo"
import FormikInput from "@/components/formik/FormikInput"
import { LoadingButton } from "@mui/lab"
import VisibilityIcon from "@mui/icons-material/Visibility"
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff"
import { useIntl } from "react-intl"
import { useSelector } from "react-redux"
import { RootState } from "@/stores"

interface Props {
    isSubmitting: boolean
    handleSubmit: (event: FormEvent<HTMLFormElement>) => void
}

const FormFields: React.FC<Props> = ({
    isSubmitting,
    handleSubmit
}): ReactElement => {
    const intl = useIntl()

    const { isLoading } = useSelector((state: RootState) => state.app)

    const [showPassword, setShowPassword] = useState<boolean>(false)

    return (
        <Form onSubmit={handleSubmit}>
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "baseline",
                    gap: "1rem"
                }}>
                <Box mb="2rem">
                    <Logo button />
                </Box>
                <Typography>{intl.formatMessage({ id: "sign_up_desc" })}</Typography>
                <Field
                    name="first_name"
                    label={intl.formatMessage({ id: "first_name" })}
                    fullWidth
                    component={FormikInput}
                />
                <Field
                    name="last_name"
                    label={intl.formatMessage({ id: "last_name" })}
                    fullWidth
                    component={FormikInput}
                />
                <Field
                    name="email"
                    label={intl.formatMessage({ id: "login_email" })}
                    fullWidth
                    component={FormikInput}
                    disabled
                />
                <Field
                    name="phone"
                    label={intl.formatMessage({ id: "phone" })}
                    fullWidth
                    component={FormikInput}
                />
                <Field
                    name="password"
                    type={showPassword ? "text" : "password"}
                    label={intl.formatMessage({ id: "password" })}
                    fullWidth
                    component={FormikInput}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    onClick={() => setShowPassword((prev) => !showPassword)}
                                    edge="end">
                                    {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                                </IconButton>
                            </InputAdornment>
                        )
                    }}
                />
                <Field
                    name="confirm_password"
                    type={showPassword ? "text" : "password"}
                    label={intl.formatMessage({ id: "confirm_password" })}
                    fullWidth
                    component={FormikInput}
                />
                <LoadingButton
                    loading={isSubmitting || isLoading}
                    type="submit"
                    variant="contained"
                    fullWidth>
                    {intl.formatMessage({ id: "signup" })}
                </LoadingButton>
            </Box>
        </Form>
    )
}


export default FormFields