import React, { FormEvent, ReactElement, useState } from "react"
import { Field, Form } from "formik"
import { useSelector } from "react-redux"
import {
  Autocomplete,
  Box,
  Divider,
  FormLabel,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Typography
} from "@mui/material"
import { useIntl } from "react-intl"
import "yup-phone-lite"
import { LoadingButton } from "@mui/lab"
import VisibilityIcon from "@mui/icons-material/Visibility"
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff"

import Logo from "../../Logo"
import FormikInput from "@/components/formik/FormikInput"
import FormikSelect from "@/components/formik/FormikSelect"
import { RootState } from "@/stores"
import FormikFile from "@/components/formik/FormikFile"
import FormikMultiSelect from "@/components/formik/FormikMultiSelect"
import { DebtorCodeOption, customerTypeOptions } from "."

interface Props {
  isSubmitting: boolean
  debtorCodes: DebtorCodeOption[]
  handleSubmit: (event: FormEvent<HTMLFormElement>) => void
}

const DebtorFormFields: React.FC<Props> = ({
  isSubmitting,
  debtorCodes,
  handleSubmit
}): ReactElement => {
  const intl = useIntl()

  const { isLoading, envSettings } = useSelector((state: RootState) => state.app)
  const { data: companyData } = useSelector((state: RootState) => state.company.showCompany)
  const { data: debtorData } = useSelector((state: RootState) => state.debtor.list)

  const [showPassword, setShowPassword] = useState<boolean>(false)

  return (
    <Form onSubmit={handleSubmit}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "baseline",
          gap: "1rem"
        }}>
        <Box mb="2rem">
          <Logo button />
        </Box>
        <Typography>{intl.formatMessage({ id: "sign_up_desc" })}</Typography>
        <Field
          name="customer_type"
          fullWidth
          label={intl.formatMessage({ id: "customer_type" })}
          options={customerTypeOptions.map((option) => ({
            label: intl.formatMessage({ id: option.label }),
            value: option.value
          }))}
          component={FormikSelect}
          disabled
        />
        <Field
          name="companies"
          fullWidth
          label={intl.formatMessage({ id: "companies" })}
          options={
            companyData !== null ? [{ label: companyData.name_01, value: companyData.id }] : []
          }
          component={FormikMultiSelect}
          disabled
        />
        <Field
          name="company_name"
          label={intl.formatMessage({ id: "company_name" })}
          fullWidth
          component={FormikInput}
          disabled
        />
        <Field
          name="company_reg_no"
          label={intl.formatMessage({ id: "company_reg_no" })}
          fullWidth
          component={FormikInput}
          disabled={debtorData.length > 0 ? (!!companyData) : false}
        />
        <Field
          name="company_addr"
          label={intl.formatMessage({ id: "company_addr" })}
          fullWidth
          component={FormikInput}
          disabled
        />
        <Field
          name="first_name"
          label={intl.formatMessage({ id: "first_name" })}
          fullWidth
          component={FormikInput}
        />
        <Field
          name="last_name"
          label={intl.formatMessage({ id: "last_name" })}
          fullWidth
          component={FormikInput}
        />
        <Field
          name="email"
          label={intl.formatMessage({ id: "login_email" })}
          fullWidth
          component={FormikInput}
          disabled
        />
        <Field
          name="phone"
          label={intl.formatMessage({ id: "phone" })}
          fullWidth
          component={FormikInput}
        />
        <Field
          name="password"
          type={showPassword ? "text" : "password"}
          label={intl.formatMessage({ id: "password" })}
          fullWidth
          component={FormikInput}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={() => setShowPassword((prev) => !showPassword)} edge="end">
                  {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                </IconButton>
              </InputAdornment>
            )
          }}
        />
        <Field
          name="confirm_password"
          type={showPassword ? "text" : "password"}
          label={intl.formatMessage({ id: "confirm_password" })}
          fullWidth
          component={FormikInput}
        />
        {companyData !== null && (
          <>
            <Divider
              textAlign="center"
              sx={{
                m: 1,
                width: "100%"
              }}>
              <Typography
                sx={{
                  fontWeight: 500
                }}>
                {intl.formatMessage({ id: "selected_company" })}
              </Typography>
            </Divider>
            {
                envSettings.DEBTOR_HIDE_SSM_REGISTRATION == false && (
                    <Field
                    name={`agreement_${companyData.id}`}
                    label={companyData.name_01 + " " + intl.formatMessage({ id: "agreement" })}
                    fullWidth
                    component={FormikFile}
                  />
                )
            }
            <Stack
              spacing={1}
              sx={{
                width: "100%"
              }}>
              <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>
                {intl.formatMessage({ id: "debtor_codes" })}
              </FormLabel>
              <Autocomplete
                multiple
                disablePortal
                options={debtorData.map((debtor: any) => ({
                  value: debtor.code,
                  label: debtor.code
                }))}
                value={debtorCodes}
                renderInput={(params) => <TextField {...params} />}
                disabled
                getOptionDisabled={() => {
                  return debtorCodes.length > 9
                }}
              />
            </Stack>
          </>
        )}
        <LoadingButton
          loading={isSubmitting || isLoading}
          type="submit"
          variant="contained"
          fullWidth>
          {intl.formatMessage({ id: "signup" })}
        </LoadingButton>
      </Box>
    </Form>
  )
}

export default DebtorFormFields
