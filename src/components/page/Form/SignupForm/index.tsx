import React, { ReactElement, useEffect, useState } from "react"
import { Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { Box, Typography } from "@mui/material"
import { useIntl } from "react-intl"
import { array, object, string, ref } from "yup"
import "yup-phone-lite"
import { useRouter } from "next/navigation"
import { palette } from "@/components/utils/theme"

import { RootState } from "@/stores"
import SignupLinkAction from "@/stores/SignupLink/Action"
import UserType, { UserTypeValues } from "@/stores/User/Types"
import { Company } from "@/stores/Company/Types"
import DebtorFormFields from "./DebtorFormFields"
import FormFields from "./FormFields"
import TncModal from "../TncModal"
import CompanyAction from "@/stores/Company/Action"

export interface DebtorCodeOption {
  value: string
  label: string
}

interface IProps {
  token: string
}

export interface SignupFormValues {
  customer_type: string
  companies: number[]
  company_name: string
  company_reg_no: string
  company_addr: string
  first_name: string
  last_name: string
  phone: string
  email: string
  password: string
  confirm_password: string
  user_type: number
  token: string
  ssm_form: File | null
  form_49: File | null
  bank_statement: File | null
  tnc_confirmation: {
    company_name: string
    name: string
    ic_number: string | number
    designation: string
  }
}

export const customerTypeOptions = [{ value: "existing_customer", label: "existing_customer" }]

const SignupForm: React.FC<IProps> = ({ token }): ReactElement => {
  const dispatch = useDispatch()
  const intl = useIntl()
  const router = useRouter()

  const { data } = useSelector((state: RootState) => state.signupLink.item)
  const { data: companyData } = useSelector((state: RootState) => state.company.showCompany)
  const { data: debtorData } = useSelector((state: RootState) => state.debtor.list)

  const [finishSubmit, setFinishSubmit] = useState<boolean>(false)
  const [debtorCodes, setDebtorCodes] = useState<DebtorCodeOption[]>([])
  const [isTncModalOpen, setIsTncModalOpen] = useState<boolean>(false)
  const [acknowledge, setAcknowledge] = useState<boolean>(false)

  useEffect(() => {
    setDebtorCodes(debtorData.map((d) => ({ value: d.code, label: d.code })))
  }, [debtorData])

  useEffect(() => {
    if (companyData) {
      dispatch(CompanyAction.fetchTermConditionPublic(companyData.id))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [companyData])

  const initialValues = {
    customer_type: customerTypeOptions[0].value,
    companies: [companyData ? companyData.id : 0] ?? [],
    company_name: debtorData.length > 0 ? debtorData[0].name_01 : "",
    company_reg_no: debtorData.length > 0 ? debtorData[0].co_reg_no : "",
    company_addr:
      debtorData.length > 0
        ? debtorData[0].bill_unit_no +
          debtorData[0].bill_building_name +
          debtorData[0].bill_street_name
        : "",
    first_name: "",
    last_name: "",
    phone: data?.phone ?? "",
    email: data?.email ?? "",
    password: "",
    confirm_password: "",
    user_type: data?.link_type ?? 2,
    token,
    ssm_form: null,
    form_49: null,
    bank_statement: null,
    [`debtor_code_${companyData ? companyData.id : 0}`]: debtorData.map((d) => d.code),
    tnc_confirmation: {
      company_name: "",
      name: "",
      ic_number: "",
      designation: ""
    }
  }

  const createValidationSchema = (companies: Company[]) => {
    let shape: any = {}

    companies.forEach((company) => {
      shape[`debtor_code_${company.id}`] = array()
        .of(string().required(intl.formatMessage({ id: "debtor_code_required" })))
        .required(intl.formatMessage({ id: "debtor_code_required" }))
        .min(1, intl.formatMessage({ id: "debtor_code_required" }))

      //TODO: Add agreement schema
    })

    return object().shape(shape)
  }

  const validationSchema = object({
    companies:
      data?.link_type === UserTypeValues[UserType.DEBTOR]
        ? array().min(1, intl.formatMessage({ id: "error_companies_required" }))
        : array().optional(),
    company_name:
      data?.link_type === UserTypeValues[UserType.DEBTOR]
        ? string().required(intl.formatMessage({ id: "error_company_name_required" }))
        : string().optional(),
    company_reg_no:
      data?.link_type === UserTypeValues[UserType.DEBTOR] && debtorData[0]?.co_reg_no !== ""
        ? string().required(intl.formatMessage({ id: "error_company_reg_no_required" }))
        : string().optional(),
    company_addr:
      data?.link_type === UserTypeValues[UserType.DEBTOR]
        ? string().required(intl.formatMessage({ id: "error_company_addr_required" }))
        : string().optional(),
    first_name: string().required(intl.formatMessage({ id: "error_first_name_required" })),
    last_name: string().required(intl.formatMessage({ id: "error_last_name_required" })),
    phone: string().phone("MY", intl.formatMessage({ id: "error_phone_format_my" })),
    email: string()
      .required(intl.formatMessage({ id: "error_email_required" }))
      .email(intl.formatMessage({ id: "error_email_invalid" })),
    password: string().required(intl.formatMessage({ id: "error_password_required" })),
    confirm_password: string()
      .required(intl.formatMessage({ id: "error_confirm_password_required" }))
      .oneOf([ref("password")], "Passwords must match"),
    tnc_confirmation: object({
      company_name: acknowledge
        ? string().required(intl.formatMessage({ id: "error_company_name_required" }))
        : string(),
      name: acknowledge
        ? string().required(intl.formatMessage({ id: "error_name_required" }))
        : string(),
      ic_number: acknowledge
        ? string().required(intl.formatMessage({ id: "error_ic_number_required" }))
        : string(),
      designation: acknowledge
        ? string().required(intl.formatMessage({ id: "error_designation_required" }))
        : string()
    })
  }).concat(createValidationSchema(companyData ? [companyData] : []))

  if (finishSubmit && data !== null && data.link_type === UserTypeValues[UserType.DEBTOR]) {
    return (
      <Box
        sx={{
          background: "rgba(255,255,255,0.5)",
          backdropFilter: "blur(10px)",
          height: "75%",
          width: "90%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          gap: "1rem",
          padding: "5rem"
        }}>
        <Typography
          sx={{
            fontSize: "2rem",
            fontWeight: "bold",
            color: palette.purple.dark
          }}>
          {intl.formatMessage({ id: "signup_successfully" })}
        </Typography>
      </Box>
    )
  }

  return (
    <Box
      sx={{
        width: "75%",
        paddingTop: "4rem",
        paddingBottom: "4rem"
      }}>
      {!finishSubmit &&
        data !== null &&
        (data.link_type === UserTypeValues[UserType.DEBTOR] ? (
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values, actions) => {
              setIsTncModalOpen(true)
            }}>
            {({ isSubmitting, values, handleSubmit, setErrors, setSubmitting }) => (
              <React.Fragment>
                <DebtorFormFields
                  isSubmitting={isSubmitting}
                  debtorCodes={debtorCodes}
                  handleSubmit={handleSubmit}
                />
                <TncModal
                  open={isTncModalOpen}
                  isAcknowledge={acknowledge}
                  onAcknowledgeChange={() => {
                    setAcknowledge(!acknowledge)
                  }}
                  onClose={() => {
                    setIsTncModalOpen(false)
                    setSubmitting(false)
                  }}
                  onConfirm={() => {
                    setIsTncModalOpen(false)
                    dispatch(
                      SignupLinkAction.selfSignup(
                        setSubmitting,
                        {
                          ...values,
                          [`debtor_code_${companyData ? companyData.id : 0}`]: debtorCodes
                            .map((d) => d.value)
                            .join(";")
                        },
                        (errors: any) => {
                          setErrors(errors)
                        },
                        () => {
                          setFinishSubmit(true)
                        }
                      )
                    )
                  }}
                />
              </React.Fragment>
            )}
          </Formik>
        ) : (
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values, actions) => {
              setAcknowledge(false)
              setIsTncModalOpen(true)
            }}>
            {({ isSubmitting, values, handleSubmit, setSubmitting }) => (
              <React.Fragment>
                <FormFields isSubmitting={isSubmitting} handleSubmit={handleSubmit} />
                <TncModal
                  open={isTncModalOpen}
                  isAcknowledge={acknowledge}
                  onAcknowledgeChange={() => {
                    setAcknowledge(!acknowledge)
                  }}
                  onClose={() => {
                    setIsTncModalOpen(false)
                    setSubmitting(false)
                  }}
                  onConfirm={() => {
                    setIsTncModalOpen(false)
                    dispatch(
                      SignupLinkAction.signupBySignupLink(setSubmitting, values, () => {
                        router.push("/auth/login")
                      })
                    )
                  }}
                />
              </React.Fragment>
            )}
          </Formik>
        ))}
    </Box>
  )
}

export default SignupForm
