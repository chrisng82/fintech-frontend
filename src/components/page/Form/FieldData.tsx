import React from "react"
import FormikInput from "@/components/formik/FormikInput"
import { palette } from "@/components/utils/theme"
import { Box, Chip, Collapse, Skeleton, Typography } from "@mui/material"
import { Field } from "formik"
import { useIntl } from "react-intl"
import FormikSelect from "@/components/formik/FormikSelect"
import FormikMultiSelectChipped from "@/components/formik/FormikMultiSelectChipped"
import FormikDatePicker from "@/components/formik/FormikDatePicker"
import FormikSearchSelect from "@/components/formik/FromikSearchSelect"
import FormikMultiSearchSelectChipped from "@/components/formik/FormikMultiSearchSelectChipped"

interface Props {
  editMode: boolean
  fieldName?: string
  disabled?: boolean
  isSelect?: boolean
  isMultiSelectChipped?: boolean
  isTextArea?: boolean
  isDate?: boolean
  isSearchSelect?: boolean
  isMultiSearchSelectChipped?: boolean
  options?: {
    label: string
    value: string | number
  }[]
  maxValues?: number
  isLoading?: boolean
  borderBottom?: boolean
  label?: string
  value?: string | number | boolean | null
  type?: string

  onOpen?: (event: any) => void
  onClose?: (event: any) => void
  getOptionLabel?: (option: any) => string
  renderValue?: (value: number) => any
  debounceOnChange?: (value: any) => void
  optionDisabled?: (option: any) => boolean
}

const FieldData: React.FC<Props> = ({
  editMode,
  fieldName,
  disabled,
  isSelect,
  isMultiSelectChipped,
  isMultiSearchSelectChipped,
  isTextArea,
  isDate,
  isSearchSelect,
  options,
  maxValues,
  isLoading,
  borderBottom = true,
  label,
  value,
  type,
  onOpen,
  onClose,
  getOptionLabel,
  renderValue,
  debounceOnChange,
  optionDisabled
}) => {
  const intl = useIntl()

  return (
    <Box
      sx={{
        width: "100%",
        borderBottom: editMode || !borderBottom ? "none" : "1px solid lightgrey",
        paddingBottom: "0.5rem"
      }}>
      {label && (
        <Typography
          sx={{
            fontWeight: 500
          }}>
          {label}
        </Typography>
      )}
      {isLoading ? (
        <Skeleton height={40} />
      ) : (
        <React.Fragment>
          {fieldName && (
            <Collapse in={editMode}>
              {isSelect ? (
                <Field
                  name={fieldName}
                  disabled={disabled}
                  component={FormikSelect}
                  options={options}
                />
              ) : isMultiSelectChipped ? (
                <Field name={fieldName} disabled={disabled} component={FormikMultiSelectChipped} />
              ) : isMultiSearchSelectChipped ? (
                <Field
                  name={fieldName}
                  disabled={disabled}
                  component={FormikMultiSearchSelectChipped}
                  options={options}
                  maxValues={maxValues}
                  getOptionLabel={getOptionLabel}
                  debounceOnChange={debounceOnChange}
                  optionDisabled={optionDisabled}
                />
              ) : isDate ? (
                <Field name={fieldName} disabled={disabled} component={FormikDatePicker} />
              ) : isSearchSelect ? (
                <Field
                  name={fieldName}
                  disabled={disabled}
                  component={FormikSearchSelect}
                  options={options}
                  loading={isLoading}
                  debounceOnChange={debounceOnChange}
                  onOpen={onOpen}
                  onClose={onClose}
                />
              ) : type ? (
                <Field
                  name={fieldName}
                  disabled={disabled}
                  multiline={isTextArea}
                  component={FormikInput}
                  type={type}
                />
              ) : (
                <Field
                  name={fieldName}
                  disabled={disabled}
                  multiline={isTextArea}
                  component={FormikInput}
                />
              )}
            </Collapse>
          )}
          <Collapse in={!editMode}>
            {value === undefined || value === null || value === "" ? (
              <Typography
                sx={{
                  fontWeight: 400,
                  fontStyle: "italic",
                  color: palette.grey.dark
                }}>
                {intl.formatMessage({ id: "no_data" })}
              </Typography>
            ) : (
              value !== undefined &&
              value !== null &&
              value !== "" &&
              (isMultiSelectChipped ? (
                <Box
                  sx={{
                    display: "flex",
                    flexWrap: "wrap",
                    alignItems: "center",
                    paddingTop: "8px"
                  }}>
                  {(value as string).split(";").map((value: string, index: number) => (
                    <Chip
                      key={`${fieldName}_${index}`}
                      label={value}
                      sx={{
                        mr: 1,
                        mb: 1
                      }}
                    />
                  ))}
                </Box>
              ) : (
                <Typography
                  sx={{
                    fontWeight: 400,
                    color: palette.grey.dark
                  }}>
                  {renderValue?.(value as number) ?? value}
                </Typography>
              ))
            )}
          </Collapse>
        </React.Fragment>
      )}
    </Box>
  )
}

export default FieldData
