import React, { PropsWithChildren, ReactElement } from "react"
import { Avatar } from "@mui/material"

interface Props extends PropsWithChildren {
  name?: string
  src?: string
  size?: number | string
}

const UserAvatar: React.FC<Props> = ({ name, src, size, children }): ReactElement => {
  const splitName = name ? name.trim().split(" ").slice(0, 2) : []
  
  return (
    <Avatar
      src={src}
      sx={{
        width: size ?? "48px",
        height: size ?? "48px",
        fontSize: size && `calc(${size} / 2)`,
        backgroundColor: "#cd4693"
      }}>
      {children
        ? children
        : !Boolean(src) && splitName.length > 0
        ? `${splitName.map((n) => n[0]).join("")}`
        : undefined}
    </Avatar>
  )
}

export default UserAvatar
