import GreyButton from "@/components/Base/GreyButton"
import FormikInput from "@/components/formik/FormikInput"
import { palette } from "@/components/utils/theme"
import { RootState } from "@/stores"
import CompanyAction from "@/stores/Company/Action"
import { LoadingButton } from "@mui/lab"
import { Box, Drawer, Typography } from "@mui/material"
import { Field, Form, Formik } from "formik"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

interface Props {
  open: boolean
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

const CompaniesFilter: React.FC<Props> = ({ open, setOpen }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { filters, isLoading } = useSelector((state: RootState) => state.company.companyList)

  const handleDrawerClose = () => {
    setOpen(false)
  }

  return (
    <Drawer
      sx={{
        flexShrink: 0,
        "& .MuiDrawer-paper": {
          margin: "8px",
          boxSizing: "border-box",
          border: "1px solid lightgrey",
          height: "calc(100% - 16px)",
          background: palette.purple.lightest,
          padding: "1rem 1rem"
        },

        "& .MuiBackdrop-root": {
          backgroundColor: "transparent"
        }
      }}
      PaperProps={{
        elevation: 3
      }}
      onClose={handleDrawerClose}
      anchor="right"
      open={open}>
      <Typography mb={3}>{intl.formatMessage({ id: "companies_filter" })}</Typography>
      <Formik
        enableReinitialize
        initialValues={{
          code: filters.code ?? "",
          name: filters.name ?? ""
        }}
        onSubmit={(values) => {
          dispatch(CompanyAction.fetchCompanyList(values))
        }}>
        {({ handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "1rem"
              }}>
              <Field
                name="code"
                label={intl.formatMessage({ id: "company_code" })}
                component={FormikInput}
              />
              <Field
                name="name"
                label={intl.formatMessage({ id: "company_name" })}
                component={FormikInput}
              />

              <Box
                sx={{
                  display: "flex",
                  gap: "1rem"
                }}>
                <GreyButton
                  fullWidth
                  variant="contained"
                  isLoadingButton
                  loading={isLoading}
                  onClick={() => {
                    dispatch(
                      CompanyAction.fetchCompanyList({
                        code: "",
                        name: ""
                      })
                    )
                  }}>
                  {intl.formatMessage({ id: "reset" })}
                </GreyButton>
                <LoadingButton type="submit" fullWidth variant="contained">
                  {intl.formatMessage({ id: "filter" })}
                </LoadingButton>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </Drawer>
  )
}

export default CompaniesFilter
