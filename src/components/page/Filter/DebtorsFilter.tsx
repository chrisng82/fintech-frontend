import GreyButton from "@/components/Base/GreyButton"
import FormikInput from "@/components/formik/FormikInput"
import { palette } from "@/components/utils/theme"
import { RootState } from "@/stores"
import DebtorAction from "@/stores/Debtor/Action"
import { LoadingButton } from "@mui/lab"
import { Box, Drawer, Typography } from "@mui/material"
import { Field, Form, Formik } from "formik"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

interface Props {
  open: boolean
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

const DebtorsFilter: React.FC<Props> = ({ open, setOpen }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { current_page, per_page, filters, isLoading } = useSelector(
    (state: RootState) => state.debtor.list
  )

  const handleDrawerClose = () => {
    setOpen(false)
  }

  return (
    <Drawer
      sx={{
        flexShrink: 0,
        "& .MuiDrawer-paper": {
          margin: "8px",
          boxSizing: "border-box",
          border: "1px solid lightgrey",
          height: "calc(100% - 16px)",
          background: palette.purple.lightest,
          padding: "1rem 1rem"
        },

        "& .MuiBackdrop-root": {
          backgroundColor: "transparent"
        }
      }}
      PaperProps={{
        elevation: 3
      }}
      onClose={handleDrawerClose}
      anchor="right"
      open={open}>
      <Typography mb={3}>{intl.formatMessage({ id: "debtors_filter" })}</Typography>
      <Formik
        enableReinitialize
        initialValues={{
          code: filters.code ?? "",
          name: filters.name ?? ""
        }}
        onSubmit={(values) => {
          dispatch(
            DebtorAction.fetchDebtors(current_page, per_page, {
              ...filters,
              code: values.code,
              name: values.name
            })
          )
        }}>
        {({ handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "1rem"
              }}>
              <Field
                name="code"
                label={intl.formatMessage({ id: "debtor_code" })}
                component={FormikInput}
              />
              <Field
                name="name"
                label={intl.formatMessage({ id: "debtor_name" })}
                component={FormikInput}
              />

              <Box
                sx={{
                  display: "flex",
                  gap: "1rem"
                }}>
                <GreyButton
                  fullWidth
                  type="reset"
                  variant="contained"
                  isLoadingButton
                  loading={isLoading}
                  onClick={() => {
                    dispatch(
                      DebtorAction.fetchDebtors(current_page, per_page, {
                        ...filters,
                        code: "",
                        name: ""
                      })
                    )
                  }}>
                  {intl.formatMessage({ id: "reset" })}
                </GreyButton>
                <LoadingButton type="submit" fullWidth variant="contained">
                  {intl.formatMessage({ id: "filter" })}
                </LoadingButton>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </Drawer>
  )
}

export default DebtorsFilter
