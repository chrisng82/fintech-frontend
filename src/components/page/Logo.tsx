import { Box } from "@mui/material"
import React, { ReactElement } from "react"

interface Props {
  button?: boolean
}

const logoUrl = process.env.NEXT_PUBLIC_PROJECT_LOGO

const Content: React.FC = () => (
  <Box
    sx={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    }}>
    <img height={130} className="object-cover" src={logoUrl} alt="logo" />
  </Box>
)

const Logo: React.FC<Props> = ({ button = false }): ReactElement => {
  if (button) {
    return <Content />
  }

  return <Content />
}

export default Logo
