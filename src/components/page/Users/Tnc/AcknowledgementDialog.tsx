import {
  AppBar,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Toolbar,
  Typography
} from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import TncEditor from "../../Companies/ViewCompany/TncTab/TncEditor"
import { useSelector } from "react-redux"
import { RootState } from "@/stores"

interface Props {
  contents: string
  open: boolean
  handleOnConfirm?: any
  handleOnRefuse?: any
}

const AcknowledgementDialog: React.FC<Props> = ({
  contents,
  open,
  handleOnConfirm,
  handleOnRefuse
}): ReactElement => {
  const intl = useIntl()
  const { user } = useSelector((state: RootState) => state.app)

  return (
    <Dialog open={open} fullScreen>
      <AppBar
        sx={{
          position: "relative",
          backgroundColor: "rgba(203, 0, 255, 0.6)",
          borderRadius: 0
        }}>
        <Toolbar>
          <Typography variant="h6" component="div">
            {`${intl.formatMessage({ id: "new" })} ${user?.companyCode} ${intl.formatMessage({ id: "term_condition" })}`}
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent dividers>
        <TncEditor contents={contents} readonly={true} />
      </DialogContent>
      <DialogActions>
        <Button variant="contained" color="warning" autoFocus onClick={handleOnRefuse}>
          {intl.formatMessage({ id: "logout" })}
        </Button>
        <Button variant="contained" color="primary" onClick={handleOnConfirm}>
          {intl.formatMessage({ id: "accept_continue" })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default AcknowledgementDialog
