import React from "react"
import { Box, Typography } from "@mui/material"
import { useIntl } from "react-intl"

import { LoadingButton } from "@mui/lab"
import DownloadIcon from "@mui/icons-material/Download"

interface Props {
  label: string
  isLoading: boolean
  onClick: () => void
  disabled?: boolean
}

const DownloadButton: React.FC<Props> = ({ label, isLoading, disabled, onClick }) => {
  const intl = useIntl()

  return (
    <Box
      sx={{
        paddingBottom: "0.5rem"
      }}>
      <Typography
        sx={{
          fontWeight: 500,
          marginBottom: 1
        }}>
        {label}
      </Typography>
      <LoadingButton
        disabled={disabled}
        variant="contained"
        size="small"
        startIcon={<DownloadIcon />}
        sx={{
          color: "white"
        }}
        loading={isLoading}
        onClick={onClick}>
        {intl.formatMessage({ id: "download" })}
      </LoadingButton>
    </Box>
  )
}

export default DownloadButton
