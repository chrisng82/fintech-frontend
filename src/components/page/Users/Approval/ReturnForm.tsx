import FormikInput from '@/components/formik/FormikInput'
import { RootState } from '@/stores'
import UserAction from '@/stores/User/Action'
import { LoadingButton } from '@mui/lab'
import { DialogActions, DialogContent, DialogContentText, Typography } from '@mui/material'
import { Field, Form, Formik } from 'formik'
import React, { ReactElement } from 'react'
import { useIntl } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import { object, string } from 'yup'

interface Props {
    onClose: () => void
}

const ReturnForm: React.FC<Props> = ({
    onClose
}): ReactElement => {
    const intl = useIntl()
    const dispatch = useDispatch()

    const { data, is_loading } = useSelector((state: RootState) => state.user.selfSignupUser.item)

    return (
        <Formik
            initialValues={{
                returned_remark: ""
            }}
            validationSchema={object({
                returned_remark: string()
            })}
            onSubmit={(values) => {
                if (data) {
                    dispatch(
                        UserAction.returnSelfSignupById(
                            data.id,
                            values
                        ))
                    onClose()
                }
            }}
        >
            {({ isSubmitting, handleSubmit }) => (
                <Form onSubmit={handleSubmit}>
                    <DialogContent sx={{
                        backgroundColor: "white",
                        display: "flex",
                        flexDirection: "column",
                        gap: 2,
                        paddingTop: 0
                    }}>
                        <DialogContentText>
                            {intl.formatMessage({ id: "return_approval_desc" })}
                        </DialogContentText>
                        <Field
                            name="returned_remark"
                            label={`${intl.formatMessage({ id: "remark" })} (${intl.formatMessage({ id: "optional" })})`}
                            component={FormikInput}
                            fullWidth
                            multiline
                            rows={4}
                            variant="outlined"
                        />
                    </DialogContent>
                    <DialogActions sx={{
                        backgroundColor: "white"
                    }}>
                        <LoadingButton type="button" variant="outlined" onClick={onClose} loading={isSubmitting || is_loading}>
                            {intl.formatMessage({ id: "cancel" })}
                        </LoadingButton>
                        <LoadingButton type="submit" color="warning" variant="contained" loading={isSubmitting || is_loading}>
                            {intl.formatMessage({ id: "confirm" })}
                        </LoadingButton>
                    </DialogActions>
                </Form>
            )}
        </Formik>
    )
}


export default ReturnForm