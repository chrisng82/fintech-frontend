import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"

import { Box, Card, CardContent, CardHeader, Grid, Typography } from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import DownloadButton from "./DownloadButton"
import UserAction from "@/stores/User/Action"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"

interface Props {}

const CompanyProfile: React.FC<Props> = (): ReactElement => {
  const dispatch = useDispatch()
  const intl = useIntl()

  const { data, is_loading: isLoading } = useSelector(
    (state: RootState) => state.user.selfSignupUser.item
  )

  const downloadAttachment = (type: string) => {
    let document = data?.documents.find((document) => document.type === type)

    if (document) {
      dispatch(UserAction.downloadDocumentById(document.id, document.file_name))
    }
  }

  return (
    <>
      {data && (
        <Card
          sx={{
            padding: "1rem 0.5rem",
            backgroundColor:
              data.user_status === ResStatusValues[ResStatusEnum.INACTIVE] ? "#FFD0D0" : "#FFF"
          }}>
          <CardHeader
            title={
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between"
                }}>
                <Typography>{intl.formatMessage({ id: "company_profile" })}</Typography>
              </Box>
            }
          />
          <CardContent>
            <Grid container spacing={2} rowSpacing={4}>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "first_name" })}
                  value={data.first_name}
                />
              </Grid>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "last_name" })}
                  value={data.last_name}
                />
              </Grid>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "phone" })}
                  value={data.phone}
                />
              </Grid>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "email" })}
                  value={data.email}
                />
              </Grid>
              <Grid item xs={12}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "customer_type" })}
                  value={intl.formatMessage({ id: data.customer_type ?? "customer_type" })}
                />
              </Grid>
              <Grid item xs={12}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "company_reg_no" })}
                  value={data.company_reg_no}
                />
              </Grid>
              <Grid item xs={12}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "company_name" })}
                  value={data.company_name}
                />
              </Grid>
              {data.customer_type === "new_customer" && (
                <>
                  <Grid item xs={12}>
                    <FieldData
                      editMode={false}
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "company_addr" })}
                      value={data.company_addr}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DownloadButton
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "ssm_form" })}
                      onClick={() => {
                        downloadAttachment("ssm_form")
                      }}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DownloadButton
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "form_49" })}
                      onClick={() => {
                        downloadAttachment("form_49")
                      }}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DownloadButton
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "bank_statement" })}
                      onClick={() => {
                        downloadAttachment("bank_statement")
                      }}
                    />
                  </Grid>
                </>
              )}
            </Grid>
          </CardContent>
        </Card>
      )}
    </>
  )
}

export default CompanyProfile
