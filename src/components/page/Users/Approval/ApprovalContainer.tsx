import { Stack } from "@mui/material"
import React, { ReactElement } from "react"
import TabContainer from "./TabContainer"
import ApprovalList from "./ApprovalList"

export enum SelectedApprovalTabEnum {
  ALL = "all",
  WAITING_ON_YOU = "waiting_on_you",
  PENDING = "pending",
  APPROVED = "approved",
  REJECTED = "rejected"
}

interface Props {
  selectedTab: SelectedApprovalTabEnum
}

const ApprovalContainer: React.FC<Props> = ({ selectedTab }): ReactElement => {
  return (
    <Stack
      sx={{
        width: "100%"
      }}>
      <TabContainer selectedTab={selectedTab} />

      <ApprovalList selectedTab={selectedTab} />
    </Stack>
  )
}

export default ApprovalContainer
