import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"

import { Box, Card, CardContent, CardHeader, Grid, Typography } from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import DownloadButton from "./DownloadButton"
import UserAction from "@/stores/User/Action"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"

interface Props {}

const DebtorAcknowledgement: React.FC<Props> = (): ReactElement => {
  const intl = useIntl()

  const { data, is_loading: isLoading } = useSelector(
    (state: RootState) => state.user.selfSignupUser.item
  )

  return (
    <>
      {data && (
        <Card
          sx={{
            padding: "1rem 0.5rem",
            backgroundColor:
              data.user_status === ResStatusValues[ResStatusEnum.INACTIVE] ? "#FFD0D0" : "#FFF"
          }}>
          <CardHeader
            title={
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between"
                }}>
                <Typography>{intl.formatMessage({ id: "debtor_acknowledgement" })}</Typography>
              </Box>
            }
          />
          <CardContent>
            <Grid container spacing={2} rowSpacing={4}>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "company_name" })}
                  value={data.tnc_confirmation?.company_name}
                />
              </Grid>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "name" })}
                  value={data.tnc_confirmation?.name}
                />
              </Grid>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "ic_number" })}
                  value={data.tnc_confirmation?.ic_number}
                />
              </Grid>
              <Grid item xs={6}>
                <FieldData
                  editMode={false}
                  isLoading={isLoading}
                  label={intl.formatMessage({ id: "designation" })}
                  value={data.tnc_confirmation?.designation}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      )}
    </>
  )
}

export default DebtorAcknowledgement
