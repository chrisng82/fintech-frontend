import React, { ReactElement, useEffect, useState } from "react"

import { SelectedApprovalTabEnum } from "./ApprovalContainer"
import { useDispatch, useSelector } from "react-redux"
import UserAction from "@/stores/User/Action"
import { useIntl } from "react-intl"

import { Box, Button, Card, CircularProgress, Grid, Stack, Switch, Typography } from "@mui/material"
import { RootState } from "@/stores"
import { motion } from "framer-motion"
import CompanyProfile from "./CompanyProfile"
import SelectedCompany from "./SelectedCompany"
import GeneralModal from "@/components/modals/GeneralModal"
import CheckCircleIcon from "@mui/icons-material/CheckCircle"
import AssignmentReturnIcon from "@mui/icons-material/AssignmentReturn"
import DoDisturbIcon from "@mui/icons-material/DoDisturb"
import useAuth from "@/components/hooks/useAuth"
import ApprovalStatus from "./ApprovalStatus"
import ApprovalForm from "./ApprovalForm"
import { useRouter } from "next/navigation"
import ReturnForm from "./ReturnForm"
import DebtorAcknowledgement from "./DebtorAcknowledgement"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"
import SelfSignupStatusEnum, { SelfSignupStatusValues } from "@/constants/SelfSignupStatus"

interface Props {
  selectedTab: SelectedApprovalTabEnum
  id: number
}

const ViewApproval: React.FC<Props> = ({ selectedTab, id }): ReactElement => {
  const intl = useIntl()
  const { isSuperAdmin, isEfichainAdmin, isCompanyAdmin, isCompanyStaff } = useAuth()
  const dispatch = useDispatch()
  const router = useRouter()

  const [visibleRejectModal, setVisibleRejectModal] = useState(false)
  const [visibleReturnModal, setVisibleReturnModal] = useState(false)
  const [visibleApproveModal, setVisibleApproveModal] = useState(false)

  const { data, is_loading: isLoading } = useSelector(
    (state: RootState) => state.user.selfSignupUser.item
  )

  useEffect(() => {
    dispatch(UserAction.fetchSelfSignupUser(id))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const displayAction = () => {
    if (data) {
      if (data.status === 0 || data.status === 100) return false
      if (isSuperAdmin || isEfichainAdmin) {
        return true
      }

      if (data.status === 1) {
        if (isCompanyStaff) {
          return true
        }
      }
      if (data.status === 2) {
        if (isCompanyAdmin) {
          return true
        }
      }
    } else {
      return false
    }
  }

  const handleCloseRejectModal = () => {
    setVisibleRejectModal(false)
  }

  const handleConfirmRejectModal = () => {
    dispatch(UserAction.rejectSelfSignupById(id))
    setVisibleRejectModal(false)
  }

  const handleCloseApproveModal = () => {
    setVisibleApproveModal(false)
  }

  const handleConfirmApproveModal = () => {
    dispatch(UserAction.adminApproveSelfSignupById(id))
    setVisibleApproveModal(false)
  }

  const handleCloseReturnModal = () => {
    setVisibleReturnModal(false)
  }

  return (
    <Stack
      sx={{
        width: "100%"
      }}>
      {data && !isLoading ? (
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: 0.3, duration: 0.5 }}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "1rem"
            }}>
            <GeneralModal
              open={visibleRejectModal}
              title={intl.formatMessage({ id: "reject_approval_title" })}
              content={intl.formatMessage({ id: "reject_approval_desc" })}
              isLoading={isLoading}
              confirmColor="error"
              onClose={handleCloseRejectModal}
              onConfirm={handleConfirmRejectModal}
            />

            <GeneralModal
              open={visibleReturnModal}
              title={intl.formatMessage({ id: "return_approval_title" })}
              isLoading={isLoading}>
              <ReturnForm onClose={handleCloseReturnModal} />
            </GeneralModal>

            {data.status === 2 && isCompanyAdmin ? (
              <GeneralModal
                open={visibleApproveModal}
                title={intl.formatMessage({ id: "admin_approve_approval_title" })}
                content={intl.formatMessage({ id: "admin_approve_approval_desc" })}
                isLoading={isLoading}
                onClose={handleCloseApproveModal}
                onConfirm={handleConfirmApproveModal}
              />
            ) : (
              <GeneralModal
                open={visibleApproveModal}
                title={intl.formatMessage({ id: "approve_approval_title" })}
                isLoading={isLoading}>
                <ApprovalForm setVisibleApproveModal={setVisibleApproveModal} />
              </GeneralModal>
            )}

            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                gap: "1rem",
                justifyContent: "space-between"
              }}>
              <Button
                variant="contained"
                color="secondary"
                onClick={() => {
                  router.push(`/users/approval/${selectedTab}`)
                }}>
                {intl.formatMessage({ id: "back" })}
              </Button>
              {/* {data.status === SelfSignupStatusValues[SelfSignupStatusEnum.APPROVED] ? (
                <Stack direction="row" spacing={1} alignItems="center">
                  <Typography>{intl.formatMessage({ id: "inactive" })}</Typography>
                  <Switch
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                      if (event.target.checked) {
                        dispatch(UserAction.setUserStatus(data))
                      } else {
                        dispatch(UserAction.setUserStatus(data))
                      }
                    }}
                    inputProps={{ "aria-label": "Switch demo" }}
                    sx={{ transform: "scale(1.5)" }}
                    color="success"
                    defaultChecked={data.user_status === ResStatusValues[ResStatusEnum.ACTIVE]}
                    checked={data.user_status === ResStatusValues[ResStatusEnum.ACTIVE]}
                  />
                  <Typography>{intl.formatMessage({ id: "active" })}</Typography>
                </Stack>
              ) : (
                ""
              )} */}

              {displayAction() && !isLoading && (
                <>
                  <Box
                    sx={{
                      display: "flex",
                      gap: 2
                    }}>
                    <Button
                      variant="contained"
                      color="error"
                      onClick={() => setVisibleRejectModal(true)}
                      startIcon={<DoDisturbIcon />}>
                      {intl.formatMessage({ id: "reject" })}
                    </Button>
                    {data.status === 2 && isCompanyAdmin && (
                      <Button
                        variant="contained"
                        color="warning"
                        onClick={() => setVisibleReturnModal(true)}
                        startIcon={<AssignmentReturnIcon />}>
                        {intl.formatMessage({ id: "return" })}
                      </Button>
                    )}
                    <Button
                      variant="contained"
                      onClick={() => setVisibleApproveModal(true)}
                      startIcon={<CheckCircleIcon />}>
                      {intl.formatMessage({ id: "approve" })}
                    </Button>
                  </Box>
                </>
              )}
            </Box>

            <ApprovalStatus />
            <DebtorAcknowledgement />
            <CompanyProfile />
            <SelectedCompany selectedTab={selectedTab} />
          </Box>
        </motion.div>
      ) : (
        <Card
          sx={{
            padding: "1rem 0.5rem"
          }}>
          <CircularProgress />
        </Card>
      )}
    </Stack>
  )
}

export default ViewApproval
