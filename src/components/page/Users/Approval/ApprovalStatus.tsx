import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"

import { Box, Card, CardContent, CardHeader, Grid, Typography } from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import dayjs from "dayjs"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"

interface Props {}

const ApprovalStatus: React.FC<Props> = (): ReactElement => {
  const intl = useIntl()

  const { data, is_loading: isLoading } = useSelector(
    (state: RootState) => state.user.selfSignupUser.item
  )

  return (
    <>
      {data && (
        <Card
          sx={{
            padding: "1rem 0.5rem",
            backgroundColor:
              data.user_status === ResStatusValues[ResStatusEnum.INACTIVE] ? "#FFD0D0" : "#FFF"
          }}>
          <CardHeader
            title={
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between"
                }}>
                <Typography>{intl.formatMessage({ id: "approval_status" })}</Typography>
              </Box>
            }
          />
          <CardContent>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "1rem"
              }}>
              <Grid container spacing={2} rowSpacing={4}>
                <Grid item xs={12}>
                  <FieldData
                    editMode={false}
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "status" })}
                    value={intl.formatMessage({ id: `approval_status_${data.status}` })}
                  />
                </Grid>
                <Grid item xs={4}>
                  <FieldData
                    editMode={false}
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "approval_level_1" })}
                    value={
                      data.approval_1
                        ? `${data.approval_1?.first_name} ${data.approval_1?.last_name} (${data.approval_1.email})`
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={4}>
                  <FieldData
                    editMode={false}
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "approval_level_2" })}
                    value={
                      data.approval_2
                        ? `${data.approval_2?.first_name} ${data.approval_2?.last_name} (${data.approval_2.email})`
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={4}>
                  {data.status === 0 ? (
                    <FieldData
                      editMode={false}
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "rejected_by" })}
                      value={
                        data.rejected_by
                          ? `${data.rejected_by?.first_name} ${data.rejected_by?.last_name}  (${data.rejected_by.email})`
                          : ""
                      }
                    />
                  ) : data.returned_by ? (
                    <FieldData
                      editMode={false}
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "returned_by" })}
                      value={
                        data.rejected_by
                          ? `${data.rejected_by?.first_name} ${data.rejected_by?.last_name}  (${data.rejected_by.email})`
                          : ""
                      }
                    />
                  ) : null}
                </Grid>

                <Grid item xs={4}>
                  <FieldData
                    editMode={false}
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "approval_level_1_at" })}
                    value={
                      data.approval_1_at
                        ? dayjs(data.approval_1_at).format("DD/MM/YYYY HH:mm A")
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={4}>
                  <FieldData
                    editMode={false}
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "approval_level_2_at" })}
                    value={
                      data.approval_2_at
                        ? dayjs(data.approval_2_at).format("DD/MM/YYYY HH:mm A")
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={4}>
                  {data.status === 0 ? (
                    <FieldData
                      editMode={false}
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "rejected_by_at" })}
                      value={
                        data.rejected_at ? dayjs(data.rejected_at).format("DD/MM/YYYY HH:mm A") : ""
                      }
                    />
                  ) : data.returned_by ? (
                    <FieldData
                      editMode={false}
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "returned_by_at" })}
                      value={
                        data.returned_at ? dayjs(data.returned_at).format("DD/MM/YYYY HH:mm A") : ""
                      }
                    />
                  ) : null}
                </Grid>
                {data.status === 1 && data.returned_remark && (
                  <Grid item xs={12}>
                    <FieldData
                      editMode={false}
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "returned_remark" })}
                      value={data.returned_remark}
                    />
                  </Grid>
                )}
              </Grid>
            </Box>
          </CardContent>
        </Card>
      )}
    </>
  )
}

export default ApprovalStatus
