import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"

import { Box, DialogContent, Grid, Typography } from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import UserAction from "@/stores/User/Action"
import { LoadingButton } from "@mui/lab"
import DebtorAction from "@/stores/Debtor/Action"
import { debounce } from "lodash"
import { Form, Formik } from "formik"
import { SelfSignupUserCompany } from "@/stores/User/Types"
import { array, object, string } from "yup"

interface Props {
  setVisibleApproveModal: React.Dispatch<React.SetStateAction<boolean>>
}

const ApprovalForm: React.FC<Props> = ({ setVisibleApproveModal }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { data, is_loading: isLoading } = useSelector(
    (state: RootState) => state.user.selfSignupUser.item
  )
  const { data: debtors } = useSelector((state: RootState) => state.debtor.list)

  const onClose = () => {
    setVisibleApproveModal(false)
  }

  const handleOnDebtorCodeChange = (companyCode: string, value: string) => {
    dispatch(DebtorAction.fetchDebtorByCompanyCode(companyCode, [`selfSignup:${value}`]))
  }

  const debounceOnChange = debounce(handleOnDebtorCodeChange, 1000)

  const createValidationSchema = (companies: SelfSignupUserCompany[] = []) => {
    let shape: any = {}

    companies.forEach((company) => {
      shape[`debtor_code_${company.id}`] = array().of(string())
    })

    return object().shape(shape)
  }

  const createInitialValues = (companies: SelfSignupUserCompany[] = []) => {
    let initialValues: any = {}

    companies.forEach((company) => {
      initialValues[`debtor_code_${company.id}`] = company.debtors
        ? company.debtors.map((debtor) => debtor.code)
        : []
    })

    return initialValues
  }

  return (
    <DialogContent sx={{
      backgroundColor: "white"
    }}>
      {data && (
        <Formik
          initialValues={createInitialValues(data?.companies)}
          validationSchema={createValidationSchema(data?.companies)}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            if (data) {
              dispatch(
                UserAction.approveSelfSignupById(
                  data?.id,
                  values,
                  (errors: any) => {
                    if (errors) {
                      setErrors(errors)
                    } else {
                      setVisibleApproveModal(false)
                    }
                  },
                  () => {
                    setVisibleApproveModal(false)
                  }
                )
              )
            }

            setSubmitting(false)
          }}>
          {({ isSubmitting, handleSubmit, values }) => (
            <Form onSubmit={handleSubmit}>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: "1rem"
                }}>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "1rem"
                  }}>
                  {data.companies?.map((company) => (
                    <>
                      <Box
                        sx={{
                          display: "flex",
                          justifyContent: "space-between"
                        }}>
                        <Typography>{company.name_01}</Typography>
                      </Box>
                      <Box
                        sx={{
                          display: "flex",
                          flexDirection: "column",
                          gap: "1rem"
                        }}>
                        <Grid container spacing={2} rowSpacing={4}>
                          <Grid item xs={12}>
                            <FieldData
                              editMode={false}
                              isMultiSelectChipped
                              isLoading={isLoading}
                              label={intl.formatMessage({
                                id: "debtor_code_enter_during_signup"
                              })}
                              value={company.user_debtor_code.join(";") ?? ""}
                            />
                          </Grid>
                          <Grid item xs={12}>
                            <FieldData
                              editMode
                              fieldName={`debtor_code_${company.id}`}
                              isMultiSearchSelectChipped
                              options={
                                debtors.length > 0
                                  ? debtors
                                    .filter(
                                      (debtor) =>
                                        debtor.company_id === company.id &&
                                        !values[`debtor_code_${company.id}`]?.includes(
                                          debtor.code
                                        )
                                    )
                                    .map((debtor) => ({
                                      label: `${debtor.name_01} (${debtor.code})`,
                                      value: debtor.code
                                    }))
                                  : []
                              }
                              debounceOnChange={(value) => debounceOnChange(company.code, value)}
                              label={intl.formatMessage({ id: "debtor_code" })}
                              optionDisabled={() => {
                                return values[`debtor_code_${company.id}`].length > 9
                              }}
                            />
                          </Grid>
                        </Grid>
                      </Box>
                    </>
                  ))}
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "end",
                    gap: 1
                  }}>
                  <LoadingButton
                    onClick={onClose}
                    variant="outlined"
                    loading={isLoading || isSubmitting}>
                    {intl.formatMessage({ id: "cancel" })}
                  </LoadingButton>
                  <LoadingButton
                    variant="contained"
                    type="submit"
                    loading={isLoading || isSubmitting}>
                    {intl.formatMessage({ id: "confirm" })}
                  </LoadingButton>
                </Box>
              </Box>
            </Form>
          )}
        </Formik>
      )}
    </DialogContent>
  )
}

export default ApprovalForm
