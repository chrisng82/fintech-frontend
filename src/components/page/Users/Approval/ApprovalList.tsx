import React, { useEffect, useState } from "react"
import dayjs from "dayjs"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"
import { SelectedApprovalTabEnum } from "./ApprovalContainer"

import Datatable, { IColumn } from "@/components/Datatable/DocumentsDatatable"
import {
  Autocomplete,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  FormLabel,
  Input,
  Stack,
  TextField,
  Typography
} from "@mui/material"
import UserAction from "@/stores/User/Action"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"
import Link from "next/link"
import ArticleIcon from "@mui/icons-material/Article"
import { useRouter } from "next/navigation"
import DebtorSelector from "@/components/Autocomplete/DebtorSelector"
import { DebtorCodeOption } from "../../Form/SignupForm"
import SnackbarAction from "@/stores/Snackbar/Action"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"
import SelfSignupStatusEnum, { SelfSignupStatusValues } from "@/constants/SelfSignupStatus"

interface IProps {
  selectedTab: SelectedApprovalTabEnum
}
interface Option {
  label: string
  value: string | number
}

const ApprovalList: React.FC<IProps> = (props) => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const userStatusOptions: Option[] = [
    {
      label: ResStatusEnum.INACTIVE,
      value: ResStatusValues[ResStatusEnum.INACTIVE]
    },
    { label: ResStatusEnum.ACTIVE, value: ResStatusValues[ResStatusEnum.ACTIVE] }
  ]
  const { selectedTab } = props
  const [emailInput, setEmailInput] = useState<string>("")
  const [userStatus, setSelectedUserStatus] = useState<Option | null>(null)

  const { data, page_size, current_page, total, is_loading, filters } = useSelector(
    (state: RootState) => state.user.selfSignupUser.list
  )

  useEffect(() => {
    dispatch(UserAction.fetchSelfSignupUsers(selectedTab))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const columns: IColumn[] = [
    { id: "no", label: "No", minWidth: 28, align: "center", sortable: false, filterable: false },
    {
      id: "customer_type",
      label: intl.formatMessage({ id: "customer_type" }),
      minWidth: 100,
      type: "string",
      sortable: true,
      format: (value) => intl.formatMessage({ id: value as string })
    },
    {
      id: "company_name",
      label: intl.formatMessage({ id: "company_name" }),
      minWidth: 100,
      sortable: true,
      type: "string"
    },
    {
      id: "company_reg_no",
      label: intl.formatMessage({ id: "company_reg_no" }),
      minWidth: 100,
      sortable: true,
      type: "string"
    },
    {
      id: "first_name",
      label: intl.formatMessage({ id: "first_name" }),
      minWidth: 100,
      sortable: true,
      type: "string"
    },
    {
      id: "last_name",
      label: intl.formatMessage({ id: "last_name" }),
      minWidth: 100,
      sortable: true,
      type: "string"
    },
    {
      id: "email",
      label: intl.formatMessage({ id: "email" }),
      minWidth: 100,
      sortable: true,
      type: "string"
    },
    {
      id: "companies",
      label: intl.formatMessage({ id: "linked_debtor_codes" }),
      minWidth: 100,
      sortable: true,
      render: (row) =>
        row.companies.map((v: any) => {
          if (!v.user_debtor_code && !v.debtor) return null

          return (
            <Chip
              key={v.id}
              sx={{
                m: 0.5
              }}
              label={v.user_debtor_code ? v.user_debtor_code : v.debtor ? v.debtor.code : ""}
            />
          )
        })
    },
    {
      id: "status",
      label: intl.formatMessage({ id: "status" }),
      minWidth: 100,
      render: (row, column) => (
        <Stack>
          <Typography>{intl.formatMessage({ id: `approval_status_${row.status}` })}</Typography>
          {/* {row.status === SelfSignupStatusValues[SelfSignupStatusEnum.APPROVED] ? (
            <Button
              variant="contained"
              size="small"
              sx={{
                color: "white",
                backgroundColor:
                  row.users.status === ResStatusValues[ResStatusEnum.ACTIVE] ? "#06B323" : "#970606"
              }}>
              {row.users.status === ResStatusValues[ResStatusEnum.ACTIVE]
                ? intl.formatMessage({ id: "active" })
                : intl.formatMessage({ id: "inactive" })}
            </Button>
          ) : (
            ""
          )} */}
        </Stack>
      )
    },
    {
      id: "created_at",
      label: intl.formatMessage({ id: "created_at" }),
      minWidth: 70,
      type: "date",
      sortable: true,
      format: (value) => dayjs.utc(value).local().format("DD/MM/YYYY HH:mm")
    },
    {
      id: "actions",
      label: "Action",
      minWidth: 60,
      align: "center",
      sortable: false,
      filterable: false,
      render: (row, column) => (
        <Link href={`/users/approval/${selectedTab}/${row.id}`}>
          <LoadingButton
            variant="contained"
            size="small"
            startIcon={<ArticleIcon />}
            sx={{
              color: "white"
            }}>
            {intl.formatMessage({ id: "view" })}
          </LoadingButton>
        </Link>
      )
    }
  ]

  const handleChangePage = (event: any, newPage: number) => {
    dispatch(UserAction.fetchSelfSignupUsers(selectedTab, newPage + 1, page_size))
  }

  const handleChangeRowsPerPage = (event: any) => {
    const newPageSize = parseInt(event.target.value, 10)

    dispatch(UserAction.fetchSelfSignupUsers(selectedTab, 1, newPageSize))
  }

  const handleFilter = () => {
    if (filters.user_email) {
      const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
      const isEmail = regex.test(filters.user_email)
      if (!isEmail) {
        dispatch(
          SnackbarAction.openSnackbar(intl.formatMessage({ id: "email_type_invalid" }), "error")
        )
        return false
      }
    }

    dispatch(UserAction.fetchSelfSignupUsers(selectedTab, 1, page_size))
  }

  const handleUserStatusChange = (event: any, newValue: Option | null) => {
    setSelectedUserStatus(newValue)
    if (newValue?.value) {
      dispatch(
        UserAction.setSelfSignupUsersFilters({
          ...filters,
          user_status: newValue?.value
        })
      )
    }
  }

  const handleResetFilter = () => {
    setSelectedUserStatus(null)
    setEmailInput("")
    dispatch(
      UserAction.fetchSelfSignupUsers(selectedTab, 1, page_size, {
        ...filters,
        debtor_codes: [],
        user_email: "",
        user_status: ""
      })
    )
  }

  return (
    <Stack spacing={2}>
      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardHeader>
          <Typography variant="h6">{intl.formatMessage({ id: "filters" })}</Typography>
        </CardHeader>
        <CardContent>
          <Stack spacing={2}>
            <Stack direction="row" spacing={2}>
              <DebtorSelector
                data={filters.debtor_codes.map((d) => ({
                  label: d,
                  value: d
                }))}
                onChange={(value: DebtorCodeOption[]) =>
                  dispatch(
                    UserAction.setSelfSignupUsersFilters({
                      ...filters,
                      debtor_codes: value.map((v) => v.value)
                    })
                  )
                }
              />
              {/* <Stack>
                <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>
                  {intl.formatMessage({ id: "user_status" })}
                </FormLabel>
                <Autocomplete
                  value={userStatus}
                  disablePortal
                  id="user_status"
                  options={userStatusOptions}
                  sx={{ width: 300 }}
                  renderInput={(params) => <TextField {...params} />}
                  onChange={handleUserStatusChange}
                  disabled={selectedTab !== SelectedApprovalTabEnum.APPROVED}
                />
              </Stack> */}
            </Stack>
            {/* <FormLabel sx={{ textAlign: "left", mb: 1, fontWeight: 500 }}>
              {intl.formatMessage({ id: "email" })}
            </FormLabel>
            <Input
              value={emailInput}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setEmailInput(event.target.value)
                dispatch(
                  UserAction.setSelfSignupUsersFilters({
                    ...filters,
                    user_email: event.target.value
                  })
                )
              }}
            /> */}
          </Stack>
        </CardContent>
        <CardActions
          sx={{
            justifyContent: "flex-end"
          }}>
          <Button variant="contained" color="secondary" onClick={handleResetFilter}>
            {intl.formatMessage({ id: "reset" })}
          </Button>
          <Button variant="contained" color="primary" onClick={handleFilter}>
            {intl.formatMessage({ id: "filter" })}
          </Button>
        </CardActions>
      </Card>
      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardContent>
          <Datatable
            docType={SelectedApprovalTabEnum.WAITING_ON_YOU}
            data={data}
            columns={columns}
            isLoading={is_loading}
            page={current_page}
            rowsPerPage={page_size}
            totalRows={total}
            idKey="id"
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
            onRowClick={(row) => router.push(`/users/approval/${selectedTab}/${row.id}`)}
            rowStyle={(row) => {
              if (row.returned_by) {
                return {
                  backgroundColor: "rgba(255, 255, 0, 0.3)"
                }
              }
              return {}
            }}
          />
        </CardContent>
      </Card>
    </Stack>
  )
}

export default ApprovalList
