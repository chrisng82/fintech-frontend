import React, { ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Dialog,
  DialogContent,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography
} from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import DownloadButton from "./DownloadButton"
import UserAction from "@/stores/User/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import NoDataSVG from "@/assets/SVG/NoDataSVG"
import { SelectedApprovalTabEnum } from "./ApprovalContainer"
import useAuth from "@/components/hooks/useAuth"
import DebtorSelector from "@/components/Autocomplete/DebtorSelector"
import { DebtorCodeOption } from "../../Form/SignupForm"
import {
  SelfSignupUserDocument,
  SelfSignupUserCompany,
  CompanyDebtorCodes
} from "@/stores/User/Types"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"
import { LoadingButton } from "@mui/lab"
import dayjs from "dayjs"
import { CompanyTnc } from "@/stores/App/Types"
import TncEditor from "../../Companies/ViewCompany/TncTab/TncEditor"

interface CompanyRowProps {
  company: SelfSignupUserCompany
  debtorCodes: CompanyDebtorCodes[]
  documents: SelfSignupUserDocument[]
  editMode: boolean
  setDebtorCodes: React.Dispatch<React.SetStateAction<CompanyDebtorCodes[]>>
  userTnc?: CompanyTnc
}

const CompanyRow: React.FC<CompanyRowProps> = ({
  company,
  debtorCodes,
  documents,
  editMode,
  setDebtorCodes,
  userTnc
}): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { data, is_loading: isLoading } = useSelector(
    (state: RootState) => state.user.selfSignupUser.item
  )
  const [previewTnc, setPreviewTnc] = useState<boolean>(false)

  const downloadAttachment = (companyId: number) => {
    let document = documents.find(
      (document) => document.company_id && document.company_id === companyId
    )

    if (document) {
      if (data?.user_status === ResStatusValues[ResStatusEnum.ACTIVE]) {
        dispatch(UserAction.downloadDocumentById(document.id, document.file_name))
      } else {
        dispatch(
          SnackbarAction.openSnackbar(intl.formatMessage({ id: "user_not_active" }), "error")
        )
      }
    }
  }
  return (
    <React.Fragment key={`selected-company-${company.id}`}>
      <TableRow tabIndex={-1} key={company.id}>
        <TableCell colSpan={3}>
          <Typography>{company.name_01}</Typography>
        </TableCell>
        <TableCell colSpan={3}>
          <Tooltip
            title={`${intl.formatMessage({ id: "preview" })} ${intl.formatMessage({ id: "term_condition" })}`}
            arrow
            slotProps={{
              popper: {
                modifiers: [
                  {
                    name: "offset",
                    options: {
                      offset: [70, 0]
                    }
                  }
                ]
              }
            }}>
            <Box
              sx={{ cursor: "pointer" }}
              onClick={() => {
                setPreviewTnc(true)
              }}>
              {userTnc?.user_tnc?.status === ResStatusValues[ResStatusEnum.ACTIVE] ? (
                <Typography align="right">{`${intl.formatMessage({ id: "term_condition_accepted" })}`}</Typography>
              ) : (
                <Typography align="right">{`${intl.formatMessage({ id: "term_condition_declined" })}`}</Typography>
              )}
              {userTnc?.tnc?.code ? (
                <Typography variant="subtitle2" gutterBottom align="right">
                  {`${userTnc.tnc.code} # ${intl.formatMessage({ id: "revision" })}  ${userTnc.tnc.revision_code}`}
                </Typography>
              ) : (
                ""
              )}
              {userTnc?.user_tnc?.acknowledgement_date ? (
                <Typography variant="subtitle2" gutterBottom align="right">
                  {dayjs(userTnc?.user_tnc?.acknowledgement_date).format("DD/MM/YYYY HH:mm A")}
                </Typography>
              ) : (
                <Typography variant="subtitle2" gutterBottom align="right">
                  {intl.formatMessage({ id: "no_acknowledgement_date" })}
                </Typography>
              )}
            </Box>
          </Tooltip>
          <Dialog
            open={previewTnc}
            onClose={() => {
              setPreviewTnc(false)
            }}
            fullWidth
            maxWidth="lg">
            <Box
              sx={{
                backgroundColor: "white",
                padding: 2,
                maxHeight: "80vh"
              }}>
              <DialogContent
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: "1rem"
                }}>
                <TncEditor contents={userTnc?.tnc?.contents ?? ""} readonly={true} />
              </DialogContent>
            </Box>
          </Dialog>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <FieldData
            editMode={false}
            isMultiSelectChipped
            isLoading={isLoading}
            borderBottom={false}
            label={intl.formatMessage({ id: "debtor_code_enter_during_signup" })}
            value={
              company.debtors
                .filter((debtor) => debtor.user_debtor_code)
                .map((debtor) => debtor.user_debtor_code)
                .concat(company.user_debtor_code)
                .join(";") ?? ""
            }
          />
        </TableCell>
        <TableCell>
          {editMode ? (
            <DebtorSelector
              data={debtorCodes.find((d) => d.companyId === company.id)?.debtorCodes ?? []}
              labelId="linked_debtor_codes"
              required
              suggestedName={data?.company_name}
              onChange={(value: DebtorCodeOption[]) => {
                setDebtorCodes((prevState) =>
                  prevState.map((prev) =>
                    prev.companyId === company.id ? { ...prev, debtorCodes: value } : prev
                  )
                )
              }}
            />
          ) : (
            <FieldData
              editMode={false}
              isMultiSelectChipped
              isLoading={isLoading}
              borderBottom={false}
              label={intl.formatMessage({ id: "linked_debtor_codes" })}
              value={company.debtors.map((debtor) => debtor.code).join(";") ?? ""}
            />
          )}
        </TableCell>
        <TableCell>
          {documents.find(
            (document) => document.company_id && document.company_id === company.id
          ) ? (
            <DownloadButton
              disabled={data?.user_status !== ResStatusValues[ResStatusEnum.ACTIVE]}
              isLoading={isLoading}
              label={intl.formatMessage({ id: "agreement" })}
              onClick={() => {
                downloadAttachment(company.id)
              }}
            />
          ) : (
            <></>
          )}
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

interface Props {
  selectedTab: SelectedApprovalTabEnum
}

const SelectedCompany: React.FC<Props> = ({ selectedTab }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isSuperAdmin, isEfichainAdmin, isCompanyAdmin } = useAuth()

  const [editMode, setEditMode] = useState<boolean>(false)
  const [debtorCodes, setDebtorCodes] = useState<CompanyDebtorCodes[]>([])

  const { data } = useSelector((state: RootState) => state.user.selfSignupUser.item)

  const isSubmittable = !debtorCodes.some((d) => d.debtorCodes.length === 0)

  const resetDebtorCodes = () => {
    if (data?.companies) {
      setDebtorCodes(
        data.companies.map((company) => ({
          companyId: company.id,
          debtorCodes: company.debtors.map((debtor) => ({
            value: debtor.code,
            label: debtor.code
          }))
        }))
      )
    }
  }

  useEffect(() => {
    resetDebtorCodes()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data?.companies])

  return (
    <>
      {data && (
        <Card
          sx={{
            padding: "1rem 0.5rem",
            backgroundColor:
              data.user_status === ResStatusValues[ResStatusEnum.INACTIVE] ? "#FFD0D0" : "#FFF"
          }}>
          <CardHeader
            title={
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between"
                }}>
                <Typography>{intl.formatMessage({ id: "selected_company" })}</Typography>
                {selectedTab === SelectedApprovalTabEnum.APPROVED &&
                  (isSuperAdmin || isEfichainAdmin || isCompanyAdmin) && (
                    <Stack direction="row" spacing={2}>
                      {editMode && (
                        <Button
                          variant="contained"
                          color="secondary"
                          onClick={() => {
                            setEditMode(false)
                            resetDebtorCodes()
                          }}>
                          {intl.formatMessage({ id: "cancel" })}
                        </Button>
                      )}
                      <Button
                        variant="contained"
                        color="warning"
                        disabled={
                          !isSubmittable ||
                          data.user_status === ResStatusValues[ResStatusEnum.INACTIVE]
                        }
                        onClick={() => {
                          if (editMode) {
                            if (isSubmittable) {
                              dispatch(
                                UserAction.updateApprovedSelfSignupById(data.id, debtorCodes)
                              )
                              setEditMode(false)
                            }
                          } else {
                            setEditMode(true)
                          }
                        }}>
                        {intl.formatMessage({ id: editMode ? "save" : "edit" })}
                      </Button>
                    </Stack>
                  )}
              </Box>
            }
          />
          <CardContent>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "1rem"
              }}>
              <TableContainer>
                <Table>
                  <TableBody
                    sx={{
                      "& .MuiTableCell-root": {
                        borderBottom: "none"
                      }
                    }}>
                    {data.companies ? (
                      data.companies.map((company) => (
                        <CompanyRow
                          key={`company-${company.id}`}
                          company={company}
                          debtorCodes={debtorCodes}
                          documents={data.documents}
                          editMode={editMode}
                          setDebtorCodes={setDebtorCodes}
                          userTnc={data?.user_tncs}
                        />
                      ))
                    ) : (
                      <TableRow>
                        <TableCell colSpan={3}>
                          <Box
                            sx={{
                              padding: "5rem"
                            }}>
                            <NoDataSVG />
                          </Box>
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          </CardContent>
        </Card>
      )}
    </>
  )
}

export default SelectedCompany
