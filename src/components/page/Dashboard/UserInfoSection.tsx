"use client"

import {
  Box,
  Button,
  Collapse,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  IconButton,
  Paper,
  Stack,
  Switch,
  Tooltip,
  Typography
} from "@mui/material"
import React, { ReactElement, useEffect, useState } from "react"
import UserAvatar from "../Avatar/UserAvatar"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { useIntl } from "react-intl"
import { palette } from "@/components/utils/theme"
import dayjs from "dayjs"
import { ResStatusValues } from "@/constants/ResStatus"
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft"
import { motion } from "framer-motion"
import UserType from "@/stores/User/Types"
import UserAction from "@/stores/User/Action"
import HandshakeIcon from "@mui/icons-material/Handshake"
import TncEditor from "../Companies/ViewCompany/TncTab/TncEditor"
import AppAction from "@/stores/App/Action"

const UserInfoSection: React.FC = (): ReactElement => {
  const dispatch = useDispatch()
  const intl = useIntl()

  const { user, companyTnc } = useSelector((state: RootState) => state.app)
  const { kernelArtisanCommand } = useSelector((state: RootState) => state.user)
  const [previewTnc, setPreviewTnc] = useState<boolean>(false)

  const [showMore, setShowMore] = useState<boolean>(false)

  const handleToggleShowMore = () => {
    setShowMore((prev) => !prev)
  }

  useEffect(() => {
    if (user?.type_str === UserType.SUPER_ADMIN) {
      dispatch(UserAction.getArtisanKernelCommand())
    }
    console.log(companyTnc)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <motion.div
      initial={{
        y: -50,
        opacity: 0
      }}
      animate={{
        y: 0,
        opacity: 1
      }}
      transition={{
        duration: 0.75
      }}>
      <Paper
        elevation={3}
        sx={{
          borderRadius: "1rem",
          background: "#fff"
        }}>
        {user?.type_str === UserType.SUPER_ADMIN ? (
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between", // Align items horizontally
              alignItems: "center", // Center items vertically
              gap: "1rem",
              borderRadius: "1rem",
              padding: "1rem",
              textAlign: "left" // Align text content to the left
            }}>
            <Typography>{intl.formatMessage({ id: "kernel_artisan_commands" })}</Typography>

            <Switch
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                if (event.target.checked) {
                  dispatch(UserAction.updateArtisanKernelCommand(true))
                } else {
                  dispatch(UserAction.updateArtisanKernelCommand(false))
                }
              }}
              inputProps={{ "aria-label": "Switch demo" }}
              sx={{ transform: "scale(1.5)" }}
              color="success"
              checked={kernelArtisanCommand}
            />
          </Box>
        ) : (
          ""
        )}
        <Box
          sx={{
            display: "flex",
            gap: "1rem",
            borderRadius: "1rem",
            padding: "1rem"
          }}>
          <UserAvatar name={`${user?.first_name} ${user?.last_name}`} size="96px" />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center"
            }}>
            <Box
              sx={{
                display: "flex",
                gap: "1rem"
              }}>
              <Box>
                <Box
                  sx={{
                    display: "flex",
                    gap: "1rem"
                  }}>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.grey.main
                    }}>
                    {intl.formatMessage({ id: "user_name" })}:
                  </Typography>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.primary.main
                    }}>
                    {`${user?.first_name} ${user?.last_name}`}
                  </Typography>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    gap: "1rem"
                  }}>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.grey.main
                    }}>
                    {intl.formatMessage({ id: "company" })}:
                  </Typography>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.primary.main
                    }}>
                    {user?.companyCode}
                  </Typography>
                </Box>
              </Box>
              <Box>
                <Box
                  sx={{
                    display: "flex",
                    gap: "1rem"
                  }}>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.grey.main
                    }}>
                    {intl.formatMessage({ id: "email" })}:
                  </Typography>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.primary.main
                    }}>
                    {user?.email}
                  </Typography>
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    gap: "1rem"
                  }}>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.grey.main
                    }}>
                    {intl.formatMessage({ id: "status" })}:
                  </Typography>
                  <Typography
                    sx={{
                      fontWeight: 500,
                      color: palette.primary.main
                    }}>
                    {user?.status && ResStatusValues[user.status]}
                  </Typography>
                </Box>
              </Box>
            </Box>
            <Collapse in={showMore}>
              <Box
                sx={{
                  display: "flex",
                  gap: "1rem"
                }}>
                <Box>
                  <Box
                    sx={{
                      display: "flex",
                      gap: "1rem"
                    }}>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.grey.main
                      }}>
                      {intl.formatMessage({ id: "email_verified_at" })}:
                    </Typography>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.primary.main
                      }}>
                      {dayjs(user?.email_verified_at).format("ddd, d MMM YYYY HH:mm A")}
                    </Typography>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      gap: "1rem"
                    }}>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.grey.main
                      }}>
                      {intl.formatMessage({ id: "account_created_at" })}:
                    </Typography>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.primary.main
                      }}>
                      {dayjs(user?.created_at).format("ddd, d MMM YYYY HH:mm A")}
                    </Typography>
                  </Box>
                </Box>
                <Box>
                  <Box
                    sx={{
                      display: "flex",
                      gap: "1rem"
                    }}>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.grey.main
                      }}>
                      {intl.formatMessage({ id: "password_changed_at" })}:
                    </Typography>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.primary.main
                      }}>
                      {dayjs(user?.password_changed_at).format("ddd, d MMM YYYY HH:mm A")}
                    </Typography>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      gap: "1rem"
                    }}>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.grey.main
                      }}>
                      {intl.formatMessage({ id: "last_login" })}:
                    </Typography>
                    <Typography
                      sx={{
                        fontWeight: 500,
                        color: palette.primary.main
                      }}>
                      {dayjs(user?.last_login).format("ddd, d MMM YYYY HH:mm A")}
                    </Typography>
                  </Box>
                </Box>
              </Box>
            </Collapse>
          </Box>
        </Box>

        <Divider
          onClick={handleToggleShowMore}
          sx={{
            cursor: "pointer",
            paddingBottom: "1rem",
            ml: 1,
            mr: 1
          }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              gap: "1rem"
            }}>
            <Typography
              sx={{
                fontWeight: 500,
                color: palette.purple.main
              }}>
              {showMore
                ? intl.formatMessage({ id: "show_less" })
                : intl.formatMessage({ id: "show_more" })}
            </Typography>
            <ChevronLeftIcon
              sx={{
                transform: showMore ? "rotate(-270deg)" : "rotate(-90deg)",
                transition: "transform 0.3s cubic-bezier(0.4, 0, 0.2, 1)",
                color: palette.purple.main
              }}
            />
          </Box>
        </Divider>
        {user?.type_str === UserType.DEBTOR ? (
          <Stack direction="row" justifyContent="end">
            {companyTnc?.tnc ? (
              <Box>
                <Tooltip title={`${intl.formatMessage({ id: "term_condition" })}`}>
                  <IconButton
                    onClick={() => {
                      if (user) {
                        dispatch(AppAction.appGetCompanyTnc(user))
                        setPreviewTnc(true)
                      }
                    }}>
                    <HandshakeIcon color="primary" />
                  </IconButton>
                </Tooltip>
                <Dialog
                  open={previewTnc}
                  onClose={() => {
                    setPreviewTnc(false)
                  }}
                  fullWidth
                  maxWidth="lg">
                  <DialogTitle>{`${user?.companyCode} ${intl.formatMessage({ id: "term_condition" })}`}</DialogTitle>
                  <Box
                    sx={{
                      backgroundColor: "white",
                      padding: 2,
                      maxHeight: "80vh",
                      overflow: "hidden"
                    }}>
                    <DialogContent
                      sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "1rem"
                      }}>
                      <TncEditor contents={companyTnc.tnc.contents ?? ""} readonly={true} />
                    </DialogContent>
                  </Box>
                </Dialog>
              </Box>
            ) : (
              ""
            )}
          </Stack>
        ) : (
          ""
        )}
      </Paper>
    </motion.div>
  )
}

export default UserInfoSection
