import { Box, Grid, Paper, Typography } from "@mui/material"
import React, { PropsWithChildren, ReactElement } from "react"
import { useIntl } from "react-intl"
import BusinessIcon from "@mui/icons-material/Business"
import GroupIcon from "@mui/icons-material/Group"
import InventoryIcon from "@mui/icons-material/Inventory"
import { motion } from "framer-motion"
import { palette } from "@/components/utils/theme"
import Link from "next/link"
import UserType from "@/stores/User/Types"
import { useSelector } from "react-redux"

const CardBox = ({ children }: PropsWithChildren) => {
  return (
    <Paper
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        gap: "1rem",
        aspectRatio: "1/1",
        padding: "1.5rem",
        borderRadius: "1rem",
        background: "#fff",
        cursor: "pointer",
        transition: "all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94)",

        "& svg": {
          fontSize: "3rem"
        },

        "& p": {
          fontSize: "1.5rem"
        },

        "&:hover": {
          transform: "scale(1.05)",
          background: palette.purple.main,
          "& svg, p": {
            color: "#fff"
          }
        }
      }}>
      {children}
    </Paper>
  )
}

const cardBoxes = [
  {
    icon: <BusinessIcon />,
    text: "company_list",
    link: "/companies",
    user_type: [
      UserType.SUPER_ADMIN,
      UserType.COMPANY_ADMIN,
      UserType.COMPANY_STAFF,
      UserType.EFICHAIN_ADMIN,
      UserType.EFICHAIN_PRODUCT
    ]
  },
  {
    icon: <GroupIcon />,
    text: "debtor_list",
    link: "/debtors",
    user_type: [
      UserType.SUPER_ADMIN,
      UserType.COMPANY_ADMIN,
      UserType.COMPANY_STAFF,
      UserType.EFICHAIN_ADMIN,
      UserType.EFICHAIN_PRODUCT
    ]
  },
  {
    icon: <InventoryIcon />,
    text: "documents",
    link: "/documents",
    user_type: [
      UserType.SUPER_ADMIN,
      UserType.COMPANY_ADMIN,
      UserType.COMPANY_STAFF,
      UserType.EFICHAIN_ADMIN,
      UserType.EFICHAIN_PRODUCT,
      UserType.DEBTOR
    ]
  }
]

const GetStartedSection: React.FC = (): ReactElement => {
  const intl = useIntl()

  const { user } = useSelector((state: any) => state.app)

  return (
    <motion.div
      initial={{
        y: -50,
        opacity: 0
      }}
      animate={{
        y: 0,
        opacity: 1
      }}
      transition={{
        duration: 0.75
      }}>
      <Box
        component="section"
        id="get-started"
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          gap: "2rem"
        }}>
        <Typography
          sx={{
            fontSize: "2rem"
          }}>
          {intl.formatMessage({ id: "get_started" })}
        </Typography>
        <Grid
          container
          spacing={2}
          sx={{
            justifyContent: "center"
          }}>
          {cardBoxes.map((cardBox) => {
            return cardBox.user_type?.includes(user?.type_str) ? (
              <Grid key={`get-started_${cardBox.text}`} item xs={12} md={6} lg={3}>
                <Link href={cardBox.link}>
                  <CardBox>
                    {cardBox.icon}
                    <Typography>{intl.formatMessage({ id: cardBox.text })}</Typography>
                  </CardBox>
                </Link>
              </Grid>
            ) : null
          })}
        </Grid>
      </Box>
    </motion.div>
  )
}

export default GetStartedSection
