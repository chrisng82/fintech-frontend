import { Stack } from "@mui/material"
import React, { ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { EficoreSyncStatusValues } from "@/constants/EficoreSyncStatus"
import TabContainer from "./TabContainer"
import DocumentsProgress from "./DocumentsProgress"
import OthersProgress from "./OthersProgress"

export enum SelectedTabEnum {
  DOCUMENTS = "documents",
  OTHERS = "others"
}

interface IProps {
  selectedTab: SelectedTabEnum
}

const EficoreSyncContainer: React.FC<IProps> = ({ selectedTab }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isLoading, data } = useSelector((state: RootState) => state.eficoreSync.list)

  const [isAllCompleted, setIsAllCompleted] = useState(true)

  useEffect(() => {
    data.forEach((item) => {
      if (item.status !== EficoreSyncStatusValues.COMPLETED) {
        setIsAllCompleted(false)
      }
    })
  }, [data])

  return (
    <Stack
      sx={{
        width: "100%"
      }}>
      <TabContainer selectedTab={selectedTab} />

      {selectedTab === SelectedTabEnum.DOCUMENTS && <DocumentsProgress />}
      {selectedTab === SelectedTabEnum.OTHERS && <OthersProgress />}
    </Stack>
  )
}

export default EficoreSyncContainer
