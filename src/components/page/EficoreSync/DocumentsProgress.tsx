import { Box, Card, CardContent, CardHeader, Stack, Typography } from "@mui/material"
import React, { ReactElement, useEffect, useState } from "react"
import PendingDocuments from "./PendingDocuments"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"
import RefreshIcon from "@mui/icons-material/Refresh"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import EficoreSyncAction from "@/stores/EficoreSync/Action"
import SyncedProgressTable from "./SyncedProgressTable"
import { EficoreSyncStatusValues } from "@/constants/EficoreSyncStatus"
import SnackbarAction from "@/stores/Snackbar/Action"
import { SelectedTabEnum } from "."

const DocumentsProgress: React.FC = (): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isLoading, stats, total } = useSelector((state: RootState) => state.eficoreSync.list)

  return (
    <Stack spacing={2}>
      <Card>
        <CardHeader
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between"
              }}>
              <Typography>{intl.formatMessage({ id: "pending_sync" })}</Typography>
              <LoadingButton
                variant="contained"
                loading={isLoading}
                startIcon={<RefreshIcon />}
                onClick={() => {
                  if (stats.in_progress > 0 || stats.pending > 0) {
                    dispatch(
                      SnackbarAction.openSnackbar(
                        intl.formatMessage({
                          id: "error_cannot_create_job_while_not_all_completed"
                        }),
                        "error"
                      )
                    )
                  } else {
                    dispatch(EficoreSyncAction.createEficoreSyncJob(SelectedTabEnum.DOCUMENTS))
                  }
                }}>
                {intl.formatMessage({ id: "create_sync_schedule" })}
              </LoadingButton>
            </Box>
          }
        />
        <CardContent>
          <PendingDocuments syncType={SelectedTabEnum.DOCUMENTS} />
        </CardContent>
      </Card>
      <Card>
        <CardHeader
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between"
              }}>
              <Typography>{intl.formatMessage({ id: "sync_progress" })}</Typography>
              <LoadingButton
                variant="contained"
                color="success"
                loading={isLoading}
                startIcon={<RefreshIcon />}
                onClick={() => {
                  dispatch(EficoreSyncAction.fetchEficoreSyncs(SelectedTabEnum.DOCUMENTS))
                }}>
                {intl.formatMessage({ id: "refresh" })}
              </LoadingButton>
            </Box>
          }
        />
        <CardContent>
          <SyncedProgressTable syncType={SelectedTabEnum.DOCUMENTS} />
        </CardContent>
      </Card>
    </Stack>
  )
}

export default DocumentsProgress
