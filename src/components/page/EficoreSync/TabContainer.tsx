import { palette } from "@/components/utils/theme"
import { Box, Typography } from "@mui/material"
import React, { PropsWithChildren, ReactElement } from "react"
import { SelectedTabEnum } from "."
import { useRouter } from "next/navigation"
import { motion } from "framer-motion"
import { toProperCase } from "@/components/utils/TextHelper"
import Link from "next/link"

interface TabBoxProps extends PropsWithChildren {
  active: boolean
  link: string
}

const TabBox = ({ active, link, children }: TabBoxProps) => {
  return (
    <Link href={link}>
      <Box
        sx={{
          display: "flex",
          padding: "0.8rem 1.5rem",
          borderBottom: active ? `solid 3px ${palette.purple.main}` : "none",
          textDecoration: "none",
          cursor: "pointer",
          transition: "all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94)",

          "& p": {
            color: active ? "rgba(0, 0, 0, 0.87);" : "rgba(0, 0, 0, 0.3)",
            fontWeight: 700,

            "&:hover": {
              transform: "scale(1.05)"
            }
          }
        }}>
        {children}
      </Box>
    </Link>
  )
}

interface IProps {
  selectedTab: SelectedTabEnum
}

const TabContainer: React.FC<IProps> = ({ selectedTab }): ReactElement => {
  const router = useRouter()

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}>
      <Box
        sx={{
          position: "relative",
          display: "flex",
          alignItems: "center",
          mb: 2,
          gap: "1rem",
          borderBottom: "solid 1px #ccc"
        }}>
        <TabBox
          active={selectedTab === SelectedTabEnum.DOCUMENTS}
          link={`/admin/eficore-sync/${SelectedTabEnum.DOCUMENTS}`}>
          <Typography>{toProperCase(SelectedTabEnum.DOCUMENTS)}</Typography>
        </TabBox>
        <TabBox
          active={selectedTab === SelectedTabEnum.OTHERS}
          link={`/admin/eficore-sync/${SelectedTabEnum.OTHERS}`}>
          <Typography>{toProperCase(SelectedTabEnum.OTHERS)}</Typography>
        </TabBox>
      </Box>
    </motion.div>
  )
}

export default TabContainer
