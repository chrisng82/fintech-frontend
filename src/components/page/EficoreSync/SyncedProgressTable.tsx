import NoDataSVG from "@/assets/SVG/NoDataSVG"
import { RootState } from "@/stores"
import EficoreSyncAction from "@/stores/EficoreSync/Action"
import {
  Box,
  Chip,
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow
} from "@mui/material"
import dayjs from "dayjs"
import React, { ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import customParseFormat from "dayjs/plugin/customParseFormat"
import { EficoreSync } from "@/stores/EficoreSync/Types"
import { EficoreSyncStatusValues } from "@/constants/EficoreSyncStatus"
import { SelectedTabEnum } from "."

dayjs.extend(customParseFormat)

interface RowProps {
  syncType: SelectedTabEnum
  record: EficoreSync
  index: number
  page: number
  rowsPerPage: number
}

interface IPros {
  syncType: SelectedTabEnum
}

const RecordRow: React.FC<RowProps> = ({ syncType, record, index, page, rowsPerPage }) => {
  return (
    <React.Fragment>
      <TableRow>
        <TableCell>{(page - 1) * rowsPerPage + index + 1}</TableCell>
        {syncType === SelectedTabEnum.DOCUMENTS ? <TableCell>{record.doc_type}</TableCell> : null}
        <TableCell>{record.company_code}</TableCell>
        <TableCell align="right">
          {record.total_synced_record}/{record.total_record}
        </TableCell>
        <TableCell>{dayjs(record.created_at).format("DD/MM/YYYY")}</TableCell>
        <TableCell>
          <Chip
            color={
              record.status === 0
                ? "error"
                : record.status === 1
                  ? "success"
                  : record.status === 2
                    ? "info"
                    : "warning"
            }
            label={EficoreSyncStatusValues[record.status]}
          />
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

const SyncedProgressTable: React.FC<IPros> = ({ syncType }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { data, isLoading, page_size, current_page, last_page, total } = useSelector(
    (state: RootState) => state.eficoreSync.list
  )

  const [rowsPerPage, setRowsPerPage] = useState<number>(10)

  const handleChangePage = (event: any, newPage: number) => {
    dispatch(EficoreSyncAction.setEficoreSyncsPagination(page_size, newPage + 1, last_page, total))
    dispatch(EficoreSyncAction.fetchEficoreSyncs(syncType, newPage + 1, page_size))
  }

  const handleChangeRowsPerPage = (event: any) => {
    const newPageSize = parseInt(event.target.value, 10)
    setRowsPerPage(newPageSize)
    dispatch(EficoreSyncAction.setEficoreSyncsPagination(newPageSize, 1, last_page, total))
    dispatch(EficoreSyncAction.fetchEficoreSyncs(syncType, 1, page_size))
  }

  useEffect(() => {
    dispatch(EficoreSyncAction.fetchEficoreSyncs(syncType))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <TableContainer
      sx={{
        "& th": {
          fontWeight: "bold !important"
        },
        "& td": {
          fontWeight: "500 !important"
        }
      }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{intl.formatMessage({ id: "no." })}</TableCell>
            {syncType === SelectedTabEnum.DOCUMENTS ? (
              <TableCell>{intl.formatMessage({ id: "doc_type" })}</TableCell>
            ) : null}
            <TableCell>{intl.formatMessage({ id: "company" })}</TableCell>
            <TableCell align="right">
              {intl.formatMessage({
                id:
                  syncType === SelectedTabEnum.DOCUMENTS
                    ? "total_synced_documents"
                    : "total_synced_debtors"
              })}
            </TableCell>
            <TableCell>{intl.formatMessage({ id: "date_created" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "status" })}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {isLoading ? (
            <TableRow>
              <TableCell colSpan={6}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center"
                  }}>
                  <CircularProgress
                    sx={{
                      width: "3rem !important",
                      height: "3rem !important"
                    }}
                  />
                </Box>
              </TableCell>
            </TableRow>
          ) : data.length > 0 ? (
            data.map((record: EficoreSync, index: number) => (
              <RecordRow
                key={`doc-${record.id}`}
                syncType={syncType}
                record={record}
                index={index}
                page={current_page}
                rowsPerPage={rowsPerPage}
              />
            ))
          ) : (
            <TableRow>
              <TableCell colSpan={6}>
                <Box
                  sx={{
                    padding: "5rem 20rem"
                  }}>
                  <NoDataSVG />
                </Box>
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>

      <TablePagination
        rowsPerPageOptions={[10, 20, 50, 100]}
        component="div"
        count={total}
        rowsPerPage={rowsPerPage}
        page={current_page - 1}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </TableContainer>
  )
}

export default SyncedProgressTable
