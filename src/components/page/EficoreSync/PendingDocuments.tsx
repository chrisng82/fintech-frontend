import { Typography, Alert, Stack, Box, CircularProgress } from "@mui/material"
import React, { ReactElement, useEffect } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { motion } from "framer-motion"
import EficoreSyncAction from "@/stores/EficoreSync/Action"
import { SelectedTabEnum } from "."

export interface IProps {
  syncType: SelectedTabEnum
}

const PendingDocumentsTable: React.FC<IProps> = ({ syncType }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isLoading, stats } = useSelector((state: RootState) => state.eficoreSync.list)

  useEffect(() => {
    dispatch(EficoreSyncAction.fetchEficoreSyncs(syncType))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Stack
      spacing={3}
      direction="row"
      sx={{
        alignItems: "baseline",
        justifyContent: "space-between"
      }}>
      <Stack
        spacing={1}
        direction="row"
        sx={{
          alignItems: "baseline"
        }}>
        {isLoading ? (
          <CircularProgress
            sx={{
              width: "3rem",
              height: "3rem"
            }}
          />
        ) : (
          <Typography
            sx={{
              fontSize: "3rem"
            }}>
            {stats.in_progress + stats.pending}
          </Typography>
        )}
        <Typography
          sx={{
            fontSize: "1.5rem",
            fontWeight: "bold"
          }}>
          {intl.formatMessage({ id: "sync_in_progress" }).toLowerCase()}
        </Typography>
      </Stack>
      {!isLoading && (
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}
          style={{
            width: "75%"
          }}>
          {stats.in_progress + stats.pending > 0 ? (
            <Alert
              severity="info"
              sx={{
                display: "flex",
                alignItems: "center"
              }}>
              {intl.formatMessage({ id: "pending_sync_documents_description" })}
            </Alert>
          ) : (
            <Alert severity="success">
              {intl.formatMessage({ id: "documents_synced_description" })}
            </Alert>
          )}
        </motion.div>
      )}
    </Stack>
  )
}

export default PendingDocumentsTable
