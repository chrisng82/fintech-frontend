"use client"
import { palette } from "@/components/utils/theme"
import { Company } from "@/stores/Company/Types"
import { Box, Paper, PaperProps, Typography } from "@mui/material"
import React, { PropsWithChildren, ReactElement } from "react"
import PersonIcon from "@mui/icons-material/Person"
import PhoneIcon from "@mui/icons-material/Phone"
import EmailIcon from "@mui/icons-material/Email"
import { motion } from "framer-motion"
import { useRouter } from "next/navigation"
import Link from "next/link"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import AppAction from "@/stores/App/Action"

interface CardBoxProps extends PaperProps, PropsWithChildren {}

const CardBox = ({ children, ...rest }: CardBoxProps) => {
  return (
    <Paper
      {...rest}
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: "100%",
        padding: "1.5rem",
        borderRadius: "1rem",
        background: "#fff",
        cursor: "pointer",
        transition: "all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94)",

        "&:hover": {
          transform: "scale(1.05)",
          background: palette.purple.main,
          "& p": {
            color: "#fff"
          }
        }
      }}>
      {children}
    </Paper>
  )
}

interface Props {
  company: Company
  index: number
}

const CompanyCard: React.FC<Props> = ({ company, index }): ReactElement => {
  const dispatch = useDispatch()
  const router = useRouter()
  const { user } = useSelector((state: RootState) => state.app)

  return (
    <Link
      href={"/companies/" + company.code}
      onClick={() => {
        if (user) {
          dispatch(AppAction.changeCompanySelect(company.code, user))
        }
      }}>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: index * 0.1, duration: 0.5 }}
        style={{
          flex: "1",
          minHeight: "0"
        }}>
        <CardBox>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "0.5rem"
            }}>
            <Typography
              sx={{
                color: palette.purple.main,
                fontSize: "1.5rem"
              }}>
              {company.code}
            </Typography>
            <Typography>{company.name_01}</Typography>
          </Box>
          <Box
            mt={2}
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "0.5rem"
            }}>
            <Typography
              sx={{
                fontWeight: 500,
                display: "flex",
                gap: "0.5rem",
                alignItems: "center",
                color: company.bill_attention === "" ? palette.grey.main : "inherit",
                fontStyle: company.bill_attention === "" ? "italic" : "normal"
              }}>
              <PersonIcon
                sx={{
                  fontSize: "1.5rem"
                }}
              />
              {company.bill_attention === "" ? "No data" : company.bill_attention}
            </Typography>
            <Typography
              sx={{
                fontWeight: 500,
                display: "flex",
                gap: "0.5rem",
                alignItems: "center",
                color: company.bill_phone_01 === "" ? palette.grey.main : "inherit",
                fontStyle: company.bill_phone_01 === "" ? "italic" : "normal"
              }}>
              <PhoneIcon
                sx={{
                  fontSize: "1.5rem"
                }}
              />
              {company.bill_phone_01 === "" ? "No data" : company.bill_phone_01}
            </Typography>
            <Typography
              sx={{
                fontWeight: 500,
                display: "flex",
                gap: "0.5rem",
                alignItems: "center",
                color: company.bill_email_01 === "" ? palette.grey.main : "inherit",
                fontStyle: company.bill_email_01 === "" ? "italic" : "normal"
              }}>
              <EmailIcon
                sx={{
                  fontSize: "1.5rem"
                }}
              />
              {company.bill_email_01 === "" ? "No data" : company.bill_email_01}
            </Typography>
          </Box>
        </CardBox>
      </motion.div>
    </Link>
  )
}

export default CompanyCard
