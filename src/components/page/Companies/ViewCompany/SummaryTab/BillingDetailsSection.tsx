import React, { ReactElement, useState } from "react"
import { motion } from "framer-motion"
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import FieldData from "@/components/page/Form/FieldData"
import { Form, Formik } from "formik"
import { object, string } from "yup"
import "yup-phone-lite"
import { useRouter } from "next/navigation"
import CompanyAction from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import EditIcon from "@mui/icons-material/Edit"
import { LoadingButton } from "@mui/lab"
import { palette } from "@/components/utils/theme"
import GreyButton from "@/components/Base/GreyButton"

const BillingDetailsSection: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { showCompany } = useSelector((state: RootState) => state.company)

  const [editMode, setEditMode] = useState<boolean>(false)

  const validationSchema = object({
    bill_email_01: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .required(intl.formatMessage({ id: "error_company_bill_email_01_required" })),
    bill_email_02: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .optional(),
    bill_fax_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    bill_fax_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    bill_phone_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    bill_phone_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional()
  })

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.6, duration: 0.5 }}>
      <Formik
        enableReinitialize
        initialValues={{
          bill_email_01: showCompany.data?.bill_email_01 ?? "",
          bill_email_02: showCompany.data?.bill_email_02 ?? "",
          bill_fax_01: showCompany.data?.bill_fax_01 ?? "",
          bill_fax_02: showCompany.data?.bill_fax_02 ?? "",
          bill_phone_01: showCompany.data?.bill_phone_01 ?? "",
          bill_phone_02: showCompany.data?.bill_phone_02 ?? "",
          bill_attention: showCompany.data?.bill_attention ?? "",
          bill_unit_no: showCompany.data?.bill_unit_no ?? "",
          bill_building_name: showCompany.data?.bill_building_name ?? "",
          bill_street_name: showCompany.data?.bill_street_name ?? "",
          bill_district_01: showCompany.data?.bill_district_01 ?? "",
          bill_district_02: showCompany.data?.bill_district_02 ?? "",
          bill_postcode: showCompany.data?.bill_postcode ?? "",
          bill_state_name: showCompany.data?.bill_state_name ?? "",
          bill_country_name: showCompany.data?.bill_country_name ?? ""
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          if (showCompany.data) {
            dispatch(
              CompanyAction.updateCompany(setSubmitting, showCompany.data.id, values, (newData) => {
                setEditMode(false)
                router.push("/companies/" + newData.code)
              })
            )
          } else {
            setSubmitting(false)
            dispatch(
              SnackbarAction.openSnackbar(
                intl.formatMessage({ id: "error_company_not_found" }),
                "error"
              )
            )
            router.push("/companies")
          }
        }}>
        {({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Card
              sx={{
                padding: "1rem 0.5rem"
              }}>
              <CardHeader
                title={
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}>
                    <Typography>{intl.formatMessage({ id: "billing_details" })}</Typography>
                    <Button
                      variant="contained"
                      color="warning"
                      disabled={editMode}
                      onClick={() => setEditMode(true)}
                      startIcon={<EditIcon />}>
                      {intl.formatMessage({ id: "edit" })}
                    </Button>
                  </Box>
                }
              />
              <CardContent>
                <Grid container spacing={2} rowSpacing={4}>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_email_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "email_01" })}
                      value={showCompany.data?.bill_email_01}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_email_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "email_02" })}
                      value={showCompany.data?.bill_email_02}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_fax_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "fax_01" })}
                      value={showCompany.data?.bill_fax_01}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_fax_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "fax_02" })}
                      value={showCompany.data?.bill_fax_02}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_phone_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "phone_01" })}
                      value={showCompany.data?.bill_phone_01}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_phone_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "phone_02" })}
                      value={showCompany.data?.bill_phone_02}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_attention"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "attention" })}
                      value={showCompany.data?.bill_attention}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_unit_no"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "unit_no" })}
                      value={showCompany.data?.bill_unit_no}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_building_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "building_name" })}
                      value={showCompany.data?.bill_building_name}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_street_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "street_name" })}
                      value={showCompany.data?.bill_street_name}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_district_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "district_01" })}
                      value={showCompany.data?.bill_district_01}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_district_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "district_02" })}
                      value={showCompany.data?.bill_district_02}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_postcode"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "postcode" })}
                      value={showCompany.data?.bill_postcode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_state_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "state_name" })}
                      value={showCompany.data?.bill_state_name}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="bill_country_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "country_name" })}
                      value={showCompany.data?.bill_country_name}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Collapse in={editMode}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <GreyButton
                    isLoadingButton
                    type="reset"
                    variant="contained"
                    loading={isSubmitting}
                    onClick={() => setEditMode(false)}>
                    {intl.formatMessage({ id: "cancel" })}
                  </GreyButton>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {intl.formatMessage({ id: "save" })}
                  </LoadingButton>
                </CardActions>
              </Collapse>
            </Card>
          </Form>
        )}
      </Formik>
    </motion.div>
  )
}

export default BillingDetailsSection
