import React, { ReactElement, useState } from "react"
import { motion } from "framer-motion"
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import { useIntl } from "react-intl"
import FieldData from "@/components/page/Form/FieldData"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import EditIcon from "@mui/icons-material/Edit"
import { LoadingButton } from "@mui/lab"
import { palette } from "@/components/utils/theme"
import { Form, Formik } from "formik"
import { object, string } from "yup"
import CompanyAction from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { useRouter } from "next/navigation"
import GreyButton from "@/components/Base/GreyButton"

const CompanyProfileSection: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { showCompany } = useSelector((state: RootState) => state.company)

  const [editMode, setEditMode] = useState<boolean>(false)

  const validationSchema = object({
    code: string().required(intl.formatMessage({ id: "error_company_code_required" })),
    co_reg_no: string().required(intl.formatMessage({ id: "error_company_reg_no_required" })),
    name_01: string().required(intl.formatMessage({ id: "error_company_name_01_required" }))
  })

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.3, duration: 0.5 }}>
      <Formik
        enableReinitialize
        initialValues={{
          code: showCompany.data?.code ?? "",
          ref_code: showCompany.data?.ref_code ?? "",
          co_reg_no: showCompany.data?.co_reg_no ?? "",
          tax_reg_no: showCompany.data?.tax_reg_no ?? "",
          name_01: showCompany.data?.name_01 ?? "",
          name_02: showCompany.data?.name_02 ?? ""
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          if (showCompany.data) {
            dispatch(
              CompanyAction.updateCompany(setSubmitting, showCompany.data.id, values, (newData) => {
                setEditMode(false)
                router.push("/companies/" + newData.code)
              })
            )
          } else {
            setSubmitting(false)
            dispatch(
              SnackbarAction.openSnackbar(
                intl.formatMessage({ id: "error_company_not_found" }),
                "error"
              )
            )
            router.push("/companies")
          }
        }}>
        {({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Card
              sx={{
                padding: "1rem 0.5rem"
              }}>
              <CardHeader
                title={
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}>
                    <Typography>{intl.formatMessage({ id: "company_profile" })}</Typography>
                    <Button
                      variant="contained"
                      color="warning"
                      disabled={editMode}
                      onClick={() => setEditMode(true)}
                      startIcon={<EditIcon />}>
                      {intl.formatMessage({ id: "edit" })}
                    </Button>
                  </Box>
                }
              />
              <CardContent>
                <Grid container spacing={2} rowSpacing={4}>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="code"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "company_code" })}
                      value={showCompany.data?.code}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ref_code"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "ref_code" })}
                      value={showCompany.data?.ref_code}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="co_reg_no"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "company_reg_no" })}
                      value={showCompany.data?.co_reg_no}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="tax_reg_no"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "tax_reg_no" })}
                      value={showCompany.data?.tax_reg_no}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <FieldData
                      editMode={editMode}
                      fieldName="name_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "company_name_01" })}
                      value={showCompany.data?.name_01}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <FieldData
                      editMode={editMode}
                      fieldName="name_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "company_name_02" })}
                      value={showCompany.data?.name_02}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Collapse in={editMode}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <GreyButton
                    isLoadingButton
                    type="reset"
                    variant="contained"
                    loading={isSubmitting}
                    onClick={() => setEditMode(false)}>
                    {intl.formatMessage({ id: "cancel" })}
                  </GreyButton>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {intl.formatMessage({ id: "save" })}
                  </LoadingButton>
                </CardActions>
              </Collapse>
            </Card>
          </Form>
        )}
      </Formik>
    </motion.div>
  )
}

export default CompanyProfileSection
