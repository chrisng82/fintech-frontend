import { RootState } from "@/stores"
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import FieldData from "@/components/page/Form/FieldData"
import { useRouter } from "next/navigation"
import { object, string } from "yup"
import "yup-phone-lite"
import { Form, Formik } from "formik"
import CompanyAction from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { LoadingButton } from "@mui/lab"
import { palette } from "@/components/utils/theme"
import EditIcon from "@mui/icons-material/Edit"
import GreyButton from "@/components/Base/GreyButton"

const ShippingDetailsSection: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { showCompany } = useSelector((state: RootState) => state.company)

  const [editMode, setEditMode] = useState<boolean>(false)

  const validationSchema = object({
    ship_email_01: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .required(intl.formatMessage({ id: "error_company_ship_email_01_required" })),
    ship_email_02: string()
      .email(intl.formatMessage({ id: "error_email_format" }))
      .optional(),
    ship_fax_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    ship_fax_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    ship_phone_01: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    ship_phone_02: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional()
  })

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.9, duration: 0.5 }}>
      <Formik
        enableReinitialize
        initialValues={{
          ship_email_01: showCompany.data?.ship_email_01 ?? "",
          ship_email_02: showCompany.data?.ship_email_02 ?? "",
          ship_fax_01: showCompany.data?.ship_fax_01 ?? "",
          ship_fax_02: showCompany.data?.ship_fax_02 ?? "",
          ship_phone_01: showCompany.data?.ship_phone_01 ?? "",
          ship_phone_02: showCompany.data?.ship_phone_02 ?? "",
          ship_attention: showCompany.data?.ship_attention ?? "",
          ship_unit_no: showCompany.data?.ship_unit_no ?? "",
          ship_building_name: showCompany.data?.ship_building_name ?? "",
          ship_street_name: showCompany.data?.ship_street_name ?? "",
          ship_district_01: showCompany.data?.ship_district_01 ?? "",
          ship_district_02: showCompany.data?.ship_district_02 ?? "",
          ship_postcode: showCompany.data?.ship_postcode ?? "",
          ship_state_name: showCompany.data?.ship_state_name ?? "",
          ship_country_name: showCompany.data?.ship_country_name ?? ""
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          if (showCompany.data) {
            dispatch(
              CompanyAction.updateCompany(setSubmitting, showCompany.data.id, values, (newData) => {
                setEditMode(false)
                router.push("/companies/" + newData.code)
              })
            )
          } else {
            setSubmitting(false)
            dispatch(
              SnackbarAction.openSnackbar(
                intl.formatMessage({ id: "error_company_not_found" }),
                "error"
              )
            )
            router.push("/companies")
          }
        }}>
        {({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Card
              sx={{
                padding: "1rem 0.5rem"
              }}>
              <CardHeader
                title={
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}>
                    <Typography>{intl.formatMessage({ id: "shipping_details" })}</Typography>
                    <Button
                      variant="contained"
                      color="warning"
                      disabled={editMode}
                      onClick={() => setEditMode(true)}
                      startIcon={<EditIcon />}>
                      {intl.formatMessage({ id: "edit" })}
                    </Button>
                  </Box>
                }
              />
              <CardContent>
                <Grid container spacing={2} rowSpacing={4}>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_email_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "email_01" })}
                      value={showCompany.data?.ship_email_01}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_email_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "email_02" })}
                      value={showCompany.data?.ship_email_02}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_fax_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "fax_01" })}
                      value={showCompany.data?.ship_fax_01}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_fax_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "fax_02" })}
                      value={showCompany.data?.ship_fax_02}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_phone_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "phone_01" })}
                      value={showCompany.data?.ship_phone_01}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_phone_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "phone_02" })}
                      value={showCompany.data?.ship_phone_02}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_attention"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "attention" })}
                      value={showCompany.data?.ship_attention}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_unit_no"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "unit_no" })}
                      value={showCompany.data?.ship_unit_no}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_building_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "building_name" })}
                      value={showCompany.data?.ship_building_name}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_street_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "street_name" })}
                      value={showCompany.data?.ship_street_name}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_district_01"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "district_01" })}
                      value={showCompany.data?.ship_district_01}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_district_02"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "district_02" })}
                      value={showCompany.data?.ship_district_02}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_postcode"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "postcode" })}
                      value={showCompany.data?.ship_postcode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_state_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "state_name" })}
                      value={showCompany.data?.ship_state_name}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <FieldData
                      editMode={editMode}
                      fieldName="ship_country_name"
                      isLoading={showCompany.isLoading}
                      label={intl.formatMessage({ id: "country_name" })}
                      value={showCompany.data?.ship_country_name}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Collapse in={editMode}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <GreyButton
                    isLoadingButton
                    type="reset"
                    variant="contained"
                    loading={isSubmitting}
                    onClick={() => setEditMode(false)}>
                    {intl.formatMessage({ id: "cancel" })}
                  </GreyButton>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {intl.formatMessage({ id: "save" })}
                  </LoadingButton>
                </CardActions>
              </Collapse>
            </Card>
          </Form>
        )}
      </Formik>
    </motion.div>
  )
}

export default ShippingDetailsSection
