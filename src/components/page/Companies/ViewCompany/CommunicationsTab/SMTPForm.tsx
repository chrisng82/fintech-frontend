import CommunicationTypeEnum, { CommunicationTypeValues } from "@/constants/CommunicationType"
import { RootState } from "@/stores"
import CompanyAction from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { Form, Formik } from "formik"
import { useRouter } from "next/navigation"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { number, object, string } from "yup"
import { SelectedTabEnum } from "../ViewCompanyContainer"
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import { toProperCase } from "@/components/utils/TextHelper"
import EditIcon from "@mui/icons-material/Edit"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"

export interface FormikValues {
  smtp_environment: number
  smtp_phone_number: string
  smtp_url: string
  smtp_token: string
}

const SMTPForm: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { data: companyData, isLoading } = useSelector(
    (state: RootState) => state.company.showCompany
  )

  const data = companyData?.communication_settings

  const [editMode, setEditMode] = useState<boolean>(false)

  const validationSchema = object({
    smtp_environment: number().required(
      intl.formatMessage({ id: "error_smtp_environment_required" })
    ),
    smtp_to_email: string().when("smtp_environment", (smtp_environment, schema) => {
      let selectedValue = null
      if (Array.isArray(smtp_environment)) {
        selectedValue = smtp_environment[0]
      }

      return selectedValue === CommunicationTypeValues[CommunicationTypeEnum.TESTING]
        ? schema.required(intl.formatMessage({ id: "error_smtp_to_email_required" }))
        : schema
    }),
    smtp_secure: string().required(intl.formatMessage({ id: "error_smtp_secure_required" })),
    smtp_host: string().required(intl.formatMessage({ id: "error_smtp_host_required" })),
    smtp_port: string().required(intl.formatMessage({ id: "error_smtp_port_required" })),
    smtp_from_email: string().required(
      intl.formatMessage({ id: "error_smtp_from_email_required" })
    ),
    smtp_from_name: string().required(intl.formatMessage({ id: "error_smtp_from_name_required" }))
  })

  return (
    <Formik
      enableReinitialize
      initialValues={{
        smtp_environment:
          data?.smtp_environment ?? (CommunicationTypeValues[CommunicationTypeEnum.LIVE] as number),
        smtp_to_email: data?.smtp_to_email ?? "",
        smtp_secure: data?.smtp_secure ?? "",
        smtp_host: data?.smtp_host ?? "",
        smtp_port: data?.smtp_port ?? "",
        smtp_username: data?.smtp_username ?? "",
        smtp_password: data?.smtp_password ?? "",
        smtp_from_email: data?.smtp_from_email ?? "",
        smtp_from_name: data?.smtp_from_name ?? ""
      }}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        if (companyData) {
          dispatch(
            CompanyAction.updateSMTPEmailConfig(
              setSubmitting,
              {
                ...values,
                company_id: companyData.id
              },
              () => {
                setEditMode(false)
              }
            )
          )
        } else {
          setSubmitting(false)

          dispatch(
            SnackbarAction.openSnackbar(
              intl.formatMessage({ id: "error_company_not_found" }),
              "error"
            )
          )
          router.push(`/companies`)
        }
      }}>
      {({ isSubmitting, handleSubmit, values }) => (
        <Form onSubmit={handleSubmit}>
          <Card
            sx={{
              padding: "1rem 0.5rem"
            }}>
            <CardHeader
              title={
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}>
                  <Typography>{intl.formatMessage({ id: "smtp_settings" })}</Typography>
                  <Button
                    variant="contained"
                    color="warning"
                    disabled={editMode}
                    onClick={() => setEditMode(true)}
                    startIcon={<EditIcon />}>
                    {intl.formatMessage({ id: "edit" })}
                  </Button>
                </Box>
              }
            />
            <CardContent>
              <Grid container spacing={2} rowSpacing={4}>
                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_environment"
                    isSelect
                    options={[
                      {
                        label: toProperCase(CommunicationTypeEnum.LIVE),
                        value: CommunicationTypeValues[CommunicationTypeEnum.LIVE]
                      },
                      {
                        label: toProperCase(CommunicationTypeEnum.TESTING),
                        value: CommunicationTypeValues[CommunicationTypeEnum.TESTING]
                      }
                    ]}
                    isLoading={isLoading}
                    renderValue={(value: number) =>
                      toProperCase(CommunicationTypeValues[value] as string)
                    }
                    label={intl.formatMessage({ id: "environment" })}
                    value={data?.smtp_environment}
                  />
                </Grid>
                {values.smtp_environment ===
                  CommunicationTypeValues[CommunicationTypeEnum.TESTING] && (
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="smtp_to_email"
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "to_email" })}
                      value={data?.smtp_to_email}
                    />
                  </Grid>
                )}
                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_username"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "username" })}
                    value={data?.smtp_username}
                  />
                </Grid>
                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_password"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "password" })}
                    value={data?.smtp_password}
                  />
                </Grid>
                <Grid item xs={4}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_secure"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "smtp_secure" })}
                    value={data?.smtp_secure}
                  />
                </Grid>
                <Grid item xs={4}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_host"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "host" })}
                    value={data?.smtp_host}
                  />
                </Grid>
                <Grid item xs={4}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_port"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "port" })}
                    value={data?.smtp_port}
                  />
                </Grid>
                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_from_email"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "from_email" })}
                    value={data?.smtp_from_email}
                  />
                </Grid>
                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="smtp_from_name"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "from_name" })}
                    value={data?.smtp_from_name}
                  />
                </Grid>
              </Grid>
            </CardContent>
            <Collapse in={editMode}>
              <CardActions
                sx={{
                  justifyContent: "flex-end"
                }}>
                <GreyButton
                  isLoadingButton
                  type="reset"
                  variant="contained"
                  loading={isSubmitting}
                  onClick={() => setEditMode(false)}>
                  {intl.formatMessage({ id: "cancel" })}
                </GreyButton>
                <LoadingButton
                  type="reset"
                  variant="contained"
                  color="warning"
                  loading={isSubmitting}>
                  {intl.formatMessage({ id: "reset" })}
                </LoadingButton>
                <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                  {intl.formatMessage({ id: "save" })}
                </LoadingButton>
              </CardActions>
            </Collapse>
          </Card>
        </Form>
      )}
    </Formik>
  )
}

export default SMTPForm
