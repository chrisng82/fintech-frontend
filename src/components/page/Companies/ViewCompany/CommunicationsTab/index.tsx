import { Box, Card, CardHeader, Stack } from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement } from "react"
import WhatsAppForm from "./WhatsAppForm"
import EficoreSyncSettingForm from "./EficoreSyncSettingForm"
import SMTPForm from "./SMTPForm"
import MiddlewareSettingForm from "./MiddlewareSettingForm"

const CommunicationsTab: React.FC = (): ReactElement => {
  return (
    <Stack spacing={3}>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.3, duration: 0.5 }}>
        <WhatsAppForm />
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.6, duration: 0.5 }}>
        <SMTPForm />
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.6, duration: 0.5 }}>
        <EficoreSyncSettingForm />
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.6, duration: 0.5 }}>
        <MiddlewareSettingForm />
      </motion.div>
    </Stack>
  )
}

export default CommunicationsTab
