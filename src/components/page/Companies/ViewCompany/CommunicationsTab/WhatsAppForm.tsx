import CommunicationTypeEnum, { CommunicationTypeValues } from "@/constants/CommunicationType"
import { RootState } from "@/stores"
import CompanyAction from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { Form, Formik } from "formik"
import { useRouter } from "next/navigation"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { number, object, string } from "yup"
import { SelectedTabEnum } from "../ViewCompanyContainer"
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import { toProperCase } from "@/components/utils/TextHelper"
import EditIcon from "@mui/icons-material/Edit"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"

export interface FormikValues {
  whatsapp_environment: number
  whatsapp_phone_number: string
  whatsapp_url: string
  whatsapp_token: string
}

const WhatsAppForm: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { data: companyData, isLoading } = useSelector(
    (state: RootState) => state.company.showCompany
  )

  const data = companyData?.communication_settings

  const [editMode, setEditMode] = useState<boolean>(false)

  const validationSchema = object({
    whatsapp_environment: number().required(
      intl.formatMessage({ id: "error_whatsapp_environment_required" })
    ),
    whatsapp_phone_number: string().when("whatsapp_environment", (whatsapp_environment, schema) => {
      let selectedValue = null
      if (Array.isArray(whatsapp_environment)) {
        selectedValue = whatsapp_environment[0]
      }

      return selectedValue === CommunicationTypeValues[CommunicationTypeEnum.TESTING]
        ? schema.required(intl.formatMessage({ id: "error_whatsapp_phone_number_required" }))
        : schema
    }),
    whatsapp_url: string().required(intl.formatMessage({ id: "error_whatsapp_url_required" })),
    whatsapp_token: string().required(intl.formatMessage({ id: "error_whatsapp_token_required" }))
  })

  return (
    <Formik
      enableReinitialize
      initialValues={{
        whatsapp_environment:
          data?.whatsapp_environment ??
          (CommunicationTypeValues[CommunicationTypeEnum.LIVE] as number),
        whatsapp_phone_number: data?.whatsapp_phone_number ?? "",
        whatsapp_url: data?.whatsapp_url ?? "",
        whatsapp_token: data?.whatsapp_token ?? ""
      }}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        if (data && companyData) {
          dispatch(
            CompanyAction.updateWhatsappConfig(
              setSubmitting,
              {
                ...values,
                company_id: companyData.id
              },
              () => {
                setEditMode(false)
              }
            )
          )
        } else {
          setSubmitting(false)

          if (!companyData) {
            dispatch(
              SnackbarAction.openSnackbar(
                intl.formatMessage({ id: "error_company_not_found" }),
                "error"
              )
            )
            router.push(`/companies`)
          } else {
            dispatch(
              SnackbarAction.openSnackbar(
                intl.formatMessage({ id: "error_whatsapp_settings_not_found" }),
                "error"
              )
            )
            router.push(`/companies/${companyData.code}/${SelectedTabEnum.COMMUNICATIONS}`)
          }
        }
      }}>
      {({ isSubmitting, handleSubmit, values }) => (
        <Form onSubmit={handleSubmit}>
          <Card
            sx={{
              padding: "1rem 0.5rem"
            }}>
            <CardHeader
              title={
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}>
                  <Typography>{intl.formatMessage({ id: "whatsapp_settings" })}</Typography>
                  <Button
                    variant="contained"
                    color="warning"
                    disabled={editMode}
                    onClick={() => setEditMode(true)}
                    startIcon={<EditIcon />}>
                    {intl.formatMessage({ id: "edit" })}
                  </Button>
                </Box>
              }
            />
            <CardContent>
              <Grid container spacing={2} rowSpacing={4}>
                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="whatsapp_environment"
                    isSelect
                    options={[
                      {
                        label: toProperCase(CommunicationTypeEnum.LIVE),
                        value: CommunicationTypeValues[CommunicationTypeEnum.LIVE]
                      },
                      {
                        label: toProperCase(CommunicationTypeEnum.TESTING),
                        value: CommunicationTypeValues[CommunicationTypeEnum.TESTING]
                      }
                    ]}
                    isLoading={isLoading}
                    renderValue={(value: number) =>
                      toProperCase(CommunicationTypeValues[value] as string)
                    }
                    label={intl.formatMessage({ id: "environment" })}
                    value={data?.whatsapp_environment}
                  />
                </Grid>
                {values.whatsapp_environment ===
                  CommunicationTypeValues[CommunicationTypeEnum.TESTING] && (
                  <Grid item xs={6}>
                    <FieldData
                      editMode={editMode}
                      fieldName="whatsapp_phone_number"
                      isLoading={isLoading}
                      label={intl.formatMessage({ id: "phone_number" })}
                      value={data?.whatsapp_phone_number}
                    />
                  </Grid>
                )}
                <Grid item xs={12}>
                  <FieldData
                    editMode={editMode}
                    fieldName="whatsapp_url"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "api_url" })}
                    value={data?.whatsapp_url}
                  />
                </Grid>
                <Grid item xs={12}>
                  <FieldData
                    editMode={editMode}
                    fieldName="whatsapp_token"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "api_token" })}
                    value={data?.whatsapp_token}
                  />
                </Grid>
              </Grid>
            </CardContent>
            <Collapse in={editMode}>
              <CardActions
                sx={{
                  justifyContent: "flex-end"
                }}>
                <GreyButton
                  isLoadingButton
                  type="reset"
                  variant="contained"
                  loading={isSubmitting}
                  onClick={() => setEditMode(false)}>
                  {intl.formatMessage({ id: "cancel" })}
                </GreyButton>
                <LoadingButton
                  type="reset"
                  variant="contained"
                  color="warning"
                  loading={isSubmitting}>
                  {intl.formatMessage({ id: "reset" })}
                </LoadingButton>
                <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                  {intl.formatMessage({ id: "save" })}
                </LoadingButton>
              </CardActions>
            </Collapse>
          </Card>
        </Form>
      )}
    </Formik>
  )
}

export default WhatsAppForm
