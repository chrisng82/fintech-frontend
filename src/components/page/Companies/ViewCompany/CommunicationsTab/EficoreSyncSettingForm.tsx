import CommunicationTypeEnum, { CommunicationTypeValues } from "@/constants/CommunicationType"
import { RootState } from "@/stores"
import CompanyAction from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { Form, Formik } from "formik"
import { useRouter } from "next/navigation"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { number, object, string } from "yup"
import { SelectedTabEnum } from "../ViewCompanyContainer"
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import EditIcon from "@mui/icons-material/Edit"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"

export interface FormikValues {
  eficore_url: number
  eficore_username: string
  eficore_password: string
}

const EficoreSyncSettingForm: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { data: companyData, isLoading } = useSelector(
    (state: RootState) => state.company.showCompany
  )

  const commSetting = companyData?.communication_settings

  const [editMode, setEditMode] = useState<boolean>(false)

  return (
    <Formik
      enableReinitialize
      initialValues={{
        eficore_url: commSetting?.eficore_url,
        eficore_username: commSetting?.eficore_username,
        eficore_password: commSetting?.eficore_password
      }}
      onSubmit={(values, { setSubmitting }) => {
        if (companyData) {
          dispatch(
            CompanyAction.updateEficoreSyncSetting(
              setSubmitting,
              {
                ...values,
                company_id: companyData.id
              },
              () => {
                setEditMode(false)
              }
            )
          )
        } else {
          setSubmitting(false)

          if (!companyData) {
            dispatch(
              SnackbarAction.openSnackbar(
                intl.formatMessage({ id: "error_company_not_found" }),
                "error"
              )
            )
            router.push(`/companies`)
          } else {
            dispatch(
              SnackbarAction.openSnackbar(
                intl.formatMessage({ id: "error_whatsapp_settings_not_found" }),
                "error"
              )
            )
            // router.push(`/companies/${companyData.code}/${SelectedTabEnum.COMMUNICATIONS}`)
          }
        }
      }}>
      {({ isSubmitting, handleSubmit, values }) => (
        <Form onSubmit={handleSubmit}>
          <Card
            sx={{
              padding: "1rem 0.5rem"
            }}>
            <CardHeader
              title={
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}>
                  <Typography>{intl.formatMessage({ id: "eficore_sync_settings" })}</Typography>
                  <Button
                    variant="contained"
                    color="warning"
                    disabled={editMode}
                    onClick={() => setEditMode(true)}
                    startIcon={<EditIcon />}>
                    {intl.formatMessage({ id: "edit" })}
                  </Button>
                </Box>
              }
            />
            <CardContent>
              <Grid container spacing={2} rowSpacing={4}>
                <Grid item xs={12}>
                  <FieldData
                    editMode={editMode}
                    fieldName="eficore_url"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "api_url" })}
                    value="********"
                  />
                </Grid>

                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="eficore_username"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "username" })}
                    value="********"
                  />
                </Grid>

                <Grid item xs={6}>
                  <FieldData
                    editMode={editMode}
                    fieldName="eficore_password"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "password" })}
                    value="********"
                    type="password"
                  />
                </Grid>
              </Grid>
            </CardContent>
            <Collapse in={editMode}>
              <CardActions
                sx={{
                  justifyContent: "flex-end"
                }}>
                <GreyButton
                  isLoadingButton
                  type="reset"
                  variant="contained"
                  loading={isSubmitting}
                  onClick={() => setEditMode(false)}>
                  {intl.formatMessage({ id: "cancel" })}
                </GreyButton>
                <LoadingButton
                  type="reset"
                  variant="contained"
                  color="warning"
                  loading={isSubmitting}>
                  {intl.formatMessage({ id: "reset" })}
                </LoadingButton>
                <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                  {intl.formatMessage({ id: "save" })}
                </LoadingButton>
              </CardActions>
            </Collapse>
          </Card>
        </Form>
      )}
    </Formik>
  )
}

export default EficoreSyncSettingForm
