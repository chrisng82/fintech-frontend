import CommunicationTypeEnum, { CommunicationTypeValues } from "@/constants/CommunicationType"
import { RootState } from "@/stores"
import CompanyAction from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { Form, Formik } from "formik"
import { useRouter } from "next/navigation"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { number, object, string } from "yup"
import { SelectedTabEnum } from "../ViewCompanyContainer"
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import FieldData from "@/components/page/Form/FieldData"
import EditIcon from "@mui/icons-material/Edit"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"

export interface FormikValues {
  eficore_url: number
  eficore_username: string
  eficore_password: string
}

const MiddlewareSettingForm: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { data: companyData, isLoading } = useSelector(
    (state: RootState) => state.company.showCompany
  )

  const commSetting = companyData?.communication_settings

  const [editMode, setEditMode] = useState<boolean>(false)
  console.log(commSetting)
  return (
    <Formik
      enableReinitialize
      initialValues={{
        middleware_url: commSetting?.middleware_url
      }}
      onSubmit={(values, { setSubmitting }) => {
        if (companyData) {
          dispatch(
            CompanyAction.updateMiddlewareSetting(
              setSubmitting,
              {
                ...values,
                company_id: companyData.id
              },
              () => {
                setEditMode(false)
              }
            )
          )
        } else {
          setSubmitting(false)
        }
      }}>
      {({ isSubmitting, handleSubmit, values }) => (
        <Form onSubmit={handleSubmit}>
          <Card
            sx={{
              padding: "1rem 0.5rem"
            }}>
            <CardHeader
              title={
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}>
                  <Typography>{intl.formatMessage({ id: "middleware_settings" })}</Typography>
                  <Button
                    variant="contained"
                    color="warning"
                    disabled={editMode}
                    onClick={() => setEditMode(true)}
                    startIcon={<EditIcon />}>
                    {intl.formatMessage({ id: "edit" })}
                  </Button>
                </Box>
              }
            />
            <CardContent>
              <Grid container spacing={2} rowSpacing={4}>
                <Grid item xs={12}>
                  <FieldData
                    editMode={editMode}
                    fieldName="middleware_url"
                    isLoading={isLoading}
                    label={intl.formatMessage({ id: "middleware_url" })}
                    value="********"
                  />
                </Grid>
              </Grid>
            </CardContent>
            <Collapse in={editMode}>
              <CardActions
                sx={{
                  justifyContent: "flex-end"
                }}>
                <GreyButton
                  isLoadingButton
                  type="reset"
                  variant="contained"
                  loading={isSubmitting}
                  onClick={() => setEditMode(false)}>
                  {intl.formatMessage({ id: "cancel" })}
                </GreyButton>
                <LoadingButton
                  type="reset"
                  variant="contained"
                  color="warning"
                  loading={isSubmitting}>
                  {intl.formatMessage({ id: "reset" })}
                </LoadingButton>
                <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                  {intl.formatMessage({ id: "save" })}
                </LoadingButton>
              </CardActions>
            </Collapse>
          </Card>
        </Form>
      )}
    </Formik>
  )
}

export default MiddlewareSettingForm
