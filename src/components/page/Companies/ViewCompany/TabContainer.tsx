import { palette } from "@/components/utils/theme"
import { Box, Button, Paper, Typography } from "@mui/material"
import React, { PropsWithChildren, ReactElement } from "react"
import { SelectedTabEnum } from "./ViewCompanyContainer"
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft"
import { useParams, useRouter } from "next/navigation"
import { motion } from "framer-motion"
import { useIntl } from "react-intl"
import { toProperCase } from "@/components/utils/TextHelper"
import GreyButton from "@/components/Base/GreyButton"
import { useIsRouteAccessible } from "@/components/utils/RouteHelper"
import Link from "next/link"

interface TabBoxProps extends PropsWithChildren {
  active: boolean
  visible: boolean
  link: string
}

const TabBox = ({ active, visible, link, children }: TabBoxProps) => {
  if (!visible) return null

  return (
    <Link href={link}>
      <Paper
        sx={{
          display: "flex",
          padding: "1rem 1.5rem",
          borderRadius: "1rem",
          background: active ? palette.purple.main : "white",
          cursor: "pointer",
          transition: "all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94)",

          "& p": {
            color: active ? "#fff" : "inherit"
          },

          "&:hover": {
            transform: "scale(1.05)",
            background: active ? palette.purple.dark : palette.purple.main,
            "& p": {
              color: "#fff"
            }
          }
        }}>
        {children}
      </Paper>
    </Link>
  )
}

interface Props {
  selectedTab: SelectedTabEnum
}

const TabContainer: React.FC<Props> = ({ selectedTab }): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const { code } = useParams()

  const basePath = `/companies/${code}`

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}>
      <Box
        sx={{
          position: "relative",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: "1.5rem",
          mb: 2,
          gap: "1rem"
        }}>
        <GreyButton
          variant="contained"
          startIcon={<ChevronLeftIcon />}
          sx={{
            position: "absolute",
            left: 0
          }}>
          <Link href="/companies">{intl.formatMessage({ id: "back" })}</Link>
        </GreyButton>

        <TabBox
          active={selectedTab === SelectedTabEnum.SUMMARY}
          visible={useIsRouteAccessible(`${basePath}/${SelectedTabEnum.SUMMARY}`)}
          link={`/companies/${code}/${SelectedTabEnum.SUMMARY}`}>
          <Typography>{toProperCase(SelectedTabEnum.SUMMARY)}</Typography>
        </TabBox>
        <TabBox
          active={selectedTab === SelectedTabEnum.EMAIL_TASKS}
          visible={useIsRouteAccessible(`${basePath}/${SelectedTabEnum.EMAIL_TASKS}`)}
          link={`/companies/${code}/${SelectedTabEnum.EMAIL_TASKS}`}>
          <Typography>{toProperCase(SelectedTabEnum.EMAIL_TASKS)}</Typography>
        </TabBox>
        <TabBox
          active={selectedTab === SelectedTabEnum.SIGNUP_LINKS}
          visible={useIsRouteAccessible(`${basePath}/${SelectedTabEnum.SIGNUP_LINKS}`)}
          link={`/companies/${code}/${SelectedTabEnum.SIGNUP_LINKS}`}>
          <Typography>{toProperCase(SelectedTabEnum.SIGNUP_LINKS)}</Typography>
        </TabBox>
        <TabBox
          active={selectedTab === SelectedTabEnum.COMMUNICATIONS}
          visible={useIsRouteAccessible(`${basePath}/${SelectedTabEnum.COMMUNICATIONS}`)}
          link={`/companies/${code}/${SelectedTabEnum.COMMUNICATIONS}`}>
          <Typography>{toProperCase(SelectedTabEnum.COMMUNICATIONS)}</Typography>
        </TabBox>
        <TabBox
          active={selectedTab === SelectedTabEnum.TERMS_CONDITIONS}
          visible={useIsRouteAccessible(`${basePath}/${SelectedTabEnum.TERMS_CONDITIONS}`)}
          link={`/companies/${code}/${SelectedTabEnum.TERMS_CONDITIONS}`}>
          <Typography>{SelectedTabEnum.TERMS_CONDITIONS}</Typography>
        </TabBox>
      </Box>
    </motion.div>
  )
}

export default TabContainer
