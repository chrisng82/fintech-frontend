import * as React from "react"
import Paper from "@mui/material/Paper"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TablePagination from "@mui/material/TablePagination"
import TableRow from "@mui/material/TableRow"
import { RootState } from "@/stores"
import { useDispatch, useSelector } from "react-redux"
import { LoadingButton } from "@mui/lab"
import { useIntl } from "react-intl"
import EditIcon from "@mui/icons-material/Edit"
import TagIcon from "@mui/icons-material/Tag"
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos"
import SyncIcon from "@mui/icons-material/Sync"
import CloudDoneIcon from "@mui/icons-material/CloudDone"
import ApprovalIcon from "@mui/icons-material/Approval"
import { AppBar, Box, Button, Dialog, IconButton, Stack, Switch, Toolbar } from "@mui/material"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"
import { useState } from "react"
import CompanyAction from "@/stores/Company/Action"
import TncEditor from "./TncEditor"

interface Column {
  id: "revision_code" | "actions"
  label: string
  minWidth?: number
  align?: "left" | "center" | "right"
  format?: (value: any) => string
  onClick?: any
  render?: (row: any, column: any) => React.ReactElement | string | number
}

interface Props {
  handleChangePage: any
}
export default function TermConditionsDataTable({ handleChangePage }: Props) {
  const dispatch = useDispatch()
  const intl = useIntl()

  const { termConditions, termConditionLoading, termConditionSaving } = useSelector(
    (state: RootState) => state.company
  )

  const [page, setPage] = useState<number>(0)
  const [rowsPerPage, setRowsPerPage] = useState<number>(10)

  const [previewTnc, setPreviewTnc] = useState<boolean>(false)
  const [previewTncData, setPreviewTncData] = useState<any>("")

  const handleEditorChange = (event: any, id: number) => {
    const html = event.target.value
    if (previewTncData) {
      const newData = {
        id: previewTncData.id,
        code: previewTncData.code,
        revision_code: previewTncData.revision_code,
        status: previewTncData.status,
        contents: html
      }
      setPreviewTncData(newData)
    }
    setTimeout(() => {
      dispatch(CompanyAction.updateTermCondition(id, html))
      dispatch(CompanyAction.setTermConditionSaving(false))
    }, 3000)
  }

  const columns: readonly Column[] = [
    {
      id: "revision_code",
      label: "Revision No",
      minWidth: 170,
      align: "left",
      render: (row: any, column: any) => {
        return row.revision_code
      }
    },
    {
      id: "actions",
      label: "Actions",
      minWidth: 170,
      align: "right",
      render: (row: any, column: any) => {
        return (
          <Stack gap={2} direction="row-reverse">
            <LoadingButton
              loading={termConditionLoading}
              color="info"
              variant="contained"
              startIcon={<EditIcon />}
              sx={{ padding: "12px 20px" }}
              onClick={() => {
                handleOpenPreview(row)
              }}>
              {`${intl.formatMessage({ id: "edit" })}`}
            </LoadingButton>
            <LoadingButton
              loading={termConditionLoading}
              color={row.status === ResStatusValues[ResStatusEnum.ACTIVE] ? "info" : "primary"}
              variant="contained"
              sx={{
                padding: "12px 20px"
              }}
              disabled={row.status === ResStatusValues[ResStatusEnum.ACTIVE]}
              onClick={() => {
                if (row.status === ResStatusValues[ResStatusEnum.INACTIVE]) {
                  dispatch(CompanyAction.setTermConditionStatus(row.id))
                }
              }}>
              {row.status === ResStatusValues[ResStatusEnum.ACTIVE]
                ? `${intl.formatMessage({ id: "active" })}`
                : `${intl.formatMessage({ id: "select" })}`}
            </LoadingButton>
          </Stack>
        )
      }
    }
  ]

  const handleOpenPreview = (data: any) => {
    setPreviewTnc(true)
    if (data) {
      const newData = {
        id: data.id,
        code: data.code,
        revision_code: data.revision_code,
        status: data.status,
        contents: data.contents
      }
      setPreviewTncData(newData)
    }
  }
  const handleClosePreview = () => {
    setPreviewTnc(false)
    setPreviewTncData("")
  }

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    sx={{ backgroundColor: "#FFF" }}
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}>
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {termConditions?.data
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                      {columns.map((column) => {
                        if (column.id !== "actions") {
                          const value = row[column.id]
                          return (
                            <TableCell key={`${column.id}`} align={column.align}>
                              {column.render
                                ? column.render(row, column)
                                : column.format
                                  ? column.format(value)
                                  : value}
                            </TableCell>
                          )
                        } else {
                          return (
                            <>
                              <TableCell key={`${column.id}_action`} align={column.align}>
                                {column.render ? column.render(row, column) : ""}
                              </TableCell>
                            </>
                          )
                        }
                      })}
                    </TableRow>
                  )
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10]}
          component="div"
          count={termConditions?.total ?? 0}
          rowsPerPage={rowsPerPage}
          page={termConditions?.page ? termConditions.page - 1 : page}
          onPageChange={handleChangePage}
        />
      </Box>
      <Dialog
        onClose={() => {
          handleClosePreview()
        }}
        open={previewTnc}
        maxWidth="xl"
        fullWidth>
        <AppBar
          sx={{
            position: "relative",
            backgroundColor: "#ECECEC",
            borderRadius: 0,
            padding: "5px"
          }}>
          <Toolbar>
            <Stack gap={2} direction={"row"}>
              <IconButton
                edge="end"
                onClick={() => {
                  handleClosePreview()
                }}
                aria-label="close">
                <ArrowBackIosIcon />
              </IconButton>
              <LoadingButton
                loading={termConditionLoading}
                color="info"
                variant="contained"
                startIcon={<ApprovalIcon />}
                sx={{ padding: "12px 20px" }}>
                {`${previewTncData.code}`}
              </LoadingButton>
              <LoadingButton
                loading={termConditionLoading}
                color="secondary"
                variant="contained"
                startIcon={<TagIcon />}
                sx={{ padding: "12px 20px" }}>
                {`${intl.formatMessage({ id: "revision" })} - ${previewTncData.revision_code}`}
              </LoadingButton>
              <Button
                color="secondary"
                variant="contained"
                startIcon={termConditionSaving ? <SyncIcon /> : <CloudDoneIcon />}
                sx={{ padding: "12px 20px" }}>
                {termConditionSaving
                  ? `${intl.formatMessage({ id: "saving" })}`
                  : intl.formatMessage({ id: "saved" })}
              </Button>
              <Button
                color={
                  previewTncData.status === ResStatusValues[ResStatusEnum.ACTIVE]
                    ? "success"
                    : "error"
                }
                variant="contained"
                sx={{ padding: "12px 20px" }}>
                {ResStatusValues[previewTncData.status]}
              </Button>
              <Switch
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                  if (previewTncData.status === ResStatusValues[ResStatusEnum.INACTIVE]) {
                    setPreviewTncData({
                      ...previewTncData,
                      status: ResStatusValues[ResStatusEnum.ACTIVE]
                    })
                    dispatch(CompanyAction.setTermConditionStatus(previewTncData.id))
                  }
                }}
                inputProps={{ "aria-label": "Switch demo" }}
                sx={{ transform: "scale(1.5)", margin: "10px" }}
                color="success"
                checked={previewTncData.status === ResStatusValues[ResStatusEnum.ACTIVE]}
              />
            </Stack>
          </Toolbar>
        </AppBar>
        <TncEditor
          contents={previewTncData.contents ?? ""}
          handleOnChange={(e: any) => {
            dispatch(CompanyAction.setTermConditionSaving(true))
            handleEditorChange(e, previewTncData.id)
          }}
        />
      </Dialog>
    </Paper>
  )
}
