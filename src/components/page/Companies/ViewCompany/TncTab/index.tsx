import { Box, Card, CardHeader, Stack } from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement } from "react"
import TermConditionEditor from "./TermConditionEditor"

const TncTab: React.FC = (): ReactElement => {
  return (
    <Stack spacing={3}>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.6, duration: 0.5 }}>
        <TermConditionEditor />
      </motion.div>
    </Stack>
  )
}

export default TncTab
