import { ReactElement, useState } from "react"
import {
  BtnBold,
  BtnItalic,
  Editor,
  EditorProvider,
  Toolbar,
  BtnBulletList,
  BtnClearFormatting,
  BtnStrikeThrough,
  BtnLink,
  BtnNumberedList,
  BtnRedo,
  BtnUnderline,
  BtnUndo,
  createButton
} from "react-simple-wysiwyg"
interface Props {
  handleOnChange?: any
  contents?: string
  readonly?: boolean
}

export default function TncEditor({ handleOnChange, contents, readonly }: Props) {
  const [html, setHtml] = useState("")
  const onChange = (e: any) => {
    if (handleOnChange) {
      handleOnChange(e)
      setHtml(e.target.value)
    }
  }

  const BtnAlignLeft = createButton("Align Left", "⬱", "justifyLeft")
  const BtnAlignCenter = createButton("Align Center", "☰", "justifyCenter")
  const BtnAlignRight = createButton("Align Right", "⇶", "justifyRight")

  return (
    <EditorProvider>
      <Editor
        containerProps={{
          style: {
            resize: "vertical",
            overflowY: "scroll",
            minHeight: "600px",
            maxHeight: "600px",
            paddingLeft: "50px"
          }
        }}
        value={contents ?? html}
        onChange={onChange}
        disabled={readonly ?? false}>
        {!readonly ? (
          <Toolbar
            style={{
              position: "sticky",
              top: 0
            }}>
            <BtnBold />
            <BtnUnderline />
            <BtnItalic />
            <BtnLink />
            <BtnAlignLeft />
            <BtnAlignCenter />
            <BtnAlignRight />
            <BtnBulletList />
            <BtnNumberedList />
            <BtnUndo />
            <BtnRedo />
          </Toolbar>
        ) : (
          ""
        )}
      </Editor>
    </EditorProvider>
  )
}
