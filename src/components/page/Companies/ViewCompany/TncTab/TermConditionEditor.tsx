import React, { ReactElement, useEffect, useRef, useState } from "react"
import {
  Box,
  Button,
  Grid,
  Stack,
  Typography,
  createTheme,
  ThemeProvider,
  Dialog,
  DialogTitle,
  AppBar,
  Toolbar,
  IconButton
} from "@mui/material"
import { debounce } from "lodash"
import SnackbarAction from "@/stores/Snackbar/Action"
import { useDispatch, useSelector } from "react-redux"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"
import CompanyAction from "@/stores/Company/Action"
import { RootState } from "@/stores"
import ApprovalIcon from "@mui/icons-material/Approval"
import AddIcon from "@mui/icons-material/Add"
import TagIcon from "@mui/icons-material/Tag"
import CloudDoneIcon from "@mui/icons-material/CloudDone"
import SyncIcon from "@mui/icons-material/Sync"
import CloseIcon from "@mui/icons-material/Close"
import VisibilityIcon from "@mui/icons-material/Visibility"
import TermConditionsDataTable from "./TermConditionsDataTable"
import TncEditor from "./TncEditor"

const TermConditionEditor: React.FC = (): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { user } = useSelector((state: RootState) => state.app)
  const { termCondition, termConditions, termConditionLoading, termConditionSaving } = useSelector(
    (state: RootState) => state.company
  )
  const [textContent, setTextContent] = useState<any>("")
  const [openDialogTnc, setOpenDialogTnc] = useState<boolean>(false)
  const [openDialogIndex, setOpenDialogIndex] = useState<boolean>(false)

  const handleEditorChange = (event: any) => {
    const html = event.target.value
    setTextContent(html)

    if (termCondition) {
      setTimeout(() => {
        dispatch(CompanyAction.updateTermCondition(termCondition?.id, html))
      }, 3000)
    }
  }

  const handleCloseDialogTnc = () => {
    setOpenDialogTnc(false)
    dispatch(CompanyAction.setTermConditionLoading(false))
  }
  const debounceOnChangeCloseTnc = debounce(handleCloseDialogTnc, 1000)

  const handleOpenDialogTnc = () => {
    if (user) {
      dispatch(CompanyAction.fetchTermCondition(user.companyId))
      setOpenDialogTnc(true)
    }
  }
  const debounceOnChangeOpenTnc = debounce(handleOpenDialogTnc, 400)

  const handleCloseDialogIndex = () => {
    setOpenDialogIndex(false)
    dispatch(CompanyAction.setTermConditionLoading(false))
  }
  const debounceOnChangeCloseIndex = debounce(handleCloseDialogIndex, 1000)

  const handleOpenDialogIndex = () => {
    setOpenDialogIndex(true)
    if (user && termConditions) {
      dispatch(CompanyAction.fetchTermConditions(user.companyId, termConditions.page))
    }
  }
  const debounceOnChangeOpenIndex = debounce(handleOpenDialogIndex, 1000)

  const handleChangePage = (event: any, newPage: number) => {
    if (user && termConditions) {
      dispatch(CompanyAction.fetchTermConditions(user.companyId, newPage + 1))
    }
  }
  const debounceOnChangePage = debounce(handleChangePage, 1000)

  useEffect(() => {
    if (user) {
      dispatch(CompanyAction.fetchTermCondition(user.companyId))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (user) {
      dispatch(CompanyAction.fetchTermCondition(user.companyId))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user])

  useEffect(() => {
    if (termCondition?.contents) {
      setTextContent(termCondition.contents)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [termCondition])
  return (
    <Stack>
      <Grid container justifyContent="space-between">
        <Grid item>
          <Stack gap={2} direction={"row"}>
            <LoadingButton
              loading={termConditionLoading}
              color="info"
              variant="contained"
              startIcon={<ApprovalIcon />}
              sx={{ padding: "12px 20px" }}>
              {termCondition?.code}
            </LoadingButton>
            <LoadingButton
              loading={termConditionLoading}
              color="secondary"
              variant="contained"
              startIcon={<TagIcon />}
              sx={{ padding: "12px 20px" }}
              onClick={() => {
                dispatch(CompanyAction.setTermConditionSaving(true))
                debounceOnChangeOpenTnc()
              }}>
              {`${intl.formatMessage({ id: "revision" })} - ${termCondition?.revision_code}`}
            </LoadingButton>
            <LoadingButton
              loading={termConditionLoading}
              color="secondary"
              variant="contained"
              startIcon={<VisibilityIcon />}
              sx={{ padding: "12px 20px" }}
              onClick={() => {
                dispatch(CompanyAction.setTermConditionLoading(true))
                debounceOnChangeOpenIndex()
              }}>
              {intl.formatMessage({ id: "revision_list" })}
            </LoadingButton>
          </Stack>
        </Grid>
      </Grid>

      <Dialog
        onClose={() => {
          debounceOnChangeCloseTnc()
        }}
        open={openDialogTnc}
        maxWidth="xl"
        fullWidth>
        <AppBar sx={{ position: "relative", backgroundColor: "#ECECEC", borderRadius: 0 }}>
          <Toolbar>
            <Stack gap={2} direction={"row"}>
              <LoadingButton
                loading={termConditionLoading}
                color="info"
                variant="contained"
                startIcon={<ApprovalIcon />}
                sx={{ padding: "12px 20px" }}
                onClick={() => {
                  dispatch(CompanyAction.setTermConditionLoading(true))
                  dispatch(CompanyAction.setTermConditionSaving(true))
                  debounceOnChangeOpenTnc()
                }}>
                {termCondition?.code}
              </LoadingButton>
              <LoadingButton
                loading={termConditionLoading}
                color="secondary"
                variant="contained"
                startIcon={<TagIcon />}
                sx={{ padding: "12px 20px" }}>
                {`${intl.formatMessage({ id: "revision" })} - ${termCondition?.revision_code}`}
              </LoadingButton>
              <Button
                color="secondary"
                variant="contained"
                startIcon={termConditionSaving ? <SyncIcon /> : <CloudDoneIcon />}
                sx={{ padding: "12px 20px" }}>
                {termConditionSaving
                  ? `${intl.formatMessage({ id: "saving" })}`
                  : intl.formatMessage({ id: "saved" })}
              </Button>
            </Stack>
            <DialogTitle sx={{ color: "#000" }}></DialogTitle>
            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div"></Typography>
            <IconButton
              edge="end"
              onClick={() => {
                debounceOnChangeCloseTnc()
              }}
              aria-label="close">
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Box sx={{ lineHeight: "35px" }}>
          <TncEditor
            handleOnChange={(event: any) => {
              dispatch(CompanyAction.setTermConditionSaving(true))
              handleEditorChange(event)
            }}
            contents={textContent}
          />
        </Box>
      </Dialog>
      <Dialog
        onClose={() => {
          debounceOnChangeCloseIndex()
        }}
        open={openDialogIndex}
        maxWidth="xl"
        fullWidth>
        <AppBar sx={{ position: "relative", backgroundColor: "#ECECEC", borderRadius: 0 }}>
          <Toolbar>
            <Stack gap={2} direction={"row"}>
              <LoadingButton
                loading={termConditionLoading}
                color="info"
                variant="contained"
                startIcon={<ApprovalIcon />}
                sx={{ padding: "12px 20px" }}>
                {termCondition?.code}
              </LoadingButton>
              <LoadingButton
                loading={termConditionLoading}
                color="secondary"
                variant="contained"
                startIcon={<TagIcon />}
                sx={{ padding: "12px 20px" }}>
                {`${intl.formatMessage({ id: "revision" })} - ${termCondition?.revision_code}`}
              </LoadingButton>
              <LoadingButton
                loading={termConditionLoading}
                color="warning"
                variant="contained"
                startIcon={<AddIcon />}
                sx={{ padding: "12px 20px" }}
                onClick={() => {
                  if (user) {
                    dispatch(CompanyAction.createTermCondition(user.companyId))
                  }
                }}>
                {intl.formatMessage({ id: "new_revision" })}
              </LoadingButton>
            </Stack>
            <DialogTitle
              sx={{
                color: "#000"
              }}>{`${intl.formatMessage({ id: "term_condition_revision_list" })}`}</DialogTitle>
            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div"></Typography>
            <IconButton
              edge="end"
              onClick={() => {
                dispatch(CompanyAction.setTermConditionLoading(true))
                debounceOnChangeCloseIndex()
              }}
              aria-label="close">
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <TermConditionsDataTable
          handleChangePage={(e: any, v: any) => debounceOnChangePage(e, v)}
        />
      </Dialog>
    </Stack>
  )
}

export default TermConditionEditor
