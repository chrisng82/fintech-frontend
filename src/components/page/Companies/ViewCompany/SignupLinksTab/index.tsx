import {
  Box,
  Button,
  ButtonGroup,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Skeleton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from "@mui/material"
import React, { ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import AddIcon from "@mui/icons-material/Add"
import { useParams, useRouter } from "next/navigation"
import { SelectedTabEnum } from "../ViewCompanyContainer"
import { motion } from "framer-motion"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import BlockIcon from "@mui/icons-material/Block"
import ContentCopyIcon from "@mui/icons-material/ContentCopy"
import WhatsAppIcon from "@mui/icons-material/WhatsApp"
import EmailIcon from "@mui/icons-material/Email"
import dayjs from "dayjs"
import SignupLinkAction from "@/stores/SignupLink/Action"
import { UserTypeValues } from "@/constants/UserTypeEnum"
import { toProperCase } from "@/components/utils/TextHelper"
import CircleIcon from "@mui/icons-material/Circle"
import { palette } from "@/components/utils/theme"
import SignupLinksTable from "./SignupLinksTable"
import SignupLinksHeader from "./SignupLinksHeader"
import ConfirmDeleteModal from "@/components/modals/ConfirmDeleteModal"
import { SignupLink } from "@/stores/SignupLink/Types"

const SignupLinksSection: React.FC = (): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const [openInvalidate, setOpenInvalidate] = useState<boolean>(false)
  const [selectedSignupLink, setSelectedSignupLink] = useState<SignupLink>()
  const [selected, setSelected] = useState<number[]>([])
  const [isAllSelected, setIsAllSelected] = useState<boolean>(false)

  const companyData = useSelector((state: RootState) => state.company.showCompany.data)
  const {
    data,
    isLoading: isSignupLinkLoading,
    per_page,
    current_page,
    total
  } = useSelector((state: RootState) => state.signupLink.list)

  useEffect(() => {
    if (companyData) {
      dispatch(SignupLinkAction.fetchSignupLinks())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [companyData])

  const handleChangePage = (event: unknown, newPage: number) => {
    dispatch(SignupLinkAction.fetchSignupLinks(newPage + 1))
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(SignupLinkAction.fetchSignupLinks(1, parseInt(event.target.value, 10)))
  }

  const handleInvalidate = (signupLink: SignupLink) => {
    setSelectedSignupLink(signupLink)
    setOpenInvalidate(true)
  }

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.3, duration: 0.5 }}>
      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <SignupLinksHeader
          companyCode={companyData?.code}
          selected={selected}
          isAllSelected={isAllSelected}
        />
        <CardContent>
          <SignupLinksTable
            handleInvalidate={handleInvalidate}
            selected={selected}
            setSelected={setSelected}
            isAllSelected={isAllSelected}
            setIsAllSelected={setIsAllSelected}
          />
          {isSignupLinkLoading ? (
            <Grid container justifyContent="flex-end">
              <Grid item xs={6}>
                <Skeleton variant="text" height={44} />
              </Grid>
            </Grid>
          ) : (
            data.length > 0 &&
            total > 0 && (
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={total}
                rowsPerPage={per_page}
                page={current_page - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            )
          )}
        </CardContent>
      </Card>
      <ConfirmDeleteModal
        open={openInvalidate}
        title={intl.formatMessage({ id: "invalidate_signup_link" })}
        content={intl.formatMessage({ id: "are_you_sure_invalidate_signup_link" })}
        onClose={() => {
          setSelectedSignupLink(undefined)
          setOpenInvalidate(false)
        }}
        onConfirm={() => {
          if (selectedSignupLink) {
            dispatch(SignupLinkAction.invalidateSignupLink(selectedSignupLink.id))
            setSelectedSignupLink(undefined)
            setOpenInvalidate(false)
          }
        }}
      />
    </motion.div>
  )
}

export default SignupLinksSection
