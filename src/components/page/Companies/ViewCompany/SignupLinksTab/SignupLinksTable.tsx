import { toProperCase } from "@/components/utils/TextHelper"
import { UserTypeValues } from "@/constants/UserTypeEnum"
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Skeleton,
  ButtonGroup,
  Button,
  Box,
  Tooltip,
  Checkbox
} from "@mui/material"
import dayjs from "dayjs"
import React, { ChangeEvent, MouseEvent, ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import EmailIcon from "@mui/icons-material/Email"
import WhatsAppIcon from "@mui/icons-material/WhatsApp"
import ContentCopyIcon from "@mui/icons-material/ContentCopy"
import BlockIcon from "@mui/icons-material/Block"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { SignupLink } from "@/stores/SignupLink/Types"
import SignupLinkAction from "@/stores/SignupLink/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import ShowUrlModal from "@/components/modals/ShowUrlModal"
import { useIsFeatureAccessible } from "@/components/utils/FeatureHelper"
import { set } from "lodash"

interface Props {
  selected: number[]
  isAllSelected: boolean

  handleInvalidate: (signupLink: SignupLink) => void
  setSelected: (selected: number[]) => void
  setIsAllSelected: (isAllSelected: boolean) => void
}

const SignupLinksTable: React.FC<Props> = ({
  handleInvalidate,
  selected,
  setSelected,
  isAllSelected,
  setIsAllSelected
}): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isLoading, data } = useSelector((state: RootState) => state.signupLink.list)

  const [copied, setCopied] = useState<number>()
  const [showUrl, setShowUrl] = useState<string>()

  const isEmailEnabled = useIsFeatureAccessible("send-email")
  const isWhatsappEnabled = useIsFeatureAccessible("send-whatsapp")

  useEffect(() => {
    if (isAllSelected) {
      const newIds = data
        .filter((n) => n.is_valid && !dayjs(n.token_expiry).isBefore(dayjs()))
        .map((n) => n.id)

      // Merge existing 'selected' IDs with new IDs and eliminate duplicates
      const mergedSelected = Array.from(new Set([...selected, ...newIds]))

      setSelected(mergedSelected)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data])

  const handleCopyText = (id: number, text: string) => {
    if (navigator.clipboard) {
      navigator.clipboard.writeText(text)
      dispatch(
        SnackbarAction.openSnackbar(intl.formatMessage({ id: "copied_to_clipboard" }), "success")
      )
      setCopied(id)
      setTimeout(() => {
        setCopied(undefined)
      }, 3000)
    } else {
      setShowUrl(text)
    }
  }

  const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>) => {
    if (selected.length > 0 && selected.length < data.length) {
      // If the checkbox is indeterminate, clear all selections when clicked
      setSelected([])
      setIsAllSelected(false)
      return
    }

    if (event.target.checked) {
      const newSelected = data
        .filter((n) => n.is_valid && !dayjs(n.token_expiry).isBefore(dayjs()))
        .map((n) => n.id)

      setSelected(newSelected)
      setIsAllSelected(true)
      return
    }
    setSelected([])
    setIsAllSelected(false)
  }

  const handleClick = (event: MouseEvent<unknown>, id: number) => {
    const selectedIndex = selected.indexOf(id)
    let newSelected: number[] = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }
    setSelected(newSelected)
  }

  const isSelected = (id: number) => selected.indexOf(id) !== -1

  return (
    <TableContainer>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow
            sx={{
              "& th": { fontWeight: "bold" }
            }}>
            <TableCell padding="checkbox">
              <Checkbox
                color="primary"
                indeterminate={selected.length > 0 && selected.length < data.length}
                checked={data.length > 0 && selected.length >= data.length}
                onChange={handleSelectAllClick}
                inputProps={{
                  "aria-label": "select all links"
                }}
              />
            </TableCell>
            <TableCell>{intl.formatMessage({ id: "email" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "link_type" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "debtor" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "created_by" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "creator_role" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "expiry" })}</TableCell>
            <TableCell sx={{ width: "20%" }}>{intl.formatMessage({ id: "actions" })}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {isLoading
            ? [...Array(10)].map((_, index) => (
                <TableRow key={index}>
                  <TableCell colSpan={7}>
                    <Skeleton variant="text" height={44} />
                  </TableCell>
                </TableRow>
              ))
            : data.map((row: SignupLink, index: number) => {
                const isExpired = dayjs(row.token_expiry).isBefore(dayjs())
                const expiredLink = isExpired || !row.is_valid
                const isItemSelected = isSelected(row.id)
                const labelId = `enhanced-table-checkbox-${index}`

                return (
                  <TableRow
                    hover
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    selected={isItemSelected}
                    key={`signup-link_${row.id}`}
                    sx={{
                      "& td": {
                        fontWeight: "500",
                        color: expiredLink
                          ? isExpired
                            ? "error.main"
                            : "text.disabled"
                          : "inherit",
                        opacity: expiredLink ? 0.6 : 1
                      },
                      "&:last-child td, &:last-child th": { border: 0 }
                    }}>
                    <TableCell
                      padding="checkbox"
                      onClick={(event) => {
                        !expiredLink ? handleClick(event, row.id) : null
                      }}>
                      <Checkbox
                        color="primary"
                        checked={isItemSelected}
                        inputProps={{
                          "aria-labelledby": labelId
                        }}
                      />
                    </TableCell>
                    <TableCell scope="row">{row.email}</TableCell>
                    <TableCell>{toProperCase(UserTypeValues[row.link_type] as string)}</TableCell>
                    <TableCell>
                      {row.debtor ? `${row.debtor.name_01} (${row.debtor.code})` : "-"}
                    </TableCell>
                    <TableCell>
                      {row.created_by
                        ? `${row.created_by.first_name} ${row.created_by.last_name}`
                        : "-"}
                    </TableCell>
                    <TableCell>
                      {row.created_by
                        ? toProperCase(UserTypeValues[row.created_by.user_type] as string)
                        : "-"}
                    </TableCell>
                    <TableCell>
                      {dayjs.utc(row.token_expiry).local().format("DD-MM-YYYY hh:mm A")}
                    </TableCell>
                    <TableCell>
                      <Box
                        sx={{
                          display: "flex",
                          gap: "0.5rem"
                        }}>
                        <ButtonGroup>
                          <Tooltip title={intl.formatMessage({ id: "send_to_email" })}>
                            <span>
                              <Button
                                disabled={expiredLink || !isEmailEnabled}
                                variant="contained"
                                onClick={() => {
                                  if (isEmailEnabled) {
                                    dispatch(SignupLinkAction.sendSignupLinkTo(row.id, "email"))
                                  }
                                }}
                                sx={{
                                  borderTopRightRadius: 0,
                                  borderBottomRightRadius: 0
                                }}>
                                <EmailIcon />
                              </Button>
                            </span>
                          </Tooltip>
                          <Tooltip
                            title={
                              row.phone === null
                                ? intl.formatMessage({ id: "signup_link_no_phone" })
                                : expiredLink
                                  ? ""
                                  : intl.formatMessage({ id: "send_to_ws" })
                            }>
                            <span>
                              <Button
                                disabled={expiredLink || !isWhatsappEnabled || row.phone === null}
                                variant="contained"
                                onClick={() => {
                                  if (isWhatsappEnabled) {
                                    dispatch(SignupLinkAction.sendSignupLinkTo(row.id, "whatsapp"))
                                  }
                                }}
                                sx={{
                                  borderLeft: "1px solid rgba(0, 0, 0, 0.12)",
                                  borderRadius: 0
                                }}>
                                <WhatsAppIcon />
                              </Button>
                            </span>
                          </Tooltip>
                          <Tooltip title={intl.formatMessage({ id: "copy_signup_link" })}>
                            <span>
                              <Button
                                color={copied === row.id ? "success" : "primary"}
                                disabled={expiredLink}
                                variant="contained"
                                onClick={() => {
                                  handleCopyText(
                                    row.id,
                                    `${process.env.NEXT_PUBLIC_BASE_URL}/auth/signup?ref=${row.token}`
                                  )
                                }}
                                sx={{
                                  borderLeft: "1px solid rgba(0, 0, 0, 0.12)",
                                  borderRadius: 0
                                }}>
                                <ContentCopyIcon />
                              </Button>
                            </span>
                          </Tooltip>
                          <Tooltip title={intl.formatMessage({ id: "invalidate_signup_link" })}>
                            <span>
                              <Button
                                disabled={expiredLink}
                                variant="contained"
                                onClick={() => {
                                  handleInvalidate(row)
                                }}
                                sx={{
                                  borderTopLeftRadius: 0,
                                  borderBottomLeftRadius: 0,
                                  borderTopRightRadius: "10px",
                                  borderBottomRightRadius: "10px"
                                }}>
                                <BlockIcon />
                              </Button>
                            </span>
                          </Tooltip>
                        </ButtonGroup>
                      </Box>
                    </TableCell>
                  </TableRow>
                )
              })}
        </TableBody>
      </Table>
      <ShowUrlModal open={!!showUrl} url={showUrl} onClose={() => setShowUrl(undefined)} />
    </TableContainer>
  )
}

export default SignupLinksTable
