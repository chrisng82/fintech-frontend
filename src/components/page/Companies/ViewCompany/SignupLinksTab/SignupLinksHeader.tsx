import { palette } from "@/components/utils/theme"
import {
  CardHeader,
  Typography,
  Stack,
  Button,
  Box,
  ButtonGroup,
  ClickAwayListener,
  MenuList,
  MenuItem,
  Popover
} from "@mui/material"
import React, { ReactElement, useRef, useState } from "react"
import { SelectedTabEnum } from "../ViewCompanyContainer"
import { useIntl } from "react-intl"
import AddIcon from "@mui/icons-material/Add"
import EmailIcon from "@mui/icons-material/Email"
import WhatsAppIcon from "@mui/icons-material/WhatsApp"
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown"
import CircleIcon from "@mui/icons-material/Circle"
import Link from "next/link"
import { useDispatch } from "react-redux"
import SignupLinkAction from "@/stores/SignupLink/Action"
interface Props {
  companyCode?: string
  selected: number[]
  isAllSelected: boolean
}

export enum generateTypeEnum {
  SINGLE = "single",
  BATCH = "batch"
}

const SignupLinksHeader: React.FC<Props> = ({
  companyCode,
  selected,
  isAllSelected
}): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const [openGenerateMenu, setOpenGenerateMenu] = useState(false)
  const anchorRef = useRef<HTMLDivElement>(null)

  const handleToggleGenerateMenu = () => {
    setOpenGenerateMenu((prevOpen) => !prevOpen)
  }

  const handleCloseGenerateMenu = (event: Event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return
    }

    setOpenGenerateMenu(false)
  }

  return (
    <CardHeader
      title={
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center"
          }}>
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <Typography>{intl.formatMessage({ id: "signup_links" })}</Typography>
            <Box sx={{ display: "flex", gap: 1 }}>
              <Typography
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "0.5rem",
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  color: palette.grey.main
                }}>
                <CircleIcon
                  sx={{
                    color: "error.main",
                    fontSize: "1rem"
                  }}
                />
                {intl.formatMessage({ id: "expired" })}
              </Typography>
              <Typography
                sx={{
                  display: "flex",
                  alignItems: "center",
                  fontSize: "0.875rem",
                  gap: "0.5rem",
                  fontWeight: 500,
                  color: palette.grey.main
                }}>
                <CircleIcon
                  sx={{
                    fontSize: "1rem",
                    color: "text.disabled"
                  }}
                />
                {intl.formatMessage({ id: "invalidated" })}
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              display: "flex",
              gap: "1rem"
            }}>
            <ButtonGroup variant="contained" ref={anchorRef} sx={{ boxShadow: "none" }}>
              <Button
                onClick={() => {
                  dispatch(
                    SignupLinkAction.sendBatchSignupLinksTo(selected, isAllSelected, "email")
                  )
                }}
                variant="contained"
                startIcon={<EmailIcon />}
                disabled={selected?.length === 0}
                sx={{ borderRadius: "10px" }}>
                {intl.formatMessage({ id: "email" })}
              </Button>
              <Button
                onClick={() => {
                  dispatch(
                    SignupLinkAction.sendBatchSignupLinksTo(selected, isAllSelected, "whatsapp")
                  )
                }}
                variant="contained"
                startIcon={<WhatsAppIcon />}
                disabled={selected?.length === 0}
                sx={{ borderRadius: "10px" }}>
                {intl.formatMessage({ id: "whatsapp" })}
              </Button>
            </ButtonGroup>
            <Link
              href={
                companyCode
                  ? `/companies/${companyCode}/${SelectedTabEnum.SIGNUP_LINKS}/generate/${generateTypeEnum.SINGLE}`
                  : "#"
              }>
              <Button
                variant="contained"
                startIcon={<AddIcon />}
                sx={{ padding: "12px 20px" }}>
                {intl.formatMessage({ id: "generate" })}
              </Button>
            </Link>
            {/* <ButtonGroup
              variant="contained"
              aria-label="outlined primary button group"
              ref={anchorRef}
              sx={{ boxShadow: "none" }}>
              <Link
                href={
                  companyCode
                    ? `/companies/${companyCode}/${SelectedTabEnum.SIGNUP_LINKS}/generate/${generateTypeEnum.SINGLE}`
                    : "#"
                }>
                <Button
                  variant="contained"
                  startIcon={<AddIcon />}
                  sx={{ borderRadius: "10px 0 0 10px", padding: "12px 20px" }}>
                  {intl.formatMessage({ id: "generate" })}
                </Button>
              </Link>
              <Button
                size="small"
                aria-controls={openGenerateMenu ? "split-button-menu" : undefined}
                aria-expanded={openGenerateMenu ? "true" : undefined}
                aria-haspopup="menu"
                onClick={handleToggleGenerateMenu}
                sx={{ borderRadius: "0 10px 10px 0", padding: "10px 10px" }}>
                <ArrowDropDownIcon />
              </Button>
            </ButtonGroup> */}
            {/* <Popover
              open={openGenerateMenu}
              anchorEl={anchorRef.current}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right"
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right"
              }}>
              <Box>
                <ClickAwayListener onClickAway={handleCloseGenerateMenu}>
                  <MenuList id="split-button-menu" autoFocusItem>
                    <MenuItem>
                      <Link
                        href={
                          companyCode
                            ? `/companies/${companyCode}/${SelectedTabEnum.SIGNUP_LINKS}/generate/${generateTypeEnum.BATCH}`
                            : "#"
                        }>
                        {intl.formatMessage({ id: "batch_generate" })}
                      </Link>
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Box>
            </Popover> */}
          </Box>
        </Box>
      }
    />
  )
}

export default SignupLinksHeader
