import { Box, Stack, Typography } from "@mui/material"
import React, { ReactElement } from "react"
import CreateForm from "./CreateForm"
import GreyButton from "@/components/Base/GreyButton"
import { motion } from "framer-motion"
import { SelectedTabEnum } from "../../ViewCompanyContainer"
import { useIntl } from "react-intl"
import Link from "next/link"
import { generateTypeEnum } from "../SignupLinksHeader"

interface Props {
  companyCode: string
  type: generateTypeEnum
}

const CreateSignupLink: React.FC<Props> = ({ companyCode, type }): ReactElement => {
  const intl = useIntl()

  return (
    <Stack
      spacing={3}
      sx={{
        width: "100%"
      }}>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.5 }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: "1rem"
          }}>
          <Link href={`/companies/${companyCode}/${SelectedTabEnum.SIGNUP_LINKS}`}>
            <GreyButton variant="contained">{intl.formatMessage({ id: "back" })}</GreyButton>
          </Link>
        </Box>
      </motion.div>
      <CreateForm type={type} />
    </Stack>
  )
}

export default CreateSignupLink
