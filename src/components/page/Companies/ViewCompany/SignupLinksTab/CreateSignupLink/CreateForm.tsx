import { Form, Formik } from "formik"
import { useRouter } from "next/navigation"
import React, { ReactElement, useEffect, useRef, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { array, date, object, string } from "yup"
import { RootState } from "@/stores"
import { motion } from "framer-motion"
import {
  Box,
  Button,
  ButtonGroup,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  ClickAwayListener,
  Grid,
  MenuItem,
  MenuList,
  Popover,
  Stack,
  Typography
} from "@mui/material"
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown"
import { LoadingButton } from "@mui/lab"
import { SelectedTabEnum } from "../../ViewCompanyContainer"
import UserTypeEnum, { UserTypeValues } from "@/constants/UserTypeEnum"
import dayjs from "dayjs"
import SignupLinkAction from "@/stores/SignupLink/Action"
import FieldData from "@/components/page/Form/FieldData"
import { toProperCase } from "@/components/utils/TextHelper"
import DebtorAction from "@/stores/Debtor/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { debounce } from "lodash"
import { generateTypeEnum } from "../SignupLinksHeader"
import { DebtorCodeOption } from "@/components/page/Form/SignupForm"

interface Props {
  type: generateTypeEnum
}
export interface FormikValues {
  email: string
  debtor_code: object
  phone: string
  link_type: number
  token_expiry: dayjs.Dayjs
  batch_generate: boolean
  send_email: boolean
}

const CreateForm: React.FC<Props> = ({ type }): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const user = useSelector((state: RootState) => state.app.user)
  const companyData = useSelector((state: RootState) => state.company.showCompany.data)
  const { data: debtors, isLoading } = useSelector((state: RootState) => state.debtor.list)

  const anchorRef = useRef<HTMLDivElement>(null)
  const [openGenerateMenu, setOpenGenerateMenu] = useState(false)

  const isDebtorFieldEnabled =
    process.env.NEXT_PUBLIC_ENABLE_DEBTOR_FIELD_GENERATE_SIGNUP_LINK === "true" ||
    process.env.NEXT_PUBLIC_ENABLE_DEBTOR_FIELD_GENERATE_SIGNUP_LINK === "TRUE"

  const isMultipleDebtorField =
    process.env.NEXT_PUBLIC_MULTIPLE_DEBTOR_FIELD_GENERATE_SIGNUP_LINK === "true" ||
    process.env.NEXT_PUBLIC_MULTIPLE_DEBTOR_FIELD_GENERATE_SIGNUP_LINK === "TRUE"

  const handleToggleGenerateMenu = () => {
    setOpenGenerateMenu((prevOpen) => !prevOpen)
  }

  const handleCloseGenerateMenu = (event: Event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return
    }

    setOpenGenerateMenu(false)
  }

  const validationSchema = object({
    email:
      type === generateTypeEnum.SINGLE
        ? string()
            .email(intl.formatMessage({ id: "error_email_format" }))
            .required(intl.formatMessage({ id: "error_email_required" }))
        : string().optional(),
    debtor_code:
      user?.type_str === UserTypeEnum.COMPANY_STAFF &&
      type === generateTypeEnum.SINGLE &&
      isDebtorFieldEnabled
        ? array()
            .of(object())
            .required(intl.formatMessage({ id: "debtor_code_required" }))
            .min(1, intl.formatMessage({ id: "debtor_code_required" }))
        : array().optional(),
    phone: string()
      .phone("MY", intl.formatMessage({ id: "error_phone_format_my" }))
      .optional(),
    token_expiry: date().required(intl.formatMessage({ id: "error_link_expiry_required" }))
  })

  const handleOnDebtorCodeChange = (value: string) => {
    dispatch(DebtorAction.fetchDebtorCodes(value))
  }

  const debounceOnChange = debounce(handleOnDebtorCodeChange, 1000)

  useEffect(() => {
    handleOnDebtorCodeChange("")
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Formik
      initialValues={{
        email: "",
        debtor_code: [] as DebtorCodeOption[],
        phone: "",
        link_type:
          user?.type_str === UserTypeEnum.COMPANY_STAFF
            ? (UserTypeValues[UserTypeEnum.DEBTOR] as number)
            : (UserTypeValues[UserTypeEnum.COMPANY_ADMIN] as number),
        token_expiry: dayjs().add(3, "day"),
        batch_generate: type === generateTypeEnum.BATCH,
        send_email: false
      }}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        if (companyData) {
          dispatch(
            SignupLinkAction.generateSignupLink(
              setSubmitting,
              {
                ...values,
                token_expiry: values.token_expiry.toDate(),
                company_id: companyData.id,
                ...(values.debtor_code && {
                  debtor_code: isDebtorFieldEnabled
                    ? values.debtor_code.map((item) => item.value)
                    : []
                })
              },
              () => {
                router.push(`/companies/${companyData.code}/${SelectedTabEnum.SIGNUP_LINKS}`)
              }
            )
          )
        } else {
          setSubmitting(false)
          dispatch(SnackbarAction.openSnackbar("No company data", "error"))
        }
      }}>
      {({ isSubmitting, handleSubmit, values }) => (
        <Form onSubmit={handleSubmit}>
          <Stack spacing={3}>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.3, duration: 0.5 }}>
              <Card
                sx={{
                  padding: "1rem 0.5rem"
                }}>
                <CardHeader
                  title={
                    <Typography>
                      {intl.formatMessage({
                        id:
                          type === generateTypeEnum.SINGLE
                            ? "generate_signup_link"
                            : "generate_batch_signup_links"
                      })}
                    </Typography>
                  }
                />
                <CardContent>
                  <Grid container spacing={2} rowSpacing={4}>
                    {type === generateTypeEnum.SINGLE && (
                      <>
                        <Grid item xs={6}>
                          <FieldData
                            editMode
                            fieldName="email"
                            label={intl.formatMessage({ id: "email" })}
                          />
                        </Grid>
                        {values.link_type === UserTypeValues[UserTypeEnum.DEBTOR] &&
                          isDebtorFieldEnabled && (
                            <Grid item xs={6}>
                              <FieldData
                                editMode
                                fieldName={`debtor_code`}
                                isMultiSearchSelectChipped
                                options={
                                  debtors.length > 0
                                    ? debtors
                                        .filter(
                                          (debtor) =>
                                            !values.debtor_code.some(
                                              (item) => item.value === debtor.code
                                            )
                                        )
                                        .map((debtor) => ({
                                          label: debtor.name_01,
                                          value: debtor.code
                                        }))
                                    : []
                                }
                                maxValues={isMultipleDebtorField ? 0 : 1}
                                getOptionLabel={(option) => `${option.label} (${option.value})`}
                                debounceOnChange={debounceOnChange}
                                label={intl.formatMessage({ id: "debtor_code" })}
                                optionDisabled={(option) =>
                                  isMultipleDebtorField ? values.debtor_code.length >= 10 : false
                                }
                              />
                            </Grid>
                          )}
                        <Grid item xs={6}>
                          <FieldData
                            editMode
                            fieldName="phone"
                            label={intl.formatMessage({ id: "phone" })}
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <FieldData
                            editMode
                            fieldName="link_type"
                            isSelect
                            options={
                              user?.type_str === UserTypeEnum.COMPANY_STAFF
                                ? [
                                    {
                                      label: toProperCase(UserTypeEnum.DEBTOR),
                                      value: UserTypeValues[UserTypeEnum.DEBTOR]
                                    }
                                  ]
                                : [
                                    {
                                      label: toProperCase(UserTypeEnum.COMPANY_ADMIN),
                                      value: UserTypeValues[UserTypeEnum.COMPANY_ADMIN]
                                    },
                                    {
                                      label: toProperCase(UserTypeEnum.COMPANY_STAFF),
                                      value: UserTypeValues[UserTypeEnum.COMPANY_STAFF]
                                    }
                                  ]
                            }
                            renderValue={(value: number) => UserTypeValues[value]}
                            label={intl.formatMessage({ id: "link_type" })}
                          />
                        </Grid>
                      </>
                    )}
                    <Grid item xs={6}>
                      <FieldData
                        editMode
                        fieldName="token_expiry"
                        isDate
                        label={intl.formatMessage({ id: "link_expiry" })}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </motion.div>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.6, duration: 0.5 }}>
              <Card
                sx={{
                  padding: "1rem 0.5rem"
                }}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}
                    sx={{ mr: type === generateTypeEnum.SINGLE ? 0 : 1 }}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  {type === generateTypeEnum.SINGLE ? (
                    <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                      {intl.formatMessage({ id: "generate" })}
                    </LoadingButton>
                  ) : (
                    <>
                      <ButtonGroup
                        variant="contained"
                        aria-label="outlined primary button group"
                        ref={anchorRef}
                        sx={{
                          ".MuiLoadingButton-root": { border: "none !important" },
                          boxShadow: "none"
                        }}>
                        <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                          {intl.formatMessage({ id: "generate" })}
                        </LoadingButton>
                        <Button
                          size="small"
                          aria-controls={openGenerateMenu ? "split-button-menu" : undefined}
                          aria-expanded={openGenerateMenu ? "true" : undefined}
                          aria-haspopup="menu"
                          onClick={handleToggleGenerateMenu}
                          sx={{ borderRadius: "0 10px 10px 0", padding: "10px 10px" }}>
                          <ArrowDropDownIcon />
                        </Button>
                      </ButtonGroup>
                      <Popover
                        open={openGenerateMenu}
                        anchorEl={anchorRef.current}
                        anchorOrigin={{
                          vertical: "bottom",
                          horizontal: "right"
                        }}
                        transformOrigin={{
                          vertical: "top",
                          horizontal: "right"
                        }}>
                        <Box>
                          <ClickAwayListener onClickAway={handleCloseGenerateMenu}>
                            <MenuList id="split-button-menu" autoFocusItem>
                              <MenuItem
                                onClick={() => {
                                  values.send_email = true
                                  handleSubmit()
                                }}
                                disabled={isSubmitting}>
                                {intl.formatMessage({ id: "generate_and_send_email" })}
                              </MenuItem>
                            </MenuList>
                          </ClickAwayListener>
                        </Box>
                      </Popover>
                    </>
                  )}
                </CardActions>
              </Card>
            </motion.div>
          </Stack>
        </Form>
      )}
    </Formik>
  )
}

export default CreateForm
