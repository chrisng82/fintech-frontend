import { CircularProgress, Stack } from "@mui/material"
import React, { ReactElement } from "react"
import TabContainer from "./TabContainer"
import SummaryTab from "./SummaryTab"
import SignupLinksTab from "./SignupLinksTab"
import EmailTaskTab from "./EmailTask"
import CommunicationsTab from "./CommunicationsTab"
import TncTab from "./TncTab"
import { useParams } from "next/navigation"
import { useIsRouteAccessible } from "@/components/utils/RouteHelper"

export enum SelectedTabEnum {
  SUMMARY = "summary",
  EMAIL_TASKS = "email-tasks",
  SIGNUP_LINKS = "signup-links",
  COMMUNICATIONS = "communications",
  TERMS_CONDITIONS = "TnC"
}

interface Props {
  selectedTab: SelectedTabEnum
}

const ViewCompanyContainer: React.FC<Props> = ({ selectedTab }): ReactElement => {
  const { code } = useParams()

  const basePath = `/companies/${code}`

  const isSummaryAccessible = useIsRouteAccessible(`${basePath}/${SelectedTabEnum.SUMMARY}`)
  const isEmailTasksAccessible = useIsRouteAccessible(`${basePath}/${SelectedTabEnum.EMAIL_TASKS}`)
  const isSignupLinksAccessible = useIsRouteAccessible(
    `${basePath}/${SelectedTabEnum.SIGNUP_LINKS}`
  )
  const isCommunicationsAccessible = useIsRouteAccessible(
    `${basePath}/${SelectedTabEnum.COMMUNICATIONS}`
  )
  const isTnCsAccessible = useIsRouteAccessible(`${basePath}/${SelectedTabEnum.TERMS_CONDITIONS}`)

  return (
    <Stack
      sx={{
        width: "100%"
      }}>
      {code && code !== "" ? (
        <React.Fragment>
          <TabContainer selectedTab={selectedTab} />
          {selectedTab === SelectedTabEnum.SUMMARY && isSummaryAccessible && <SummaryTab />}
          {selectedTab === SelectedTabEnum.EMAIL_TASKS && isEmailTasksAccessible && (
            <EmailTaskTab />
          )}
          {selectedTab === SelectedTabEnum.SIGNUP_LINKS && isSignupLinksAccessible && (
            <SignupLinksTab />
          )}
          {selectedTab === SelectedTabEnum.COMMUNICATIONS && isCommunicationsAccessible && (
            <CommunicationsTab />
          )}
          {selectedTab === SelectedTabEnum.TERMS_CONDITIONS && isTnCsAccessible && <TncTab />}
        </React.Fragment>
      ) : (
        <CircularProgress />
      )}
    </Stack>
  )
}

export default ViewCompanyContainer
