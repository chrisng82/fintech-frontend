import { CardContent, Grid, Typography } from "@mui/material"
import { isValidCron } from "cron-validator"
import cronstrue from "cronstrue"
import React, { ReactElement } from "react"
import CronGuideTable from "./CronGuideTable"
import { CompanyEmailTask } from "@/stores/CompanyEmailTask/Types"
import { FormikValues } from "./ViewEmailTask/EmailTaskSchedule"
import { useIntl } from "react-intl"
import FieldData from "@/components/page/Form/FieldData"

interface Props {
  data?: CompanyEmailTask | null
  editMode: boolean
  isLoading: boolean
  values: FormikValues
}

const ScheduleFields: React.FC<Props> = ({ data, editMode, isLoading, values }): ReactElement => {
  const intl = useIntl()

  return (
    <CardContent>
      <Grid container spacing={2} rowSpacing={4}>
        <Grid item xs={6}>
          <FieldData
            editMode={editMode}
            fieldName="cron_pattern"
            isLoading={isLoading}
            label={intl.formatMessage({ id: "cron_pattern" })}
            value={data?.cron_pattern}
          />
          {values.cron_pattern !== "" &&
            isValidCron(values.cron_pattern.trim(), { seconds: false }) && (
              <Typography>{cronstrue.toString(values.cron_pattern)}</Typography>
            )}
        </Grid>
        <Grid item xs={6}>
          <CronGuideTable />
        </Grid>
      </Grid>
    </CardContent>
  )
}

export default ScheduleFields
