import {
  Box,
  Button,
  ButtonGroup,
  Card,
  CardContent,
  CardHeader,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography
} from "@mui/material"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import AddIcon from "@mui/icons-material/Add"
import { useParams, useRouter } from "next/navigation"
import { motion } from "framer-motion"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import ToggleOnIcon from "@mui/icons-material/ToggleOn"
import ToggleOffIcon from "@mui/icons-material/ToggleOff"
import EditIcon from "@mui/icons-material/Edit"
import DeleteIcon from "@mui/icons-material/Delete"
import cronstrue from "cronstrue"
import CompanyEmailTaskAction from "@/stores/CompanyEmailTask/Action"
import { CompanyEmailTask } from "@/stores/CompanyEmailTask/Types"
import ConfirmDeleteModal from "@/components/modals/ConfirmDeleteModal"
import { SelectedTabEnum } from "../ViewCompanyContainer"
import Link from "next/link"

const ManageEmailTaskSection: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const { code } = useParams()
  const dispatch = useDispatch()

  const [confirmDelete, setConfirmDelete] = useState<boolean>(false)
  const [deletingEmailTask, setDeletingEmailTask] = useState<CompanyEmailTask>()

  const { data: companyData, isLoading } = useSelector(
    (state: RootState) => state.company.showCompany
  )
  const data = useSelector((state: RootState) => state.companyEmailTask.emailTasks.data)

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.3, duration: 0.5 }}>
      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardHeader
          title={
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between"
              }}>
              <Typography>{intl.formatMessage({ id: "email_tasks" })}</Typography>
              <Link href={`/companies/${code}/${SelectedTabEnum.EMAIL_TASKS}/create`}>
                <Button variant="contained" startIcon={<AddIcon />}>
                  {intl.formatMessage({ id: "add" })}
                </Button>
              </Link>
            </Box>
          }
        />
        <CardContent>
          <TableContainer>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow
                  sx={{
                    "& th": { fontWeight: "bold" }
                  }}>
                  <TableCell>{intl.formatMessage({ id: "code" })}</TableCell>
                  <TableCell>{intl.formatMessage({ id: "email_title" })}</TableCell>
                  <TableCell>{intl.formatMessage({ id: "email_schedule" })}</TableCell>
                  <TableCell sx={{ width: "20%" }}>
                    {intl.formatMessage({ id: "actions" })}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isLoading
                  ? [...Array(10)].map((_, index) => (
                      <TableRow key={index}>
                        <TableCell colSpan={4}>
                          <Skeleton variant="text" height={44} />
                        </TableCell>
                      </TableRow>
                    ))
                  : data.map((row) => (
                      <TableRow
                        key={row.code}
                        sx={{
                          "& td": {
                            fontWeight: "500"
                          },
                          "&:last-child td, &:last-child th": { border: 0 }
                        }}>
                        <TableCell scope="row">{row.code}</TableCell>
                        <TableCell>{row.title}</TableCell>
                        <TableCell>{cronstrue.toString(row.cron_pattern)}</TableCell>
                        <TableCell>
                          <Box
                            sx={{
                              display: "flex",
                              gap: "0.5rem"
                            }}>
                            <ButtonGroup>
                              <Button
                                variant="contained"
                                onClick={() => {
                                  dispatch(CompanyEmailTaskAction.toggleCompanyEmailTask(row.id))
                                }}>
                                <Tooltip
                                  title={intl.formatMessage({
                                    id: row.is_active ? "toggle_off" : "toggle_on"
                                  })}>
                                  {row.is_active ? <ToggleOnIcon /> : <ToggleOffIcon />}
                                </Tooltip>
                              </Button>
                              <Link
                                href={
                                  companyData
                                    ? `/companies/${companyData.code}/${SelectedTabEnum.EMAIL_TASKS}/${row.code}`
                                    : "#"
                                }>
                                <Button variant="contained">
                                  <EditIcon />
                                </Button>
                              </Link>
                              <Button
                                variant="contained"
                                onClick={() => {
                                  setConfirmDelete(true)
                                  setDeletingEmailTask(row)
                                }}>
                                <DeleteIcon />
                              </Button>
                            </ButtonGroup>
                          </Box>
                        </TableCell>
                      </TableRow>
                    ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
        <ConfirmDeleteModal
          open={confirmDelete}
          onClose={() => setConfirmDelete(false)}
          onConfirm={() => {
            if (deletingEmailTask) {
              dispatch(
                CompanyEmailTaskAction.deleteCompanyEmailTask(deletingEmailTask.id, () =>
                  setConfirmDelete(false)
                )
              )
            }
          }}
        />
      </Card>
    </motion.div>
  )
}

export default ManageEmailTaskSection
