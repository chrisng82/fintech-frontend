import {
  TableContainer,
  Paper,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Typography
} from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"

const CronGuideTable: React.FC = (): ReactElement => {
  const intl = useIntl()

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableBody>
          <TableRow>
            <TableCell variant="head" sx={{ width: "10%" }}>
              {intl.formatMessage({ id: "cron_guide_pattern" })}
            </TableCell>
            <TableCell>
              <Typography variant="body1">
                {intl.formatMessage({ id: "cron_guide_pattern_sample" })}
              </Typography>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_pattern_desc" })}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell variant="head" sx={{ width: "10%" }}>
              {intl.formatMessage({ id: "cron_guide_allowed_values" })}
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_allowed_values_minutes" })}
              </Typography>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_allowed_values_hours" })}
              </Typography>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_allowed_values_day_of_month" })}
              </Typography>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_allowed_values_month" })}
              </Typography>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_allowed_values_day" })}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell variant="head" sx={{ width: "10%" }}>
              {intl.formatMessage({ id: "cron_guide_any" })}
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_any_desc" })}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell variant="head" sx={{ width: "10%" }}>
              {intl.formatMessage({ id: "cron_guide_separator" })}
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_separator_desc" })}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell variant="head" sx={{ width: "10%" }}>
              {intl.formatMessage({ id: "cron_guide_range" })}
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_range_desc" })}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell variant="head" sx={{ width: "10%" }}>
              {intl.formatMessage({ id: "cron_guide_step" })}
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2">
                {intl.formatMessage({ id: "cron_guide_step_desc" })}
              </Typography>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default CronGuideTable
