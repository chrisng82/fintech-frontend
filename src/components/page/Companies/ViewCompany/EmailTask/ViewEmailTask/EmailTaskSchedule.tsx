import { Box, Button, Card, CardActions, CardHeader, Collapse, Typography } from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import EditIcon from "@mui/icons-material/Edit"
import { Form, Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { object, string } from "yup"
import CompanyEmailTaskAction from "@/stores/CompanyEmailTask/Action"
import { useRouter } from "next/navigation"
import SnackbarAction from "@/stores/Snackbar/Action"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"
import { isValidCron } from "cron-validator"
import ScheduleFields from "../ScheduleFields"
import { SelectedTabEnum } from "../../ViewCompanyContainer"

export interface FormikValues {
  cron_pattern: string
}

const EmailTaskSchedule: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const [editMode, setEditMode] = useState<boolean>(false)

  const showCompany = useSelector((state: RootState) => state.company.showCompany)
  const emailTask = useSelector((state: RootState) => state.companyEmailTask.emailTask)

  const validationSchema = object({
    cron_pattern: string().required(
      intl.formatMessage({ id: "error_email_task_cron_pattern_required" })
    )
  })

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.9, duration: 0.5 }}>
      <Formik
        enableReinitialize
        initialValues={{
          cron_pattern: emailTask.data?.cron_pattern ?? ""
        }}
        validationSchema={validationSchema}
        validate={(values) => {
          let errors: Record<"cron_pattern", string> | {} = {}

          if (values.cron_pattern) {
            if (!isValidCron(values.cron_pattern, { seconds: false })) {
              errors = {
                cron_pattern: intl.formatMessage({ id: "invalid_cron" })
              }
            }
          }

          return errors
        }}
        onSubmit={(values, { setSubmitting }) => {
          if (emailTask.data && showCompany.data) {
            dispatch(
              CompanyEmailTaskAction.updateCompanyEmailTask(
                setSubmitting,
                emailTask.data.id,
                values,
                () => {
                  setEditMode(false)
                }
              )
            )
          } else {
            setSubmitting(false)

            if (!showCompany.data) {
              dispatch(
                SnackbarAction.openSnackbar(
                  intl.formatMessage({ id: "error_company_not_found" }),
                  "error"
                )
              )
              router.push(`/companies`)
            } else {
              dispatch(
                SnackbarAction.openSnackbar(
                  intl.formatMessage({ id: "error_email_task_not_found" }),
                  "error"
                )
              )
              router.push(`/companies/${showCompany.data.code}/${SelectedTabEnum.EMAIL_TASKS}`)
            }
          }
        }}>
        {({ isSubmitting, handleSubmit, values }) => (
          <Form onSubmit={handleSubmit}>
            <Card
              sx={{
                padding: "1rem 0.5rem"
              }}>
              <CardHeader
                title={
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}>
                    <Typography>{intl.formatMessage({ id: "when_to_schedule" })}</Typography>
                    <Button
                      variant="contained"
                      color="warning"
                      disabled={editMode}
                      onClick={() => setEditMode(true)}
                      startIcon={<EditIcon />}>
                      {intl.formatMessage({ id: "edit" })}
                    </Button>
                  </Box>
                }
              />
              <ScheduleFields editMode={editMode} isLoading={emailTask.isLoading} values={values} />
              <Collapse in={editMode}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <GreyButton
                    isLoadingButton
                    type="reset"
                    variant="contained"
                    loading={isSubmitting}
                    onClick={() => setEditMode(false)}>
                    {intl.formatMessage({ id: "cancel" })}
                  </GreyButton>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {intl.formatMessage({ id: "save" })}
                  </LoadingButton>
                </CardActions>
              </Collapse>
            </Card>
          </Form>
        )}
      </Formik>
    </motion.div>
  )
}

export default EmailTaskSchedule
