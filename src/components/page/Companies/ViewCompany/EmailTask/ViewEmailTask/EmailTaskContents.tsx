import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import EditIcon from "@mui/icons-material/Edit"
import { Form, Formik, FormikErrors } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { object, string } from "yup"
import CompanyEmailTaskAction from "@/stores/CompanyEmailTask/Action"
import { useRouter } from "next/navigation"
import { SelectedTabEnum } from "../../ViewCompanyContainer"
import SnackbarAction from "@/stores/Snackbar/Action"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"
import ContentFields from "../ContentFields"

export type SetFieldValue = (
  field: string,
  value: any,
  shouldValidate?: boolean | undefined
) => Promise<void | FormikErrors<FormikValues>>

export interface FormikValues {
  title: string
  subtitle: string
  body: string
}

const EmailTaskContents: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const [editMode, setEditMode] = useState<boolean>(false)

  const showCompany = useSelector((state: RootState) => state.company.showCompany)
  const emailTask = useSelector((state: RootState) => state.companyEmailTask.emailTask)

  const validationSchema = object({
    title: string().required(intl.formatMessage({ id: "error_email_task_title_required" })),
    body: string().required(intl.formatMessage({ id: "error_email_task_subtitle_required" }))
  })

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.6, duration: 0.5 }}>
      <Formik
        enableReinitialize
        initialValues={{
          title: emailTask.data?.title ?? "",
          subtitle: emailTask.data?.subtitle ?? "",
          body: emailTask.data?.body ?? ""
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          if (emailTask.data && showCompany.data) {
            dispatch(
              CompanyEmailTaskAction.updateCompanyEmailTask(
                setSubmitting,
                emailTask.data.id,
                values,
                () => {
                  setEditMode(false)
                }
              )
            )
          } else {
            setSubmitting(false)

            if (!showCompany.data) {
              dispatch(
                SnackbarAction.openSnackbar(
                  intl.formatMessage({ id: "error_company_not_found" }),
                  "error"
                )
              )
              router.push(`/companies`)
            } else {
              dispatch(
                SnackbarAction.openSnackbar(
                  intl.formatMessage({ id: "error_email_task_not_found" }),
                  "error"
                )
              )
              router.push(`/companies/${showCompany.data.code}/${SelectedTabEnum.EMAIL_TASKS}`)
            }
          }
        }}>
        {({ isSubmitting, handleSubmit, setFieldValue, values }) => (
          <Form onSubmit={handleSubmit}>
            <Card
              sx={{
                padding: "1rem 0.5rem"
              }}>
              <CardHeader
                title={
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}>
                    <Typography>{intl.formatMessage({ id: "email_contents" })}</Typography>
                    <Button
                      variant="contained"
                      color="warning"
                      disabled={editMode}
                      onClick={() => setEditMode(true)}
                      startIcon={<EditIcon />}>
                      {intl.formatMessage({ id: "edit" })}
                    </Button>
                  </Box>
                }
              />
              <ContentFields
                data={emailTask.data}
                editMode={editMode}
                isLoading={emailTask.isLoading}
                values={values}
                setFieldValue={setFieldValue}
              />
              <Collapse in={editMode}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <GreyButton
                    isLoadingButton
                    type="reset"
                    variant="contained"
                    loading={isSubmitting}
                    onClick={() => setEditMode(false)}>
                    {intl.formatMessage({ id: "cancel" })}
                  </GreyButton>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {intl.formatMessage({ id: "save" })}
                  </LoadingButton>
                </CardActions>
              </Collapse>
            </Card>
          </Form>
        )}
      </Formik>
    </motion.div>
  )
}

export default EmailTaskContents
