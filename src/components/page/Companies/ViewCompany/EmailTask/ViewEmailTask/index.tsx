import { Stack } from "@mui/material"
import React, { ReactElement, useEffect } from "react"
import { useDispatch } from "react-redux"
import EmailTaskGeneral from "./EmailTaskGeneral"
import EmailTaskHeader from "./EmailTaskHeader"
import CompanyEmailTaskAction from "@/stores/CompanyEmailTask/Action"
import EmailTaskSchedule from "./EmailTaskSchedule"
import EmailTaskContents from "./EmailTaskContents"

interface Props {
  companyCode: string
  emailTaskCode: string
}

const ViewEmailTask: React.FC<Props> = ({ companyCode, emailTaskCode }): ReactElement => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(
      CompanyEmailTaskAction.fetchCompanyEmailTaskByCompanyCodeAndTaskCode(
        companyCode,
        emailTaskCode
      )
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Stack
      spacing={3}
      sx={{
        width: "100%"
      }}>
      <EmailTaskHeader companyCode={companyCode} emailTaskCode={emailTaskCode} />
      <EmailTaskGeneral />
      <EmailTaskContents />
      <EmailTaskSchedule />
    </Stack>
  )
}

export default ViewEmailTask
