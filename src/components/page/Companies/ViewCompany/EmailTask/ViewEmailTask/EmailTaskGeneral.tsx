import {
  Box,
  Button,
  Card,
  CardActions,
  CardHeader,
  Collapse,
  Grid,
  Typography
} from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement, useState } from "react"
import { useIntl } from "react-intl"
import EditIcon from "@mui/icons-material/Edit"
import { Form, Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { object, string } from "yup"
import CompanyEmailTaskAction from "@/stores/CompanyEmailTask/Action"
import { useRouter } from "next/navigation"
import SnackbarAction from "@/stores/Snackbar/Action"
import GreyButton from "@/components/Base/GreyButton"
import { LoadingButton } from "@mui/lab"
import GeneralFields from "../GeneralFields"
import { SelectedTabEnum } from "../../ViewCompanyContainer"

export interface FormikValues {
  code: string
  extra_recipients: string
  extra_cc: string
  task_type: number
}

const EmailTaskGeneral: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const [editMode, setEditMode] = useState<boolean>(false)

  const showCompany = useSelector((state: RootState) => state.company.showCompany)
  const emailTask = useSelector((state: RootState) => state.companyEmailTask.emailTask)

  const validationSchema = object({
    code: string().required(intl.formatMessage({ id: "error_email_task_code_required" }))
  })

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.3, duration: 0.5 }}>
      <Formik
        enableReinitialize
        initialValues={{
          code: emailTask.data?.code ?? "",
          extra_recipients: emailTask.data?.extra_recipients ?? "",
          extra_cc: emailTask.data?.extra_cc ?? "",
          task_type: emailTask.data?.task_type ?? 1
        }}
        validationSchema={validationSchema}
        validate={(values: FormikValues) => {
          type KeyType =
            | "extra_recipients"
            | `extra_recipients_${number}`
            | "extra_cc"
            | `extra_cc_${number}`
          let errors: Record<KeyType, string> | {} = {}

          const validateMultipleEmails = (field: "extra_recipients" | "extra_cc") => {
            const emails = values[field].split(";")
            emails.forEach((email: string, index: number) => {
              if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
                errors = {
                  ...errors,
                  [field]: intl.formatMessage({ id: "error_invalid_email" }),
                  [`${field}_${index}`]: intl.formatMessage({ id: "error_invalid_email" })
                }
              }
            })
          }

          if (values.extra_recipients) {
            validateMultipleEmails("extra_recipients")
          }
          if (values.extra_cc) {
            validateMultipleEmails("extra_cc")
          }

          return errors
        }}
        onSubmit={(values, { setSubmitting }) => {
          if (emailTask.data && showCompany.data) {
            dispatch(
              CompanyEmailTaskAction.updateCompanyEmailTask(
                setSubmitting,
                emailTask.data.id,
                values,
                () => {
                  setEditMode(false)
                }
              )
            )
          } else {
            setSubmitting(false)

            if (!showCompany.data) {
              dispatch(
                SnackbarAction.openSnackbar(
                  intl.formatMessage({ id: "error_company_not_found" }),
                  "error"
                )
              )
              router.push(`/companies`)
            } else {
              dispatch(
                SnackbarAction.openSnackbar(
                  intl.formatMessage({ id: "error_email_task_not_found" }),
                  "error"
                )
              )
              router.push(`/companies/${showCompany.data.code}/${SelectedTabEnum.EMAIL_TASKS}`)
            }
          }
        }}>
        {({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Card
              sx={{
                padding: "1rem 0.5rem"
              }}>
              <CardHeader
                title={
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}>
                    <Typography>{intl.formatMessage({ id: "general" })}</Typography>
                    <Button
                      variant="contained"
                      color="warning"
                      disabled={editMode}
                      onClick={() => setEditMode(true)}
                      startIcon={<EditIcon />}>
                      {intl.formatMessage({ id: "edit" })}
                    </Button>
                  </Box>
                }
              />
              <GeneralFields
                data={emailTask.data}
                editMode={editMode}
                isLoading={emailTask.isLoading}
              />
              <Collapse in={editMode}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <GreyButton
                    isLoadingButton
                    type="reset"
                    variant="contained"
                    loading={isSubmitting}
                    onClick={() => setEditMode(false)}>
                    {intl.formatMessage({ id: "cancel" })}
                  </GreyButton>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {intl.formatMessage({ id: "save" })}
                  </LoadingButton>
                </CardActions>
              </Collapse>
            </Card>
          </Form>
        )}
      </Formik>
    </motion.div>
  )
}

export default EmailTaskGeneral
