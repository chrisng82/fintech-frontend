import CompanyEmailTaskAction from "@/stores/CompanyEmailTask/Action"
import { Form, Formik } from "formik"
import { useRouter } from "next/navigation"
import React, { PropsWithChildren, ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { object, string } from "yup"
import { RootState } from "@/stores"
import EmailTaskTypeEnum, { EmailTaskTypeValues } from "@/constants/EmailTaskType"
import { motion } from "framer-motion"
import { Card, CardActions, CardHeader, Stack, Typography } from "@mui/material"
import GeneralFields from "../GeneralFields"
import ScheduleFields from "../ScheduleFields"
import { LoadingButton } from "@mui/lab"
import { isValidCron } from "cron-validator"
import { SelectedTabEnum } from "../../ViewCompanyContainer"
import ContentFields from "../ContentFields"

export interface FormikValues {
  code: string
  extra_recipients: string
  extra_cc: string
  task_type: number

  title: string
  subtitle: string
  body: string

  cron_pattern: string
}

interface Props extends PropsWithChildren {}

const CreateForm: React.FC<Props> = ({ children }): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { data: company } = useSelector((state: RootState) => state.company.showCompany)

  const validationSchema = object({
    code: string().required(intl.formatMessage({ id: "error_email_task_code_required" })),
    title: string().required(intl.formatMessage({ id: "error_email_task_title_required" })),
    body: string().required(intl.formatMessage({ id: "error_email_task_body_required" })),
    cron_pattern: string().required(
      intl.formatMessage({ id: "error_email_task_cron_pattern_required" })
    )
  })

  return (
    <Formik
      enableReinitialize
      initialValues={{
        code: "",
        extra_recipients: "",
        extra_cc: "",
        task_type: EmailTaskTypeValues[EmailTaskTypeEnum.STATEMENT] as number,

        title: "",
        subtitle: "",
        body: "",

        cron_pattern: ""
      }}
      validationSchema={validationSchema}
      validate={(values: FormikValues) => {
        type KeyType =
          | "extra_recipients"
          | `extra_recipients_${number}`
          | "extra_cc"
          | `extra_cc_${number}`
          | "cron_pattern"
        let errors: Record<KeyType, string> | {} = {}

        const validateMultipleEmails = (field: "extra_recipients" | "extra_cc") => {
          const emails = values[field].split(";")
          emails.forEach((email: string, index: number) => {
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
              errors = {
                ...errors,
                [field]: intl.formatMessage({ id: "error_invalid_email" }),
                [`${field}_${index}`]: intl.formatMessage({ id: "error_invalid_email" })
              }
            }
          })
        }

        if (values.extra_recipients) {
          validateMultipleEmails("extra_recipients")
        }
        if (values.extra_cc) {
          validateMultipleEmails("extra_cc")
        }

        if (values.cron_pattern) {
          if (!isValidCron(values.cron_pattern, { seconds: false })) {
            errors = {
              cron_pattern: intl.formatMessage({ id: "invalid_cron" })
            }
          }
        }

        return errors
      }}
      onSubmit={(values, { setSubmitting }) => {
        if (company) {
          dispatch(
            CompanyEmailTaskAction.createCompanyEmailTask(
              setSubmitting,
              {
                ...values,
                company_id: company.id,
                is_active: true
              },
              () => {
                router.push(`/companies/${company.code}/${SelectedTabEnum.EMAIL_TASKS}`)
              }
            )
          )
        }
      }}>
      {({ isSubmitting, handleSubmit, values, setFieldValue }) => (
        <Form onSubmit={handleSubmit}>
          <Stack spacing={3}>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.3, duration: 0.5 }}>
              <Card
                sx={{
                  padding: "1rem 0.5rem"
                }}>
                <CardHeader
                  title={<Typography>{intl.formatMessage({ id: "general" })}</Typography>}
                />
                <GeneralFields createMode isLoading={isSubmitting} />
              </Card>
            </motion.div>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.6, duration: 0.5 }}>
              <Card
                sx={{
                  padding: "1rem 0.5rem"
                }}>
                <CardHeader
                  title={<Typography>{intl.formatMessage({ id: "email_contents" })}</Typography>}
                />
                <ContentFields<FormikValues>
                  editMode
                  isLoading={isSubmitting}
                  values={values}
                  setFieldValue={setFieldValue}
                />
              </Card>
            </motion.div>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 0.9, duration: 0.5 }}>
              <Card
                sx={{
                  padding: "1rem 0.5rem"
                }}>
                <CardHeader
                  title={<Typography>{intl.formatMessage({ id: "when_to_schedule" })}</Typography>}
                />
                <ScheduleFields editMode isLoading={isSubmitting} values={values} />
              </Card>
            </motion.div>
            <motion.div
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: 1.2, duration: 0.5 }}>
              <Card
                sx={{
                  padding: "1rem 0.5rem"
                }}>
                <CardActions
                  sx={{
                    justifyContent: "flex-end"
                  }}>
                  <LoadingButton
                    type="reset"
                    variant="contained"
                    color="warning"
                    loading={isSubmitting}>
                    {intl.formatMessage({ id: "reset" })}
                  </LoadingButton>
                  <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {intl.formatMessage({ id: "save" })}
                  </LoadingButton>
                </CardActions>
              </Card>
            </motion.div>
          </Stack>
        </Form>
      )}
    </Formik>
  )
}

export default CreateForm
