import { Fab, Stack } from "@mui/material"
import React, { ReactElement } from "react"
import CreateEmailTaskHeader from "./CreateEmailTaskHeader"
import CreateForm from "./CreateForm"
import { palette } from "@/components/utils/theme"
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow"

interface Props {
  companyCode: string
}

const CreateEmailTask: React.FC<Props> = ({ companyCode }): ReactElement => {
  return (
    <Stack
      spacing={3}
      sx={{
        width: "100%"
      }}>
      <CreateEmailTaskHeader companyCode={companyCode} />
      <CreateForm />
      <Fab
        onClick={() => {
          window.scrollTo({
            top: document.body.scrollHeight,
            behavior: "smooth"
          })
        }}
        sx={{
          position: "fixed",
          bottom: "1rem",
          right: "calc(56px + 1.5rem)",
          background: palette.purple.main,
          "&:hover": {
            background: palette.purple.dark
          }
        }}>
        <DoubleArrowIcon
          sx={{
            color: "white",
            transform: "rotate(90deg)"
          }}
        />
      </Fab>
      <Fab
        color="primary"
        onClick={() => {
          window.scrollTo({
            top: 0,
            behavior: "smooth"
          })
        }}
        sx={{
          position: "fixed",
          bottom: "1rem",
          right: "1rem",
          background: palette.purple.main,
          "&:hover": {
            background: palette.purple.dark
          }
        }}>
        <DoubleArrowIcon
          sx={{
            color: "white",
            transform: "rotate(-90deg)"
          }}
        />
      </Fab>
    </Stack>
  )
}

export default CreateEmailTask
