import GreyButton from "@/components/Base/GreyButton"
import { Box, Typography } from "@mui/material"
import { useRouter } from "next/navigation"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { motion } from "framer-motion"
import { SelectedTabEnum } from "../../ViewCompanyContainer"
import Link from "next/link"

interface Props {
  companyCode: string
}

const CreateEmailTaskHeader: React.FC<Props> = ({ companyCode }): ReactElement => {
  const intl = useIntl()
  const router = useRouter()

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          gap: "1rem"
        }}>
        <Link href={`/companies/${companyCode}/${SelectedTabEnum.EMAIL_TASKS}`}>
          <GreyButton variant="contained">{intl.formatMessage({ id: "back" })}</GreyButton>
        </Link>
        <Typography
          sx={{
            fontSize: "1.5rem",
            fontWeight: "bold"
          }}>
          {intl.formatMessage({ id: "create_email_task" })}
        </Typography>
      </Box>
    </motion.div>
  )
}

export default CreateEmailTaskHeader
