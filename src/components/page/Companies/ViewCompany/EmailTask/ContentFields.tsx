import { Button, CardContent, Grid, Collapse, Typography } from "@mui/material"
import React, { ReactElement } from "react"
import FieldData from "@/components/page/Form/FieldData"
import { useIntl } from "react-intl"
import { CompanyEmailTask } from "@/stores/CompanyEmailTask/Types"
import { SetFieldValue } from "./ViewEmailTask/EmailTaskContents"
import AddIcon from "@mui/icons-material/Add"
import { palette } from "@/components/utils/theme"
import EmailTaskTypeEnum, { EmailTaskTypeValues } from "@/constants/EmailTaskType"
import { FormikValues } from "./CreateEmailTask/CreateForm"

interface Props<T> {
  data?: CompanyEmailTask | null
  editMode: boolean
  isLoading: boolean
  values: T

  setFieldValue: SetFieldValue
}

const ContentFields = <T extends Partial<FormikValues>>({
  data,
  editMode,
  isLoading,
  values,

  setFieldValue
}: Props<T>): ReactElement => {
  const intl = useIntl()

  const placeholderButtons = [
    {
      label: intl.formatMessage({ id: "attention" }),
      value: "{{ATTENTION}}"
    },
    {
      label: intl.formatMessage({ id: "contact" }),
      value: "{{CONTACT}}"
    },
    {
      label: intl.formatMessage({ id: "day" }),
      value: "{{DAY}}"
    },
    {
      label: intl.formatMessage({ id: "month" }),
      value: "{{MONTH}}"
    },
    {
      label: intl.formatMessage({ id: "year" }),
      value: "{{YEAR}}"
    },
    {
      label: intl.formatMessage({ id: "overdue_table" }),
      value: "{{OVERDUE_TABLE}}"
    }
  ]

  const handleAddPlaceholder = (placeholder: string) => {
    setFieldValue("body", `${values.body} ${placeholder}`)
  }

  return (
    <CardContent>
      <Grid container spacing={2} rowSpacing={4}>
        <Grid item xs={12}>
          <FieldData
            editMode={editMode}
            fieldName="title"
            isLoading={isLoading}
            label={intl.formatMessage({ id: "email_title" })}
            value={data?.title}
          />
        </Grid>
        <Grid item xs={12}>
          <FieldData
            editMode={editMode}
            fieldName="subtitle"
            isLoading={isLoading}
            label={intl.formatMessage({ id: "email_subtitle" })}
            value={data?.subtitle}
          />
        </Grid>
        <Grid item xs={12}>
          <FieldData
            editMode={editMode}
            fieldName="body"
            isTextArea
            isLoading={isLoading}
            label={intl.formatMessage({ id: "body" })}
            value={data?.body}
          />
          <Collapse in={editMode}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography
                  sx={{
                    mt: 2,
                    color: palette.grey.main,
                    fontWeight: 500
                  }}>
                  {intl.formatMessage({ id: "click_add_placeholders" })}
                </Typography>
              </Grid>
              {placeholderButtons
                .filter((placeholder) => {
                  if (
                    (data && data.task_type !== EmailTaskTypeValues[EmailTaskTypeEnum.OVERDUE]) ||
                    values.task_type !== EmailTaskTypeValues[EmailTaskTypeEnum.OVERDUE]
                  ) {
                    return placeholder.value !== "{{OVERDUE_TABLE}}"
                  }

                  return true
                })
                .map((button, index) => (
                  <Grid key={`placeholder-${index}`} item>
                    <Button
                      variant="contained"
                      startIcon={<AddIcon />}
                      onClick={() => handleAddPlaceholder(button.value)}>
                      {button.label}
                    </Button>
                  </Grid>
                ))}
            </Grid>
          </Collapse>
        </Grid>
      </Grid>
    </CardContent>
  )
}

export default ContentFields
