import { toProperCase } from "@/components/utils/TextHelper"
import EmailTaskTypeEnum, { EmailTaskTypeValues } from "@/constants/EmailTaskType"
import { CardContent, Grid } from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { CompanyEmailTask } from "@/stores/CompanyEmailTask/Types"
import FieldData from "@/components/page/Form/FieldData"

interface Props {
  data?: CompanyEmailTask | null
  createMode?: boolean
  editMode?: boolean
  isLoading: boolean
}

const GeneralFields: React.FC<Props> = ({
  data,
  createMode = false,
  editMode = false,
  isLoading
}): ReactElement => {
  const intl = useIntl()

  return (
    <CardContent>
      <Grid container spacing={2} rowSpacing={4}>
        <Grid item xs={8}>
          <FieldData
            editMode={createMode || editMode}
            fieldName="code"
            disabled={editMode}
            isLoading={isLoading}
            label={intl.formatMessage({ id: "company_code" })}
            value={data?.code}
          />
        </Grid>
        <Grid item xs={4}>
          <FieldData
            editMode={createMode || editMode}
            fieldName="task_type"
            isSelect
            options={[
              {
                label: toProperCase(EmailTaskTypeEnum.STATEMENT),
                value: EmailTaskTypeValues[EmailTaskTypeEnum.STATEMENT]
              },
              {
                label: toProperCase(EmailTaskTypeEnum.OVERDUE),
                value: EmailTaskTypeValues[EmailTaskTypeEnum.OVERDUE]
              }
            ]}
            isLoading={isLoading}
            renderValue={(value: number) => EmailTaskTypeValues[value]}
            label={intl.formatMessage({ id: "task_type" })}
            value={data?.task_type}
          />
        </Grid>
        <Grid item xs={12}>
          <FieldData
            editMode={createMode || editMode}
            fieldName="extra_recipients"
            isMultiSelectChipped
            isLoading={isLoading}
            label={intl.formatMessage({ id: "extra_recipients" })}
            value={data?.extra_recipients}
          />
        </Grid>
        <Grid item xs={12}>
          <FieldData
            editMode={createMode || editMode}
            fieldName="extra_cc"
            isMultiSelectChipped
            isLoading={isLoading}
            label={intl.formatMessage({ id: "extra_cc" })}
            value={data?.extra_cc}
          />
        </Grid>
      </Grid>
    </CardContent>
  )
}

export default GeneralFields
