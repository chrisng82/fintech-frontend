import NoDataSVG from "@/assets/SVG/NoDataSVG"
import { FtpSyncLogStatusValues } from "@/constants/FtpSyncLogStatus"
import { RootState } from "@/stores"
import FtpSyncAction from "@/stores/FtpSync/Action"
import { FtpSyncLog } from "@/stores/FtpSync/Types"
import {
  Box,
  Chip,
  CircularProgress,
  Collapse,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from "@mui/material"
import dayjs from "dayjs"
import React, { ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp"
import { DocAttachment } from "@/stores/DocAttachment/Types"
import { LoadingButton } from "@mui/lab"
import customParseFormat from "dayjs/plugin/customParseFormat"
import DownloadIcon from "@mui/icons-material/Download"
import DocAttachmentAction from "@/stores/DocAttachment/Action"

dayjs.extend(customParseFormat)

interface SyncedFileRowProps {
  docAttachment: DocAttachment
}

const SyncedFileRow: React.FC<SyncedFileRowProps> = ({ docAttachment }) => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isLoading } = useSelector((state: RootState) => state.docAttachment.download)

  const fileParts = (docAttachment.pivot?.file_name ?? "").split(".")[0].split("_")
  const currencyAmount = parseInt(fileParts[4]) / 100

  return (
    <TableRow key={docAttachment.id}>
      <TableCell>{docAttachment.pivot?.file_name}</TableCell>
      <TableCell>{fileParts[1]}</TableCell>
      <TableCell>{decodeURIComponent(fileParts[0])}</TableCell>
      <TableCell>{decodeURIComponent(fileParts[2])}</TableCell>
      <TableCell>{fileParts[3]}</TableCell>
      <TableCell>{dayjs(fileParts[5], "DDMMYYYY").format("DD/MM/YYYY")}</TableCell>
      <TableCell>
        RM&nbsp;
        {intl.formatNumber(currencyAmount, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        })}
      </TableCell>
      <TableCell>
        <LoadingButton
          variant="contained"
          size="small"
          color="primary"
          startIcon={<DownloadIcon />}
          loading={isLoading}
          onClick={() => {
            dispatch(DocAttachmentAction.downloadByS3Url(docAttachment.url))
          }}>
          PDF
        </LoadingButton>
      </TableCell>
    </TableRow>
  )
}

interface RowProps {
  record: FtpSyncLog
}

const LogRow: React.FC<RowProps> = ({ record }) => {
  const intl = useIntl()
  const [expand, setExpand] = useState<boolean>(false)
  const [rowsPerPage, setRowsPerPage] = useState<number>(10)
  const [page, setPage] = useState<number>(1)

  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage + 1)
  }

  const handleChangeRowsPerPage = (event: any) => {
    const newPageSize = parseInt(event.target.value, 10)
    setPage(1)
    setRowsPerPage(newPageSize)
  }

  return (
    <React.Fragment>
      <TableRow>
        <TableCell width={50}>
          <IconButton size="small" onClick={() => setExpand((prev) => !prev)}>
            {expand ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>{record.pending_files}</TableCell>
        <TableCell>{record.synced_files}</TableCell>
        <TableCell>{dayjs(record.created_at).format("DD/MM/YYYY HH:mm:ss")}</TableCell>
        <TableCell>
          <Chip
            color={record.status === 0 ? "warning" : record.status === 1 ? "success" : "error"}
            label={FtpSyncLogStatusValues[record.status]}
          />
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={5}>
          <Collapse in={expand} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                {intl.formatMessage({ id: "synced_files" })}
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>{intl.formatMessage({ id: "file_name" })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: "doc_type" })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: "company" })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: "debtor" })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: "doc_code" })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: "doc_date" })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: "amount" })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: "actions" })}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {record.doc_attachments && record.doc_attachments.length > 0 ? (
                    record.doc_attachments
                      .filter(
                        (_, index) =>
                          index >= (page - 1) * rowsPerPage && index < page * rowsPerPage
                      )
                      .map((docAttachment) => (
                        <SyncedFileRow
                          key={`doc-${record.id}_att-${docAttachment.id}`}
                          docAttachment={docAttachment}
                        />
                      ))
                  ) : (
                    <TableRow>
                      <TableCell colSpan={5}>
                        <Box
                          sx={{
                            padding: "5rem 20rem"
                          }}>
                          <NoDataSVG />
                        </Box>
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
              <TablePagination
                rowsPerPageOptions={[10, 20, 50, 100]}
                component="div"
                count={(record.doc_attachments ?? []).length}
                rowsPerPage={rowsPerPage}
                page={page - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

const SyncedRecordsTable: React.FC = (): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { data, isLoading } = useSelector((state: RootState) => state.ftpSync.syncLogs)

  useEffect(() => {
    dispatch(FtpSyncAction.fetchFtpSyncLogs())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <TableContainer
      sx={{
        "& th": {
          fontWeight: "bold !important"
        },
        "& td": {
          fontWeight: "500 !important"
        }
      }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>{intl.formatMessage({ id: "pending_files" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "synced_files" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "sync_started_at" })}</TableCell>
            <TableCell>{intl.formatMessage({ id: "status" })}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {isLoading ? (
            <TableRow>
              <TableCell colSpan={5}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center"
                  }}>
                  <CircularProgress
                    sx={{
                      width: "3rem !important",
                      height: "3rem !important"
                    }}
                  />
                </Box>
              </TableCell>
            </TableRow>
          ) : data.length > 0 ? (
            data.map((record) => <LogRow key={`doc-${record.id}`} record={record} />)
          ) : (
            <TableRow>
              <TableCell colSpan={5}>
                <Box
                  sx={{
                    padding: "5rem 20rem"
                  }}>
                  <NoDataSVG />
                </Box>
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default SyncedRecordsTable
