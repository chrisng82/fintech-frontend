import { LoadingButton } from "@mui/lab"
import { Typography, Alert, Stack, Box, CircularProgress, IconButton } from "@mui/material"
import React, { ReactElement, useEffect } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import FtpSyncAction from "@/stores/FtpSync/Action"
import { RootState } from "@/stores"
import { motion } from "framer-motion"

const PendingFilesTable: React.FC = (): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { data, isLoading } = useSelector((state: RootState) => state.ftpSync.pendingFiles)

  useEffect(() => {
    dispatch(FtpSyncAction.fetchFtpSyncPendingFiles())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Stack
      spacing={3}
      direction="row"
      sx={{
        alignItems: "baseline"
      }}>
      <Stack
        spacing={1}
        direction="row"
        sx={{
          alignItems: "baseline"
        }}>
        {isLoading ? (
          <CircularProgress
            sx={{
              width: "3rem",
              height: "3rem"
            }}
          />
        ) : (
          <Typography
            sx={{
              fontSize: "3rem"
            }}>
            {data.length}
          </Typography>
        )}
        <Typography
          sx={{
            fontSize: "1.5rem",
            fontWeight: "bold"
          }}>
          {intl.formatMessage({ id: "files" }).toLowerCase()}
        </Typography>
      </Stack>
      {!isLoading && (
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}
          style={{
            width: "100%"
          }}>
          {data.length > 0 ? (
            <Alert
              severity="info"
              action={
                <LoadingButton
                  color="inherit"
                  size="small"
                  loading={isLoading}
                  onClick={() => {
                    dispatch(FtpSyncAction.syncFtpPendingFiles())
                  }}>
                  {intl.formatMessage({ id: "sync_now" })}
                </LoadingButton>
              }
              sx={{
                display: "flex",
                alignItems: "center"
              }}>
              {intl.formatMessage({ id: "pending_sync_files_description" })}
            </Alert>
          ) : (
            <Alert severity="success">
              {intl.formatMessage({ id: "files_synced_description" })}
            </Alert>
          )}
        </motion.div>
      )}
    </Stack>
  )
}

export default PendingFilesTable
