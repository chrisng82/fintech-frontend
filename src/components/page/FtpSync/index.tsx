import { Box, Card, CardContent, CardHeader, Stack, Typography } from "@mui/material"
import React, { ReactElement } from "react"
import PendingFiles from "./PendingFiles"
import SyncedRecordsTable from "./SyncedRecordsTable"
import { useIntl } from "react-intl"
import { LoadingButton } from "@mui/lab"
import RefreshIcon from "@mui/icons-material/Refresh"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import FtpSyncAction from "@/stores/FtpSync/Action"

const FtpSyncContainer: React.FC = (): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { isLoading } = useSelector((state: RootState) => state.ftpSync.pendingFiles)

  return (
    <Stack spacing={2}>
      <Card>
        <CardHeader
          title={
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between"
              }}>
              <Typography>{intl.formatMessage({ id: "pending_files" })}</Typography>
              <LoadingButton
                variant="contained"
                color="success"
                loading={isLoading}
                startIcon={<RefreshIcon />}
                onClick={() => {
                  dispatch(FtpSyncAction.fetchFtpSyncPendingFiles())
                }}>
                {intl.formatMessage({ id: "refresh" })}
              </LoadingButton>
            </Box>
          }
        />
        <CardContent>
          <PendingFiles />
        </CardContent>
      </Card>
      <Card>
        <CardHeader title={<Typography>{intl.formatMessage({ id: "sync_history" })}</Typography>} />
        <CardContent>
          <SyncedRecordsTable />
        </CardContent>
      </Card>
    </Stack>
  )
}

export default FtpSyncContainer
