import React, { ReactElement } from "react"
import { motion } from "framer-motion"
import { Card, CardContent, CardHeader, Grid, Typography } from "@mui/material"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { useRouter } from "next/navigation"
import FieldData from "@/components/page/Form/FieldData"

const DebtorProfileSection: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { item } = useSelector((state: RootState) => state.debtor)

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.3, duration: 0.5 }}>
      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardHeader
          title={<Typography>{intl.formatMessage({ id: "debtor_profile" })}</Typography>}
        />
        <CardContent>
          <Grid container spacing={2} rowSpacing={4}>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "debtor_code" })}
                value={item.data?.code}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "company_reg_no" })}
                value={item.data?.co_reg_no}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "tax_reg_no" })}
                value={item.data?.tax_reg_no}
              />
            </Grid>
            <Grid item xs={12}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "name_01" })}
                value={item.data?.name_01}
              />
            </Grid>
            <Grid item xs={12}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "name_02" })}
                value={item.data?.name_02}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "term_code" })}
                value={item.data?.term_code}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "area_code" })}
                value={item.data?.area_code}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "area_desc" })}
                value={item.data?.area_desc_01}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </motion.div>
  )
}

export default DebtorProfileSection
