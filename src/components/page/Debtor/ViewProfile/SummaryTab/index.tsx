"use client"
import { Box } from "@mui/material"
import React, { ReactElement } from "react"
import DebtorProfileSection from "./DebtorProfileSection"
import BillingDetailsSection from "./BillingDetailsSection"
import ShippingDetailsSection from "./ShippingDetailsSection"

const SummaryTab: React.FC = (): ReactElement => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: "1rem"
      }}>
      <DebtorProfileSection />
      <BillingDetailsSection />
      <ShippingDetailsSection />
    </Box>
  )
}

export default SummaryTab
