"use client"
import FieldData from "@/components/page/Form/FieldData"
import { RootState } from "@/stores"
import { BaseDebtorContact, DebtorContact } from "@/stores/Debtor/Types"
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  FormControlLabel,
  Grid,
  Typography
} from "@mui/material"
import { FieldArray, Form, Formik } from "formik"
import React, { ReactElement, useEffect, useState } from "react"
import { useIntl } from "react-intl"
import { useSelector } from "react-redux"
import shortid from "shortid"
import IOSSwitch from "@/components/Base/Switch"
import { convertNullToUndefined } from "@/components/utils/MapHelper"
import ContactForm from "@/components/page/Form/ContactForm"

interface FormikValues extends BaseDebtorContact {
  editMode: boolean
}

const ContactsTab: React.FC = (): ReactElement => {
  const intl = useIntl()

  const { data, isLoading } = useSelector((state: RootState) => state.debtor.item)

  const [contacts, setContacts] = useState<DebtorContact[]>([])
  const [expanded, setExpanded] = React.useState<number>(0)

  useEffect(() => {
    setContacts(data?.contacts ?? [])
  }, [data])

  const handleChange = (panel: number) => {
    if (expanded !== panel) {
      setExpanded(panel)
    }
  }

  const handleAddContact = () => {
    setContacts([
      ...contacts,
      {
        id: 0,
        debtor_id: data?.id ?? 0,
        title: "",
        name: "",
        email: "",
        phone: "",
        department: "",
        direct_phone: "",
        direct_fax: "",
        im: "",
        remark: "",
        is_main: contacts.length === 0,
        created_at: new Date(),
        updated_at: new Date()
      }
    ])
    setExpanded(contacts.length)
  }

  const handleRemoveContact = (index: number) => {
    const newContacts = [...contacts]
    newContacts.splice(index, 1)
    setContacts(newContacts)
    setExpanded(newContacts.length - 1)
  }

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column"
      }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-end",
          mb: 2
        }}>
        <Button variant="contained" color="primary" onClick={handleAddContact}>
          Add Contact
        </Button>
      </Box>

      {contacts.map((contact, index) => (
        <ContactForm
          key={`contact-form_${contact.id}`}
          index={index}
          length={contacts.length}
          expanded={expanded}
          data={contact}
          setExpended={setExpanded}
          handleChange={handleChange}
          removeContact={handleRemoveContact}
        />
      ))}

      {contacts.length === 0 && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            minHeight: "50vh",
            backgroundColor: "#fff",
            borderRadius: "10px"
          }}>
          <Typography variant="body1" sx={{ mt: 2 }}>
            {intl.formatMessage({ id: "no_contacts" })}
          </Typography>
        </Box>
      )}
    </Box>
  )
}

export default ContactsTab
