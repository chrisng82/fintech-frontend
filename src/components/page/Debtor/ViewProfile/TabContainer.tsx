import { palette } from "@/components/utils/theme"
import { Box, Paper, Typography } from "@mui/material"
import React, { PropsWithChildren, ReactElement } from "react"
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft"
import { motion } from "framer-motion"
import { useIntl } from "react-intl"
import { toProperCase } from "@/components/utils/TextHelper"
import GreyButton from "@/components/Base/GreyButton"
import { SelectedTabEnum } from "."
import Link from "next/link"
import { useRouter } from "next/navigation"

interface TabBoxProps extends PropsWithChildren {
  active: boolean
  link: string
}

const TabBox = ({ active, link, children }: TabBoxProps) => {
  return (
    <Link href={link}>
      <Paper
        sx={{
          display: "flex",
          padding: "1rem 1.5rem",
          borderRadius: "1rem",
          background: active ? palette.purple.main : "white",
          cursor: "pointer",
          transition: "all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94)",

          "& p": {
            color: active ? "#fff" : "inherit"
          },

          "&:hover": {
            transform: "scale(1.05)",
            background: active ? palette.purple.dark : palette.purple.main,
            "& p": {
              color: "#fff"
            }
          }
        }}>
        {children}
      </Paper>
    </Link>
  )
}

interface Props {
  selectedTab: SelectedTabEnum
}

const TabContainer: React.FC<Props> = ({ selectedTab }): ReactElement => {
  const intl = useIntl()
  const router = useRouter();

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}>
      <Box
        sx={{
          position: "relative",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: "1.5rem",
          mb: 2,
          gap: "1rem"
        }}>
        <Link href="/">
          <GreyButton
            variant="contained"
            startIcon={<ChevronLeftIcon />}
            sx={{
              position: "absolute",
              left: 0
            }}>
            {intl.formatMessage({ id: "back" })}
          </GreyButton>
        </Link>

        <TabBox
          active={selectedTab === SelectedTabEnum.SUMMARY}
          link={`/debtor/profile/${SelectedTabEnum.SUMMARY}`}>
          <Typography>{toProperCase(SelectedTabEnum.SUMMARY)}</Typography>
        </TabBox>
        <TabBox
          active={selectedTab === SelectedTabEnum.CONTACTS}
          link={`/debtor/profile/${SelectedTabEnum.CONTACTS}`}>
          <Typography>{toProperCase(SelectedTabEnum.CONTACTS)}</Typography>
        </TabBox>
      </Box>
    </motion.div>
  )
}

export default TabContainer
