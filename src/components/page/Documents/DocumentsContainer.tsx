import { Card, CardContent, MenuItem, Select, Stack, Typography } from "@mui/material"
import React, { ReactElement, useState } from "react"
import TabContainer from "./ViewDocuments/TabContainer"
import Invoice from "./ViewDocuments/Invoice"
import CreditNote from "./ViewDocuments/CreditNote"
import DebitNote from "./ViewDocuments/DebitNote"
import DebtorPayment from "./ViewDocuments/DebtorPayment"
import MonthlyStatement from "./ViewDocuments/MonthlyStatement"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { useIntl } from "react-intl"
import useAuth from "@/components/hooks/useAuth"
import DebtorAction from "@/stores/Debtor/Action"

export enum SelectedTabEnum {
  INVOICE = "invoice",
  CREDIT_NOTE = "credit-note",
  DEBIT_NOTE = "debit-note",
  DEBTOR_PAYMENT = "debtor-payment",
  MONTHLY_STATEMENT = "monthly-statement"
}

interface Props {
  selectedTab: SelectedTabEnum
}

const DocumentsContainer: React.FC<Props> = ({ selectedTab }): ReactElement => {
  const intl = useIntl()
  const { isDebtor } = useAuth()
  const dispatch = useDispatch()

  const { user } = useSelector((state: RootState) => state.app)
  const { selectedFilterDebtorId } = useSelector((state: RootState) => state.debtor)

  const handleOnChangeDebtor = (value: string) => {
    dispatch(DebtorAction.setSelectedFilterDebtorId(parseInt(value)))
  }

  return (
    <Stack
      sx={{
        width: "100%"
      }}>
      {isDebtor && (
        <Card sx={{ mb: 3 }}>
          <CardContent>
            <Typography variant="h6" component="div">
              {intl.formatMessage({ id: "debtor" })} :
            </Typography>
            <Select
              displayEmpty
              defaultValue={selectedFilterDebtorId}
              onChange={(e) => {
                handleOnChangeDebtor(String(e.target.value))
              }}
              sx={{ width: "100%" }}>
              {user?.debtors && user.debtors.length > 0 ? (
                user.debtors.map((debtor) => (
                  <MenuItem key={debtor.id} value={debtor.id}>
                    {debtor.name_01} {debtor.name_02} ({debtor.code})
                  </MenuItem>
                ))
              ) : (
                <MenuItem value="" disabled>
                  {intl.formatMessage({ id: "no_debtor" })}
                </MenuItem>
              )}
            </Select>
          </CardContent>
        </Card>
      )}
      <TabContainer selectedTab={selectedTab} />

      {selectedTab === SelectedTabEnum.INVOICE && (
        <Invoice companyCode={user?.companyCode} selectedTab={selectedTab} />
      )}
      {selectedTab === SelectedTabEnum.CREDIT_NOTE && (
        <CreditNote companyCode={user?.companyCode} selectedTab={selectedTab} />
      )}
      {selectedTab === SelectedTabEnum.DEBIT_NOTE && (
        <DebitNote companyCode={user?.companyCode} selectedTab={selectedTab} />
      )}
      {selectedTab === SelectedTabEnum.DEBTOR_PAYMENT && (
        <DebtorPayment companyCode={user?.companyCode} selectedTab={selectedTab} />
      )}
      {selectedTab === SelectedTabEnum.MONTHLY_STATEMENT && (
        <MonthlyStatement companyCode={user?.companyCode} selectedTab={selectedTab} />
      )}
    </Stack>
  )
}

export default DocumentsContainer
