import { Paper, Skeleton, TextField } from "@mui/material"
import { styled } from "@mui/material/styles"

export const Panel = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: "30px",
  borderRadius: "10px"
}))

export const TextFieldDetail = styled(TextField)({
  width: "100%",
  marginBottom: "10px"
})

export const RectangularSkeleton = styled(Skeleton)({
  marginBottom: "10px"
})
