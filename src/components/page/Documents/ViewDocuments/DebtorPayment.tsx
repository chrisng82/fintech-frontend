import React, { useEffect } from "react"
import dayjs from "dayjs"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"
import { SelectedTabEnum } from "../DocumentsContainer"
import DebtorPaymentAction from "@/stores/DebtorPayment/Action"

import Datatable, { IColumn } from "@/components/Datatable/DocumentsDatatable"
import { isNumber } from "lodash"
import { Card, CardContent } from "@mui/material"
import Filter, { IFilter } from "@/components/Datatable/Filter"
import { DebtorPaymentsFilters } from "@/stores/DebtorPayment/Types"
import { useUpdateEffect } from "usehooks-ts"
import useAuth from "@/components/hooks/useAuth"

interface IProps {
  companyCode: string | undefined
  selectedTab: SelectedTabEnum
}

const DebtorPayment: React.FC<IProps> = (props) => {
  const { companyCode, selectedTab } = props
  const { user } = useSelector((state: RootState) => state.app)

  const dispatch = useDispatch()

  const { isDebtor } = useAuth()

  const { data, page_size, current_page, last_page, total, is_loading, sorts } = useSelector(
    (state: RootState) => state.debtorPayment.list
  )
  const { selectedFilterDebtorId } = useSelector((state: RootState) => state.debtor)
  const debtorId = selectedFilterDebtorId ?? 0

  useEffect(() => {
    dispatachFetchDebtorPayments(1, page_size)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useUpdateEffect(() => {
    setFilters((filters) => {
      return filters.map((filter) => {
        if (filter.type === "debtorId") {
          return { ...filter, value: debtorId }
        }
        return filter
      })
    })
    dispatachFetchDebtorPayments(1, page_size)
  }, [companyCode, page_size, debtorId])

  const [filters, setFilters] = React.useState<IFilter[]>([])

  useEffect(() => {
    const newFilters: DebtorPaymentsFilters = {
      code: "",
      debtorCode: "",
      debtorName: "",
      pdfOnly: false,
      debtorId: debtorId,
      paymentStatus: "",
      docDate: []
    }

    filters.map((f) => {
      newFilters[f.type] = f.value
    })
    dispatch(DebtorPaymentAction.setDebtorPaymentsFilters(newFilters))
    dispatachFetchDebtorPayments(1, page_size, newFilters)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters, sorts])

  const handleSort = (columnId: string) => {
    const sortIndex = sorts.findIndex((sort) => sort.field === columnId)

    if (sortIndex >= 0) {
      const selectedSortIndex = sorts[sortIndex]
      if (selectedSortIndex.order == "asc") {
        dispatch(
          DebtorPaymentAction.setDebtorPaymentsSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: ""
            }
          ])
        )
      } else if (selectedSortIndex.order == "desc") {
        dispatch(
          DebtorPaymentAction.setDebtorPaymentsSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "asc"
            }
          ])
        )
      } else if (selectedSortIndex.order == "") {
        dispatch(
          DebtorPaymentAction.setDebtorPaymentsSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "desc"
            }
          ])
        )
      }
    } else {
      dispatch(DebtorPaymentAction.setDebtorPaymentsSorts([{ field: columnId, order: "desc" }]))
    }
  }

  const columns: IColumn[] = [
    {
      id: "checkbox",
      label: "",
      minWidth: 30,
      align: "center",
      sortable: false,
      filterable: false
    },
    { id: "no", label: "No", minWidth: 28, align: "center", sortable: false },
    {
      id: "docCode",
      label: "Doc. No",
      minWidth: 100,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "docCode") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "docCode")].order
          : "",
      handleSort: () => {
        handleSort("docCode")
      },
      type: "string"
    },
    {
      id: "docDate",
      label: "Document Date",
      minWidth: 70,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "docDate") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "docDate")].order
          : "",
      handleSort: () => {
        handleSort("docDate")
      },
      type: "date",
      format: (value) => dayjs(value).format("DD-MM-YYYY")
    },
    {
      id: "paymentMethod",
      label: "Type",
      minWidth: 60,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "paymentMethod") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "paymentMethod")].order
          : "",
      handleSort: () => {
        handleSort("paymentMethod")
      },
      type: "string",
      format: (value) => (value === "c" ? "Cash" : value === "q" ? "Cheque" : "Online")
    },
    {
      id: "debtorCode",
      label: "Debtor Code",
      minWidth: 60,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtorCode") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtorCode")].order
          : "",
      handleSort: () => {
        handleSort("debtorCode")
      },
      type: "string"
    },
    {
      id: "debtorName",
      label: "Debtor Name",
      minWidth: 60,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtorName") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtorName")].order
          : "",
      handleSort: () => {
        handleSort("debtorName")
      },
      type: "string"
    },
    {
      id: "payment",
      label: "Payment (RM)",
      minWidth: 60,
      align: "right",
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "payment") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "payment")].order
          : "",
      handleSort: () => {
        handleSort("payment")
      },
      type: "number",
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    },
    {
      id: "balance",
      label: "Balance (RM)",
      minWidth: 60,
      align: "right",
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "balance") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "balance")].order
          : "",
      handleSort: () => {
        handleSort("balance")
      },
      type: "number",
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    },
    {
      id: "action",
      label: "Action",
      minWidth: 60,
      align: "center",
      sortable: false
    }
  ]

  const handleChangePage = (event: any, newPage: number) => {
    dispatch(
      DebtorPaymentAction.setDebtorPaymentsPagination(page_size, newPage + 1, last_page, total)
    )
    dispatachFetchDebtorPayments(newPage + 1, page_size)
  }

  const handleChangeRowsPerPage = (event: any) => {
    const newPageSize = parseInt(event.target.value, 10)
    dispatch(DebtorPaymentAction.setDebtorPaymentsPagination(newPageSize, 1, last_page, total))
    dispatachFetchDebtorPayments(1, page_size)
  }

  const dispatachFetchDebtorPayments = (
    page: number,
    page_size: number,
    filters?: DebtorPaymentsFilters
  ) => {
    if ((isDebtor && debtorId !== 0) || !isDebtor) {
      dispatch(DebtorPaymentAction.fetchDebtorPayments(page, page_size, filters))
    }
  }

  return (
    <>
      <Card sx={{ mb: 2 }}>
        <CardContent>
          <Filter selectedTab={selectedTab} filters={filters} setFilters={setFilters} />
        </CardContent>
      </Card>

      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardContent>
          <Datatable
            docType={SelectedTabEnum.DEBTOR_PAYMENT}
            data={data}
            columns={columns}
            isLoading={is_loading}
            page={current_page}
            rowsPerPage={page_size}
            totalRows={total}
            idKey="id"
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </CardContent>
      </Card>
    </>
  )
}

export default DebtorPayment
