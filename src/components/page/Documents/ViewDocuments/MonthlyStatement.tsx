import React, { useEffect } from "react"
import dayjs from "dayjs"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"
import { SelectedTabEnum } from "../DocumentsContainer"
import MonthlyStatementAction from "@/stores/MonthlyStatement/Action"

import Datatable, { IColumn } from "@/components/Datatable/DocumentsDatatable"
import { filter, isNumber } from "lodash"
import { Card, CardContent } from "@mui/material"
import Filter, { IFilter } from "@/components/Datatable/Filter"
import { MonthlyStatementsFilters } from "@/stores/MonthlyStatement/Types"
import { useUpdateEffect } from "usehooks-ts"
import useAuth from "@/components/hooks/useAuth"

interface IProps {
  companyCode: string | undefined
  selectedTab: SelectedTabEnum
}

const MonthlyStatement: React.FC<IProps> = (props) => {
  const dispatch = useDispatch()

  const { isDebtor } = useAuth()

  const { companyCode, selectedTab } = props
  const { user } = useSelector((state: RootState) => state.app)
  const { data, page_size, current_page, last_page, total, is_loading, sorts } = useSelector(
    (state: RootState) => state.monthlyStatement.list
  )
  const { selectedFilterDebtorId } = useSelector((state: RootState) => state.debtor)
  const debtorId = selectedFilterDebtorId ?? 0

  const [filters, setFilters] = React.useState<IFilter[]>([])

  useEffect(() => {
    dispatachFetchMonthlyStatements(1, page_size)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useUpdateEffect(() => {
    setFilters((filters) => {
      return filters.map((filter) => {
        if (filter.type === "debtorId") {
          return { ...filter, value: debtorId }
        }
        return filter
      })
    })
    dispatachFetchMonthlyStatements(1, page_size)
  }, [companyCode, page_size, debtorId])

  useEffect(() => {
    const newFilters: MonthlyStatementsFilters = {
      code: "",
      debtorCode: "",
      debtorName: "",
      pdfOnly: false,
      debtorId: debtorId,
      paymentStatus: "",
      docDate: ""
    }

    filters.map((f: IFilter) => {
      newFilters[f.type] = f.value
    })
    dispatch(MonthlyStatementAction.setMonthlyStatementsFilters(newFilters))
    dispatachFetchMonthlyStatements(1, page_size, newFilters)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters, sorts])

  const handleSort = (columnId: string) => {
    const sortIndex = sorts.findIndex((sort) => sort.field === columnId)

    if (sortIndex >= 0) {
      const selectedSortIndex = sorts[sortIndex]
      if (selectedSortIndex.order == "asc") {
        dispatch(
          MonthlyStatementAction.setMonthlyStatementsSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: ""
            }
          ])
        )
      } else if (selectedSortIndex.order == "desc") {
        dispatch(
          MonthlyStatementAction.setMonthlyStatementsSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "asc"
            }
          ])
        )
      } else if (selectedSortIndex.order == "") {
        dispatch(
          MonthlyStatementAction.setMonthlyStatementsSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "desc"
            }
          ])
        )
      }
    } else {
      dispatch(
        MonthlyStatementAction.setMonthlyStatementsSorts([{ field: columnId, order: "desc" }])
      )
    }
  }

  const columns: IColumn[] = [
    {
      id: "checkbox",
      label: "",
      minWidth: 30,
      align: "center",
      sortable: false,
      filterable: false
    },
    { id: "no", label: "No", minWidth: 28, align: "center", sortable: false, filterable: false },
    {
      id: "doc_code",
      label: "Statement No.",
      minWidth: 100,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "doc_code") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "doc_code")].order
          : "",
      handleSort: () => {
        handleSort("doc_code")
      },
      type: "string"
    },
    {
      id: "statement_date",
      label: "Statement Date",
      minWidth: 70,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "statement_date") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "statement_date")].order
          : "",
      handleSort: () => {
        handleSort("statement_date")
      },
      type: "date",
      format: (value) => dayjs(value).format("DD-MM-YYYY")
    },
    {
      id: "debtor_code",
      label: "Debtor Code",
      minWidth: 60,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtor_code") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtor_code")].order
          : "",
      handleSort: () => {
        handleSort("debtor_code")
      },
      type: "string"
    },
    {
      id: "debtor_name",
      label: "Debtor Name",
      minWidth: 150,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtor_name") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtor_name")].order
          : "",
      handleSort: () => {
        handleSort("debtor_name")
      },
      type: "string"
    },
    {
      id: "action",
      label: "Action",
      minWidth: 60,
      align: "center",
      sortable: false,
      filterable: false
    }
  ]

  const handleChangePage = (event: any, newPage: number) => {
    dispatch(
      MonthlyStatementAction.setMonthlyStatementsPagination(
        page_size,
        newPage + 1,
        last_page,
        total
      )
    )
    dispatachFetchMonthlyStatements(newPage + 1, page_size)
  }

  const handleChangeRowsPerPage = (event: any) => {
    const newPageSize = parseInt(event.target.value, 10)
    dispatch(
      MonthlyStatementAction.setMonthlyStatementsPagination(newPageSize, 1, last_page, total)
    )
    dispatachFetchMonthlyStatements(1, page_size)
  }

  const dispatachFetchMonthlyStatements = (
    page: number,
    page_size: number,
    filters?: MonthlyStatementsFilters
  ) => {
    if ((isDebtor && debtorId !== 0) || !isDebtor) {
      dispatch(MonthlyStatementAction.fetchMonthlyStatements(page, page_size, filters))
    }
  }

  return (
    <>
      <Card sx={{ mb: 2 }}>
        <CardContent>
          <Filter selectedTab={selectedTab} filters={filters} setFilters={setFilters} />
        </CardContent>
      </Card>

      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardContent>
          <Datatable
            docType={SelectedTabEnum.MONTHLY_STATEMENT}
            data={data}
            columns={columns}
            isLoading={is_loading}
            page={current_page}
            rowsPerPage={page_size}
            totalRows={total}
            idKey="id"
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </CardContent>
      </Card>
    </>
  )
}

export default MonthlyStatement
