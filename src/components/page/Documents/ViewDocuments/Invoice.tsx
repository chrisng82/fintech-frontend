import React, { useEffect } from "react"
import dayjs from "dayjs"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"
import { SelectedTabEnum } from "../DocumentsContainer"
import InvoiceAction from "@/stores/Invoice/Action"

import Datatable, { IColumn } from "@/components/Datatable/DocumentsDatatable"
import { isNumber } from "lodash"
import { Button, Card, CardContent } from "@mui/material"
import Filter, { IFilter } from "@/components/Datatable/Filter"
import { InvoicesFilters } from "@/stores/Invoice/Types"
import { useUpdateEffect } from "usehooks-ts"
import useAuth from "@/components/hooks/useAuth"
import { LoadingButton } from "@mui/lab"
import FileDownloadIcon from "@mui/icons-material/FileDownload"
import DocAttachmentAction from "@/stores/DocAttachment/Action"

interface IProps {
  companyCode: string | undefined
  selectedTab: SelectedTabEnum
}

const Invoice: React.FC<IProps> = (props) => {
  const dispatch = useDispatch()

  const { isDebtor } = useAuth()

  const { companyCode, selectedTab } = props
  const { user } = useSelector((state: RootState) => state.app)
  const { data, page_size, current_page, last_page, total, is_loading, sorts } = useSelector(
    (state: RootState) => state.invoice.list
  )
  const { selectedFilterDebtorId } = useSelector((state: RootState) => state.debtor)
  const debtorId = selectedFilterDebtorId ?? 0

  const [filters, setFilters] = React.useState<IFilter[]>([])

  useEffect(() => {
    dispatchFetchInvoices(1, page_size)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useUpdateEffect(() => {
    setFilters((filters) =>
      filters.map((filter) =>
        filter.type === "debtorId" ? { ...filter, value: debtorId } : filter
      )
    )
    dispatchFetchInvoices(1, page_size)
  }, [companyCode, page_size, debtorId])

  useEffect(() => {
    const newFilters: InvoicesFilters = {
      code: "",
      debtorCode: "",
      debtorName: "",
      pdfOnly: false,
      debtorId: debtorId,
      paymentStatus: "",
      docDate: ""
    }

    filters.map((f: IFilter) => {
      newFilters[f.type] = f.value
    })

    dispatch(InvoiceAction.setInvoicesFilters(newFilters))
    dispatchFetchInvoices(1, page_size, newFilters)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters, sorts])

  const handleSort = (columnId: string) => {
    const sortIndex = sorts.findIndex((sort) => sort.field === columnId)

    if (sortIndex >= 0) {
      const selectedSortIndex = sorts[sortIndex]
      if (selectedSortIndex.order == "asc") {
        dispatch(
          InvoiceAction.setInvoicesSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: ""
            }
          ])
        )
      } else if (selectedSortIndex.order == "desc") {
        dispatch(
          InvoiceAction.setInvoicesSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "asc"
            }
          ])
        )
      } else if (selectedSortIndex.order == "") {
        dispatch(
          InvoiceAction.setInvoicesSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "desc"
            }
          ])
        )
      }
    } else {
      dispatch(InvoiceAction.setInvoicesSorts([{ field: columnId, order: "desc" }]))
    }
  }

  const columns: IColumn[] = [
    {
      id: "checkbox",
      label: "",
      minWidth: 30,
      align: "center",
      sortable: false,
      filterable: false
    },
    {
      id: "no",
      label: "No",
      minWidth: 28,
      align: "center",
      sortable: false,
      filterable: false
    },
    {
      id: "docCode",
      label: "Inv. No",
      minWidth: 100,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "docCode") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "docCode")].order
          : "",
      handleSort: () => {
        handleSort("docCode")
      },
      type: "string",
      render: (row, column) => {
        return (
          <LoadingButton
            startIcon={<FileDownloadIcon />}
            variant="contained"
            color="info"
            onClick={() => {
              if (companyCode) {
                dispatch(DocAttachmentAction.docAttachmentDlMiddleware(row.docCode, companyCode))
              }
            }}>
            {row.docCode}
          </LoadingButton>
        )
      }
    },
    {
      id: "docDate",
      label: "Document Date",
      minWidth: 70,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "docDate") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "docDate")].order
          : "",
      handleSort: () => {
        handleSort("docDate")
      },
      type: "date",
      format: (value) => dayjs(value).format("DD-MM-YYYY")
    },
    {
      id: "debtorCode",
      label: "Debtor Code",
      minWidth: 60,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtorCode") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtorCode")].order
          : "",
      handleSort: () => {
        handleSort("debtorCode")
      },
      type: "string"
    },
    {
      id: "debtorName",
      label: "Debtor Name",
      minWidth: 150,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtorName") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtorName")].order
          : "",
      handleSort: () => {
        handleSort("debtorName")
      },
      type: "string"
    },
    {
      id: "netAmt",
      label: "Net Amount (RM)",
      minWidth: 60,
      align: "right",
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "netAmt") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "netAmt")].order
          : "",
      handleSort: () => {
        handleSort("netAmt")
      },
      filterable: false,
      type: "number",
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    },
    {
      id: "payment",
      label: "Payment Status",
      minWidth: 60,
      align: "center",
      sortable: true,
      filterable: false,
      type: "string",
      render: (row, column) => {
        return parseFloat(row.txn_paid_amt) >= parseFloat(row.txn_amt) ? "PAID" : "NOT PAID"
      }
    },
    {
      id: "balance",
      label: "Balance (RM)",
      minWidth: 60,
      align: "right",
      sortable: true,
      filterable: false,
      type: "number",
      render: (row, column) => {
        return (parseFloat(row.txn_amt) - parseFloat(row.txn_paid_amt)).toFixed(2)
      }
    },
    {
      id: "action",
      label: "Action",
      minWidth: 60,
      align: "center",
      sortable: false,
      filterable: false
    }
  ]

  const handleChangePage = (event: any, newPage: number) => {
    dispatch(InvoiceAction.setInvoicesPagination(page_size, newPage + 1, last_page, total))
    dispatchFetchInvoices(newPage + 1, page_size)
  }

  const handleChangeRowsPerPage = (event: any) => {
    const newPageSize = parseInt(event.target.value, 10)
    dispatch(InvoiceAction.setInvoicesPagination(newPageSize, 1, last_page, total))
    dispatchFetchInvoices(1, page_size)
  }

  const dispatchFetchInvoices = (page: number, page_size: number, filters?: InvoicesFilters) => {
    if ((isDebtor && debtorId !== 0) || !isDebtor) {
      dispatch(InvoiceAction.fetchInvoices(page, page_size, filters))
    }
  }

  return (
    <>
      <Card sx={{ mb: 2 }}>
        <CardContent>
          <Filter selectedTab={selectedTab} filters={filters} setFilters={setFilters} />
        </CardContent>
      </Card>

      <Card
        sx={{
          padding: "1rem 0.5rem",
          width: "100%"
        }}>
        <CardContent>
          <Datatable
            docType={SelectedTabEnum.INVOICE}
            data={data}
            columns={columns}
            isLoading={is_loading}
            page={current_page}
            rowsPerPage={page_size}
            totalRows={total}
            idKey="id"
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </CardContent>
      </Card>
    </>
  )
}

export default Invoice
