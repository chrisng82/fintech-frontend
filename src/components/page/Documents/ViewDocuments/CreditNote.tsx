import React, { useEffect } from "react"
import dayjs from "dayjs"
import { useDispatch, useSelector } from "react-redux"

import { RootState } from "@/stores"
import { SelectedTabEnum } from "../DocumentsContainer"
import CreditNoteAction from "@/stores/CreditNote/Action"

import Datatable, { IColumn } from "@/components/Datatable/DocumentsDatatable"
import { isNumber } from "lodash"
import Filter, { IFilter } from "@/components/Datatable/Filter"
import { CreditNotesFilters } from "@/stores/CreditNote/Types"
import { Card, CardContent } from "@mui/material"
import { useUpdateEffect } from "usehooks-ts"
import useAuth from "@/components/hooks/useAuth"

interface IProps {
  companyCode: string | undefined
  selectedTab: SelectedTabEnum
}

const CreditNote: React.FC<IProps> = (props) => {
  const { companyCode, selectedTab } = props
  const { user } = useSelector((state: RootState) => state.app)

  const dispatch = useDispatch()

  const { isDebtor } = useAuth()

  const { data, page_size, current_page, last_page, total, is_loading, sorts } = useSelector(
    (state: RootState) => state.creditNote.list
  )
  const { selectedFilterDebtorId } = useSelector((state: RootState) => state.debtor)
  const debtorId = selectedFilterDebtorId ?? 0

  const [filters, setFilters] = React.useState<IFilter[]>([])

  useEffect(() => {
    dispatchFetchCreditNotes(1, page_size)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useUpdateEffect(() => {
    setFilters((filters) => {
      return filters.map((filter) => {
        if (filter.type === "debtorId") {
          return { ...filter, value: debtorId }
        }
        return filter
      })
    })
    dispatchFetchCreditNotes(1, page_size)
  }, [companyCode, page_size, debtorId])

  useEffect(() => {
    const newFilters: CreditNotesFilters = {
      code: "",
      debtorCode: "",
      debtorName: "",
      pdfOnly: false,
      debtorId: debtorId,
      paymentStatus: "",
      docDate: []
    }

    filters.map((f) => {
      newFilters[f.type] = f.value
    })
    dispatch(CreditNoteAction.setCreditNotesFilters(newFilters))
    dispatchFetchCreditNotes(1, page_size, newFilters)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters, sorts])

  const handleSort = (columnId: string) => {
    const sortIndex = sorts.findIndex((sort) => sort.field === columnId)

    if (sortIndex >= 0) {
      const selectedSortIndex = sorts[sortIndex]
      if (selectedSortIndex.order == "asc") {
        dispatch(
          CreditNoteAction.setCreditNotesSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: ""
            }
          ])
        )
      } else if (selectedSortIndex.order == "desc") {
        dispatch(
          CreditNoteAction.setCreditNotesSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "asc"
            }
          ])
        )
      } else if (selectedSortIndex.order == "") {
        dispatch(
          CreditNoteAction.setCreditNotesSorts([
            ...sorts.filter((d) => d.field != columnId),
            {
              field: columnId,
              order: "desc"
            }
          ])
        )
      }
    } else {
      dispatch(CreditNoteAction.setCreditNotesSorts([{ field: columnId, order: "desc" }]))
    }
  }

  const columns: IColumn[] = [
    {
      id: "checkbox",
      label: "",
      minWidth: 30,
      align: "center",
      sortable: false,
      filterable: false
    },
    {
      id: "no",
      label: "No",
      minWidth: 28,
      align: "center",
      sortable: false
    },
    {
      id: "docCode",
      label: "C/N No",
      minWidth: 100,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "docCode") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "docCode")].order
          : "",
      handleSort: () => {
        handleSort("docCode")
      },
      type: "string"
    },
    {
      id: "docDate",
      label: "Document Date",
      minWidth: 70,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "docDate") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "docDate")].order
          : "",
      handleSort: () => {
        handleSort("docDate")
      },
      type: "date",
      format: (value) => dayjs(value).format("DD-MM-YYYY")
    },
    {
      id: "debtorCode",
      label: "Debtor Code",
      minWidth: 60,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtorCode") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtorCode")].order
          : "",
      handleSort: () => {
        handleSort("debtorCode")
      },
      type: "string"
    },
    {
      id: "debtorName",
      label: "Debtor Name",
      minWidth: 150,
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "debtorName") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "debtorName")].order
          : "",
      handleSort: () => {
        handleSort("debtorName")
      },
      type: "string"
    },
    {
      id: "netAmt",
      label: "Net Amount (RM)",
      minWidth: 60,
      align: "right",
      sortable: true,
      order:
        sorts.findIndex((sort) => sort.field === "netAmt") >= 0
          ? sorts[sorts.findIndex((sort) => sort.field === "netAmt")].order
          : "",
      handleSort: () => {
        handleSort("netAmt")
      },
      type: "number",
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    },
    {
      id: "payment",
      label: "Payment Status",
      minWidth: 60,
      align: "center",
      sortable: true,
      type: "string",
      render: (row: any, column: any) => {
        return parseFloat(row.txn_paid_amt) >= parseFloat(row.txn_amt) ? "PAID" : "NOT PAID"
      }
    },
    {
      id: "balance",
      label: "Balance (RM)",
      minWidth: 60,
      align: "right",
      sortable: true,
      type: "number",
      render: (row: any, column: any) => {
        return (parseFloat(row.txn_amt) - parseFloat(row.txn_paid_amt)).toFixed(2)
      }
    },
    {
      id: "action",
      label: "Action",
      minWidth: 60,
      align: "center",
      sortable: false
    }
  ]

  const handleChangePage = (event: any, newPage: number) => {
    dispatch(CreditNoteAction.setCreditNotesPagination(page_size, newPage + 1, last_page, total))
    dispatchFetchCreditNotes(newPage + 1, page_size)
  }

  const handleChangeRowsPerPage = (event: any) => {
    const newPageSize = parseInt(event.target.value, 10)
    dispatch(CreditNoteAction.setCreditNotesPagination(newPageSize, 1, last_page, total))
    dispatchFetchCreditNotes(1, page_size)
  }

  const dispatchFetchCreditNotes = (
    page: number,
    page_size: number,
    filters?: CreditNotesFilters
  ) => {
    if ((isDebtor && debtorId !== 0) || !isDebtor) {
      dispatch(CreditNoteAction.fetchCreditNotes(page, page_size, filters))
    }
  }

  return (
    <>
      <Card sx={{ mb: 2 }}>
        <CardContent>
          <Filter selectedTab={selectedTab} filters={filters} setFilters={setFilters} />
        </CardContent>
      </Card>

      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardContent>
          <Datatable
            docType={SelectedTabEnum.CREDIT_NOTE}
            data={data}
            columns={columns}
            isLoading={is_loading}
            page={current_page}
            rowsPerPage={page_size}
            totalRows={total}
            idKey="id"
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </CardContent>
      </Card>
    </>
  )
}

export default CreditNote
