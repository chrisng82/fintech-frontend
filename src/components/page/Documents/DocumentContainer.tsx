import { Stack } from "@mui/material"
import React, { ReactElement } from "react"
import TabContainer from "./ViewDocument/TabContainer"
import Invoice from "./ViewDocument/Invoice"
import CreditNote from "./ViewDocument/CreditNote"
import DebitNote from "./ViewDocument/DebitNote"
import { SelectedTabEnum } from "./DocumentsContainer"

export enum DocumentStatusEnum {
  DRAFT = 0,
  VOID = 1,
  PENDING = 2,
  WIP = 3,
  COMPLETED = 4,
  DMS = 5
}

// Computed names
const documentStatusNames = {
  [DocumentStatusEnum.DRAFT]: "Draft",
  [DocumentStatusEnum.VOID]: "Void",
  [DocumentStatusEnum.PENDING]: "Pending",
  [DocumentStatusEnum.WIP]: "WIP",
  [DocumentStatusEnum.COMPLETED]: "Completed",
  [DocumentStatusEnum.DMS]: "DMS"
}

interface IProps {
  selectedTab: SelectedTabEnum
  id: number
}

const DocumentContainer: React.FC<IProps> = (props): ReactElement => {
  const { selectedTab, id } = props

  return (
    <Stack
      sx={{
        width: "100%"
      }}>
      <TabContainer selectedTab={selectedTab} />
      {selectedTab === SelectedTabEnum.INVOICE && <Invoice id={id} />}
      {selectedTab === SelectedTabEnum.CREDIT_NOTE && <CreditNote id={id} />}
      {selectedTab === SelectedTabEnum.DEBIT_NOTE && <DebitNote id={id} />}
    </Stack>
  )
}

export default DocumentContainer
