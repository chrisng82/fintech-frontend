import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from "@mui/material"

import CreditNoteAction from "@/stores/CreditNote/Action"
import { RootState } from "@/stores"
import { Panel, RectangularSkeleton, TextFieldDetail } from "../Documents.styles"
import Datatable, { IColumn } from "@/components/Datatable/DocumentsDatatable"
import { isNumber } from "lodash"
import { SelectedTabEnum } from "../DocumentsContainer"
import { DocumentStatusEnum } from "../DocumentContainer"
import { useUpdateEffect } from "usehooks-ts"

function LodingSkelelton() {
  return (
    <>
      <Grid item xs={6}>
        <RectangularSkeleton variant="rectangular" width="100%" height={61} />
        <RectangularSkeleton variant="rectangular" width="100%" height={61} />
        <RectangularSkeleton variant="rectangular" width="100%" height={61} />
      </Grid>
      <Grid item xs={6}>
        <RectangularSkeleton variant="rectangular" width="100%" height={61} />
        <RectangularSkeleton variant="rectangular" width="100%" height={61} />
      </Grid>
      <Grid item xs={12}>
        <RectangularSkeleton variant="rectangular" width="100%" height={61} />
      </Grid>
    </>
  )
}

interface IProps {
  id: number
}

const CreditNote: React.FC<IProps> = (props) => {
  const { id } = props

  const dispatch = useDispatch()

  const { data, is_loading } = useSelector((state: RootState) => state.creditNote.item)
  const { data: details_data, is_loading: details_is_loading } = useSelector(
    (state: RootState) => state.creditNote.item.details
  )
  const { user } = useSelector((state: RootState) => state.app)
  const columns: IColumn[] = [
    { id: "no", label: "No", minWidth: 28, align: "center" },
    { id: "item_code", label: "Item", minWidth: 100 },
    { id: "desc_01", label: "Description", minWidth: 70 },
    {
      id: "qty",
      label: "Quantity",
      minWidth: 60,
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    },
    { id: "uom_code", label: "UOM", minWidth: 50 },
    {
      id: "sale_price",
      label: "List Price (RM)",
      minWidth: 80,
      align: "right",
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    },
    {
      id: "gross_amt",
      label: "Order Amount (RM)",
      minWidth: 60,
      align: "right",
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    },
    {
      id: "net_amt",
      label: "Net Amount (RM)",
      minWidth: 60,
      align: "right",
      format: (value) => (isNumber(value) ? value.toFixed(2) : parseFloat(value).toFixed(2))
    }
  ]

  useEffect(() => {
    dispatch(CreditNoteAction.fetchCreditNote(id))
    dispatch(CreditNoteAction.fetchCreditNoteDetails(id))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useUpdateEffect(() => {
    dispatch(CreditNoteAction.fetchCreditNote(id))
    dispatch(CreditNoteAction.fetchCreditNoteDetails(id))
  }, [user?.companyCode])

  return (
    <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
      <Grid item xs={12}>
        <Panel elevation={3}>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            style={{ textAlign: "left" }}>
            <Grid item xs={12}>
              Document Section
            </Grid>
            {!is_loading && data ? (
              <>
                <Grid item xs={6}>
                  <TextFieldDetail
                    id="outlined-basic"
                    label="C/N. No"
                    variant="outlined"
                    defaultValue={data.docCode ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="C/N. Date"
                    variant="outlined"
                    defaultValue={data.docDate ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Payment Status"
                    variant="outlined"
                    defaultValue={data.txn_paid_amt >= data.txn_amt ? "PAID" : "NOT PAID"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Credit Terms"
                    variant="outlined"
                    defaultValue={data.creditTerms ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Company"
                    variant="outlined"
                    defaultValue={data.companyName ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Project"
                    variant="outlined"
                    defaultValue={data.projectCode ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Balance (RM)"
                    variant="outlined"
                    defaultValue={(data.txn_amt - data.txn_paid_amt).toFixed(2)}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Status"
                    variant="outlined"
                    defaultValue={DocumentStatusEnum[data.status] ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Description"
                    variant="outlined"
                    multiline
                    rows={2}
                    defaultValue={data.desc ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                </Grid>
              </>
            ) : (
              <LodingSkelelton />
            )}
          </Grid>
        </Panel>
      </Grid>

      <Grid item xs={12}>
        <Panel elevation={3}>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            style={{ textAlign: "left" }}>
            <Grid item xs={12}>
              Customer Section
            </Grid>
            {!is_loading && data ? (
              <>
                <Grid item xs={6}>
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Customer"
                    variant="outlined"
                    defaultValue={data.debtorName ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Currency"
                    variant="outlined"
                    defaultValue={data.currency ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Currency Rate"
                    variant="outlined"
                    defaultValue={data.currencyRate ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Email"
                    variant="outlined"
                    defaultValue={data.email ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Phone"
                    variant="outlined"
                    defaultValue={data.phone ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextFieldDetail
                    id="outlined-basic"
                    label="Billing Address"
                    variant="outlined"
                    defaultValue={`${data.unitNo}, ${data.buildingName}` ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    variant="outlined"
                    defaultValue={
                      `${data.district01}, ${data.district02}, ${data.streetName}, ` ?? "N/A"
                    }
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    variant="outlined"
                    defaultValue={`${data.postcode}, ${data.stateName},` ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                  <TextFieldDetail
                    id="outlined-basic"
                    variant="outlined"
                    defaultValue={`${data.countryName}` ?? "N/A"}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                </Grid>
              </>
            ) : (
              <LodingSkelelton />
            )}
          </Grid>
        </Panel>
      </Grid>

      <Grid item xs={12}>
        <Panel elevation={3}>
          <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
            <Grid item xs={12}>
              <TableContainer sx={{ maxHeight: 600 }}>
                <Table>
                  <TableHead>
                    <TableRow>
                      {columns.map((column) => (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          style={{ minWidth: column.minWidth }}>
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!details_is_loading && details_data ? (
                      details_data.map((row, index) => {
                        return (
                          <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                            {columns.map((column) => {
                              const value = row[column.id]
                              if (column.id === "no") {
                                return (
                                  <TableCell key={`head-${column.id}`} align="center">
                                    {index + 1}
                                  </TableCell>
                                )
                              }

                              return (
                                <TableCell key={column.id} align={column.align}>
                                  {column.format ? column.format(parseFloat(value)) : value}
                                </TableCell>
                              )
                            })}
                          </TableRow>
                        )
                      })
                    ) : (
                      <TableRow>
                        <TableCell colSpan={8} align="center">
                          Loading...
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
        </Panel>
      </Grid>
    </Grid>
  )
}

export default CreditNote
