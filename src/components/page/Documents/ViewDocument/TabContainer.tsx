import { palette } from "@/components/utils/theme"
import { Box, Button, Paper, Typography } from "@mui/material"
import React, { PropsWithChildren, ReactElement } from "react"
import { SelectedTabEnum } from "../DocumentsContainer"
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft"
import { useParams, useRouter } from "next/navigation"
import { motion } from "framer-motion"
import { useIntl } from "react-intl"
import { toProperCase } from "@/components/utils/TextHelper"
import GreyButton from "@/components/Base/GreyButton"
import Link from "next/link"

interface TabBoxProps extends PropsWithChildren {
  active: boolean
  onClick: () => void
}

const TabBox = ({ active, onClick, children }: TabBoxProps) => {
  return (
    <Paper
      onClick={onClick}
      sx={{
        display: "flex",
        padding: "0.8rem 1.5rem",
        background: "unset",
        boxShadow: "unset",
        borderRadius: "unset",
        borderBottom: active ? `solid 3px ${palette.purple.main}` : "none",
        cursor: "pointer",
        transition: "all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94)",

        "& p": {
          color: active ? "rgba(0, 0, 0, 0.87);" : "rgba(0, 0, 0, 0.3)",
          fontWeight: 700
        },

        "&:hover": {
          transform: "scale(1.05)",
          borderBottom: active ? `solid 3px ${palette.purple.main}` : "none"
        }
      }}>
      {children}
    </Paper>
  )
}

interface Props {
  selectedTab: SelectedTabEnum
}

const TabContainer: React.FC<Props> = ({ selectedTab }): ReactElement => {
  const intl = useIntl()
  const router = useRouter()

  const { code } = useParams()

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}>
      <Box
        sx={{
          position: "relative",
          display: "flex",
          alignItems: "center",
          mb: 2,
          gap: "1rem"
        }}>
        <Link href={`/documents/${selectedTab}`}>
          <GreyButton variant="contained" startIcon={<ChevronLeftIcon />}>
            {intl.formatMessage({ id: "back" })}
          </GreyButton>
        </Link>
      </Box>
    </motion.div>
  )
}

export default TabContainer
