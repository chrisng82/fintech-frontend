"use client"
import { palette } from "@/components/utils/theme"
import { Box, ButtonGroup, Paper, PaperProps, Typography } from "@mui/material"
import React, { PropsWithChildren, ReactElement, useEffect } from "react"
import PersonIcon from "@mui/icons-material/Person"
import PhoneIcon from "@mui/icons-material/Phone"
import EmailIcon from "@mui/icons-material/Email"
import { Debtor } from "@/stores/Debtor/Types"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import DebtorAction from "@/stores/Debtor/Action"
import { RootState } from "@/stores"
import SnackbarAction from "@/stores/Snackbar/Action"
import { LoadingButton } from "@mui/lab"

interface CardBoxProps extends PaperProps, PropsWithChildren {}

const CardBox = ({ children, ...rest }: CardBoxProps) => {
  return (
    <Paper
      {...rest}
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: "100%",
        padding: "1.5rem",
        borderRadius: "1rem",
        background: "#fff",
        cursor: "pointer",
        transition: "all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94)",

        "&:hover": {
          transform: "scale(1.05)",
          background: palette.purple.main,
          "& p": {
            color: "#fff"
          }
        }
      }}>
      {children}
    </Paper>
  )
}

interface Props {
  debtor: Debtor
  isOpenBill: boolean
  onLoad: () => void
}

const DebtorCard: React.FC<Props> = ({ debtor, isOpenBill, onLoad }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { user } = useSelector((state: RootState) => state.app)
  const { generateDocumentLoading } = useSelector((state: RootState) => state.debtor)

  useEffect(() => {
    onLoad()
  }, [onLoad])

  return (
    <CardBox>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: "0.5rem"
        }}>
        <Typography
          sx={{
            color: palette.purple.main,
            fontSize: "1.5rem"
          }}>
          {debtor.code}
        </Typography>
        <Typography>{debtor.name_01}</Typography>
      </Box>
      <Box
        mt={2}
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: "0.5rem"
        }}>
        <Typography
          sx={{
            fontWeight: 500,
            display: "flex",
            gap: "0.5rem",
            alignItems: "center",
            color: debtor.bill_attention === "" ? palette.grey.main : "inherit",
            fontStyle: debtor.bill_attention === "" ? "italic" : "normal"
          }}>
          <PersonIcon
            sx={{
              fontSize: "1.5rem"
            }}
          />
          {debtor.bill_attention === "" ? "No data" : debtor.bill_attention}
        </Typography>
        <Typography
          sx={{
            fontWeight: 500,
            display: "flex",
            gap: "0.5rem",
            alignItems: "center",
            color: debtor.bill_phone_01 === "" ? palette.grey.main : "inherit",
            fontStyle: debtor.bill_phone_01 === "" ? "italic" : "normal"
          }}>
          <PhoneIcon
            sx={{
              fontSize: "1.5rem"
            }}
          />
          {debtor.bill_phone_01 === "" ? "No data" : debtor.bill_phone_01}
        </Typography>
        <Typography
          sx={{
            fontWeight: 500,
            display: "flex",
            gap: "0.5rem",
            alignItems: "center",
            color: debtor.bill_email_01 === "" ? palette.grey.main : "inherit",
            fontStyle: debtor.bill_email_01 === "" ? "italic" : "normal"
          }}>
          <EmailIcon
            sx={{
              fontSize: "1.5rem"
            }}
          />
          {debtor.bill_email_01 === "" ? "No data" : debtor.bill_email_01}
        </Typography>
        <ButtonGroup>
          <LoadingButton
            variant="contained"
            color="success"
            size="small"
            fullWidth
            disabled={true}
            loading={generateDocumentLoading}
            onClick={(e) => {
              e.preventDefault()
              e.stopPropagation()

              if (user && user.companyCode) {
                dispatch(DebtorAction.generateStatement(debtor.id, user.companyCode, isOpenBill))
              } else {
                dispatch(
                  SnackbarAction.openSnackbar(
                    intl.formatMessage({ id: "error_generate_statement_session_expired" }),
                    "error"
                  )
                )
              }
            }}>
            {intl.formatMessage({ id: "statement" })}
          </LoadingButton>
          <LoadingButton
            variant="contained"
            color="warning"
            size="small"
            fullWidth
            disabled={!debtor.total_overdues || debtor.total_overdues === 0}
            loading={generateDocumentLoading}
            onClick={(e) => {
              e.preventDefault()
              e.stopPropagation()

              if (user && user.companyCode) {
                dispatch(DebtorAction.generateOverdue(debtor.id, user.companyCode))
              } else {
                dispatch(
                  SnackbarAction.openSnackbar(
                    intl.formatMessage({ id: "error_generate_overdue_session_expired" }),
                    "error"
                  )
                )
              }
            }}>
            {intl.formatMessage({ id: "overdue" })}
          </LoadingButton>
        </ButtonGroup>
      </Box>
    </CardBox>
  )
}

export default DebtorCard
