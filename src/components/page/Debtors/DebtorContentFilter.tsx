import IOSSwitch from "@/components/Base/Switch"
import { RootState } from "@/stores"
import DebtorAction from "@/stores/Debtor/Action"
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Collapse,
  FormControlLabel,
  Grid,
  Typography
} from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"
import { DatePicker } from "@mui/x-date-pickers/DatePicker"
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider"
import FieldData from "../Form/FieldData"
import dayjs from "dayjs"

interface Props {
  isOpenBill: boolean
  setIsFetching: React.Dispatch<React.SetStateAction<boolean>>
  setIsOpenBill: React.Dispatch<React.SetStateAction<boolean>>
}

const DebtorContentFilter: React.FC<Props> = ({
  isOpenBill,
  setIsFetching,
  setIsOpenBill
}): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { filters, per_page } = useSelector((state: RootState) => state.debtor.list)

  const handleToggleHasStatements = () => {
    setIsFetching(true)
    dispatch(
      DebtorAction.fetchDebtors(
        1,
        per_page,
        {
          ...filters,
          has_statements: !filters.has_statements
        },
        () => {
          setIsFetching(false)
        }
      )
    )
  }

  const handleToggleHasOverdues = () => {
    setIsFetching(true)
    dispatch(
      DebtorAction.fetchDebtors(
        1,
        per_page,
        {
          ...filters,
          has_overdues: !filters.has_overdues
        },
        () => {
          setIsFetching(false)
        }
      )
    )
  }

  const handleDateChange = (value: any) => {
    setIsFetching(true)
    const newDate = value ? dayjs(value).utc(true).format() : null
    dispatch(
      DebtorAction.fetchDebtors(
        1,
        per_page,
        {
          ...filters,
          end_date: newDate
        },
        () => {
          setIsFetching(false)
        }
      )
    )
  }

  return (
    <Card
      sx={{
        m: 1.5
      }}>
      <CardHeader
        title={
          <Typography
            sx={{
              fontWeight: 500
            }}>
            {intl.formatMessage({ id: "filters" })}
          </Typography>
        }
      />
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6} md={4}>
            <FormControlLabel
              labelPlacement="start"
              control={<IOSSwitch sx={{ m: 1 }} disabled checked={isOpenBill} />}
              label={
                <Typography
                  sx={{
                    fontWeight: 500
                  }}>
                  {intl.formatMessage({ id: "open_bill" })}
                </Typography>
              }
            />
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <FormControlLabel
              labelPlacement="start"
              control={
                <IOSSwitch
                  sx={{ m: 1 }}
                  checked={filters.has_statements}
                  onChange={handleToggleHasStatements}
                />
              }
              label={
                <Typography
                  sx={{
                    fontWeight: 500
                  }}>
                  {intl.formatMessage({ id: "debtors_with_statements" })}
                </Typography>
              }
            />
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <FormControlLabel
              labelPlacement="start"
              control={
                <IOSSwitch
                  sx={{ m: 1 }}
                  checked={filters.has_overdues}
                  onChange={handleToggleHasOverdues}
                />
              }
              label={
                <Typography
                  sx={{
                    fontWeight: 500
                  }}>
                  {intl.formatMessage({ id: "debtors_with_overdues" })}
                </Typography>
              }
            />
          </Grid>

          <Grid
            item
            xs={12}
            sx={{
              display: "flex",
              gap: "2rem",
              alignItems: "center"
            }}>
            <FormControlLabel
              labelPlacement="start"
              control={
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    value={dayjs(filters.end_date)}
                    onChange={handleDateChange}
                    format="DD/MM/YYYY"
                    sx={{
                      maxWidth: "200px",
                      "& .MuiOutlinedInput-notchedOutline": {
                        borderColor: "rgba(0, 0, 0, 0.23) !important"
                      }
                    }}
                  />
                </LocalizationProvider>
              }
              label={
                <Typography
                  sx={{
                    fontWeight: 500,
                    mr: 1
                  }}>
                  {intl.formatMessage({ id: "end_date" })}
                </Typography>
              }
            />
            <Collapse in={Boolean(filters.end_date)} orientation="horizontal">
              <Button variant="contained" color="warning" onClick={() => handleDateChange(null)}>
                Clear
              </Button>
            </Collapse>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}

export default DebtorContentFilter
