"use client"
import { palette } from "@/components/utils/theme"
import { Badge, Grid, IconButton, Typography } from "@mui/material"
import React, { ReactElement, useMemo, useState } from "react"
import BusinessIcon from "@mui/icons-material/Business"
import FilterAltIcon from "@mui/icons-material/FilterAlt"
import { useIntl } from "react-intl"
import { motion } from "framer-motion"
import DebtorsFilter from "../Filter/DebtorsFilter"
import { useSelector } from "react-redux"
import { RootState } from "@/stores"

const DebtorHeader: React.FC = (): ReactElement => {
  const intl = useIntl()

  const [open, setOpen] = useState<boolean>(false)

  const { name, code } = useSelector((state: RootState) => state.debtor.list.filters)

  const badgeNumber = useMemo(() => {
    let count = 0
    if (name !== "") {
      count++
    }
    if (code !== "") {
      count++
    }
    return count
  }, [name, code])

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}>
      <Grid
        container
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}>
        <Grid
          item
          xs={12}
          sm={6}
          sx={{
            display: "flex",
            alignItems: "center",
            gap: "1rem",

            "& p": {
              color: "#fff"
            }
          }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({ id: "debtors" })}
          </Typography>
        </Grid>

        <Grid
          item
          xs={12}
          sm={6}
          sx={{
            display: "flex",
            justifyContent: {
              xs: "flex-start",
              sm: "flex-end"
            },
            alignItems: "center",
            gap: "1rem"
          }}>
          <Badge color="secondary" badgeContent={badgeNumber} overlap="circular">
            <IconButton
              onClick={() => setOpen(true)}
              sx={{
                background: palette.purple.main,
                color: "#fff",
                "&:hover": {
                  background: palette.purple.dark,
                  color: "#fff"
                }
              }}>
              <FilterAltIcon />
            </IconButton>
          </Badge>
          <DebtorsFilter open={open} setOpen={setOpen} />
        </Grid>
      </Grid>
    </motion.div>
  )
}

export default DebtorHeader
