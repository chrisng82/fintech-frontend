import React, { ReactElement } from "react"
import TabContainer from "./TabContainer"
import { Stack } from "@mui/material"
import SummaryTab from "./SummaryTab"
import ContactsTab from "./ContactsTab"
import StatementsTab from "./StatementsTab"

export enum SelectedTabEnum {
  SUMMARY = "summary",
  CONTACTS = "contacts",
  STATEMENTS = "statements",
  OVERDUES = "overdues"
}

interface Props {
  selectedTab: SelectedTabEnum
}

const ViewDebtor: React.FC<Props> = ({ selectedTab }): ReactElement => {
  return (
    <Stack
      sx={{
        width: "100%"
      }}>
      <TabContainer selectedTab={selectedTab} />
      {selectedTab === SelectedTabEnum.SUMMARY && <SummaryTab />}
      {selectedTab === SelectedTabEnum.CONTACTS && <ContactsTab />}
      {selectedTab === SelectedTabEnum.STATEMENTS && <StatementsTab />}
      {selectedTab === SelectedTabEnum.OVERDUES && <div>OVERDUES</div>}
    </Stack>
  )
}

export default ViewDebtor
