import { RootState } from "@/stores"
import { Card, CardContent, CardHeader, Grid, Typography } from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useSelector } from "react-redux"
import FieldData from "@/components/page/Form/FieldData"

const ShippingDetailsSection: React.FC = (): ReactElement => {
  const intl = useIntl()

  const { item } = useSelector((state: RootState) => state.debtor)

  return (
    <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.9, duration: 0.5 }}>
      <Card
        sx={{
          padding: "1rem 0.5rem"
        }}>
        <CardHeader
          title={<Typography>{intl.formatMessage({ id: "shipping_details" })}</Typography>}
        />
        <CardContent>
          <Grid container spacing={2} rowSpacing={4}>
            <Grid item xs={6}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "email_01" })}
                value={item.data?.ship_email_01}
              />
            </Grid>
            <Grid item xs={6}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "email_02" })}
                value={item.data?.ship_email_02}
              />
            </Grid>
            <Grid item xs={3}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "fax_01" })}
                value={item.data?.ship_fax_01}
              />
            </Grid>
            <Grid item xs={3}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "fax_02" })}
                value={item.data?.ship_fax_02}
              />
            </Grid>
            <Grid item xs={3}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "phone_01" })}
                value={item.data?.ship_phone_01}
              />
            </Grid>
            <Grid item xs={3}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "phone_02" })}
                value={item.data?.ship_phone_02}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "attention" })}
                value={item.data?.ship_attention}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "unit_no" })}
                value={item.data?.ship_unit_no}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "building_name" })}
                value={item.data?.ship_building_name}
              />
            </Grid>
            <Grid item xs={12}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "street_name" })}
                value={item.data?.ship_street_name}
              />
            </Grid>
            <Grid item xs={6}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "district_01" })}
                value={item.data?.ship_district_01}
              />
            </Grid>
            <Grid item xs={6}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "district_02" })}
                value={item.data?.ship_district_02}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "postcode" })}
                value={item.data?.ship_postcode}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "state_name" })}
                value={item.data?.ship_state_name}
              />
            </Grid>
            <Grid item xs={4}>
              <FieldData
                editMode={false}
                isLoading={item.isLoading}
                label={intl.formatMessage({ id: "country_name" })}
                value={item.data?.ship_country_name}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </motion.div>
  )
}

export default ShippingDetailsSection
