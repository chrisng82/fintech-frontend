import NoDataSVG from "@/assets/SVG/NoDataSVG"
import IOSSwitch from "@/components/Base/Switch"
import { RootState } from "@/stores"
import { CreditNote } from "@/stores/CreditNote/Types"
import { DebitNote } from "@/stores/DebitNote/Types"
import { DebtorPayment } from "@/stores/DebtorPayment/Types"
import { Invoice } from "@/stores/Invoice/Types"
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  FormControlLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from "@mui/material"
import dayjs from "dayjs"
import React, { ReactElement, useMemo, useState } from "react"
import { FormattedNumber, useIntl } from "react-intl"
import { useSelector } from "react-redux"
import { Statement } from "."

interface Props {
  data: Statement[]
  isOpenBill: boolean
  setIsOpenBill: React.Dispatch<React.SetStateAction<boolean>>
}

const StatementTable: React.FC<Props> = ({ data, isOpenBill, setIsOpenBill }): ReactElement => {
  const intl = useIntl()

  const { isLoading } = useSelector((state: RootState) => state.debtor.item)

  const handleToggleIsOpenBill = () => {
    setIsOpenBill((prev) => !prev)
  }

  return (
    <Card>
      <CardHeader
        title={
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center"
            }}>
            <Typography>{intl.formatMessage({ id: "statement_of_account" })}</Typography>
            <FormControlLabel
              labelPlacement="start"
              control={<IOSSwitch sx={{ m: 1 }} disabled checked={isOpenBill} />}
              label={
                <Typography
                  sx={{
                    fontWeight: 500
                  }}>
                  {intl.formatMessage({ id: "open_bill" })}
                </Typography>
              }
            />
          </Box>
        }
      />
      <CardContent>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{intl.formatMessage({ id: "date" })}</TableCell>
                <TableCell>{intl.formatMessage({ id: "s_m" })}</TableCell>
                <TableCell>{intl.formatMessage({ id: "ref_no" })}</TableCell>
                <TableCell>{intl.formatMessage({ id: "description" })}</TableCell>
                <TableCell>{intl.formatMessage({ id: "due_date" })}</TableCell>
                <TableCell>{intl.formatMessage({ id: "debit" })}</TableCell>
                <TableCell>{intl.formatMessage({ id: "credit" })}</TableCell>
                <TableCell>{intl.formatMessage({ id: "balance" })}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {isLoading ? (
                <TableRow>
                  <TableCell colSpan={8}>
                    <Box
                      sx={{
                        padding: "5rem",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                      }}>
                      <CircularProgress />
                    </Box>
                  </TableCell>
                </TableRow>
              ) : data.length > 0 ? (
                data.map((statement) => (
                  <TableRow
                    key={`${statement.docCode}-${statement.docDate}`}
                    sx={{
                      "& td": {
                        fontWeight: 400
                      }
                    }}>
                    <TableCell>{dayjs(statement.docDate).format("DD/MM/YYYY")}</TableCell>
                    <TableCell>{statement.salesmanCode}</TableCell>
                    <TableCell>
                      <p>
                        {`${statement.ref_code_01 ?? ""}${
                          statement.ref_code_02 !== "" ? ` / ${statement.ref_code_02 ?? ""}` : ""
                        }`}
                      </p>
                      <p>{statement.docCode}</p>
                    </TableCell>
                    <TableCell>
                      {`${statement.desc_01 ?? ""}${
                        statement.desc_02 !== "" ? ` / ${statement.desc_02 ?? ""}` : ""
                      }`}
                    </TableCell>
                    <TableCell>
                      {statement.type !== "payment" &&
                        dayjs(statement.txn_due_date).format("DD/MM/YYYY")}
                    </TableCell>
                    <TableCell>
                      {["invoice", "dn"].includes(statement.type ?? "") && (
                        <FormattedNumber
                          value={statement.statement_amt}
                          minimumFractionDigits={2}
                          maximumFractionDigits={2}
                        />
                      )}
                    </TableCell>
                    <TableCell>
                      {["cn", "payment"].includes(statement.type ?? "") && (
                        <FormattedNumber
                          value={statement.statement_amt}
                          minimumFractionDigits={2}
                          maximumFractionDigits={2}
                        />
                      )}
                    </TableCell>
                    <TableCell>
                      <FormattedNumber
                        value={statement.balance}
                        minimumFractionDigits={2}
                        maximumFractionDigits={2}
                      />
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan={2} />
                  <TableCell colSpan={4}>
                    <Box
                      sx={{
                        padding: "5rem"
                      }}>
                      <NoDataSVG />
                    </Box>
                  </TableCell>
                  <TableCell colSpan={2} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
    </Card>
  )
}

export default StatementTable
