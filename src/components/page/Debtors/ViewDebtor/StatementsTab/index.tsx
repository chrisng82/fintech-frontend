import { Stack } from "@mui/material"
import React, { ReactElement, useMemo, useState } from "react"
import StatementTable from "./StatementTable"
import DebtorDetails from "./DebtorDetails"
import { Invoice } from "@/stores/Invoice/Types"
import { DebitNote } from "@/stores/DebitNote/Types"
import { CreditNote } from "@/stores/CreditNote/Types"
import { DebtorPayment } from "@/stores/DebtorPayment/Types"
import { useSelector } from "react-redux"
import { RootState } from "@/stores"
import MonthlyBalance from "./MonthlyBalance"
import dayjs from "dayjs"
import { motion } from "framer-motion"

export type Statement = (Invoice | DebitNote | CreditNote | DebtorPayment) & {
  statement_amt: number
  balance: number
}

const StatementsTab: React.FC = (): ReactElement => {
  const [isOpenBill, setIsOpenBill] = useState<boolean>(true)
  const { data } = useSelector((state: RootState) => state.debtor.item)

  const statements = data?.statements ? [...data.statements] : []

  const mappedStatements = useMemo(
    () =>
      statements
        .reduce(
          (
            acc: Statement[],
            statement: Invoice | DebitNote | CreditNote | DebtorPayment,
            index: number
          ) => {
            let statement_amt = parseFloat(statement.net_amt)

            if (isOpenBill) {
              statement_amt = statement.txn_amt - statement.txn_paid_amt
            }

            let balance = index === 0 ? 0 : acc[index - 1].balance

            if (["invoice", "dn"].includes(statement.type ?? "")) {
              balance += statement_amt
            } else if (["cn", "payment"].includes(statement.type ?? "")) {
              balance -= statement_amt
            }

            acc.push({
              ...statement,
              statement_amt,
              balance
            })

            return acc
          },
          []
        )
        .filter((statement) => (isOpenBill ? statement.statement_amt !== 0 : true)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data, isOpenBill]
  )

  return (
    <Stack spacing={2}>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.3, duration: 0.5 }}>
        <DebtorDetails isOpenBill={isOpenBill} />
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.6, duration: 0.5 }}>
        <MonthlyBalance data={mappedStatements} />
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ delay: 0.9, duration: 0.5 }}>
        <StatementTable
          data={mappedStatements}
          isOpenBill={isOpenBill}
          setIsOpenBill={setIsOpenBill}
        />
      </motion.div>
    </Stack>
  )
}

export default StatementsTab
