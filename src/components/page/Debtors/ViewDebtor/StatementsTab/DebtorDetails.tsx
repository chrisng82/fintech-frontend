import { RootState } from "@/stores"
import DebtorAction from "@/stores/Debtor/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { LoadingButton } from "@mui/lab"
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Grid,
  Typography
} from "@mui/material"
import React, { ReactElement } from "react"
import { useIntl } from "react-intl"
import { useDispatch, useSelector } from "react-redux"

interface Props {
  isOpenBill: boolean
}

const DebtorDetails: React.FC<Props> = ({ isOpenBill }): ReactElement => {
  const intl = useIntl()
  const dispatch = useDispatch()

  const { user } = useSelector((state: RootState) => state.app)
  const {
    generateDocumentLoading,
    item: { data, isLoading }
  } = useSelector((state: RootState) => state.debtor)

  return (
    <Card>
      <CardHeader
        title={
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center"
            }}>
            <Typography>{intl.formatMessage({ id: "debtor_details" })}</Typography>
            <LoadingButton
              variant="contained"
              color="success"
              size="small"
              disabled={!data || (data?.statements ?? []).length === 0}
              loading={generateDocumentLoading}
              onClick={(e) => {
                e.stopPropagation()

                if (data && user?.companyCode) {
                  dispatch(DebtorAction.generateStatement(data.id, user.companyCode, isOpenBill))
                } else {
                  dispatch(
                    SnackbarAction.openSnackbar(
                      intl.formatMessage({ id: "error_generate_statement_session_expired" }),
                      "error"
                    )
                  )
                }
              }}>
              {intl.formatMessage({ id: "statement" })}
            </LoadingButton>
          </Box>
        }
      />
      <CardContent>
        {isLoading ? (
          <Grid container>
            <Grid
              item
              xs={12}
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}>
              <CircularProgress />
            </Grid>
          </Grid>
        ) : (
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography sx={{ fontWeight: 600 }}>{intl.formatMessage({ id: "name" })}</Typography>
              <Typography
                sx={{
                  fontWeight: 400
                }}>
                {data?.name_01}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography sx={{ fontWeight: 600 }}>
                {intl.formatMessage({ id: "address" })}
              </Typography>
              <Typography
                sx={{
                  fontWeight: 400
                }}>
                {[
                  data?.bill_unit_no ?? "",
                  data?.bill_building_name ?? "",
                  data?.bill_street_name ?? "",
                  data?.bill_district_01 ?? ""
                ]
                  .filter(Boolean)
                  .map((str) => str.replace(/,$/, ""))
                  .join(", ")}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography sx={{ fontWeight: 600 }}>{intl.formatMessage({ id: "tel" })}</Typography>
              <Typography sx={{ fontWeight: 400 }}>
                {data?.contacts?.find((item) => item.is_main)?.phone ?? data?.bill_phone_01}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography sx={{ fontWeight: 600 }}>{intl.formatMessage({ id: "fax" })}</Typography>
              <Typography sx={{ fontWeight: 400 }}>
                {data?.contacts?.find((item) => item.is_main)?.direct_fax ??
                data?.bill_fax_01 !== "" ? (
                  data?.bill_fax_01
                ) : (
                  <i style={{ color: "#9e9e9e" }}>No Data</i>
                )}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography sx={{ fontWeight: 600 }}>
                {intl.formatMessage({ id: "a_c_no" })}
              </Typography>
              <Typography sx={{ fontWeight: 400 }}>{data?.code}</Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography sx={{ fontWeight: 600 }}>{intl.formatMessage({ id: "term" })}</Typography>
              <Typography sx={{ fontWeight: 400 }}>{data?.term_code}</Typography>
            </Grid>
          </Grid>
        )}
      </CardContent>
    </Card>
  )
}

export default DebtorDetails
