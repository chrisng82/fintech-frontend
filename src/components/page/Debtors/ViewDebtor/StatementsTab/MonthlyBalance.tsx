import React, { ReactElement } from "react"
import { Statement } from "."
import { Card, CardContent, CardHeader, Grid, Typography } from "@mui/material"
import dayjs from "dayjs"
import { useIntl } from "react-intl"
import customParseFormat from "dayjs/plugin/customParseFormat"
import { palette } from "@/components/utils/theme"
import { numberToRinggitMalaysiaWords } from "@/components/utils/TextHelper"

dayjs.extend(customParseFormat)

interface Props {
  data: Statement[]
}

const MonthlyBalance: React.FC<Props> = ({ data }): ReactElement => {
  const intl = useIntl()

  const months = Array.from({ length: 10 }, (_, i) => dayjs().subtract(10 - i - 1, "month"))
    .concat(Array.from({ length: 2 }, (_, i) => dayjs().add(i + 1, "month")))
    .map((month) => ({
      month: month.format("MMM'YYYY"),
      balance: 0
    }))

  const balanceByMonths = data.reduce((acc, statement: Statement) => {
    const month = dayjs(statement.docDate).format("MMM'YYYY")

    return acc.map((monthGroup) => {
      if (monthGroup.month === month) {
        if (["invoice", "dn"].includes(statement.type ?? "")) {
          monthGroup.balance += statement.statement_amt
        } else if (["cn", "payment"].includes(statement.type ?? "")) {
          monthGroup.balance -= statement.statement_amt
        }
      }

      return monthGroup
    })
  }, months)

  const currentMonthIndex = balanceByMonths.findIndex(
    (monthGroup) => monthGroup.month === dayjs().format("MMM'YYYY")
  )

  const totalBalance = data.reduce((acc, item) => {
    if (item.type === "invoice" || item.type === "dn") {
      return acc + item.statement_amt
    } else if (item.type === "cn" || item.type === "payment") {
      return acc - item.statement_amt
    }

    return acc
  }, 0)

  return (
    <Card>
      <CardHeader title={<Typography>Monthly Balance</Typography>} />
      <CardContent>
        <Grid container spacing={3}>
          {balanceByMonths.map((monthGroup, index) => (
            <Grid key={monthGroup.month} item xs={2}>
              <Card
                sx={{
                  backgroundColor:
                    monthGroup.month === dayjs().format("MMM'YYYY")
                      ? palette.purple.main
                      : currentMonthIndex > index
                      ? palette.purple.dark
                      : (theme) => theme.palette.success.main,
                  opacity: currentMonthIndex > index ? 0.8 : 1
                }}>
                <CardHeader
                  title={<Typography sx={{ color: "white" }}>{monthGroup.month}</Typography>}
                />
                <CardContent>
                  <Typography
                    sx={{
                      color: "white"
                    }}>
                    RM{" "}
                    {intl.formatNumber(monthGroup.balance, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2
                    })}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
          <Grid item xs={12}>
            <Typography sx={{ fontWeight: 600 }}>{intl.formatMessage({ id: "total" })}</Typography>
            <Typography sx={{ fontWeight: 400 }}>
              RM{" "}
              {intl.formatNumber(totalBalance, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
              })}
            </Typography>
            <Typography sx={{ fontWeight: 400 }}>
              {`${numberToRinggitMalaysiaWords(totalBalance)
                .replace("minus", "negative")
                .replace(",", "")
                .toUpperCase()} ONLY`}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}

export default MonthlyBalance
