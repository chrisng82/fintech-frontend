import { Container as MUIContainer, ContainerProps } from "@mui/material"
import React, { ReactElement } from "react"

const Container: React.FC<ContainerProps> = ({ sx, children }): ReactElement => {
  return (
    <MUIContainer
      sx={{
        ...sx,
        minHeight: "100vh"
      }}>
      {children}
    </MUIContainer>
  )
}

export default Container
