import { LoadingButton, LoadingButtonProps } from "@mui/lab"
import { Button, ButtonProps } from "@mui/material"
import React, { PropsWithChildren, ReactElement } from "react"
import { palette } from "../utils/theme"

type Props = PropsWithChildren<ButtonProps | LoadingButtonProps> & {
  isLoadingButton?: boolean
}

const GreyButton: React.FC<Props> = ({ isLoadingButton, children, ...rest }): ReactElement => {
  const greyButtonSx = {
    ...rest.sx,
    background: palette.grey.light,
    color: palette.primary.main,

    "&:hover": {
      background: palette.grey.main
    }
  }

  if (isLoadingButton) {
    return (
      <LoadingButton {...rest} sx={greyButtonSx}>
        {children}
      </LoadingButton>
    )
  }

  return (
    <Button {...rest} sx={greyButtonSx}>
      {children}
    </Button>
  )
}

export default GreyButton
