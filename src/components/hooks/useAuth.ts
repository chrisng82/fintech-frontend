import UserType from "@/stores/User/Types"
import React, { ReactElement } from "react"
import { useSelector } from "react-redux"

interface ReturnType {
  isLoggedIn: boolean
  isSuperAdmin: boolean
  isEfichainAdmin: boolean
  isEfichainProduct: boolean
  isIntegration: boolean
  isCompanyAdmin: boolean
  isCompanyStaff: boolean
  isDebtor: boolean
}

const useAuth = (): ReturnType => {
  const { user } = useSelector((state: any) => state.app)

  const isSuperAdmin = user?.type_str === UserType.SUPER_ADMIN
  const isEfichainAdmin = user?.type_str === UserType.EFICHAIN_ADMIN
  const isEfichainProduct = user?.type_str === UserType.EFICHAIN_PRODUCT
  const isIntegration = user?.type_str === UserType.INTEGRATION
  const isCompanyAdmin = user?.type_str === UserType.COMPANY_ADMIN
  const isCompanyStaff = user?.type_str === UserType.COMPANY_STAFF
  const isDebtor = user?.type_str === UserType.DEBTOR

  return {
    isLoggedIn: user !== null,
    isSuperAdmin,
    isEfichainAdmin,
    isEfichainProduct,
    isIntegration,
    isCompanyAdmin,
    isCompanyStaff,
    isDebtor
  }
}

export default useAuth
