export interface BaseModel {
  created_at: string | Date
  id: number
  updated_at: string | Date
}

export interface BaseDocumentModel {
  currency: string
  currencyRate: number
  debtorName: string
  docCode: string
  docDate: string
  ref_code_01: string
  ref_code_02: string
  phone: string
  salesmanCode: string
  projectCode: string
  status: number
  txn_amt: number
  txn_paid_amt: number
  txn_due_date: string
  net_amt: string
  companyName: string
  desc: string
  desc_01?: string
  desc_02?: string
  email: string
  unitNo: string
  buildingName: string
  district01: string
  district02: string
  streetName: string
  postcode: number
  stateName: string
  countryName: string
  type?: string
}
