const EficoreSyncStatusEnum = {
  FAILED: "FAILED",
  COMPLETED: "COMPLETED",
  IN_PROGRESS: "SYNCING",
  PENDING: "PENDING",
  CANCELED: "CANCELED"
}

export const EficoreSyncStatusValues = {
  [EficoreSyncStatusEnum.FAILED]: 0,
  0: EficoreSyncStatusEnum.FAILED,

  [EficoreSyncStatusEnum.COMPLETED]: 1,
  1: EficoreSyncStatusEnum.COMPLETED,

  [EficoreSyncStatusEnum.IN_PROGRESS]: 2,
  2: EficoreSyncStatusEnum.IN_PROGRESS,

  [EficoreSyncStatusEnum.PENDING]: 3,
  3: EficoreSyncStatusEnum.PENDING,

  [EficoreSyncStatusEnum.CANCELED]: 4,
  4: EficoreSyncStatusEnum.CANCELED
}
