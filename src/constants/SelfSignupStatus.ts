const SelfSignupStatusEnum = {
  REJECTED: "REJECTED",
  PENDING_APPROVAL_LEVEL_1: "PENDING_APPROVAL_LEVEL_1",
  PENDING_APPROVAL_LEVEL_2: "PENDING_APPROVAL_LEVEL_2",
  APPROVED: "APPROVED"
}

export const SelfSignupStatusValues = {
  [SelfSignupStatusEnum.REJECTED]: 0,
  0: SelfSignupStatusEnum.REJECTED,

  [SelfSignupStatusEnum.PENDING_APPROVAL_LEVEL_1]: 1,
  1: SelfSignupStatusEnum.PENDING_APPROVAL_LEVEL_1,

  [SelfSignupStatusEnum.PENDING_APPROVAL_LEVEL_2]: 2,
  2: SelfSignupStatusEnum.PENDING_APPROVAL_LEVEL_2,

  [SelfSignupStatusEnum.APPROVED]: 100,
  100: SelfSignupStatusEnum.APPROVED
}

export default SelfSignupStatusEnum
