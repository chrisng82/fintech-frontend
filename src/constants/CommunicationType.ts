enum CommunicationTypeEnum {
  LIVE = "LIVE",
  TESTING = "TESTING"
}

export const CommunicationTypeValues: Record<
  CommunicationTypeEnum | number,
  CommunicationTypeEnum | number
> = {
  [CommunicationTypeEnum.LIVE]: 1,
  1: CommunicationTypeEnum.LIVE,

  [CommunicationTypeEnum.TESTING]: 0,
  0: CommunicationTypeEnum.TESTING
}

export default CommunicationTypeEnum
