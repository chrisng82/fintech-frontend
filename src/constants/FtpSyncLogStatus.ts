const FtpSyncLogStatusEnum = {
  SYNCING: "SYNCING",
  SUCCESS: "SUCCESS",
  FAILED: "FAILED"
}

export const FtpSyncLogStatusValues = {
  [FtpSyncLogStatusEnum.SYNCING]: 0,
  0: FtpSyncLogStatusEnum.SYNCING,

  [FtpSyncLogStatusEnum.SUCCESS]: 1,
  1: FtpSyncLogStatusEnum.SUCCESS,

  [FtpSyncLogStatusEnum.FAILED]: 2,
  2: FtpSyncLogStatusEnum.FAILED
}

export default FtpSyncLogStatusEnum
