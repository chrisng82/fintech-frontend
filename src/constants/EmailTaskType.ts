enum EmailTaskTypeEnum {
  STATEMENT = "STATEMENT",
  OVERDUE = "OVERDUE"
}

export const EmailTaskTypeValues: Record<EmailTaskTypeEnum | number, EmailTaskTypeEnum | number> = {
  [EmailTaskTypeEnum.STATEMENT]: 1,
  1: EmailTaskTypeEnum.STATEMENT,

  [EmailTaskTypeEnum.OVERDUE]: 2,
  2: EmailTaskTypeEnum.OVERDUE
}

export default EmailTaskTypeEnum
