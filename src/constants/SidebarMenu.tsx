import UserType from "@/stores/User/Types"
import { IntlShape } from "react-intl"

export interface Menu {
  key: string
  label: string
  link?: string
  icon?: React.ReactNode
  user_type?: string[]
  children?: Menu[]
}

export const sidebarMenuGroups = (intl: IntlShape): Menu[] => [
  {
    key: "sidebar-menu-general",
    label: intl.formatMessage({ id: "general" }),
    children: [
      {
        key: "sidebar-menu-general-companies",
        label: intl.formatMessage({ id: "companies" }),
        link: "/companies",
        user_type: [
          UserType.SUPER_ADMIN,
          UserType.COMPANY_ADMIN,
          UserType.COMPANY_STAFF,
          UserType.EFICHAIN_ADMIN,
          UserType.EFICHAIN_PRODUCT
        ]
      },
      {
        key: "sidebar-menu-users",
        label: intl.formatMessage({ id: "users" }),
        link: "/users",
        user_type: [
          UserType.SUPER_ADMIN,
          UserType.COMPANY_ADMIN,
          UserType.COMPANY_STAFF,
          UserType.EFICHAIN_ADMIN,
          UserType.EFICHAIN_PRODUCT
        ]
      }
    ],
    user_type: [
      UserType.SUPER_ADMIN,
      UserType.COMPANY_ADMIN,
      UserType.COMPANY_STAFF,
      UserType.EFICHAIN_ADMIN,
      UserType.EFICHAIN_PRODUCT
    ]
  },
  {
    key: "sidebar-menu-accounts_receivable",
    label: intl.formatMessage({ id: "accounts_receivable" }),
    children: [
      {
        key: "sidebar-menu-accounts_receivable-debtors",
        label: intl.formatMessage({ id: "debtors" }),
        link: "/debtors",
        user_type: [
          UserType.SUPER_ADMIN,
          UserType.COMPANY_ADMIN,
          UserType.COMPANY_STAFF,
          UserType.EFICHAIN_ADMIN,
          UserType.EFICHAIN_PRODUCT
        ]
      },
      {
        key: "sidebar-menu-accounts_receivable-documents",
        label: intl.formatMessage({ id: "documents" }),
        link: "/documents",
        user_type: [
          UserType.SUPER_ADMIN,
          UserType.COMPANY_ADMIN,
          UserType.COMPANY_STAFF,
          UserType.EFICHAIN_ADMIN,
          UserType.EFICHAIN_PRODUCT,
          UserType.DEBTOR
        ]
      }
    ],
    user_type: [
      UserType.SUPER_ADMIN,
      UserType.COMPANY_ADMIN,
      UserType.COMPANY_STAFF,
      UserType.EFICHAIN_ADMIN,
      UserType.EFICHAIN_PRODUCT,
      UserType.DEBTOR
    ]
  },
  {
    key: "sidebar-menu-admin",
    label: intl.formatMessage({ id: "super_admin" }),
    children: [
      {
        key: "sidebar-menu-admin-ftp_sync",
        label: intl.formatMessage({ id: "ftp_sync" }),
        link: "/admin/ftp-sync",
        user_type: [UserType.SUPER_ADMIN, UserType.EFICHAIN_ADMIN, UserType.EFICHAIN_PRODUCT]
      },
      {
        key: "sidebar-menu-admin-eficore_sync",
        label: intl.formatMessage({ id: "eficore_sync" }),
        link: "/admin/eficore-sync",
        user_type: [UserType.SUPER_ADMIN, UserType.EFICHAIN_ADMIN, UserType.EFICHAIN_PRODUCT]
      }
    ],
    user_type: [UserType.SUPER_ADMIN, UserType.EFICHAIN_ADMIN, UserType.EFICHAIN_PRODUCT]
  }
]
