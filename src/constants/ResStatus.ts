const ResStatusEnum = {
  NULL: "NULL",
  DELETED: "DELETED",
  INACTIVE: "INACTIVE",
  BLOCKED: "BLOCKED",
  CONFLICT: "CONFLICT",
  NEW: "NEW",
  WIP: "WIP",
  ACTIVE: "ACTIVE"
}

export const ResStatusValues = {
  [ResStatusEnum.NULL]: 0,
  0: ResStatusEnum.NULL,

  [ResStatusEnum.DELETED]: 1,
  1: ResStatusEnum.DELETED,

  [ResStatusEnum.INACTIVE]: 2,
  2: ResStatusEnum.INACTIVE,

  [ResStatusEnum.BLOCKED]: 3,
  3: ResStatusEnum.BLOCKED,

  [ResStatusEnum.CONFLICT]: 4,
  4: ResStatusEnum.CONFLICT,

  [ResStatusEnum.NEW]: 5,
  5: ResStatusEnum.NEW,

  [ResStatusEnum.WIP]: 50,
  50: ResStatusEnum.WIP,

  [ResStatusEnum.ACTIVE]: 100,
  100: ResStatusEnum.ACTIVE
}

export default ResStatusEnum
