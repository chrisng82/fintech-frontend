enum UserTypeEnum {
  NULL = "NULL",
  SUPER_ADMIN = "SUPER_ADMIN",
  EFICHAIN_ADMIN = "EFICHAIN_ADMIN",
  EFICHAIN_PRODUCT = "EFICHAIN_PRODUCT",
  INTEGRATION = "INTEGRATION",
  COMPANY_ADMIN = "COMPANY_ADMIN",
  COMPANY_STAFF = "COMPANY_STAFF",
  DEBTOR = "DEBTOR"
}

export const UserTypeValues: Record<UserTypeEnum | number, UserTypeEnum | number> = {
  [UserTypeEnum.NULL]: 0,
  0: UserTypeEnum.NULL,

  [UserTypeEnum.SUPER_ADMIN]: 1,
  1: UserTypeEnum.SUPER_ADMIN,

  [UserTypeEnum.EFICHAIN_ADMIN]: 2,
  2: UserTypeEnum.EFICHAIN_ADMIN,

  [UserTypeEnum.EFICHAIN_PRODUCT]: 3,
  3: UserTypeEnum.EFICHAIN_PRODUCT,

  [UserTypeEnum.INTEGRATION]: 4,
  4: UserTypeEnum.INTEGRATION,

  [UserTypeEnum.COMPANY_ADMIN]: 5,
  5: UserTypeEnum.COMPANY_ADMIN,

  [UserTypeEnum.COMPANY_STAFF]: 6,
  6: UserTypeEnum.COMPANY_STAFF,

  [UserTypeEnum.DEBTOR]: 7,
  7: UserTypeEnum.DEBTOR
}

export default UserTypeEnum
