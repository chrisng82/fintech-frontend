export const isEnvValueTrue = (value: string | null): boolean => {
    if (!!value && value?.toLowerCase() === "true") {
        return true;
    }
    return false;
}