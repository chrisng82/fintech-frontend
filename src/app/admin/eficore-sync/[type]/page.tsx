"use client"

import HeaderedContainer from "@/components/page/HeaderedContainer"
import { Typography } from "@mui/material"
import { motion } from "framer-motion"
import React, { ReactElement, useEffect } from "react"
import { useIntl } from "react-intl"
import DnsIcon from "@mui/icons-material/Dns"
import { useRouter } from "next/navigation"
import useAuth from "@/components/hooks/useAuth"
import EficoreSyncContainer, { SelectedTabEnum } from "@/components/page/EficoreSync"

interface IProps {
  params: {
    type: SelectedTabEnum
  }
}

export default function EficoreSyncPage({ params: { type } }: IProps) {
  const intl = useIntl()
  const router = useRouter()

  const { isSuperAdmin, isEfichainAdmin, isEfichainProduct } = useAuth()
  useEffect(() => {
    if (!isSuperAdmin && !isEfichainAdmin && !isEfichainProduct) {
      router.push("/not-found")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <DnsIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({ id: "eficore_sync" })}
          </Typography>
        </motion.div>
      }>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.5 }}>
        <EficoreSyncContainer selectedTab={type} />
      </motion.div>
    </HeaderedContainer>
  )
}
