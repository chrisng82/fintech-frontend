"use client"

import { SelectedTabEnum } from "@/components/page/EficoreSync/index"
import AutoNavigate from "@/components/utils/AutoNavigate"

export default function EficoreSync() {
  return <AutoNavigate navigateTo={`/admin/eficore-sync/${SelectedTabEnum.DOCUMENTS}`} />
}
