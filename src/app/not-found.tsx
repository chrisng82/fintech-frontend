"use client"
import NotFoundSVG from "@/assets/SVG/NotFoundSVG"
import { Box, Button, Container, Typography } from "@mui/material"
import Link from "next/link"
import { useRouter } from "next/navigation"

export default function NotFound() {
  const router = useRouter()

  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        gap: 2,
        minHeight: "100vh"
      }}>
      <NotFoundSVG />
      <Typography variant="h5" component="div" gutterBottom>
        Oops! Page not found.
      </Typography>
      <Typography variant="body1" component="div" gutterBottom>
        We&apos;re sorry, but the page you requested could not be found.
      </Typography>
      <Button variant="contained" color="primary">
        <Link href="/">Go to Home Page</Link>
      </Button>
    </Container>
  )
}
