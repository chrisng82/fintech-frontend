"use client"

import { SelectedTabEnum } from "@/components/page/Debtor/ViewProfile"
import AutoNavigate from "@/components/utils/AutoNavigate"

export default function DebtorPage() {
  return <AutoNavigate navigateTo={`/debtor/profile/${SelectedTabEnum.SUMMARY}`} />
}