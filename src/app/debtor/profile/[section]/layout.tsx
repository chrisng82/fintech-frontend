"use client"

import useAuth from "@/components/hooks/useAuth"
import { SelectedTabEnum } from "@/components/page/Debtor/ViewProfile"
import { useRouter } from "next/navigation"
import { useEffect } from "react"

export default function DebtorProfileSectionLayout({
  params: { section },
  children
}: {
  params: { section: SelectedTabEnum }
  children: React.ReactNode
}) {
  const router = useRouter()

  const { isDebtor } = useAuth()

  useEffect(() => {
    if (!isDebtor) {
      router.push("/not-found")
    }

    if (!Object.values(SelectedTabEnum).includes(section)) {
      router.push(`/debtor/profile/${SelectedTabEnum.SUMMARY}`)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return children
}
