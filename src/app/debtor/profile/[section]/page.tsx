"use client"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { CircularProgress, Typography } from "@mui/material"
import { useEffect } from "react"
import { useIntl } from "react-intl"
import BusinessIcon from "@mui/icons-material/Business"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { motion } from "framer-motion"
import ViewDebtor, { SelectedTabEnum } from "@/components/page/Debtor/ViewProfile"
import DebtorAction from "@/stores/Debtor/Action"

interface Props {
  params: {
    section: SelectedTabEnum
  }
}

export default function ViewCompany({ params: { section } }: Props) {
  const intl = useIntl()

  const dispatch = useDispatch()

  const { item } = useSelector((state: RootState) => state.debtor)
  const { user } = useSelector((state: RootState) => state.app)

  useEffect(() => {
    if (item.data == null && user) {
      dispatch(DebtorAction.fetchDebtorById(user.selectedDebtorId))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {item.isLoading ? (
              <CircularProgress
                sx={{
                  color: "#fff"
                }}
              />
            ) : (
              intl.formatMessage({ id: "profile" })
            )}
          </Typography>
        </motion.div>
      }>
      <ViewDebtor selectedTab={section} />
    </HeaderedContainer>
  )
}
