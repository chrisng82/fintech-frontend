"use client"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { CircularProgress, Typography } from "@mui/material"
import { useEffect } from "react"
import BusinessIcon from "@mui/icons-material/Business"
import { useDispatch, useSelector } from "react-redux"
import CompanyAction from "@/stores/Company/Action"
import { RootState } from "@/stores"
import DocumentsContainer, { SelectedTabEnum } from "@/components/page/Documents/DocumentsContainer"
import { motion } from "framer-motion"
import { useIntl } from "react-intl"

interface Props {
  params: {
    type: SelectedTabEnum
  }
}

export default function ViewDocuments({ params: { type } }: Props) {
  const intl = useIntl()

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({ id: "documents" })}
          </Typography>
        </motion.div>
      }>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.5 }}>
        <DocumentsContainer selectedTab={type} />
      </motion.div>
    </HeaderedContainer>
  )
}
