"use client"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { Typography } from "@mui/material"
import BusinessIcon from "@mui/icons-material/Business"
import DocumentContainer from "@/components/page/Documents/DocumentContainer"
import { motion } from "framer-motion"
import { useIntl } from "react-intl"
import { SelectedTabEnum } from "@/components/page/Documents/DocumentsContainer"

interface Props {
  params: {
    type: SelectedTabEnum
    id: number
  }
}

export default function ViewDocument({ params: { type, id } }: Props) {
  const intl = useIntl()

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({ id: type })}
          </Typography>
        </motion.div>
      }>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.5 }}>
        <DocumentContainer selectedTab={type} id={id} />
      </motion.div>
    </HeaderedContainer>
  )
}
