"use client"

import { SelectedTabEnum } from "@/components/page/Documents/DocumentsContainer"
import AutoNavigate from "@/components/utils/AutoNavigate"

export default function Documents() {
  return <AutoNavigate navigateTo={`/documents/${SelectedTabEnum.INVOICE}`} />
}
