"use client"

import { SelectedApprovalTabEnum } from "@/components/page/Users/Approval/ApprovalContainer"
import AutoNavigate from "@/components/utils/AutoNavigate"

export default function Approval() {
  return <AutoNavigate navigateTo={`/users/approval/${SelectedApprovalTabEnum.WAITING_ON_YOU}`} />
}
