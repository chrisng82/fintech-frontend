"use client"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { Typography } from "@mui/material"
import BusinessIcon from "@mui/icons-material/Business"
import DocumentContainer from "@/components/page/Documents/DocumentContainer"
import { motion } from "framer-motion"
import { useIntl } from "react-intl"
import { SelectedApprovalTabEnum } from "@/components/page/Users/Approval/ApprovalContainer"
import PersonAddIcon from "@mui/icons-material/PersonAdd"
import { useSelector } from "react-redux"
import { RootState } from "@/stores"
import ViewApproval from "@/components/page/Users/Approval/ViewApproval"

interface Props {
  params: {
    type: SelectedApprovalTabEnum
    id: number
  }
}

export default function ViewDocument({ params: { type, id } }: Props) {
  const intl = useIntl()

  const { data } = useSelector((state: RootState) => state.user.selfSignupUser.item)

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <PersonAddIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {data?.company_name}
          </Typography>
        </motion.div>
      }>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.5 }}>
        <ViewApproval selectedTab={type} id={id} />
      </motion.div>
    </HeaderedContainer>
  )
}
