"use client"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { Typography } from "@mui/material"
import { motion } from "framer-motion"
import { useIntl } from "react-intl"
import ApprovalContainer, {
  SelectedApprovalTabEnum
} from "@/components/page/Users/Approval/ApprovalContainer"
import PersonAddIcon from "@mui/icons-material/PersonAdd"

interface Props {
  params: {
    type: SelectedApprovalTabEnum
  }
}

export default function ViewApprovalList({ params: { type } }: Props) {
  const intl = useIntl()

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <PersonAddIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({ id: "user_approval" })}
          </Typography>
        </motion.div>
      }>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.5 }}>
        <ApprovalContainer selectedTab={type} />
      </motion.div>
    </HeaderedContainer>
  )
}
