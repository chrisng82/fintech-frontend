"use client"
import { palette } from "@/components/utils/theme"
import { Box, CircularProgress, Grid, Stack, Typography } from "@mui/material"
import { useIntl } from "react-intl"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useRouter, useSearchParams } from "next/navigation"

import { RootState } from "@/stores"
import { waveBackground } from "@/components/utils/PatternHelper"
import ResetPasswordForm from "@/components/page/Form/ResetPasswordForm"
import AppAction from "@/stores/App/Action"

const ResetPassword = () => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()
  const searchParams = useSearchParams()

  const ref = searchParams.get("ref")
  const appTitle = process.env.NEXT_PUBLIC_APP_TITLE

  const [validateSuccess, setValidateSuccess] = useState<boolean>(false)

  const {
    resetPassword: { isValidating }
  } = useSelector((state: RootState) => state.app)

  useEffect(() => {
    if (ref) {
      dispatch(
        AppAction.validateResetPasswordToken(
          ref,
          () => {
            setValidateSuccess(true)
          },
          () => {
            router.push("/auth/login")
          }
        )
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Grid
      container
      sx={{
        minHeight: "100vh",
        width: "100vw",
        display: "flex"
      }}>
      <Grid
        item
        md={6}
        sx={{
          background: `${waveBackground},
            linear-gradient(335deg, rgba(0,2,36,0.8) 0%, rgba(9,46,121,0.8) 0%, rgba(203,0,255,0.8) 100%)`,
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}>
        <Box
          sx={{
            background: "rgba(255,255,255,0.5)",
            backdropFilter: "blur(10px)",
            height: "75%",
            width: "90%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: "1rem",
            padding: "5rem"
          }}>
          <Typography
            sx={{
              fontSize: "3rem",
              fontWeight: "bold",
              color: "white"
            }}>
            {appTitle}
            &nbsp;
            <span
              style={{
                color: palette.purple.dark
              }}>
              {intl.formatMessage({ id: "portal" })}.
            </span>
          </Typography>
        </Box>
      </Grid>
      <Grid
        item
        md={6}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}>
        {validateSuccess ? (
          <ResetPasswordForm />
        ) : isValidating ? (
          <Stack direction="row" gap={2} alignItems="center">
            <Typography>{intl.formatMessage({ id: "validating_token" })}</Typography>
            <CircularProgress />
          </Stack>
        ) : null}
      </Grid>
    </Grid>
  )
}

export default ResetPassword
