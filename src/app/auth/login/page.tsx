"use client"
import { palette } from "@/components/utils/theme"
import { Box, Grid, Typography } from "@mui/material"
import { useIntl } from "react-intl"
import { useEffect } from "react"
import { useSelector } from "react-redux"
import { useRouter } from "next/navigation"

import LoginForm from "@/components/page/Form/LoginForm"
import { RootState } from "@/stores"
import { waveBackground } from "@/components/utils/PatternHelper"

const Login = () => {
  const intl = useIntl()

  const router = useRouter()
  const appTitle = process.env.NEXT_PUBLIC_APP_TITLE

  const { user, token } = useSelector((state: RootState) => state.app)

  useEffect(() => {
    if (token && token !== "" && user) {
      router.replace("/")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, token])

  return (
    <Grid
      container
      sx={{
        minHeight: "100vh",
        width: "100vw",
        display: "flex"
      }}>
      <Grid
        item
        md={6}
        sx={{
          background: `${waveBackground},
            linear-gradient(335deg, rgba(0,2,36,0.8) 0%, rgba(9,46,121,0.8) 0%, rgba(203,0,255,0.8) 100%)`,
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}>
        <Box
          sx={{
            background: "rgba(255,255,255,0.5)",
            backdropFilter: "blur(10px)",
            height: "75%",
            width: "90%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: "1rem",
            padding: "5rem"
          }}>
          <Typography
            sx={{
              fontSize: "3rem",
              fontWeight: "bold",
              color: "white"
            }}>
            {appTitle}
            &nbsp;
            <span
              style={{
                color: palette.purple.dark
              }}>
              {intl.formatMessage({ id: "portal" })}.
            </span>
          </Typography>
        </Box>
      </Grid>
      <Grid
        item
        md={6}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}>
        <LoginForm />
      </Grid>
    </Grid>
  )
}

export default Login
