"use client"
import { useIntl } from "react-intl"
import { palette } from "@/components/utils/theme"
import { Box, Grid, Typography } from "@mui/material"
import SignupForm from "@/components/page/Form/SignupForm"
import { useEffect } from "react"
import { useDispatch } from "react-redux"
import SignupLinkAction from "@/stores/SignupLink/Action"
import { useRouter, useSearchParams } from "next/navigation"
import SelfSignupForm from "@/components/page/Form/SelfSignupForm"
import { waveBackground } from "@/components/utils/PatternHelper"

const Signup = () => {
  const intl = useIntl()
  const dispatch = useDispatch()
  const searchParams = useSearchParams()
  const ref = searchParams.get("ref")
  const appTitle = process.env.NEXT_PUBLIC_APP_TITLE

  const router = useRouter()

  useEffect(() => {
    if (ref) {
      dispatch(
        SignupLinkAction.validateSignupLinkToken(ref, () => {
          router.push("/auth/login")
        })
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Grid
      container
      sx={{
        minHeight: "100vh",
        width: "100vw",
        display: "flex"
      }}>
      <Grid
        item
        md={6}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}>
        {ref ? <SignupForm token={ref} /> : <SelfSignupForm />}
      </Grid>
      <Grid
        item
        style={{ height: "100vh", position: "fixed", left: "50%", width: "100%" }}
        md={6}
        sx={{
          background: `${waveBackground},
          linear-gradient(335deg, rgba(0,2,36,0.8) 0%, rgba(9,46,121,0.8) 0%, rgba(203,0,255,0.8) 100%)`,
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}>
        <Box
          sx={{
            background: "rgba(255,255,255,0.5)",
            backdropFilter: "blur(10px)",
            height: "75%",
            width: "90%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: "1rem",
            padding: "5rem"
          }}>
          <Typography
            sx={{
              fontSize: "3rem",
              fontWeight: "bold",
              color: "white"
            }}>
            {appTitle}
            &nbsp;
            <span
              style={{
                color: palette.purple.dark
              }}>
              {intl.formatMessage({ id: "portal" })}.
            </span>
          </Typography>
        </Box>
      </Grid>
    </Grid>
  )
}

export default Signup
