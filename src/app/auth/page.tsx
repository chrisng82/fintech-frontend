"use client"

import AuthBase from "@/components/utils/AutoNavigate"

export default function Auth() {
  return <AuthBase navigateTo="/auth/login" />
}
