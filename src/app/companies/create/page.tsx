"use client"
import AddCompanyForm from "@/components/page/Form/AddCompanyForm"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { Typography } from "@mui/material"
import { useIntl } from "react-intl"
import BusinessIcon from "@mui/icons-material/Business"
import { motion } from "framer-motion"
import { useRouter } from "next/navigation"
import GreyButton from "@/components/Base/GreyButton"
import Link from "next/link"

export default function CompaniesCreate() {
  const intl = useIntl()
  const router = useRouter()

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({ id: "create_company" })}
          </Typography>
        </motion.div>
      }>
      <Link href="/companies">
        <GreyButton
          variant="contained"
          sx={{
            mb: 3
          }}>
          {intl.formatMessage({ id: "back" })}
        </GreyButton>
      </Link>
      <AddCompanyForm />
    </HeaderedContainer>
  )
}
