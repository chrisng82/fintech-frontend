"use client"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { CircularProgress, Typography } from "@mui/material"
import { useEffect } from "react"
import BusinessIcon from "@mui/icons-material/Business"
import { useDispatch, useSelector } from "react-redux"
import CompanyAction from "@/stores/Company/Action"
import { RootState } from "@/stores"
import ViewCompanyContainer, {
  SelectedTabEnum
} from "@/components/page/Companies/ViewCompany/ViewCompanyContainer"
import { motion } from "framer-motion"

interface Props {
  params: {
    code: string
    section: SelectedTabEnum
  }
}

export default function ViewCompany({ params: { code, section } }: Props) {
  const dispatch = useDispatch()
  const { showCompany } = useSelector((state: RootState) => state.company)

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {showCompany.isLoading ? (
              <CircularProgress
                sx={{
                  color: "#fff"
                }}
              />
            ) : (
              showCompany.data?.name_01
            )}
          </Typography>
        </motion.div>
      }>
      <ViewCompanyContainer selectedTab={section} />
    </HeaderedContainer>
  )
}
