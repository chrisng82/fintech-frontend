"use client"

import HeaderedContainer from "@/components/page/HeaderedContainer"
import { Typography } from "@mui/material"
import { motion } from "framer-motion"
import BusinessIcon from "@mui/icons-material/Business"
import { useIntl } from "react-intl"
import CreateEmailTask from "@/components/page/Companies/ViewCompany/EmailTask/CreateEmailTask"
import { useEffect } from "react"
import { useRouter } from "next/navigation"
import { SelectedTabEnum } from "@/components/page/Companies/ViewCompany/ViewCompanyContainer"

export default function EmailTaskView({
  params: { code, section }
}: {
  params: { code: string; section: SelectedTabEnum }
}) {
  const intl = useIntl()
  const router = useRouter()

  useEffect(() => {
    if (section !== SelectedTabEnum.EMAIL_TASKS) {
      router.push(`/companies/${code}/${SelectedTabEnum.EMAIL_TASKS}`)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (section !== SelectedTabEnum.EMAIL_TASKS) return null

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({ id: "create_email_task" })}
          </Typography>
        </motion.div>
      }>
      <CreateEmailTask companyCode={code} />
    </HeaderedContainer>
  )
}
