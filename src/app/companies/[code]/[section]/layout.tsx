"use client"

import { SelectedTabEnum } from "@/components/page/Companies/ViewCompany/ViewCompanyContainer"
import { useIsRouteAccessible } from "@/components/utils/RouteHelper"
import CompanyAction from "@/stores/Company/Action"
import { usePathname, useRouter } from "next/navigation"
import { useEffect } from "react"
import { useDispatch } from "react-redux"

export default function CompanySettingsSection({
  params: { code, section },
  children
}: {
  params: { code: string; section: SelectedTabEnum }
  children: React.ReactNode
}) {
  const router = useRouter()
  const pathname = usePathname()
  const dispatch = useDispatch()

  const isRouteAccessible = useIsRouteAccessible(pathname)
  useEffect(() => {
    if (!Object.values(SelectedTabEnum).includes(section) || !isRouteAccessible) {
      router.push(`/companies/${code}/${SelectedTabEnum.SUMMARY}`)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    dispatch(CompanyAction.showCompany(code))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return children
}
