"use client"

import HeaderedContainer from "@/components/page/HeaderedContainer"
import { RootState } from "@/stores"
import { CircularProgress, Typography } from "@mui/material"
import { motion } from "framer-motion"
import { useDispatch, useSelector } from "react-redux"
import BusinessIcon from "@mui/icons-material/Business"
import { useEffect } from "react"
import CompanyAction from "@/stores/Company/Action"
import ViewEmailTask from "@/components/page/Companies/ViewCompany/EmailTask/ViewEmailTask"
import { SelectedTabEnum } from "@/components/page/Companies/ViewCompany/ViewCompanyContainer"
import { useRouter } from "next/navigation"

export default function EmailTaskView({
  params: { code, section, emailTaskCode }
}: {
  params: { code: string; section: SelectedTabEnum; emailTaskCode: string }
}) {
  const router = useRouter()
  const dispatch = useDispatch()

  const { showCompany } = useSelector((state: RootState) => state.company)

  useEffect(() => {
    if (section !== SelectedTabEnum.EMAIL_TASKS) {
      router.push(`/companies/${code}/${SelectedTabEnum.EMAIL_TASKS}`)
    } else {
      dispatch(CompanyAction.showCompany(code))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {showCompany.isLoading ? (
              <CircularProgress
                sx={{
                  color: "#fff"
                }}
              />
            ) : (
              showCompany.data?.name_01
            )}
          </Typography>
        </motion.div>
      }>
      <ViewEmailTask companyCode={code} emailTaskCode={emailTaskCode} />
    </HeaderedContainer>
  )
}
