"use client"

import { generateTypeEnum } from "@/components/page/Companies/ViewCompany/SignupLinksTab/SignupLinksHeader"
import AutoNavigate from "@/components/utils/AutoNavigate"
import { useParams } from "next/navigation"

export default function CompanyCode() {
  const { code, section } = useParams()
  return (
    <AutoNavigate
      navigateTo={`/companies/${code}/${section}/generate/${generateTypeEnum.SINGLE}`}
    />
  )
}
