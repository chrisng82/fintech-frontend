"use client"

import HeaderedContainer from "@/components/page/HeaderedContainer"
import { Typography } from "@mui/material"
import { motion } from "framer-motion"
import BusinessIcon from "@mui/icons-material/Business"
import { useIntl } from "react-intl"
import { useEffect } from "react"
import { useRouter } from "next/navigation"
import { SelectedTabEnum } from "@/components/page/Companies/ViewCompany/ViewCompanyContainer"
import { generateTypeEnum } from "@/components/page/Companies/ViewCompany/SignupLinksTab/SignupLinksHeader"
import CreateSignupLink from "@/components/page/Companies/ViewCompany/SignupLinksTab/CreateSignupLink"

export default function EmailTaskView({
  params: { code, section, type }
}: {
  params: { code: string; section: SelectedTabEnum; type: generateTypeEnum }
}) {
  const intl = useIntl()
  const router = useRouter()

  useEffect(() => {
    if (section !== SelectedTabEnum.SIGNUP_LINKS) {
      router.push(`/companies/${code}/${SelectedTabEnum.SIGNUP_LINKS}`)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (section !== SelectedTabEnum.SIGNUP_LINKS) return null

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {intl.formatMessage({
              id:
                type === generateTypeEnum.SINGLE
                  ? "generate_signup_link"
                  : "generate_batch_signup_links"
            })}
          </Typography>
        </motion.div>
      }>
      <CreateSignupLink companyCode={code} type={type} />
    </HeaderedContainer>
  )
}
