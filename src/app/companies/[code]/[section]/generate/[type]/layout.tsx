"use client"

import { generateTypeEnum } from "@/components/page/Companies/ViewCompany/SignupLinksTab/SignupLinksHeader"
import { SelectedTabEnum } from "@/components/page/Companies/ViewCompany/ViewCompanyContainer"
import { useRouter } from "next/navigation"
import { useEffect } from "react"

export default function CompanySettingsSection({
  params: { code, type },
  children
}: {
  params: { code: string; type: generateTypeEnum }
  children: React.ReactNode
}) {
  const router = useRouter()

  useEffect(() => {
    if (!Object.values(generateTypeEnum).includes(type)) {
      router.push(
        `/companies/${code}/${SelectedTabEnum.SIGNUP_LINKS}/generate/${generateTypeEnum.SINGLE}`
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return children
}
