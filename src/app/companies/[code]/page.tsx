"use client"

import { SelectedTabEnum } from "@/components/page/Companies/ViewCompany/ViewCompanyContainer"
import AutoNavigate from "@/components/utils/AutoNavigate"
import { useParams } from "next/navigation"

export default function CompanyCode() {
  const { code } = useParams()
  return <AutoNavigate navigateTo={`/companies/${code}/${SelectedTabEnum.SUMMARY}`} />
}
