"use client"
import { Grid, Skeleton } from "@mui/material"
import React, { ReactElement, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import CompanyAction from "@/stores/Company/Action"
import { RootState } from "@/stores"
import CompanyCard from "@/components/page/Companies/CompanyCard"
import CompanyHeader from "@/components/page/Companies/CompanyHeader"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import useAuth from "@/components/hooks/useAuth"
import { useRouter } from "next/navigation"

const Companies: React.FC = (): ReactElement => {
  const dispatch = useDispatch()
  const router = useRouter()

  const { data, isLoading } = useSelector((state: RootState) => state.company.companyList)
  const { isSuperAdmin, isCompanyAdmin, isCompanyStaff, isEfichainAdmin, isEfichainProduct } =
    useAuth()

  useEffect(() => {
    if (
      !isSuperAdmin &&
      !isCompanyAdmin &&
      !isCompanyStaff &&
      !isEfichainAdmin &&
      !isEfichainProduct
    ) {
      router.push("/not-found")
    }

    dispatch(CompanyAction.fetchCompanyList())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <HeaderedContainer header={<CompanyHeader />}>
      <Grid container spacing={2} sx={{ display: "flex", flexWrap: "wrap" }}>
        {isLoading
          ? [1, 2, 3, 4, 5, 6].map((i) => (
              <Grid key={`skeleton-${i}`} item xs={12} md={6} lg={4}>
                <Skeleton key={i} variant="rounded" width="100%" height={200} animation="wave" />
              </Grid>
            ))
          : data.map((company, index) => (
              <Grid
                key={company.code}
                item
                xs={12}
                md={6}
                lg={4}
                sx={{
                  display: "flex",
                  flexDirection: "column"
                }}>
                <CompanyCard company={company} index={index} />
              </Grid>
            ))}
      </Grid>
    </HeaderedContainer>
  )
}

export default Companies
