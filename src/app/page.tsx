"use client"

import GetStartedSection from "@/components/page/Dashboard/GetStartedSection"
import UserInfoSection from "@/components/page/Dashboard/UserInfoSection"
import HeaderedContainer from "@/components/page/HeaderedContainer"

export default function Home() {
  return (
    <HeaderedContainer header={<UserInfoSection />}>
      <GetStartedSection />
    </HeaderedContainer>
  )
}
