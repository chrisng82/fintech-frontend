"use client"

import {
  Box,
  Card,
  CardContent,
  CircularProgress,
  FormControlLabel,
  Grid,
  Skeleton,
  Typography,
  useMediaQuery,
  useTheme
} from "@mui/material"
import React, { ReactElement, useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import DebtorHeader from "@/components/page/Debtors/DebtorHeader"
import DebtorCard from "@/components/page/Debtors/DebtorCard"
import DebtorAction from "@/stores/Debtor/Action"
import { useUpdateEffect } from "usehooks-ts"
import { Debtor } from "@/stores/Debtor/Types"
import {
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
  Grid as VirtualizedGrid,
  WindowScroller
} from "react-virtualized"
import "react-virtualized/styles.css"
import { motion } from "framer-motion"
import { useRouter } from "next/navigation"
import IOSSwitch from "@/components/Base/Switch"
import { useIntl } from "react-intl"
import DebtorContentFilter from "@/components/page/Debtors/DebtorContentFilter"
import NoDataSVG from "@/assets/SVG/NoDataSVG"
import Link from "next/link"
import useAuth from "@/components/hooks/useAuth"

const cache = new CellMeasurerCache({
  fixedWidth: true,
  defaultHeight: 100
})

const DebtorsPage: React.FC = (): ReactElement => {
  const intl = useIntl()
  const router = useRouter()
  const dispatch = useDispatch()

  const { data, per_page, current_page, last_page, isLoading } = useSelector(
    (state: RootState) => state.debtor.list
  )

  const { user } = useSelector((state: RootState) => state.app)
  const { isSuperAdmin, isCompanyAdmin, isCompanyStaff, isEfichainAdmin, isEfichainProduct } =
    useAuth()

  const [debtors, setDebtors] = useState<Debtor[]>([])
  const [isOpenBill, setIsOpenBill] = useState<boolean>(true)
  const [isFetching, setIsFetching] = useState<boolean>(false)

  const theme = useTheme()
  const isMd = useMediaQuery(theme.breakpoints.between("md", "lg"))
  const isLg = useMediaQuery(theme.breakpoints.up("lg"))

  const getDivisor = () => {
    if (isLg) return 3
    else if (isMd) return 2
    return 1
  }

  const getItemsPerPage = () => {
    const divisor = getDivisor()

    const remainder = per_page % divisor
    if (remainder === 0) {
      return per_page
    }
    return per_page + divisor - remainder
  }

  const itemsPerPage = getItemsPerPage()

  useEffect(() => {
    if (
      !isSuperAdmin &&
      !isCompanyAdmin &&
      !isCompanyStaff &&
      !isEfichainAdmin &&
      !isEfichainProduct
    ) {
      router.push("/not-found")
    }

    dispatch(DebtorAction.fetchDebtors(current_page, itemsPerPage))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useUpdateEffect(() => {
    setDebtors([])
    dispatch(DebtorAction.fetchDebtors(current_page, itemsPerPage))
  }, [user?.companyCode])

  useUpdateEffect(() => {
    if (current_page === 1) {
      window.scrollTo(0, 0)
      setDebtors(data)
    } else {
      setDebtors((prev) => [...prev, ...data])
    }
  }, [data, current_page])

  useUpdateEffect(() => {
    const handleScroll = () => {
      if (
        window.innerHeight + document.documentElement.scrollTop !==
          document.documentElement.offsetHeight ||
        isLoading
      )
        return

      if (current_page === last_page) return

      if (isLoading) return

      dispatch(DebtorAction.fetchDebtors(current_page + 1, itemsPerPage))
    }

    window.addEventListener("scroll", handleScroll)
    return () => window.removeEventListener("scroll", handleScroll)
  }, [isLoading, current_page, last_page, itemsPerPage])

  const cellRenderer = ({
    columnIndex,
    key,
    rowIndex,
    parent,
    style
  }: {
    columnIndex: number
    key: string
    rowIndex: number
    parent: any
    style: React.CSSProperties
  }) => {
    const index = rowIndex * getDivisor() + columnIndex
    if (index < debtors.length) {
      return (
        <CellMeasurer
          key={key}
          cache={cache}
          columnIndex={columnIndex}
          parent={parent}
          rowIndex={rowIndex}>
          {({ measure, registerChild }) => (
            <motion.div
              ref={(element) => {
                if (element && registerChild) {
                  registerChild(element)
                }
              }}
              key={key}
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              transition={{ duration: 0.5 }}
              style={{
                ...style,
                padding: "0.5rem 0",
                boxSizing: "border-box"
              }}>
              <Link href={"/debtors/" + encodeURIComponent(debtors[index].code)}>
                <Box
                  sx={{
                    margin:
                      (index + 1) % getDivisor() === 0
                        ? "0 0.55rem 0.5rem 0.75rem"
                        : "0 0.25rem 0.5rem 0.75rem",
                    height: "100%"
                  }}>
                  <DebtorCard debtor={debtors[index]} onLoad={measure} isOpenBill={isOpenBill} />
                </Box>
              </Link>
            </motion.div>
          )}
        </CellMeasurer>
      )
    }
  }

  return (
    <HeaderedContainer header={<DebtorHeader />}>
      <DebtorContentFilter
        isOpenBill={isOpenBill}
        setIsOpenBill={setIsOpenBill}
        setIsFetching={setIsFetching}
      />
      {(isLoading && debtors.length === 0) || isFetching ? (
        <Grid container spacing={2} sx={{ display: "flex", flexWrap: "wrap" }}>
          {[1, 2, 3, 4, 5, 6].map((i) => (
            <Grid key={`skeleton-${i}`} item xs={12} md={6} lg={4}>
              <Skeleton key={i} variant="rounded" width="100%" height={200} animation="wave" />
            </Grid>
          ))}
        </Grid>
      ) : debtors.length > 0 ? (
        <WindowScroller>
          {({ height, isScrolling, onChildScroll, scrollTop }) => (
            <AutoSizer disableHeight>
              {({ width }) => (
                <VirtualizedGrid
                  autoHeight
                  height={height}
                  width={width}
                  columnWidth={width / getDivisor()}
                  columnCount={getDivisor()}
                  rowHeight={cache.rowHeight} // Set to the height of your items
                  rowCount={Math.ceil(debtors.length / getDivisor())}
                  cellRenderer={cellRenderer}
                  isScrolling={isScrolling}
                  onScroll={onChildScroll}
                  scrollTop={scrollTop}
                />
              )}
            </AutoSizer>
          )}
        </WindowScroller>
      ) : (
        <Box
          sx={{
            padding: "5rem 20rem"
          }}>
          <NoDataSVG />
        </Box>
      )}
      {isLoading && debtors.length > 0 && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 2
          }}>
          <CircularProgress />
        </Box>
      )}
    </HeaderedContainer>
  )
}

export default DebtorsPage
