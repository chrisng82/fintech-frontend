"use client"

import { SelectedTabEnum } from "@/components/page/Debtors/ViewDebtor"
import AutoNavigate from "@/components/utils/AutoNavigate"

export default function CompanyCode({ params: { code } }: { params: { code: string } }) {
  return <AutoNavigate navigateTo={`/debtors/${code}/${SelectedTabEnum.SUMMARY}`} />
}
