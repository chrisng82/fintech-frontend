"use client"

import { SelectedTabEnum } from "@/components/page/Debtors/ViewDebtor"
import { useRouter } from "next/navigation"
import { useEffect } from "react"

export default function CompanySettingsSection({
  params: { code, section },
  children
}: {
  params: { code: string; section: SelectedTabEnum }
  children: React.ReactNode
}) {
  const router = useRouter()

  useEffect(() => {
    if (!Object.values(SelectedTabEnum).includes(section)) {
      router.push(`/companies/${code}/${SelectedTabEnum.SUMMARY}`)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return children
}
