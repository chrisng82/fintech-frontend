"use client"
import HeaderedContainer from "@/components/page/HeaderedContainer"
import { CircularProgress, Typography } from "@mui/material"
import { useEffect } from "react"
import BusinessIcon from "@mui/icons-material/Business"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/stores"
import { motion } from "framer-motion"
import ViewDebtor, { SelectedTabEnum } from "@/components/page/Debtors/ViewDebtor"
import DebtorAction from "@/stores/Debtor/Action"

interface Props {
  params: {
    code: string
    section: SelectedTabEnum
  }
}

export default function ViewCompany({ params: { code, section } }: Props) {
  const dispatch = useDispatch()

  const { item } = useSelector((state: RootState) => state.debtor)

  useEffect(() => {
    dispatch(DebtorAction.fetchDebtor(code))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <HeaderedContainer
      header={
        <motion.div
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.5 }}>
          <Typography
            sx={{
              fontSize: "2rem",
              display: "flex",
              gap: "0.5rem",
              alignItems: "center",
              color: "#fff"
            }}>
            <BusinessIcon
              sx={{
                fontSize: "2rem"
              }}
            />
            {item.isLoading ? (
              <CircularProgress
                sx={{
                  color: "#fff"
                }}
              />
            ) : (
              item.data?.name_01
            )}
          </Typography>
        </motion.div>
      }>
      <ViewDebtor selectedTab={section} />
    </HeaderedContainer>
  )
}
