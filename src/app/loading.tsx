import LoadingScreen from "@/components/page/LoadingScreen"

export default function Loading() {
  return <LoadingScreen />
}
