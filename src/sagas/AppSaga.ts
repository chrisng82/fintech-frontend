import { call, select, takeLatest } from "typed-redux-saga"
import { put } from "redux-saga/effects"
import AppAction, { AppTypes } from "../stores/App/Action"
import SnackbarAction from "../stores/Snackbar/Action"
import { RootState } from "@/stores"
import ApiService, { Result } from "./ApiService"
import {
  Authenticate,
  ChangeCompanySelect,
  Logout,
  ResetPassword,
  SendResetPasswordLink,
  ValidateResetPasswordToken,
  AppGetCompanyTnc,
  SetUserTncStatus
} from "@/stores/App/Types"
import UserType from "@/stores/User/Types"
import CompanyAction from "@/stores/Company/Action"
import DebtorAction from "@/stores/Debtor/Action"
import ResStatusEnum, { ResStatusValues } from "@/constants/ResStatus"

const getAppStore = (state: RootState) => state.app

export function* startup() {
  const app = yield* select(getAppStore)

  if (process.env.NEXT_PUBLIC_API_URL && process.env.NEXT_PUBLIC_API_URL !== "") {
    yield put(AppAction.updateApiUrl(process.env.NEXT_PUBLIC_API_URL))
  }

  if (process.env.NEXT_PUBLIC_APP_PATH && process.env.NEXT_PUBLIC_APP_PATH !== "") {
    yield put(AppAction.updateAppPath(process.env.NEXT_PUBLIC_APP_PATH))
  }

  if (app.apiUrl == null || app.apiUrl === "") {
    const fullUrl = window.location.href

    yield put(SnackbarAction.openSnackbar("API URL is not set!", "error"))

    window.location.href = fullUrl
  }

  try {
    const result: Result = yield call(ApiService.getApi, app.apiUrl, "env-settings", app.token)

    if (result.isSuccess === true) {
      yield put(AppAction.setEnvSettings(result.data))
    } else {
      yield put(
        SnackbarAction.openSnackbar(
          "Something went wrong when trying to fetch env settings. Please refresh the page.",
          "error"
        )
      )
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar(
        "Something went wrong when trying to fetch env settings. Please refresh the page.",
        "error"
      )
    )
  }

  if (app.user?.token) {
    yield put(CompanyAction.fetchCompanySelect())
  }
}

export function* authenticate({ user, setSubmitting, onSuccess }: Authenticate) {
  setSubmitting(true)
  yield put(AppAction.setAppLoading(true))

  try {
    const app = yield* select(getAppStore)

    const postData = {
      email: user.email,
      password: user.password,
      token_days: 1,
      old_token: app.token
    }

    const result: Result = yield call(
      ApiService.postApi,
      app.apiUrl,
      "login/authenticate",
      app.token,
      postData
    )

    if (result.isSuccess === true) {
      yield put(
        AppAction.authenticateSuccess(result.data.token, {
          ...result.data,
          contacts: result.data.multipleContacts,
          debtors: result.data.debtors
        })
      )

      yield put(CompanyAction.fetchCompanySelect())

      if (result.data.type_str === UserType.DEBTOR) {
        // yield put(InvoiceAction.setSearchFields("searchDebtorCode", result.data.debtor?.code ?? ""))
        // yield put(
        //   InvoiceAction.setSearchFields("searchDebtorName", result.data.debtor?.name_01 ?? "")
        // )

        // yield put(AppAction.appGetCompanyTnc(result.data))

        if (result.data.debtors.length > 0) {
          yield put(DebtorAction.setDebtor(null))
          yield put(AppAction.setSelectedDebtorId(result.data.debtors[0].id))
        }
      }

      yield put(SnackbarAction.openSnackbar("Login success", "success"))
      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Something went wrong when trying to login.", "error"))
  } finally {
    yield put(AppAction.setAppLoading(false))
    setSubmitting(false)
  }
}

export function* logout({ onSuccess }: Logout) {
  try {
    yield put(AppAction.setAppLoading(true))
    const app = yield* select(getAppStore)

    const result: Result = yield call(ApiService.postApi, app.apiUrl, "logout", app.token)

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar("Logout successful!", "success"))
      yield put(AppAction.logoutSuccess())
      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Logout unsuccessful!", "error"))
    }
  } catch (error) {
    SnackbarAction.openSnackbar("Logout unsuccessful!", "error")
  } finally {
    yield put(AppAction.setAppLoading(false))
  }
}

export function* tokenExpired() {
  yield put(AppAction.setToken(null))
  yield put(SnackbarAction.openSnackbar("Token Expired", "error"))
}

export function* changeCompanySelect({ companyCode, user }: ChangeCompanySelect) {
  try {
    yield put(CompanyAction.setCompanySelectLoading(true))
    const app = yield* select(getAppStore)

    const postData = {
      code: companyCode,
      data: user
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      "company/changeCompany",
      app.token,
      postData
    )

    if (result.isSuccess === true) {
      yield put(AppAction.changeCompanySelectSuccess(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to change company!", "error"))
    }
  } catch (error) {
    yield put(CompanyAction.setCompanySelectLoading(false))
  } finally {
    yield put(CompanyAction.setCompanySelectLoading(false))
  }
}

export function* sendResetPasswordLink({
  values,
  setSubmitting,
  onSuccess
}: SendResetPasswordLink) {
  try {
    setSubmitting(true)

    const app = yield* select(getAppStore)

    const postData = {
      email: values.email
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      "send-reset-password",
      app.token,
      postData
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      setSubmitting(false)
      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Failed to send password reset link!", "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* validateResetPasswordToken({
  token,
  onSuccess,
  onFail
}: ValidateResetPasswordToken) {
  yield put(AppAction.setIsValidatingResetToken(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `validate-reset-password-token`, // Route
      app.token,
      { token }
    )

    if (result.isSuccess === true) {
      const data = result.data

      yield put(AppAction.setResetPasswordUser(data))
      onSuccess()
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
      onFail()
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(AppAction.setIsValidatingResetToken(false))
  }
}

export function* resetPassword({ values, setSubmitting, onSuccess }: ResetPassword) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `reset-password`, // Route
      app.token,
      { ...values, user_id: app.resetPassword.user?.user_id, token: app.resetPassword.user?.token }
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(AppAction.setResetPasswordUser(null))
      yield put(AppAction.setIsValidatingResetToken(false))
      onSuccess()
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* appGetCompanyTnc({ user }: AppGetCompanyTnc) {
  try {
    yield put(CompanyAction.setShowCompanyLoading(true))

    const app = yield* select(getAppStore)

    const getData = {
      userId: user.id,
      companyId: user.companyId
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `company/tnc/user/validate`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { user_tnc } = result.data
      if (user_tnc.status !== ResStatusValues[ResStatusEnum.ACTIVE]) {
        yield put(AppAction.appSetCompanyTnc(result.data))
        yield put(AppAction.setOpenTnc(true))
      } else {
        yield put(AppAction.appSetCompanyTnc(result.data))
      }
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setShowCompanyLoading(false))
  }
}

export function* setUserTncStatus({ companyTnc }: SetUserTncStatus) {
  yield put(CompanyAction.setShowCompanyLoading(true))

  try {
    const app = yield* select(getAppStore)

    const putData = {
      companyTnc
    }

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `company/tnc/setUserTncStatus`, // Route
      app.token, // Token
      putData
    )

    if (result.isSuccess === true) {
      yield put(AppAction.appSetCompanyTnc(result.data))
      yield put(AppAction.setOpenTnc(false))
    } else {
      console.log(result)
      yield put(SnackbarAction.openSnackbar("Error on updating TnC", "error"))
      yield put(AppAction.setOpenTnc(true))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setShowCompanyLoading(false))
  }
}

export const appSaga = [
  takeLatest(AppTypes.STARTUP, startup),
  takeLatest(AppTypes.AUTHENTICATE, authenticate),
  takeLatest(AppTypes.LOGOUT, logout),
  takeLatest(AppTypes.TOKEN_EXPIRED, tokenExpired),
  takeLatest(AppTypes.CHANGE_COMPANY_SELECT, changeCompanySelect),
  takeLatest(AppTypes.SEND_RESET_PASSWORD_LINK, sendResetPasswordLink),
  takeLatest(AppTypes.VALIDATE_RESET_PASSWORD_TOKEN, validateResetPasswordToken),
  takeLatest(AppTypes.RESET_PASSWORD, resetPassword),
  takeLatest(AppTypes.APP_GET_COMPANY_TNC, appGetCompanyTnc),
  takeLatest(AppTypes.SET_USER_TNC_STATUS, setUserTncStatus)
]
