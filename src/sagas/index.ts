import { all } from "redux-saga/effects"
import { appSaga } from "./AppSaga"
import { companySaga } from "./CompanySaga"
import { companyEmailTaskSaga } from "./CompanyEmailTaskSaga"
import { signupLinkSaga } from "./SignupLinkSaga"
import { debtorSaga } from "./DebtorSaga"
import { invoiceSaga } from "./InvoiceSaga"
import { creditNoteSaga } from "./CreditNoteSaga"
import { debitNoteSaga } from "./DebitNoteSaga"
import { debtorPaymentSaga } from "./DebtorPaymentSaga"
import { ftpSyncSaga } from "./FtpSyncSaga"
import { eficoreSyncSaga } from "./EficoreSyncSaga"
import { docAttachmentSaga } from "./DocAttachmentSaga"
import { monthlyStatementSaga } from "./MonthlyStatementSaga"
import { userSaga } from "./UserSaga"

export default function* root() {
  yield all([
    ...appSaga,
    ...companySaga,
    ...companyEmailTaskSaga,
    ...signupLinkSaga,
    ...debtorSaga,
    ...invoiceSaga,
    ...creditNoteSaga,
    ...debitNoteSaga,
    ...debtorPaymentSaga,
    ...ftpSyncSaga,
    ...eficoreSyncSaga,
    ...docAttachmentSaga,
    ...monthlyStatementSaga,
    ...userSaga
  ])
}
