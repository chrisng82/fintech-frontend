import { RootState } from "@/stores"
import { call, put, select, takeLatest } from "typed-redux-saga"
import AppAction from "@/stores/App/Action"
import CompanyAction, { CompanyTypes } from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import ApiService from "./ApiService"
import {
  CompanyEmailTask,
  CreateCompanyEmailTask,
  DeleteCompanyEmailTask,
  FetchCompanyEmailTaskByCompanyCodeAndTaskCode,
  ToggleCompanyEmailTask,
  UpdateCompanyEmailTask
} from "@/stores/CompanyEmailTask/Types"
import CompanyEmailTaskAction, { CompanyEmailTaskTypes } from "@/stores/CompanyEmailTask/Action"

const getAppStore = (state: RootState) => state.app

export function* fetchCompanyEmailTaskByCompanyCodeAndTaskCode({
  companyCode,
  taskCode
}: FetchCompanyEmailTaskByCompanyCodeAndTaskCode) {
  yield* put(CompanyEmailTaskAction.setCompanyEmailTaskLoading(true))
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `companyEmailTask/showByCompanyCodeAndTaskCode/${companyCode}/${taskCode}`, // Route
      app.token, // Token
      {}
    )

    if (result.isSuccess === true) {
      yield put(CompanyEmailTaskAction.setCompanyEmailTask(result.data.model))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield* put(CompanyEmailTaskAction.setCompanyEmailTaskLoading(false))
  }
}

export function* createCompanyEmailTask({
  setSubmitting,
  data,
  onSuccess
}: CreateCompanyEmailTask) {
  setSubmitting(true)
  yield* put(CompanyEmailTaskAction.setCompanyEmailTaskLoading(true))
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `companyEmailTask/create`, // Route
      app.token, // Token
      data
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))

      const { email_tasks, ...rest } = result.data.model

      yield put(CompanyAction.setShowCompany(rest))
      yield put(CompanyEmailTaskAction.setCompanyEmailTasks(email_tasks))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
    yield* put(CompanyEmailTaskAction.setCompanyEmailTaskLoading(false))
  }
}

export function* updateCompanyEmailTask({
  setSubmitting,
  id,
  data,
  onSuccess
}: UpdateCompanyEmailTask) {
  setSubmitting(true)
  yield* put(CompanyEmailTaskAction.setCompanyEmailTaskLoading(true))
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `companyEmailTask/update/${id}`, // Route
      app.token, // Token
      data
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))

      const { email_tasks, ...rest } = result.data.model

      yield put(CompanyAction.setShowCompany(rest))
      yield put(CompanyEmailTaskAction.setCompanyEmailTasks(email_tasks))
      yield put(
        CompanyEmailTaskAction.setCompanyEmailTask(
          email_tasks.find((e: CompanyEmailTask) => e.id === id)
        )
      )

      onSuccess(data)
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
    yield* put(CompanyEmailTaskAction.setCompanyEmailTaskLoading(false))
  }
}

export function* deleteCompanyEmailTask({ id, onSuccess }: DeleteCompanyEmailTask) {
  yield* put(CompanyEmailTaskAction.setCompanyEmailTaskListLoading(true))
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.deleteApi,
      app.apiUrl,
      `companyEmailTask/delete/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))

      const { email_tasks, ...rest } = result.data.model

      yield put(CompanyAction.setShowCompany(rest))
      yield put(CompanyEmailTaskAction.setCompanyEmailTasks(email_tasks))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  }
}

export function* toggleCompanyEmailTask({ id }: ToggleCompanyEmailTask) {
  yield put(CompanyAction.setShowCompanyLoading(true))
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `companyEmailTask/toggleStatus/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      const { email_tasks, ...rest } = result.data.model

      yield put(CompanyAction.setShowCompany(rest))
      yield put(CompanyEmailTaskAction.setCompanyEmailTasks(email_tasks))
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setShowCompanyLoading(false))
  }
}

export const companyEmailTaskSaga = [
  takeLatest(
    CompanyEmailTaskTypes.FETCH_COMPANY_EMAIL_TASK_BY_COMPANY_CODE_AND_TASK_CODE,
    fetchCompanyEmailTaskByCompanyCodeAndTaskCode
  ),
  takeLatest(CompanyEmailTaskTypes.TOGGLE_COMPANY_EMAIL_TASK, toggleCompanyEmailTask),
  takeLatest(CompanyEmailTaskTypes.CREATE_COMPANY_EMAIL_TASK, createCompanyEmailTask),
  takeLatest(CompanyEmailTaskTypes.UPDATE_COMPANY_EMAIL_TASK, updateCompanyEmailTask),
  takeLatest(CompanyEmailTaskTypes.DELETE_COMPANY_EMAIL_TASK, deleteCompanyEmailTask)
]
