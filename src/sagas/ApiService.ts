// eslint-disable-next-line import/no-anonymous-default-export
import { getURLParams } from "@/components/utils/MapHelper"
import axios, { AxiosResponse } from "axios"

export interface Result {
  status?: number
  newToken?: any
  isSuccess?: boolean
  isTokenExpired?: boolean
  isPasswordExpired?: boolean
  headers?: any
  data?: any
  message?: any
}

const getApi = async (
  url: string,
  api: string,
  token: string | null,
  params: any = {},
  contentType = "application/json"
) => {
  const headers: { [key: string]: string } = getHeaders(contentType, token)

  try {
    const response = await axios.get(`${url}/${api}`, {
      params,
      headers,
      validateStatus: () => true
    })

    return handleApiResponse(response)
  } catch (error: unknown) {
    return handleApiError(error)
  }
}

const postApi = async (
  url: string,
  api: string,
  token: string | null,
  body: any = {},
  params: any = {},
  contentType = "application/json"
) => {
  const headers: { [key: string]: string } = getHeaders(contentType, token)

  try {
    const response = await axios.post(`${url}/${api}`, body, {
      params: getURLParams(params).toString(),
      headers,
      validateStatus: () => true
    })

    return handleApiResponse(response)
  } catch (error: unknown) {
    return handleApiError(error)
  }
}

const putApi = async (
  url: string,
  api: string,
  token: string | null,
  body: any = {},
  params: any = {},
  contentType = "application/json"
) => {
  const headers = getHeaders(contentType, token)

  try {
    const response = await axios.put(`${url}/${api}`, body, {
      params: getURLParams(params).toString(),
      headers,
      validateStatus: () => true
    })

    return handleApiResponse(response)
  } catch (error: unknown) {
    return handleApiError(error)
  }
}

const deleteApi = async (
  url: string,
  api: string,
  token: string | null,
  params: any = {},
  contentType = "application/json"
) => {
  const headers = getHeaders(contentType, token)

  try {
    const response = await axios.delete(`${url}/${api}`, {
      params: getURLParams(params).toString(),
      headers,
      validateStatus: () => true
    })

    return handleApiResponse(response)
  } catch (error: unknown) {
    return handleApiError(error)
  }
}

const downloadGetApi = async (
  url: string,
  api: string,
  token: string | null,
  getData: any = {},
  contentType: string = "application/octet-stream"
) => {
  try {
    const headers = getHeaders(contentType, token)

    const response = await axios.get(`${url}/${api}`, {
      params: getData,
      headers,
      responseType: "blob",
      validateStatus: () => true
    })

    return handleDownloadApiResponse(response)
  } catch (error: unknown) {
    return handleApiError(error)
  }
}

const getHeaders = (contentType: string, token: string | null) => {
  const headers: { [key: string]: string } = {
    Accept: "*/*",
    "Content-Type": contentType
  }

  if (token) {
    headers.Authorization = `Bearer ${token}`
  }

  return headers
}

const handleApiResponse = (response: AxiosResponse<any, any>) => {
  const newToken = response.headers.authorization
  const { success, message, data, errors } = response.data
  const isTokenExpired = data?.token === false
  const isPasswordExpired = data?.token === -35

  return {
    status: response.status,
    newToken,
    isSuccess: success,
    isTokenExpired,
    isPasswordExpired,
    data,
    message,
    errors
  }
}

const handleDownloadApiResponse = async (response: AxiosResponse<any, any>) => {
  const { status, headers, data } = response
  let isSuccess = status === 200
  let isTokenExpired = false
  let isPasswordExpired = false
  let message = ""
  let respData = null
  const newToken = typeof headers.authorization === "string" ? headers.authorization : null

  if (isSuccess) {
    respData = data
  } else {
    const reader = new FileReader()
    reader.readAsText(data)
    const strJson = await new Promise<string>((resolve) => {
      reader.onload = () => resolve(reader.result as string)
    })
    const objJson = JSON.parse(strJson)

    isSuccess = typeof objJson.success === "boolean" ? objJson.success : false
    message = typeof objJson.message === "string" ? objJson.message : ""

    if (objJson.data && typeof objJson.data.token !== "undefined") {
      const { token } = objJson.data
      isTokenExpired = token === false
      isPasswordExpired = token === -35
      respData = objJson.data
    }
  }

  return {
    status,
    newToken,
    isSuccess,
    isTokenExpired,
    isPasswordExpired,
    data: respData,
    message,
    headers,
    errors: null
  }
}

const handleApiError = (error: unknown) => {
  let status = 0
  let message = ""

  if (axios.isAxiosError(error)) {
    if (error.response) {
      status = error.response.status
    }

    if (status === 401) {
      message = "Authorization is required for this request."
    } else {
      message = error.message
    }
  } else if (error instanceof Error) {
    message = error.message
  }

  return {
    status,
    newToken: null,
    isSuccess: false,
    isTokenExpired: false,
    isPasswordExpired: false,
    data: null,
    message,
    errors: null
  }
}
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  getApi,
  postApi,
  putApi,
  deleteApi,
  downloadGetApi
}
