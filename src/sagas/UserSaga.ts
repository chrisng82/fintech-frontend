import { RootState } from "@/stores"
import UserAction, { UserTypes } from "@/stores/User/Action"
import {
  AdminApproveSelfSignupById,
  ApproveSelfSignupById,
  DownloadDocumentById,
  FetchSelfSignupUser,
  FetchSelfSignupUsers,
  RejectSelfSignupById,
  ReturnSelfSignupById,
  SetUserStatus,
  UpdateApprovedSelfSignupById,
  GetArtisanKernelCommand,
  UpdateArtisanKernelCommand
} from "@/stores/User/Types"
import { call, put, select, takeLatest } from "typed-redux-saga"
import ApiService from "./ApiService"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import saveFile from "@/components/utils/FileSaveHelper"

const getAppStore = (state: RootState) => state.app
const getUserStore = (state: RootState) => state.user

export function* fetchSelfSignupUsers({
  fetchType,
  page,
  page_size,
  filters
}: FetchSelfSignupUsers) {
  try {
    yield put(UserAction.setSelfSignupUsersLoading(true))

    const app = yield* select(getAppStore)
    const user = yield* select(getUserStore)

    const getData = {
      type: fetchType,
      page: page ?? user.selfSignupUser.list.current_page,
      page_size: page_size ?? user.selfSignupUser.list.page_size,
      filters: Object.entries(filters ?? user.selfSignupUser.list.filters)
        .filter(([k, v]) => v !== "" && v !== null && v !== undefined)
        .map(([k, v]) => (Array.isArray(v) ? `${k}:${JSON.stringify(v)}` : `${k}:${v}`))
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `selfSignup/index`, // Route
      app.token, // Token
      getData
    )

    if (filters) {
      yield put(UserAction.setSelfSignupUsersFilters(filters))
    }

    if (result.isSuccess === true) {
      const { data, per_page, last_page, total } = result.data

      yield put(
        UserAction.setSelfSignupUsersPagination(
          per_page,
          last_page,
          page ?? user.selfSignupUser.list.current_page,
          total
        )
      )
      yield put(UserAction.setSelfSignupUsers(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(UserAction.setSelfSignupUsersLoading(false))
  }
}

export function* fetchSelfSignupUser({ id }: FetchSelfSignupUser) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `selfSignup/show/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(UserAction.setSelfSignupUser(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching self signup user", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching self signup user", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export function* downloadDocumentById({ id, fileName }: DownloadDocumentById) {
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.downloadGetApi,
      app.apiUrl,
      `selfSignup/downloadDocument/${id}`,
      app.token
    )

    if (result.isSuccess === true) {
      saveFile(result.data, fileName)
      yield put(SnackbarAction.openSnackbar("Successfully downloaded file.", "success"))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar("Something went wrong when trying to download file.", "error")
      )
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar("Something went wrong when trying to download file.", "error")
    )
  } finally {
  }
}

export function* approveSelfSignupById({ id, data, onFail, onSuccess }: ApproveSelfSignupById) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `selfSignup/approve/${id}`, // Route
      app.token, // Token
      data,
      {},
      "multipart/form-data"
    )

    if (result.isSuccess === true) {
      onSuccess()
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(UserAction.setSelfSignupUser(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      onFail(result.errors)

      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching self signup user", "error")
      )
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetching self signup user", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export function* adminApproveSelfSignupById({ id }: AdminApproveSelfSignupById) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `selfSignup/admin-approve/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(UserAction.setSelfSignupUser(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching self signup user", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching self signup user", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export function* rejectSelfSignupById({ id }: RejectSelfSignupById) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `selfSignup/reject/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(UserAction.setSelfSignupUser(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching self signup user", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching self signup user", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export function* returnSelfSignupById({ id, values }: ReturnSelfSignupById) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `selfSignup/return/${id}`, // Route
      app.token, // Token
      values
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(UserAction.setSelfSignupUser(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching self signup user", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching self signup user", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export function* updateApprovedSelfSignupById({ id, debtorCodes }: UpdateApprovedSelfSignupById) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `selfSignup/update-approved/${id}`, // Route
      app.token, // Token
      Object.fromEntries(
        debtorCodes.map((v) => {
          return [`debtor_codes_${v.companyId}`, v.debtorCodes.map((c) => c.value)]
        })
      )
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(UserAction.setSelfSignupUser(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching self signup user", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching self signup user", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export function* setUserStatus({ data }: SetUserStatus) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)
    const putData = {
      data
    }
    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `user/setStatus`, // Route
      app.token, // Token
      putData
    )

    if (result.isSuccess === true) {
      yield put(UserAction.setSelfSignupUser(result.data))
      SnackbarAction.openSnackbar(result.message, "success")
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching self signup user", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching self signup user", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export function* getArtisanKernelCommand({}: GetArtisanKernelCommand) {
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `user/getKernelArtisanCommand`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      const { artisan_command_on } = result.data
      yield put(UserAction.setArtisanKernelCommand(Boolean(artisan_command_on)))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(
          result.message ?? "Error fetching Kernel Artisan Status",
          "error"
        )
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching Kernel Artisan Status", "error"))
  } finally {
  }
}

export function* updateArtisanKernelCommand({ kernelArtisanCommandActive }: any) {
  yield put(UserAction.setSelfSignupUserLoading(true))

  try {
    const app = yield* select(getAppStore)
    const putData = {
      kernelArtisanCommand: kernelArtisanCommandActive
    }
    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `user/setKernelArtisanCommand`, // Route
      app.token, // Token
      putData
    )

    if (result.isSuccess === true) {
      const { artisan_command_on } = result.data
      yield put(UserAction.setArtisanKernelCommand(Boolean(artisan_command_on)))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(
          result.message ?? "Error fetching Kernel Artisan Status",
          "error"
        )
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching Kernel Artisan Status", "error"))
  } finally {
    yield put(UserAction.setSelfSignupUserLoading(false))
  }
}

export const userSaga = [
  takeLatest(UserTypes.FETCH_SELF_SIGNUP_USERS, fetchSelfSignupUsers),
  takeLatest(UserTypes.FETCH_SELF_SIGNUP_USER, fetchSelfSignupUser),
  takeLatest(UserTypes.DOWNLOAD_DOCUMENT_BY_ID, downloadDocumentById),
  takeLatest(UserTypes.APPROVE_SELF_SIGNUP_BY_ID, approveSelfSignupById),
  takeLatest(UserTypes.ADMIN_APPROVE_SELF_SIGNUP_BY_ID, adminApproveSelfSignupById),
  takeLatest(UserTypes.REJECT_SELF_SIGNUP_BY_ID, rejectSelfSignupById),
  takeLatest(UserTypes.RETURN_SELF_SIGNUP_BY_ID, returnSelfSignupById),
  takeLatest(UserTypes.UPDATE_APPROVED_SELF_SIGNUP_BY_ID, updateApprovedSelfSignupById),
  takeLatest(UserTypes.SET_USER_STATUS, setUserStatus),
  takeLatest(UserTypes.GET_ARTISAN_KERNEL_COMMAND, getArtisanKernelCommand),
  takeLatest(UserTypes.UPDATE_ARTISAN_KERNEL_COMMAND, updateArtisanKernelCommand)
]
