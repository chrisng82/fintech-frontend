import { call, put, select, takeLatest } from "typed-redux-saga"
import ApiService from "./ApiService"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { RootState } from "@/stores"
import { FetchDebitNote, FetchDebitNoteDetails, FetchDebitNotes } from "@/stores/DebitNote/Types"
import DebitNoteAction, { debitNoteTypes } from "@/stores/DebitNote/Action"

const getAppStore = (state: RootState) => state.app
const getDebitNotesStore = (state: RootState) => state.debitNote
const getDebitNoteStore = (state: RootState) => state.debitNote.item

export function* fetchDebitNotes({ page, page_size, filters }: FetchDebitNotes) {
  yield put(DebitNoteAction.setDebitNotesLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      list: {
        current_page,
        page_size: current_page_size,
        filters: current_filters,
        sorts: current_sorts
      }
    } = yield select(getDebitNotesStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number; filters: string[]; sorts: string[] } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size,
      filters: [
        `doc_code:${current_filters.code}`,
        `debtor_code:${current_filters.debtorCode}`,
        `debtor_name:${current_filters.debtorName}`,
        `pdf_only:${current_filters.pdfOnly}`,
        `debtor_id:${current_filters.debtorId}`,
        `payment_status:${current_filters.paymentStatus}`,
        `doc_date:${current_filters.docDate}`
      ],
      sorts: [
        `doc_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docCode")].order}`,
        `doc_date:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docDate")].order}`,
        `debtor_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorCode")].order}`,
        `debtor_name:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorName")].order}`,
        `net_amt:${current_sorts[current_sorts.findIndex((element: any) => element.field === "netAmt")].order}`
      ]
    }

    if (filters) {
      yield put(DebitNoteAction.setDebitNotesFilters(filters))

      getData.page = 1
      getData.filters = [
        `doc_code:${filters.code}`,
        `debtor_code:${filters.debtorCode}`,
        `debtor_name:${filters.debtorName}`,
        `pdf_only:${filters.pdfOnly}`,
        `debtor_id:${filters.debtorId}`,
        `payment_status:${filters.paymentStatus}`,
        `doc_date:${filters.docDate}`
      ]
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arDnHdr/index/${app.user?.companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, current_page, last_page, total } = result.data

      yield put(
        DebitNoteAction.setDebitNotesPagination(current_page_size, current_page, last_page, total)
      )

      yield put(DebitNoteAction.setDebitNotes(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log("Result is failed")
    }
  } catch (error) {
    console.log("Error fetch debitNote list")
  } finally {
    yield put(DebitNoteAction.setDebitNotesLoading(false))
  }
}

export function* fetchDebitNote({ id }: FetchDebitNote) {
  yield put(DebitNoteAction.setDebitNoteLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arDnHdr/showHeader/${app.user?.companyCode}/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(DebitNoteAction.setDebitNote(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log(result.message)
      yield put(SnackbarAction.openSnackbar(result.message ?? "Error fetching debit note", "error"))
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching debit note", "error"))
  } finally {
    yield put(DebitNoteAction.setDebitNoteLoading(false))
  }
}

export function* fetchDebitNoteDetails({ hdrId, page, page_size }: FetchDebitNoteDetails) {
  yield put(DebitNoteAction.setDebitNoteDetailsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      details: { current_page, page_size: current_page_size }
    } = yield select(getDebitNoteStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arDnHdr/showDetails/${app.user?.companyCode}/${hdrId}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DebitNoteAction.setDebitNoteDetails(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message ?? "Error fetching debit note", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetching debit note", "error"))
  } finally {
    yield put(DebitNoteAction.setDebitNoteDetailsLoading(false))
  }
}

export const debitNoteSaga = [
  takeLatest(debitNoteTypes.FETCH_DEBIT_NOTES, fetchDebitNotes),
  takeLatest(debitNoteTypes.FETCH_DEBIT_NOTE, fetchDebitNote),
  takeLatest(debitNoteTypes.FETCH_DEBIT_NOTE_DETAILS, fetchDebitNoteDetails)
]
