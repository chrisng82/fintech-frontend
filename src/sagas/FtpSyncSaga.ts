import { call, select, takeLatest } from "typed-redux-saga"
import { put } from "redux-saga/effects"
import SnackbarAction from "../stores/Snackbar/Action"
import { RootState } from "@/stores"
import ApiService, { Result } from "./ApiService"
import FtpSyncAction, { FtpSyncTypes } from "@/stores/FtpSync/Action"
import AppAction from "@/stores/App/Action"

const getAppStore = (state: RootState) => state.app

export function* fetchFtpSyncPendingFiles() {
  yield put(FtpSyncAction.setFtpSyncPendingFilesLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result: Result = yield call(ApiService.getApi, app.apiUrl, "ftp/pending-files", app.token)

    if (result.isSuccess === true) {
      yield put(FtpSyncAction.setFtpSyncPendingFiles(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar("Something went wrong when trying to fetch ftp files.", "error")
      )
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar("Something went wrong when trying to fetch ftp files.", "error")
    )
  } finally {
    yield put(FtpSyncAction.setFtpSyncPendingFilesLoading(false))
  }
}

export function* fetchFtpSyncLogs() {
  yield put(FtpSyncAction.setFtpSyncLogsLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result: Result = yield call(ApiService.postApi, app.apiUrl, "ftp/sync-logs", app.token)

    if (result.isSuccess === true) {
      yield put(FtpSyncAction.setFtpSyncLogs(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar("Something went wrong when trying to fetch ftp files.", "error")
      )
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar("Something went wrong when trying to fetch ftp files.", "error")
    )
  } finally {
    yield put(FtpSyncAction.setFtpSyncLogsLoading(false))
  }
}

export function* syncFtpPendingFiles() {
  yield put(FtpSyncAction.setFtpSyncPendingFilesLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result: Result = yield call(ApiService.postApi, app.apiUrl, "ftp/sync", app.token)

    if (result.isSuccess === true) {
      yield put(FtpSyncAction.fetchFtpSyncPendingFiles())
      yield put(FtpSyncAction.fetchFtpSyncLogs())
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar("Something went wrong when trying to sync ftp files.", "error")
      )
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar("Something went wrong when trying to sync ftp files.", "error")
    )
  } finally {
    yield put(FtpSyncAction.setFtpSyncPendingFilesLoading(false))
  }
}

export const ftpSyncSaga = [
  takeLatest(FtpSyncTypes.FETCH_FTP_SYNC_PENDING_FILES, fetchFtpSyncPendingFiles),
  takeLatest(FtpSyncTypes.FETCH_FTP_SYNC_LOGS, fetchFtpSyncLogs),
  takeLatest(FtpSyncTypes.SYNC_FTP_PENDING_FILES, syncFtpPendingFiles)
]
