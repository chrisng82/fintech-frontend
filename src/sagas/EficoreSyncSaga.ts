import { call, select, takeLatest } from "typed-redux-saga"
import { put } from "redux-saga/effects"
import SnackbarAction from "../stores/Snackbar/Action"
import { RootState } from "@/stores"
import ApiService, { Result } from "./ApiService"
import EficoreSyncAction, { EficoreSyncTypes } from "@/stores/EficoreSync/Action"
import AppAction from "@/stores/App/Action"
import { CreateEficoreSyncJob, FetchEficoreSyncs } from "@/stores/EficoreSync/Types"

const getAppStore = (state: RootState) => state.app
const getEficoreSyncsStore = (state: RootState) => state.eficoreSync

export function* fetchEficoreSyncs({ syncType, page, page_size }: FetchEficoreSyncs) {
  yield put(EficoreSyncAction.setEficoreSyncsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      list: { current_page, page_size: current_page_size }
    } = yield select(getEficoreSyncsStore)

    const getData: { syncType: string; page: number; rowsPerPage: number } = {
      syncType: syncType,
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size
    }

    const result: Result = yield call(
      ApiService.getApi,
      app.apiUrl,
      "sync/index",
      app.token,
      getData
    )

    if (result.isSuccess === true) {
      const { data, current_page, last_page, total } = result.data["data"]
      const stats = result.data["stats"]

      yield put(
        EficoreSyncAction.setEficoreSyncsPagination(
          current_page_size,
          current_page,
          last_page,
          total
        )
      )
      yield put(EficoreSyncAction.setEficoreSyncs(data, stats))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(
          "Something went wrong when trying to fetch Eficore sync.",
          "error"
        )
      )
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar(
        (error as Error).message ?? "Something went wrong when trying to fetch Eficore sync.",
        "error"
      )
    )
  } finally {
    yield put(EficoreSyncAction.setEficoreSyncsLoading(false))
  }
}

export function* createEficoreSyncJob({ syncType }: CreateEficoreSyncJob) {
  yield put(EficoreSyncAction.setEficoreSyncsLoading(true))

  try {
    const app = yield* select(getAppStore)

    const getData: { syncType: string; companyCode: any } = {
      syncType: syncType,
      companyCode: app.user?.companyCode
    }

    const result: Result = yield call(
      ApiService.getApi,
      app.apiUrl,
      "sync/create-job",
      app.token,
      getData
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(EficoreSyncAction.fetchEficoreSyncs(syncType))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(EficoreSyncAction.setEficoreSyncsLoading(false))
  }
}

export const eficoreSyncSaga = [
  takeLatest(EficoreSyncTypes.FETCH_EFICORE_SYNCS, fetchEficoreSyncs),
  takeLatest(EficoreSyncTypes.CREATE_EFICORE_SYNC_JOB, createEficoreSyncJob)
]
