import { call, put, select, takeLatest } from "typed-redux-saga"
import ApiService from "./ApiService"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { RootState } from "@/stores"
import {
  FetchCreditNote,
  FetchCreditNoteDetails,
  FetchCreditNotes
} from "@/stores/CreditNote/Types"
import CreditNoteAction, { creditNoteTypes } from "@/stores/CreditNote/Action"

const getAppStore = (state: RootState) => state.app
const getCreditNotesStore = (state: RootState) => state.creditNote
const getCreditNoteStore = (state: RootState) => state.creditNote.item

export function* fetchCreditNotes({ page, page_size, filters }: FetchCreditNotes) {
  yield put(CreditNoteAction.setCreditNotesLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      list: {
        current_page,
        page_size: current_page_size,
        filters: current_filters,
        sorts: current_sorts
      }
    } = yield select(getCreditNotesStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number; filters: string[]; sorts: string[] } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size,
      filters: [
        `doc_code:${current_filters.code}`,
        `debtor_code:${current_filters.debtorCode}`,
        `debtor_name:${current_filters.debtorName}`,
        `pdf_only:${current_filters.pdfOnly}`,
        `debtor_id:${current_filters.debtorId}`,
        `payment_status:${current_filters.paymentStatus}`,
        `doc_date:${current_filters.docDate}`
      ],
      sorts: [
        `doc_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docCode")].order}`,
        `doc_date:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docDate")].order}`,
        `debtor_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorCode")].order}`,
        `debtor_name:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorName")].order}`,
        `net_amt:${current_sorts[current_sorts.findIndex((element: any) => element.field === "netAmt")].order}`
      ]
    }

    if (filters) {
      yield put(CreditNoteAction.setCreditNotesFilters(filters))

      getData.page = 1
      getData.filters = [
        `doc_code:${filters.code}`,
        `debtor_code:${filters.debtorCode}`,
        `debtor_name:${filters.debtorName}`,
        `pdf_only:${filters.pdfOnly}`,
        `debtor_id:${filters.debtorId}`,
        `payment_status:${filters.paymentStatus}`,
        `doc_date:${filters.docDate}`
      ]
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arCnHdr/index/${app.user?.companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, current_page, last_page, total } = result.data

      yield put(
        CreditNoteAction.setCreditNotesPagination(current_page_size, current_page, last_page, total)
      )

      yield put(CreditNoteAction.setCreditNotes(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log("Result is failed")
    }
  } catch (error) {
    console.log("Error fetch creditNote list")
  } finally {
    yield put(CreditNoteAction.setCreditNotesLoading(false))
  }
}

export function* fetchCreditNote({ id }: FetchCreditNote) {
  yield put(CreditNoteAction.setCreditNoteLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arCnHdr/showHeader/${app.user?.companyCode}/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(CreditNoteAction.setCreditNote(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log(result.message)
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching credit note", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching credit note", "error"))
  } finally {
    yield put(CreditNoteAction.setCreditNoteLoading(false))
  }
}

export function* fetchCreditNoteDetails({ hdrId, page, page_size }: FetchCreditNoteDetails) {
  yield put(CreditNoteAction.setCreditNoteDetailsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      details: { current_page, page_size: current_page_size }
    } = yield select(getCreditNoteStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arCnHdr/showDetails/${app.user?.companyCode}/${hdrId}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(CreditNoteAction.setCreditNoteDetails(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching credit note", "error")
      )
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetching credit note", "error"))
  } finally {
    yield put(CreditNoteAction.setCreditNoteDetailsLoading(false))
  }
}

export const creditNoteSaga = [
  takeLatest(creditNoteTypes.FETCH_CREDIT_NOTES, fetchCreditNotes),
  takeLatest(creditNoteTypes.FETCH_CREDIT_NOTE, fetchCreditNote),
  takeLatest(creditNoteTypes.FETCH_CREDIT_NOTE_DETAILS, fetchCreditNoteDetails)
]
