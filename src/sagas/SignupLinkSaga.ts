import { RootState } from "@/stores"
import { call, put, select, takeLatest } from "typed-redux-saga"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import ApiService from "./ApiService"
import SignupLinkAction, { SignupLinkTypes } from "@/stores/SignupLink/Action"
import {
  FetchSignupLinks,
  GenerateSignupLink,
  InvalidateSignupLink,
  SelfSignup,
  SendBatchSignupLinksTo,
  SendSignupLinkTo,
  SignupBySignupLink,
  ValidateSignupLinkToken
} from "@/stores/SignupLink/Types"
import CompanyAction from "@/stores/Company/Action"
import DebtorAction from "@/stores/Debtor/Action"
import { UserTypeValues } from "@/constants/UserTypeEnum"

const getAppStore = (state: RootState) => state.app
const getCompanyStore = (state: RootState) => state.company
const getSignupLinkStore = (state: RootState) => state.signupLink

export function* fetchSignupLinks({ page, perPage }: FetchSignupLinks) {
  try {
    yield put(SignupLinkAction.setSignupLinksLoading(true))

    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)
    const signupLink = yield* select(getSignupLinkStore)

    if (!company.showCompany.data) {
      yield* put(SnackbarAction.openSnackbar("Company not found.", "error"))
      return
    }

    const getData = {
      page: page ?? signupLink.list.current_page,
      per_page: perPage ?? signupLink.list.per_page
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `signupLink/company/${company.showCompany.data.id}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, per_page, current_page, last_page, total } = result.data

      yield put(SignupLinkAction.setSignupLinksPagination(per_page, current_page, last_page, total))
      yield put(SignupLinkAction.setSignupLinks(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(SignupLinkAction.setSignupLinksLoading(false))
  }
}

export function* generateSignupLink({ setSubmitting, data, onSuccess }: GenerateSignupLink) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `signupLink/create`, // Route
      app.token, // Token
      data
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(SignupLinkAction.fetchSignupLinks())
      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* sendSignupLinkTo({ id, actionType }: SendSignupLinkTo) {
  yield put(SignupLinkAction.setSignupLinkSending(true))
  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `signupLink/send/${id}/${actionType}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(SignupLinkAction.setSignupLinkSending(false))
  }
}
export function* sendBatchSignupLinksTo({ ids, isAll, actionType }: SendBatchSignupLinksTo) {
  try {
    const app = yield* select(getAppStore)

    const getData = {
      ids,
      isAll
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `signupLink/sendBatch/${actionType}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(SignupLinkAction.setSignupLinkSending(false))
  }
}

export function* invalidateSignupLink({ id }: InvalidateSignupLink) {
  yield put(SignupLinkAction.setSignupLinksLoading(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `signupLink/invalidate/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(SignupLinkAction.fetchSignupLinks())
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(SignupLinkAction.setSignupLinksLoading(false))
  }
}

export function* validateSignupLinkToken({ token, onFail }: ValidateSignupLinkToken) {
  yield put(SignupLinkAction.setIsValidating(true))

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `signupLink/validate`, // Route
      "1035",
      { token }
    )

    if (result.isSuccess === true) {
      const data = result.data

      yield put(SignupLinkAction.setSignupLink(data))

      if (data.link_type === UserTypeValues.DEBTOR) {
        const company = yield* call(
          ApiService.getApi,
          app.apiUrl,
          `company/fetchByCompanyId/${data.company_id}`, // Route
          app.token // Token
        )

        if (company.isSuccess === true) {
          if (!company.data) {
            yield put(SnackbarAction.openSnackbar("Company details not found", "error"))
          } else {
            yield put(CompanyAction.setShowCompany(company.data))
          }
        }

        const debtor = yield* call(
          ApiService.getApi,
          app.apiUrl,
          `debtor/fetchByToken`, // Route
          app.token, // Token
          { token }
        )

        if (debtor.isSuccess === true) {
          if (!debtor.data) {
            yield put(SnackbarAction.openSnackbar("Debtor details not found", "error"))
          } else {
            yield put(DebtorAction.setDebtors(debtor.data))
          }
        }
      }
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
      onFail()
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(SignupLinkAction.setIsValidating(false))
  }
}

export function* signupBySignupLink({ setSubmitting, data, onSuccess }: SignupBySignupLink) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `signupLink/signup`, // Route
      "1035",
      data
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* selfSignup({ setSubmitting, data, onFail, onSuccess }: SelfSignup) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)

    const formattedValues = Object.fromEntries(
      Object.entries(data).map(([key, value]) => {
        if (key.includes("debtor_code_")) {
          return [key, value !== "" ? value.split(";") : []]
        }

        return [key, value]
      })
    )

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `selfSignup/signup`, // Route
      "1035",
      formattedValues,
      {},
      "multipart/form-data"
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      if (result.errors) {
        onFail(result.errors)
      }
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export const signupLinkSaga = [
  takeLatest(SignupLinkTypes.GENERATE_SIGNUP_LINK, generateSignupLink),
  takeLatest(SignupLinkTypes.FETCH_SIGNUP_LINKS, fetchSignupLinks),
  takeLatest(SignupLinkTypes.INVALIDATE_SIGNUP_LINK, invalidateSignupLink),
  takeLatest(SignupLinkTypes.VALIDATE_SIGNUP_LINK_TOKEN, validateSignupLinkToken),
  takeLatest(SignupLinkTypes.SIGNUP_BY_SIGNUP_LINK, signupBySignupLink),
  takeLatest(SignupLinkTypes.SEND_SIGNUP_LINK_TO, sendSignupLinkTo),
  takeLatest(SignupLinkTypes.SEND_BATCH_SIGNUP_LINKS_TO, sendBatchSignupLinksTo),
  takeLatest(SignupLinkTypes.SELF_SIGNUP, selfSignup)
]
