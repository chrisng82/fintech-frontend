import { call, put, select, takeLatest } from "typed-redux-saga"
import ApiService from "./ApiService"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { RootState } from "@/stores"
import {
  FetchDebtorPayment,
  FetchDebtorPaymentDetails,
  FetchDebtorPayments
} from "@/stores/DebtorPayment/Types"
import DebtorPaymentAction, { debtorPaymentTypes } from "@/stores/DebtorPayment/Action"

const getAppStore = (state: RootState) => state.app
const getDebtorPaymentsStore = (state: RootState) => state.debtorPayment
const getDebtorPaymentStore = (state: RootState) => state.debtorPayment.item

export function* fetchDebtorPayments({ page, page_size, filters }: FetchDebtorPayments) {
  yield put(DebtorPaymentAction.setDebtorPaymentsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      list: {
        current_page,
        page_size: current_page_size,
        filters: current_filters,
        sorts: current_sorts
      }
    } = yield select(getDebtorPaymentsStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number; filters: string[]; sorts: string[] } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size,
      filters: [
        `doc_code:${current_filters.code}`,
        `debtor_code:${current_filters.debtorCode}`,
        `debtor_name:${current_filters.debtorName}`,
        `pdf_only:${current_filters.pdfOnly}`,
        `debtor_id:${current_filters.debtorId}`,
        `payment_status:${current_filters.paymentStatus}`,
        `doc_date:${current_filters.docDate}`
      ],
      sorts: [
        `doc_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docCode")].order}`,
        `doc_date:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docDate")].order}`,
        `debtor_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorCode")].order}`,
        `payment_method:${current_sorts[current_sorts.findIndex((element: any) => element.field === "paymentMethod")].order}`,
        `debtor_name:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorName")].order}`,
        `payment_amt:${current_sorts[current_sorts.findIndex((element: any) => element.field === "payment")].order}`,
        `balance_amt:${current_sorts[current_sorts.findIndex((element: any) => element.field === "balance")].order}`
      ]
    }

    if (filters) {
      yield put(DebtorPaymentAction.setDebtorPaymentsFilters(filters))

      getData.page = 1
      getData.filters = [
        `doc_code:${filters.code}`,
        `debtor_code:${filters.debtorCode}`,
        `debtor_name:${filters.debtorName}`,
        `pdf_only:${filters.pdfOnly}`,
        `debtor_id:${filters.debtorId}`,
        `payment_status:${filters.paymentStatus}`,
        `doc_date:${filters.docDate}`
      ]
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arPaymentHdr/index/${app.user?.companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, current_page, last_page, total } = result.data

      yield put(
        DebtorPaymentAction.setDebtorPaymentsPagination(
          current_page_size,
          current_page,
          last_page,
          total
        )
      )

      yield put(DebtorPaymentAction.setDebtorPayments(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log("Result is failed")
    }
  } catch (error) {
    console.log("Error fetch debtorPayment list")
  } finally {
    yield put(DebtorPaymentAction.setDebtorPaymentsLoading(false))
  }
}

export function* fetchDebtorPayment({ id }: FetchDebtorPayment) {
  yield put(DebtorPaymentAction.setDebtorPaymentLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arPaymentHdr/showHeader/${app.user?.companyCode}/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(DebtorPaymentAction.setDebtorPayment(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log(result.message)
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching credit note", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching credit note", "error"))
  } finally {
    yield put(DebtorPaymentAction.setDebtorPaymentLoading(false))
  }
}

export function* fetchDebtorPaymentDetails({ hdrId, page, page_size }: FetchDebtorPaymentDetails) {
  yield put(DebtorPaymentAction.setDebtorPaymentDetailsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      details: { current_page, page_size: current_page_size }
    } = yield select(getDebtorPaymentStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arPaymentHdr/showDetails/${app.user?.companyCode}/${hdrId}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DebtorPaymentAction.setDebtorPaymentDetails(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching credit note", "error")
      )
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetching credit note", "error"))
  } finally {
    yield put(DebtorPaymentAction.setDebtorPaymentDetailsLoading(false))
  }
}

export const debtorPaymentSaga = [
  takeLatest(debtorPaymentTypes.FETCH_DEBTOR_PAYMENTS, fetchDebtorPayments),
  takeLatest(debtorPaymentTypes.FETCH_DEBTOR_PAYMENT, fetchDebtorPayment),
  takeLatest(debtorPaymentTypes.FETCH_DEBTOR_PAYMENT_DETAILS, fetchDebtorPaymentDetails)
]
