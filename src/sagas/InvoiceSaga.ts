import { call, put, select, takeLatest } from "typed-redux-saga"
import ApiService from "./ApiService"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { RootState } from "@/stores"
import { FetchInvoice, FetchInvoiceDetails, FetchInvoices } from "@/stores/Invoice/Types"
import InvoiceAction, { invoiceTypes } from "@/stores/Invoice/Action"

const getAppStore = (state: RootState) => state.app
const getInvoicesStore = (state: RootState) => state.invoice
const getInvoiceStore = (state: RootState) => state.invoice.item

export function* fetchInvoices({ page, page_size, filters }: FetchInvoices) {
  yield put(InvoiceAction.setInvoicesLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      list: {
        current_page,
        page_size: current_page_size,
        filters: current_filters,
        sorts: current_sorts
      }
    } = yield select(getInvoicesStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number; filters: string[]; sorts: string[] } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size,
      filters: [
        `doc_code:${current_filters.code}`,
        `debtor_code:${current_filters.debtorCode}`,
        `debtor_name:${current_filters.debtorName}`,
        `pdf_only:${current_filters.pdfOnly}`,
        `debtor_id:${current_filters.debtorId}`,
        `payment_status:${current_filters.paymentStatus}`,
        `doc_date:${current_filters.docDate}`
      ],
      sorts: [
        `doc_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docCode")].order}`,
        `doc_date:${current_sorts[current_sorts.findIndex((element: any) => element.field === "docDate")].order}`,
        `debtor_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorCode")].order}`,
        `debtor_name:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtorName")].order}`,
        `net_amt:${current_sorts[current_sorts.findIndex((element: any) => element.field === "netAmt")].order}`,
        `payment_status:${current_sorts[current_sorts.findIndex((element: any) => element.field === "payment")].order}`,
        `balance:${current_sorts[current_sorts.findIndex((element: any) => element.field === "balance")].order}`
      ]
    }

    if (filters) {
      yield put(InvoiceAction.setInvoicesFilters(filters))

      getData.page = 1
      getData.filters = [
        `doc_code:${filters.code}`,
        `debtor_code:${filters.debtorCode}`,
        `debtor_name:${filters.debtorName}`,
        `pdf_only:${filters.pdfOnly}`,
        `debtor_id:${filters.debtorId}`,
        `payment_status:${filters.paymentStatus}`,
        `doc_date:${filters.docDate}`
      ]
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arInvHdr/index/${app.user?.companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, current_page, last_page, total } = result.data

      yield put(
        InvoiceAction.setInvoicesPagination(current_page_size, current_page, last_page, total)
      )

      yield put(InvoiceAction.setInvoices(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log("Result is failed")
    }
  } catch (error) {
    console.log("Error fetch invoice list")
  } finally {
    yield put(InvoiceAction.setInvoicesLoading(false))
  }
}

export function* fetchInvoice({ id }: FetchInvoice) {
  yield put(InvoiceAction.setInvoiceLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arInvHdr/showHeader/${app.user?.companyCode}/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(InvoiceAction.setInvoice(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching credit note", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching credit note", "error"))
  } finally {
    yield put(InvoiceAction.setInvoiceLoading(false))
  }
}

export function* fetchInvoiceDetails({ hdrId, page, page_size }: FetchInvoiceDetails) {
  yield put(InvoiceAction.setInvoiceDetailsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      details: { current_page, page_size: current_page_size }
    } = yield select(getInvoiceStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `arInvHdr/showDetails/${app.user?.companyCode}/${hdrId}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(InvoiceAction.setInvoiceDetails(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching credit note", "error")
      )
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetching credit note", "error"))
  } finally {
    yield put(InvoiceAction.setInvoiceDetailsLoading(false))
  }
}

export const invoiceSaga = [
  takeLatest(invoiceTypes.FETCH_INVOICES, fetchInvoices),
  takeLatest(invoiceTypes.FETCH_INVOICE, fetchInvoice),
  takeLatest(invoiceTypes.FETCH_INVOICE_DETAILS, fetchInvoiceDetails)
]
