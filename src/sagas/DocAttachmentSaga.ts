import { call, select, takeLatest } from "typed-redux-saga"
import { put } from "redux-saga/effects"
import SnackbarAction from "../stores/Snackbar/Action"
import { RootState } from "@/stores"
import ApiService, { Result } from "./ApiService"
import DocAttachmentAction, { DocAttachmentTypes } from "@/stores/DocAttachment/Action"
import AppAction from "@/stores/App/Action"
import { DocAttachmentGetDownloadData, DownloadByS3Url } from "@/stores/DocAttachment/Types"
import saveFile from "@/components/utils/FileSaveHelper"
import base64ToBlob from "@/components/utils/Base64ToBlob"
import dayjs from "dayjs"

const getAppStore = (state: RootState) => state.app
const getCompanyStore = (state: RootState) => state.company

export function* downloadByS3Url({ url }: DownloadByS3Url) {
  yield put(DocAttachmentAction.setDownloadIsLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (Array.isArray(url)) {
      url.map((singleUrl: string) => encodeURIComponent(singleUrl))

      const result: Result = yield call(
        ApiService.downloadGetApi,
        app.apiUrl,
        "doc-attachment/download-by-url",
        app.token,
        {
          url
        }
      )

      if (result.isSuccess === true) {
        const contentDisposition = result.headers["content-disposition"]
        const fileName = contentDisposition
          ? contentDisposition.split("filename=")[1].replace(/"/g, "")
          : "documents.zip"

        saveFile(result.data, fileName)
        yield put(
          SnackbarAction.openSnackbar("Batch documents successfully downloaded.", "success")
        )
      } else if (result.isTokenExpired === true) {
        yield put(AppAction.tokenExpired())
      } else {
        yield put(
          SnackbarAction.openSnackbar("Something went wrong when trying to download file.", "error")
        )
      }
    }

    if (typeof url == "string") {
      const result: Result = yield call(
        ApiService.downloadGetApi,
        app.apiUrl,
        "doc-attachment/download-by-url",
        app.token,
        {
          url: encodeURIComponent(url)
        }
      )

      if (result.isSuccess === true) {
        const contentDisposition = result.headers["content-disposition"]
        const fileName = contentDisposition
          ? contentDisposition.split("filename=")[1].replace(/"/g, "")
          : `${dayjs().unix()}.pdf`

        saveFile(result.data, fileName)
        yield put(SnackbarAction.openSnackbar("Successfully downloaded file.", "success"))
      } else if (result.isTokenExpired === true) {
        yield put(AppAction.tokenExpired())
      } else {
        yield put(
          SnackbarAction.openSnackbar("Something went wrong when trying to download file.", "error")
        )
      }
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar("Something went wrong when trying to download file.", "error")
    )
  } finally {
    yield put(DocAttachmentAction.setDownloadIsLoading(false))
  }
}

export function* docAttachmentGetDownloadData({ docAttachment }: DocAttachmentGetDownloadData) {
  yield put(DocAttachmentAction.setDownloadIsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const getData = {
      docAttachment
    }
    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `doc-attachment/getDownloadData`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DocAttachmentAction.docAttachmentSetDownloadData(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(
        SnackbarAction.openSnackbar(result.message ?? "Error fetching Download Data", "error")
      )
    }
  } catch (error) {
    console.log(error)
    yield put(SnackbarAction.openSnackbar("Error fetching Download Data", "error"))
  } finally {
    yield put(DocAttachmentAction.setDownloadIsLoading(false))
  }
}

export function* docAttachmentDlMiddleware({ docCode, companyCode }: any) {
  yield put(DocAttachmentAction.setDownloadIsLoading(true))
  try {
    const app = yield* select(getAppStore)

    const postData = {
      docCode,
      companyCode
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `company/downloadEInvoice`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      const fileName = `${docCode}.pdf`
      const base64Data = result.data.pdfBase64

      const pdfBlob = base64ToBlob(base64Data, "application/pdf")
      saveFile(pdfBlob, fileName)

      yield put(SnackbarAction.openSnackbar(result.message, "success"))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(DocAttachmentAction.setDownloadIsLoading(false))
  }
}

export const docAttachmentSaga = [
  takeLatest(DocAttachmentTypes.DOWNLOAD_BY_S3_URL, downloadByS3Url),
  takeLatest(DocAttachmentTypes.DOC_ATTACHMENT_GET_DOWNLOAD_DATA, docAttachmentGetDownloadData),
  takeLatest(DocAttachmentTypes.DOC_ATTACHMENT_DL_MIDDLEWARE, docAttachmentDlMiddleware)
]
