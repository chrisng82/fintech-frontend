import { RootState } from "@/stores"
import { call, put, select, takeLatest } from "typed-redux-saga"
import AppAction from "@/stores/App/Action"
import CompanyAction, { CompanyTypes } from "@/stores/Company/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import ApiService from "./ApiService"
import {
  CreateCompany,
  FetchCompanyList,
  ShowCompany,
  UpdateCompany,
  UpdateSMTPEmailConfig,
  UpdateWhatsappConfig,
  UpdateEficoreSyncSetting,
  FetchTermCondition,
  CreateTermCondition,
  UpdateTermCondition,
  FetchTermConditions,
  SetTermConditionStatus,
  FetchTermConditionPublic,
  UpdateMiddlewareSetting
} from "@/stores/Company/Types"
import CompanyEmailTaskAction from "@/stores/CompanyEmailTask/Action"

const getAppStore = (state: RootState) => state.app
const getCompanyStore = (state: RootState) => state.company

export function* fetchCompanySelect() {
  try {
    yield put(CompanyAction.setCompanySelectLoading(true))

    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      "company/select", // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      yield put(CompanyAction.setCompanySelect(result.data))

      if (app.user?.companyCode === "") {
        yield put(
          AppAction.authenticateSuccess(app.user.token, {
            ...app.user,
            companyId: result.data[0].value,
            companyCode: result.data[0].label
          })
        )
      }
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(
      SnackbarAction.openSnackbar(
        "Something went wrong when trying to fetch company list.",
        "error"
      )
    )
  } finally {
    yield put(CompanyAction.setCompanySelectLoading(false))
  }
}

export function* fetchCompanyList({ filters }: FetchCompanyList) {
  try {
    yield put(CompanyAction.setCompanyListLoading(true))

    const app = yield* select(getAppStore)
    const { companyList, companySelect } = yield* select(getCompanyStore)

    // if (companySelect.data.length === 0) {
    //   yield put(SnackbarAction.openSnackbar("No companies were found", "error"))
    //   return
    // }

    if (!app.user) {
      throw new Error("User not found")
    }

    const firstCompany = companySelect.data[0]

    const getData: {
      page: number
      filters: string[]
    } = {
      page: companyList.page,
      filters: []
    }

    if (filters) {
      yield put(CompanyAction.setCompanyListFilters(filters))

      getData.filters = [`code:${filters.code}`, `name:${filters.name}`]
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `company/index/${app.user.id}/${
        app.user.companyCode !== "" ? app.user.companyCode : firstCompany.label
      }`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, current_page, last_page, prev_page_url } = result.data

      if (companyList.page === last_page && prev_page_url === null) {
        yield put(CompanyAction.setHasMoreCompany(false, current_page))

        yield put(CompanyAction.setCompanyList(data))
      } else if (last_page >= current_page && data.length !== 0) {
        yield put(CompanyAction.setHasMoreCompany(true, current_page + 1))
        const prevData = companyList.data
        Array.prototype.push.apply(prevData, data)
        yield put(CompanyAction.setCompanyList(prevData))
      }
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setCompanyListLoading(false))
  }
}

export function* createCompany({ setSubmitting, data, onSuccess }: CreateCompany) {
  setSubmitting(true)
  try {
    const app = yield* select(getAppStore)

    const postData = {
      code: data.code,
      data
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `company/create`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(CompanyAction.fetchCompanyList())
      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* showCompany({ code }: ShowCompany) {
  try {
    yield put(CompanyAction.setShowCompanyLoading(true))

    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `company/show/${code}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      if (!result.data) {
        yield put(SnackbarAction.openSnackbar("Company details not found", "error"))
      } else {
        const { email_tasks, ...rest } = result.data

        yield put(CompanyAction.setShowCompany(rest))
        yield put(CompanyEmailTaskAction.setCompanyEmailTasks(email_tasks))
      }
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setShowCompanyLoading(false))
  }
}

export function* updateCompany({ setSubmitting, id, data, onSuccess }: UpdateCompany) {
  setSubmitting(true)
  try {
    const app = yield* select(getAppStore)
    const { showCompany } = yield* select(getCompanyStore)

    const postData = {
      data: {
        ...data,
        id
      }
    }

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `company/update`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      yield put(SnackbarAction.openSnackbar(result.message, "success"))
      yield put(CompanyAction.setShowCompany(result.data.model))
      onSuccess(result.data.model)
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* updateWhatsappConfig({ setSubmitting, data, onSuccess }: UpdateWhatsappConfig) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)

    if (!company.showCompany.data) {
      yield put(SnackbarAction.openSnackbar("Company details not found", "error"))
      return
    }

    const postData = {
      data: {
        environment: data.whatsapp_environment,
        phone_number: data.whatsapp_phone_number,
        url: data.whatsapp_url,
        token: data.whatsapp_token,
        companyId: data.company_id
      }
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `company/whatsapp/setting`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      const newCompanyData = {
        ...company.showCompany.data,
        communication_settings: result.data
      }

      yield put(CompanyAction.setShowCompany(newCompanyData))
      yield put(SnackbarAction.openSnackbar(result.message, "success"))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* updateSMTPEmailConfig({ setSubmitting, data, onSuccess }: UpdateSMTPEmailConfig) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)

    if (!company.showCompany.data) {
      yield put(SnackbarAction.openSnackbar("Company details not found", "error"))
      return
    }

    const postData = {
      data: {
        companyId: data.company_id,
        environment: data.smtp_environment,
        username: data.smtp_username,
        secure: data.smtp_secure,
        host: data.smtp_host,
        port: data.smtp_port,
        password: data.smtp_password,
        from_email: data.smtp_from_email,
        from_name: data.smtp_from_name,
        to_email: data.smtp_to_email
      }
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `company/smtp/setting`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      const newCompanyData = {
        ...company.showCompany.data,
        communication_settings: result.data
      }

      yield put(CompanyAction.setShowCompany(newCompanyData))
      yield put(SnackbarAction.openSnackbar(result.message, "success"))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* updateEficoreSyncSetting({
  setSubmitting,
  data,
  onSuccess
}: UpdateEficoreSyncSetting) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)

    if (!company.showCompany.data) {
      yield put(SnackbarAction.openSnackbar("Company details not found", "error"))
      return
    }

    const postData = {
      data: {
        company_id: data.company_id,
        url: data.eficore_url,
        username: data.eficore_username,
        password: data.eficore_password
      }
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `company/eficore/setting`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      const newCompanyData = {
        ...company.showCompany.data,
        communication_settings: result.data
      }

      yield put(CompanyAction.setShowCompany(newCompanyData))
      yield put(SnackbarAction.openSnackbar(result.message, "success"))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* updateMiddlewareSetting({
  setSubmitting,
  data,
  onSuccess
}: UpdateMiddlewareSetting) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)

    if (!company.showCompany.data) {
      yield put(SnackbarAction.openSnackbar("Company details not found", "error"))
      return
    }

    const postData = {
      data: {
        company_id: data.company_id,
        url: data.middleware_url
      }
    }

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `company/middleware/setting`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      const newCompanyData = {
        ...company.showCompany.data,
        communication_settings: result.data
      }

      yield put(CompanyAction.setShowCompany(newCompanyData))
      yield put(SnackbarAction.openSnackbar(result.message, "success"))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* fetchAllCompany() {
  try {
    yield put(CompanyAction.setCompanyListLoading(true))

    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `signup/companies`, // Route
      app.token ?? "1035" // Token
    )

    if (result.isSuccess === true) {
      const data = result.data

      yield put(CompanyAction.setCompanyList(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setCompanyListLoading(false))
  }
}

export function* fetchTermCondition({ company_id }: FetchTermCondition) {
  try {
    yield put(CompanyAction.setTermConditionLoading(true))

    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `company/tnc/show/${company_id}`, // Route
      app.token ?? "1035" // Token
    )

    if (result.isSuccess === true) {
      const data = result.data
      yield put(CompanyAction.setTermCondition(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setTermConditionLoading(false))
  }
}

export function* createTermCondition({ company_id }: CreateTermCondition) {
  yield put(CompanyAction.setTermConditionLoading(true))

  try {
    const app = yield* select(getAppStore)

    const postData = {}

    const result = yield* call(
      ApiService.postApi,
      app.apiUrl,
      `company/tnc/createRevision/${company_id}`, // Route
      app.token, // Token
      postData
    )

    if (result.isSuccess === true) {
      const company = yield* select(getCompanyStore)

      if (company.termCondition && company.termConditions) {
        yield put(
          CompanyAction.fetchTermConditions(
            company.termCondition.company_id,
            company.termConditions.page
          )
        )
        // yield put(CompanyAction.setTermCondition(result.data))
      }
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setTermConditionLoading(false))
  }
}

export function* updateTermCondition({ id, contents }: UpdateTermCondition) {
  yield put(CompanyAction.setTermConditionSaving(true))

  try {
    const app = yield* select(getAppStore)

    const putData = {
      contents
    }

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `company/tnc/updateContent/${id}`, // Route
      app.token, // Token
      putData
    )
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setTermConditionSaving(false))
  }
}

export function* fetchTermConditions({ company_id, page }: FetchTermConditions) {
  try {
    yield put(CompanyAction.setTermConditionLoading(true))

    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)

    const getData = {
      page
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `company/tnc/index/${company_id}`, // Route
      app.token ?? "1035", // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, total } = result.data
      const tncsData = {
        total,
        page,
        data,
        sorts: company?.termConditions?.sorts ?? []
      }
      yield put(CompanyAction.setTermConditions(tncsData))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setTermConditionLoading(false))
  }
}

export function* setTermConditionStatus({ id }: SetTermConditionStatus) {
  yield put(CompanyAction.setTermConditionSaving(true))

  try {
    const app = yield* select(getAppStore)
    const company = yield* select(getCompanyStore)

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `company/tnc/selectRevision/${id}`, // Route
      app.token // Token
    )

    if (result.isSuccess === true) {
      if (company.termCondition && company.termConditions) {
        yield put(
          CompanyAction.fetchTermConditions(
            company.termCondition.company_id,
            company.termConditions.page
          )
        )
        yield put(CompanyAction.setTermCondition(result.data))
      }
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setTermConditionSaving(false))
  }
}

export function* fetchTermConditionPublic({ company_id }: FetchTermConditionPublic) {
  try {
    yield put(CompanyAction.setTermConditionLoading(true))

    const app = yield* select(getAppStore)

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `tnc/showPublic/${company_id}`, // Route
      app.token ?? "1035" // Token
    )

    if (result.isSuccess === true) {
      const data = result.data
      yield put(CompanyAction.setTermCondition(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar((error as Error).message, "error"))
  } finally {
    yield put(CompanyAction.setTermConditionLoading(false))
  }
}

export const companySaga = [
  takeLatest(CompanyTypes.FETCH_COMPANY_SELECT, fetchCompanySelect),
  takeLatest(CompanyTypes.FETCH_COMPANY_LIST, fetchCompanyList),
  takeLatest(CompanyTypes.CREATE_COMPANY, createCompany),
  takeLatest(CompanyTypes.SHOW_COMPANY, showCompany),
  takeLatest(CompanyTypes.UPDATE_COMPANY, updateCompany),
  takeLatest(CompanyTypes.UPDATE_WHATSAPP_CONFIG, updateWhatsappConfig),
  takeLatest(CompanyTypes.UPDATE_SMTP_EMAIL_CONFIG, updateSMTPEmailConfig),
  takeLatest(CompanyTypes.UPDATE_EFICORE_SYNC_SETTING, updateEficoreSyncSetting),
  takeLatest(CompanyTypes.UPDATE_MIDDLEWARE_SETTING, updateMiddlewareSetting),
  takeLatest(CompanyTypes.FETCH_ALL_COMPANY, fetchAllCompany),
  takeLatest(CompanyTypes.FETCH_TERM_CONDITION, fetchTermCondition),
  takeLatest(CompanyTypes.CREATE_TERM_CONDITION, createTermCondition),
  takeLatest(CompanyTypes.UPDATE_TERM_CONDITION, updateTermCondition),
  takeLatest(CompanyTypes.FETCH_TERM_CONDITIONS, fetchTermConditions),
  takeLatest(CompanyTypes.SET_TERM_CONDITION_STATUS, setTermConditionStatus),
  takeLatest(CompanyTypes.FETCH_TERM_CONDITION_PUBLIC, fetchTermConditionPublic)
]
