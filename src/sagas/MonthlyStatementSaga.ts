import { call, put, select, takeLatest } from "typed-redux-saga"
import ApiService from "./ApiService"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import { RootState } from "@/stores"
import { FetchMonthlyStatements } from "@/stores/MonthlyStatement/Types"
import MonthlyStatementAction, { MonthlyStatementTypes } from "@/stores/MonthlyStatement/Action"

const getAppStore = (state: RootState) => state.app
const getMonthlyStatementsStore = (state: RootState) => state.monthlyStatement

export function* fetchMonthlyStatements({ page, page_size, filters }: FetchMonthlyStatements) {
  yield put(MonthlyStatementAction.setMonthlyStatementsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      list: {
        current_page,
        page_size: current_page_size,
        filters: current_filters,
        sorts: current_sorts
      }
    } = yield select(getMonthlyStatementsStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: { page: number; rowsPerPage: number; filters: string[]; sorts: string[] } = {
      page: page ?? current_page,
      rowsPerPage: page_size ?? current_page_size,
      filters: [
        `doc_code:${current_filters.code}`,
        `debtor_code:${current_filters.debtorCode}`,
        `debtor_name:${current_filters.debtorName}`,
        `pdf_only:${current_filters.pdfOnly}`,
        `debtor_id:${current_filters.debtorId}`
      ],
      sorts: [
        `doc_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "doc_code")].order}`,
        `statement_date:${current_sorts[current_sorts.findIndex((element: any) => element.field === "statement_date")].order}`,
        `debtor_code:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtor_code")].order}`,
        `debtor_name:${current_sorts[current_sorts.findIndex((element: any) => element.field === "debtor_name")].order}`
      ]
    }

    if (filters) {
      yield put(MonthlyStatementAction.setMonthlyStatementsFilters(filters))

      getData.page = 1
      getData.filters = [
        `doc_code:${filters.code}`,
        `debtor_code:${filters.debtorCode}`,
        `debtor_name:${filters.debtorName}`,
        `debtor_id:${filters.debtorId}`
      ]
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `monthly-statement/index/${app.user?.companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, current_page, last_page, total } = result.data

      yield put(
        MonthlyStatementAction.setMonthlyStatementsPagination(
          current_page_size,
          current_page,
          last_page,
          total
        )
      )

      yield put(MonthlyStatementAction.setMonthlyStatements(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      console.log("Result is failed")
    }
  } catch (error) {
    console.log("Error fetch Monthlystatement list")
  } finally {
    yield put(MonthlyStatementAction.setMonthlyStatementsLoading(false))
  }
}

export const monthlyStatementSaga = [
  takeLatest(MonthlyStatementTypes.FETCH_MONTHLY_STATEMENTS, fetchMonthlyStatements)
]
