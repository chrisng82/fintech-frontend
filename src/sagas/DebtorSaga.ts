import { RootState } from "@/stores"
import { call, put, select, takeLatest } from "typed-redux-saga"
import AppAction from "@/stores/App/Action"
import SnackbarAction from "@/stores/Snackbar/Action"
import ApiService from "./ApiService"
import {
  CreateDebtorContact,
  DeleteDebtorContact,
  FetchDebtor,
  FetchDebtors,
  FetchDebtorCodes,
  GenerateOverdue,
  GenerateStatement,
  UpdateDebtorContact,
  FetchDebtorByCompanyCode,
  FetchDebtorById,
  FetchSuggestedDebtorsByCompanyCode
} from "@/stores/Debtor/Types"
import DebtorAction, { DebtorTypes } from "@/stores/Debtor/Action"
import { createFilters, getURLParams } from "@/components/utils/MapHelper"
import dayjs from "dayjs"
import saveFile from "@/components/utils/FileSaveHelper"

const getAppStore = (state: RootState) => state.app
const getDebtorStore = (state: RootState) => state.debtor

export function* fetchDebtors({ page, perPage, filters, onSuccess }: FetchDebtors) {
  yield put(DebtorAction.setDebtorsLoading(true))

  try {
    const app = yield* select(getAppStore)
    const {
      list: { current_page, per_page, filters: current_filters }
    } = yield* select(getDebtorStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData: {
      page: number
      per_page: number
      filters: string[]
    } = {
      page: page ?? current_page,
      per_page: perPage ?? per_page,
      filters: createFilters({
        ...current_filters,
        end_date: current_filters.end_date ? encodeURIComponent(current_filters.end_date) : null
      })
    }

    if (filters) {
      yield put(DebtorAction.setDebtorsFilters(filters))

      const formattedFilters = {
        ...filters,
        end_date: filters.end_date ? encodeURIComponent(filters.end_date) : null
      }

      getData.page = 1
      getData.filters = createFilters(formattedFilters)
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `debtor/index/${app.user.companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      const { data, per_page, current_page, last_page, total } = result.data

      yield put(DebtorAction.setDebtorsPagination(per_page, current_page, last_page, total))
      yield put(DebtorAction.setDebtors(data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to fetch debtors", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetch debtor list", "error"))
  } finally {
    yield put(DebtorAction.setDebtorsLoading(false))
    onSuccess?.()
  }
}

export function* fetchDebtor({ code }: FetchDebtor) {
  yield put(DebtorAction.setDebtorLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (!app.user?.companyId) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData = {
      companyId: app.user.companyId,
      debtorCode: decodeURIComponent(code)
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `debtor/showByCode`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DebtorAction.setDebtor(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to fetch debtor", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetch debtor", "error"))
  } finally {
    yield put(DebtorAction.setDebtorLoading(false))
  }
}

export function* fetchDebtorById({ id }: FetchDebtorById) {
  yield put(DebtorAction.setDebtorLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (!app.user?.companyId) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    if (id === 0) {
      yield* put(SnackbarAction.openSnackbar("Debtor does not exists", "error"))
      return
    }

    const getData = {
      companyId: app.user.companyId
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `debtor/show/${id}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DebtorAction.setDebtor(result.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to fetch debtor", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetch debtor", "error"))
  } finally {
    yield put(DebtorAction.setDebtorLoading(false))
  }
}

export function* fetchDebtorCodes({ search, onSuccess }: FetchDebtorCodes) {
  yield put(DebtorAction.setDebtorsLoading(true))

  try {
    const app = yield* select(getAppStore)

    if (!app.user?.companyCode) {
      yield* put(SnackbarAction.openSnackbar("Company not found", "error"))
      return
    }

    const getData = {
      filters: createFilters({
        code_and_name: search
      })
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `debtor/index/${app.user.companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DebtorAction.setDebtorCodes(result.data.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to fetch debtor codes", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetch debtor codes", "error"))
  } finally {
    yield put(DebtorAction.setDebtorsLoading(false))
  }
}

export function* updateDebtorContact({ setSubmitting, id, data, onSuccess }: UpdateDebtorContact) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)
    const { item } = yield* select(getDebtorStore)

    if (!item.data) {
      yield* put(SnackbarAction.openSnackbar("Debtor not found", "error"))
      return
    }

    const putData = {
      values: {
        ...data,
        debtor_id: item.data.id
      }
    }

    const result = yield* call(
      ApiService.putApi,
      app.apiUrl,
      `debtor/contact/${id}`,
      app.token,
      putData
    )

    if (result.isSuccess === true) {
      yield put(DebtorAction.setDebtorContacts(result.data))
      yield put(SnackbarAction.openSnackbar("Succesfully saved the contacts details!", "success"))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to update debtor contact", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Failed to save the contact details!", "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* createDebtorContact({ setSubmitting, data, onSuccess }: CreateDebtorContact) {
  setSubmitting(true)

  try {
    const app = yield* select(getAppStore)
    const { item } = yield* select(getDebtorStore)

    if (!item.data) {
      yield* put(SnackbarAction.openSnackbar("Debtor not found", "error"))
      return
    }

    const putData = {
      values: {
        ...data,
        debtor_id: item.data.id
      }
    }

    const result = yield* call(ApiService.postApi, app.apiUrl, `debtor/contact`, app.token, putData)

    if (result.isSuccess === true) {
      yield put(DebtorAction.setDebtorContacts(result.data))
      yield put(SnackbarAction.openSnackbar("Succesfully saved the contacts details!", "success"))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to update debtor contact", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Failed to save the contact details!", "error"))
  } finally {
    setSubmitting(false)
  }
}

export function* deleteDebtorContact({ id, onSuccess }: DeleteDebtorContact) {
  yield* put(DebtorAction.setDebtorLoading(true))

  try {
    const app = yield* select(getAppStore)
    const { item } = yield* select(getDebtorStore)

    if (!id) {
      yield* put(SnackbarAction.openSnackbar("Debtor contact not found", "error"))
      return
    }

    if (!item.data) {
      yield* put(SnackbarAction.openSnackbar("Debtor not found", "error"))
      return
    }

    const result = yield* call(ApiService.deleteApi, app.apiUrl, `debtor/contact/${id}`, app.token)

    if (result.isSuccess === true) {
      yield put(DebtorAction.setDebtorContacts(result.data))
      yield put(SnackbarAction.openSnackbar("Succesfully deleted the contacts details!", "success"))

      onSuccess()
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to delete debtor contact", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Failed to delete the contact details!", "error"))
  } finally {
    yield* put(DebtorAction.setDebtorLoading(false))
  }
}

export function* generateStatement({ debtorId, companyCode, isOpenBill }: GenerateStatement) {
  try {
    yield put(DebtorAction.setGenerateDocumentLoading(true))

    const app = yield* select(getAppStore)
    const debtor = yield* select(getDebtorStore)

    const { end_date } = debtor.list.filters

    const getData = {
      isOpenBill,
      endDate: end_date ? encodeURIComponent(end_date) : null
    }

    const result = yield* call(
      ApiService.downloadGetApi,
      app.apiUrl,
      `debtor/generate-statement/${companyCode}/${debtorId}`, // Route
      app.token, // Token
      getData,
      "application/pdf"
    )

    if (result.isSuccess === true) {
      const date = dayjs().format("DD-MM-YYYY_HH-mm-ss").toLocaleLowerCase()
      saveFile(result.data, `${debtorId}_statement_${date}.pdf`)
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error generate statement letter", "error"))
  } finally {
    yield put(DebtorAction.setGenerateDocumentLoading(false))
  }
}

export function* generateOverdue({ debtorId, companyCode }: GenerateOverdue) {
  try {
    yield put(DebtorAction.setGenerateDocumentLoading(true))

    const app = yield* select(getAppStore)
    const debtor = yield* select(getDebtorStore)

    const { end_date } = debtor.list.filters

    const getData = {
      endDate: end_date ? encodeURIComponent(end_date) : null
    }

    const result = yield* call(
      ApiService.downloadGetApi,
      app.apiUrl,
      `debtor/generate-overdue/${companyCode}/${debtorId}`, // Route
      app.token, // Token
      getData,
      "application/pdf"
    )

    if (result.isSuccess === true) {
      const date = dayjs().format("DD-MM-YYYY_HH-mm-ss").toLocaleLowerCase()
      saveFile(result.data, `${debtorId}_overdue_${date}.pdf`)
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar(result.message, "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error generate overdue letter", "error"))
  } finally {
    yield put(DebtorAction.setGenerateDocumentLoading(false))
  }
}

export function* fetchDebtorByCompanyCode({ companyCode, filters }: FetchDebtorByCompanyCode) {
  yield put(DebtorAction.setDebtorsLoading(true))

  try {
    const app = yield* select(getAppStore)

    const getData = {
      filters: filters
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `debtor/index/${companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DebtorAction.setDebtorCodes(result.data.data, companyCode))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to fetch debtor codes", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetch debtor codes", "error"))
  } finally {
    yield put(DebtorAction.setDebtorsLoading(false))
  }
}

export function* fetchSuggestedDebtorsByCompanyCode({
  companyCode,
  filters
}: FetchSuggestedDebtorsByCompanyCode) {
  yield put(DebtorAction.setSuggestedDebtorsLoading(true))

  try {
    const app = yield* select(getAppStore)

    const getData = {
      filters
    }

    const result = yield* call(
      ApiService.getApi,
      app.apiUrl,
      `debtor/suggested/${companyCode}`, // Route
      app.token, // Token
      getData
    )

    if (result.isSuccess === true) {
      yield put(DebtorAction.setSuggestedDebtors(result.data.data))
    } else if (result.isTokenExpired === true) {
      yield put(AppAction.tokenExpired())
    } else {
      yield put(SnackbarAction.openSnackbar("Failed to fetch debtor codes", "error"))
    }
  } catch (error) {
    yield put(SnackbarAction.openSnackbar("Error fetch debtor codes", "error"))
  } finally {
    yield put(DebtorAction.setSuggestedDebtorsLoading(false))
  }
}

export const debtorSaga = [
  takeLatest(DebtorTypes.FETCH_DEBTORS, fetchDebtors),
  takeLatest(DebtorTypes.FETCH_DEBTOR, fetchDebtor),
  takeLatest(DebtorTypes.UPDATE_DEBTOR_CONTACT, updateDebtorContact),
  takeLatest(DebtorTypes.CREATE_DEBTOR_CONTACT, createDebtorContact),
  takeLatest(DebtorTypes.DELETE_DEBTOR_CONTACT, deleteDebtorContact),
  takeLatest(DebtorTypes.FETCH_DEBTOR_CODES, fetchDebtorCodes),
  takeLatest(DebtorTypes.GENERATE_STATEMENT, generateStatement),
  takeLatest(DebtorTypes.GENERATE_OVERDUE, generateOverdue),
  takeLatest(DebtorTypes.FETCH_DEBTOR_BY_COMPANY_CODE, fetchDebtorByCompanyCode),
  takeLatest(DebtorTypes.FETCH_DEBTOR_BY_ID, fetchDebtorById),
  takeLatest(
    DebtorTypes.FETCH_SUGGESTED_DEBTORS_BY_COMPANY_CODE,
    fetchSuggestedDebtorsByCompanyCode
  )
]
